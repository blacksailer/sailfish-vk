#include "userpage.h"

QQmlListProperty<Relative> UserPage::relatives() const
{
    return QQmlListProperty<Relative>(const_cast<UserPage*>(this), 0,&UserPage::countRelative, &UserPage::relativeAt);
}

Relative *UserPage::relativeAt(QQmlListProperty<Relative> *list, int index)
{

    UserPage *page = qobject_cast<UserPage*>(list->object);
    return page->m_relatives.at(index);

}

int UserPage::countRelative(QQmlListProperty<Relative> *list)
{

    UserPage *page = qobject_cast<UserPage*>(list->object);
    return page->m_relatives.count();

}

//QQmlListProperty<School> UserPage::schools() const
//{
//    return QQmlListProperty<School>(const_cast<UserPage*>(this), 0,&UserPage::count_school, &UserPage::schoolAt);
//}

//void UserPage::append_school(QQmlListProperty<School> *list, School *schl)
//{
//    UserPage *msgBoard = qobject_cast<UserPage *>(list->object);
//    if (schl)
//        msgBoard->m_schools.append(schl);
//}

//School *UserPage::schoolAt(QQmlListProperty<School> *prop, int index)
//{
//    {
//        UserPage *page = qobject_cast<UserPage*>(prop->object);
//        return page->m_schools.at(index);
//    }

//}

//int UserPage::count_school(QQmlListProperty<School> *lis)
//{
//    UserPage *page = qobject_cast<UserPage*>(lis->object);
//    return page->m_schools.size();

//}

QQmlListProperty<University> UserPage::universities() const
{
    return QQmlListProperty<University>(const_cast<UserPage*>(this), 0,&UserPage::countUniversity, &UserPage::universityAt);
}

University *UserPage::universityAt(QQmlListProperty<University> *list, int index)
{
    UserPage *page = qobject_cast<UserPage*>(list->object);
    return page->m_universities.at(index);

}

int UserPage::countUniversity(QQmlListProperty<University> *list)
{
    UserPage *page = qobject_cast<UserPage*>(list->object);
    return page->m_universities.size();
}


UserPage::UserPage(QObject *parent) :
    QObject(parent)
{
}


void UserPage::getResult(QVariantMap answer)
{
    if(isActive())
    {
        qDebug()<<answer;
        setAbout(answer["about"].toString());
        setActivities(answer["activities"].toString());
        setBdate(answer["bdate"].toString());
        if(answer["blacklisted"].toInt()==0)
            setBlacklisted(false);
        else
            setBlacklisted(true);
        if(answer["blacklisted_by_me"].toInt()==0)
            setBlacklisted_by_me(false);
        else
            setBlacklisted_by_me(true);
        setBooks(answer["books"].toString());
        if(answer["can_post"].toInt()==0)
            setCan_post(false);
        else
            setCan_post(true);
        if(answer["can_see_all_posts"].toInt()==0)
            setCan_see_all_posts(false);
        else
            setCan_see_all_posts(true);
        if(answer["can_see_audio"].toInt()==0)
            setCan_see_audio(false);
        else
            setCan_see_audio(true);
        if(answer["can_send_friend_request"].toInt()==0)
            setCan_send_friend_request(false);
        else
            setCan_send_friend_request(true);
        if(answer["verified"].toInt()==0)
            setVerified(false);
        else
            setVerified(true);
        if(answer["is_favorite"].toInt()==0)
            setIs_favorite(false);
        else
            setIs_favorite(true);

        if(answer["is_hidden_from_feed"].toInt()==0)
            setIs_hidden_from_feed(false);
        else
            setIs_hidden_from_feed(true);

        setFollowers_count(answer["followers_count"].toInt());
        if(answer.keys().contains("city")&&!answer["city"].toMap().isEmpty())
        {
            auto map =answer["city"].toMap();
            City* city = new City();
            city->setParent(this);
            city->setId(map["id"].toInt());
            city->setTitle(map["title"].toString());
            setCity(city);

        }
        if(answer.keys().contains("counters")&&!answer["counters"].toMap().isEmpty())
        {
            /*
"gifts": 1158,
"user_photos": 0,
"subscriptions": 0,
"pages": 0
             * */
            QVariantMap map =answer["counters"].toMap();
            Counters* counters = new Counters();
            counters->setParent(this);
            counters->setAlbums(map["albums"].toInt());
            counters->setAudios(map["audios"].toInt());
            counters->setFriends(map["friends"].toInt());
            counters->setVideos(map["videos"].toInt());
            counters->setPhotos(map["photos"].toInt());
            counters->setNotes(map["notes"].toInt());
            counters->setUser_videos(map["user_videos"].toInt());
            counters->setFollowers(map["followers"].toInt());
            counters->setGroups(map["groups"].toInt());
            counters->setOnline_friends(map["online_friends"].toInt());
            counters->setMutual_friends(map["mutual_friends"].toInt());
            setCounters(counters);
        }

        if(answer.keys().contains("country")&&!answer["country"].toMap().isEmpty())
        {
            QVariantMap map =answer["country"].toMap();
            Country* country = new Country();
            country->setParent(this);
            country->setId(map["id"].toInt());
            country->setTitle(map["title"].toString());
            setCountry(country);
        }

        if(answer.keys().contains("last_seen")&&!answer["last_seen"].toMap().isEmpty())
        {
            QVariantMap map =answer["last_seen"].toMap();
            LastSeen* lastseen = new LastSeen();
            lastseen->setParent(this);
            lastseen->setPlatform(map["platform"].toString());
            lastseen->setTime(map["time"].toInt());
            setLast_seen(lastseen);
}
        if(answer.keys().contains("ocupation")&&!answer["occupation"].toMap().isEmpty())
        {
            Occupation* occ = new Occupation();
            occ->setParent(this);
            QVariantMap map = answer["occupation"].toMap();
            occ->setId(map["id"].toInt());
            occ->setName(map["name"].toString());
            occ->setType(map["type"].toString());
            setOccupation(occ);
}
        if(answer.keys().contains("military")&&!answer["military"].toMap().isEmpty())
        {
            Military* military = new Military();
            military->setParent(this);
            QVariantMap map = answer["military"].toMap();
            military->setUnit(map["unit"].toString());
            military->setUnit_id(map["unit_id"].toInt());
            military->setCountry_id(map["country_id"].toInt());
            military->setFrom(map["from"].toInt());
            military->setUntil(map["until"].toInt());
            setMilitary(military);

}
        if(answer.keys().contains("personal")&&!answer["personal"].toMap().isEmpty())
        {
            Personal *personal = new Personal();
            personal->setParent(this);
            auto map = answer["personal"].toMap();
            personal->setInspired_by(map["inspired_by"].toString());
            personal->setLangs(map["langs"].toString());
            personal->setLife_main(map["life_main"].toString());
            personal->setPeople_main(map["people_main"].toString());
            personal->setPolitical(map["political"].toString());
            personal->setReligion(map["religion"].toString());
            personal->setSmoking(map["smoking"].toString());
            setPersonal(personal);
}
        setDomain(answer["domain"].toString());
        setFirst_name_nom(answer["first_name"].toString());
        setLast_name_nom(answer["last_name"].toString());
        setSex(answer["sex"].toString());
        setScreen_name(answer["screen_name"].toString());
        setBdate(answer["bdate"].toString());
        setTimezone(answer["timezone"].toInt());
        setPhoto_id(answer["photo_id"].toString());
        setPhoto_50(answer["photo_50"].toString());
        setPhoto_100(answer["photo_100"].toString());

        setPhoto_200(answer["photo_200"].toString());
        setPhoto_max(answer["photo_max"].toString());
        setPhoto_200_orig(answer["photo_200_orig"].toString());
        setPhoto_400_orig(answer["photo_400_orig"].toString());
        setPhoto_max_orig(answer["photo_max_orig"].toString());

        setHome_town(answer["home_town"].toString());
        setInterests(answer["interests"].toString());
        setMusic(answer["music"].toString());
        setActivities(answer["activities"].toString());
        setMovies(answer["movies"].toString());
        setTv(answer["tv"].toString());
        setBooks(answer["books"].toString());
        setGames(answer["games"].toString());
        setQuotes(answer["quotes"].toString());

        if(answer["has_photo"].toInt()==0)
            setHas_photo(false);
        else
            setHas_photo(true);
        if(answer["has_mobile"].toInt()==0)
            setHas_mobile(false);
        else
            setHas_mobile(true);

        if(answer["is_friend"].toInt()==0)
            setIs_friend(false);
        else
            setIs_Friend(true);

        setFriend_status(answer["friend_status"].toString());

        if(answer["is_online"].toInt()==0)
            setIsOnline(false);
        else
            setIsOnline(true);
        if(answer["wall_comments"].toInt()==0)
            setWall_comments(false);
        else
            setWall_comments(true);

        if(answer["can_write_private_message"].toInt()==0)
            setCan_write_private_message(false);
        else
            setCan_write_private_message(true);
        setMobile_phone(answer["mobile_phone"].toString());
        setHome_phone(answer["home_phone"].toString());
        setSite(answer["site"].toString());


        setAvatar_path(answer["photo_200"].toString());
        setIsOnline(answer["online"].toBool());
        setNameplate(answer["first_name"].toString()+" "+answer["last_name"].toString());
        setRelation(answer["relation"].toString());
//        if(answer.keys().contains("schools")&&answer["schools"].toList().count()!=0)
//        {
            QVariantList list = answer["schools"].toList();
            setSchools(list);
//            foreach (QVariant item, list) {
//                QVariantMap map = item.toMap();
//                School* school = new School();
//                school->setParent(this);
//                school->setId(map["id"].toString());
//                school->setName(map["name"].toString());
//                school->setSpeciality(map["speciality"].toString());
//                school->setClass_(map["class"].toString());
//                school->setCountry(map["country"].toInt());
//                school->setYear_from(map["year_from"].toInt());
//                school->setYear_to(map["year_to"].toInt());
//                school->setYear_graduated(map["year_graduated"].toInt());
//                school->setType(map["type"].toInt());
//                school->setType_str(map["type_str"].toString());
//                m_schools.append(school);
//                append_school(schools(),school);
//                     }
//        }
                    if(answer.keys().contains("universities")&&answer["universities"].toList().count()!=0)
                    {
                        QVariantList list = answer["universities"].toList();
                        foreach (QVariant item, list) {
                            QVariantMap map = item.toMap();
                            University* univer = new University();
                            univer->setParent(this);
                            univer->setId(map["id"].toInt());
                            univer->setName(map["name"].toString());
                            univer->setChair(map["chair"].toInt());
                            univer->setChair_name(map["chair_name"].toString());
                            univer->setCountry(map["country"].toInt());
                            univer->setFaculty(map["faculty"].toInt());
                            univer->setFaculty_name(map["faculty_name"].toString());
                            univer->setGraduation(map["graduation"].toInt());
                            univer->setEducation_form(map["education_form"].toString());
                            univer->setEducation_status(map["education_status"].toString());

                            m_universities.append(univer);
                            emit universitiesChanged();
                            //append_school(schools(),school);
                                 }
                    }
                    if(answer.keys().contains("relatives")&&answer["relatives"].toList().count()!=0)
                    {
                        QVariantList list = answer["relatives"].toList();
                        foreach (QVariant item, list) {
                            QVariantMap map = item.toMap();
                            Relative* relative = new Relative();
                            relative->setParent(this);
                            relative->setId(map["id"].toInt());
                            relative->setName(map["name"].toString());
                            relative->setType(map["type"].toString());

                            m_relatives.append(relative);
                            emit relativesChanged();
                            //append_school(schools(),school);
                                 }
                    }


        setStatus(answer["status"].toString());

                setIs_Friend(answer["is_friend"].toBool());
    }
}
void UserPage::setAvatar_path(QString arg)
{
    if (m_Avatar_path != arg) {
        m_Avatar_path = arg;
        emit Avatar_pathChanged(arg);
    }
}
void UserPage::setNameplate(QString arg)
{
    if (m_Nameplate != arg) {
        m_Nameplate = arg;
        emit NameplateChanged(arg);
    }
}
void UserPage::setAge(QString arg)
{
    if (m_Age != arg) {
        m_Age = arg;
        emit AgeChanged(arg);
    }
}
void UserPage::setStatus(QString arg)
{
    if (m_Status != arg) {
        m_Status = arg;
        emit StatusChanged(arg);
    }
}
void UserPage::setIsOnline(bool arg)
{
    if (m_isOnline != arg) {
        m_isOnline = arg;
        emit isOnlineChanged(arg);
    }
}
