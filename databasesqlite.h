#ifndef DATABASESQLITE_H
#define DATABASESQLITE_H
#include <QObject>
#include <QtCore>
#include <QtSql>
class DatabaseSqlite :public QObject
{
    Q_OBJECT
private:
    DatabaseSqlite();
    DatabaseSqlite( const DatabaseSqlite&);
    DatabaseSqlite& operator=( DatabaseSqlite& );

    void createMessagesTable();
    static DatabaseSqlite *p_instance;
    QSqlDatabase dbase;
public:
    enum LongPollColumns {
        KEY,
        SERVER,
        TS,
        PTS
    };
    static DatabaseSqlite* getInstance() {
        if(!p_instance)
                p_instance = new DatabaseSqlite();
          return p_instance;
    }
    void createTokensTable(QString token,int expire,int u_id);

    int selectToken();

    QVariantList getToken();

    void createLongPollTable();
    bool insertLongPoll(QString key,QString server,int ts, int pts);
    void updateLongPoll(LongPollColumns key,QString value);

    bool insertMessage();

public slots:
    void insertMessage(QStringList Columns, QVariantList Values);
    void insertAttachments(QVariantMap attach, int message_id);
    QStringList LongPollPrevious();



};

#endif // DATABASESQLITE_H
