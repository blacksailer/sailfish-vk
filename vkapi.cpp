#include "vkapi.h"

QString vkapi::lang = QString("ru");
QString vkapi::api_version = QString("5.52");

vkapi::vkapi(QObject *parent) :
    QObject(parent)
{
    manager = new QNetworkAccessManager;
    tryofdb = DatabaseSqlite::getInstance();//new DatabaseSqlite;
    //connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(wall_get(QNetworkReply*)));


    appendNewsfeed=false;
    setRefreshToken(false);
}

vkapi::vkapi( Parser *model, GlobalSettings *Settings)
{
    QString locale = QLocale::system().name();
    qDebug()<<locale<<QLocale::languageToString(QLocale::system().language());
    if(QLocale::system().language()!=QLocale::Russian){
    lang=QString("en");
    }

    manager = new QNetworkAccessManager;
    tryofdb = DatabaseSqlite::getInstance();
    m_Settings=Settings;
    Timer_LongPoll.setInterval(m_Settings->LongPollTimeout());
    Timer_LongPoll.setSingleShot(true);
    Timer_Queue.setSingleShot(false);
    Timer_Queue.setInterval(3000);

    Timer_Internet.setSingleShot(false);
    Timer_Internet.setInterval(10000);
    Timer_Internet.start();
    connect(&Timer_Internet,&QTimer::timeout,this,&vkapi::checkInternet);
    connect(&Timer_Queue,&QTimer::timeout,this,&vkapi::RequestQueueSizeIncreased);
    connect(this,&vkapi::RequestQueueSizeIncreased,this,&vkapi::startQueueRequest);



    //Создать папку для картинок
    QDir dir(QStandardPaths::writableLocation(QStandardPaths::PicturesLocation));
    dir.mkdir("SK");
    dir.cd(QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation));
    dir.mkdir("SK");
    dir.cd(QStandardPaths::writableLocation(QStandardPaths::MusicLocation));
    dir.mkdir("SK");
    dir.cd(QStandardPaths::writableLocation(QStandardPaths::MoviesLocation));
    dir.mkdir("SK");

    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(onReplyFinished(QNetworkReply*)));
    connect(this,SIGNAL(user_get_end(QJsonDocument)),model,SLOT(user_get_answered(QJsonDocument)));
    //WALL

    connect(this,&vkapi::wall_get_end,model,&Parser::get_Wall_Answer);
//    connect(this,&vkapi::wall_getbyid_end,model,&Parser::get_Wall_Answer);

    connect(this,&vkapi::wall_getComments_end,model,&Parser::get_Wall_Comments);
    connect(this,&vkapi::wall_createComment_end,model,&Parser::getWallCreateCommentAnswer);
    //VIDEOS
    connect(this,&vkapi::video_get_end,model,&Parser::get_Video_answer);
    connect(this,&vkapi::video_getcomments_end,model,&Parser::getComments_Video_answer);

    //Friends
    connect(this,&vkapi::friends_get_end,model,&Parser::friends_get_asnwer);

    //С повтором постов
    connect(this,SIGNAL(newsfeed_get_end(QJsonDocument,bool)),model,SLOT(get_RecNewsfeed_answer(QJsonDocument,bool)));

    connect(this,SIGNAL(audio_get_end(QJsonDocument)),model,SLOT(get_Audio_answer(QJsonDocument)));

    connect(this,SIGNAL(messages_getDialogs_end(QJsonDocument)),model,SLOT(getDialogs_Messages_answer(QJsonDocument)));
    connect(this,&vkapi::messages_searchDialogs_end,model,&Parser::searchDialogs_Messages_answer);

    connect(this,SIGNAL(messages_getHistory_end(QJsonDocument)),model,SLOT(getHistory_Messages_answer(QJsonDocument)));
    connect(this,&vkapi::messages_getById_end,model,&Parser::getMessage_ById_answer);
    connect(model,&Parser::getMessageById,this,&vkapi::messages_getById);
    connect(model,SIGNAL(need_user_data(QString,QString,QString)),this,SLOT(user_get(QString,QString,QString)));

    //Likes- связи
    connect(this,SIGNAL(likes_add_end(QJsonDocument)),model,SLOT(getLikes_add_answer(QJsonDocument)));
    connect(this,SIGNAL(likes_delete_end(QJsonDocument)),model,SLOT(getLikes_delete_answer(QJsonDocument)));
    connect(this,SIGNAL(likes_isLikes_end(QJsonDocument)),model,SLOT(getLikes_isLiked_answer(QJsonDocument)));

    //GROUPS
    connect(this,&vkapi::group_get_end,model,&Parser::group_get_answered);
    connect(this,&vkapi::group_getbyid_end,model,&Parser::group_getbyid_answered);

    connect(model,SIGNAL(errorOccurred(int)),this,SLOT(onErrorOccurred(int)));

    //Messages
    connect(model,SIGNAL(addMessagetoDb(QStringList,QVariantList)),tryofdb,SLOT(insertMessage(QStringList,QVariantList)));


    //Long Poll - Сохранить данные
    connect(this,SIGNAL(longpollServer_get_end(QJsonDocument)),model,SLOT(getLongPollServer_answer(QJsonDocument)));
    connect(this,SIGNAL(longpoll_get_end(QJsonDocument)),model,SLOT(getInitiateLongPoll_answer(QJsonDocument)));

    //Long Poll - Сохранить данные
    connect(model,SIGNAL(keyLongPoll(QString)),m_Settings,SLOT(setKey_LongPoll(QString)));
    connect(model,SIGNAL(serverLongPoll(QString)),m_Settings,SLOT(setServer_LongPoll(QString)));
    connect(model,SIGNAL(tsLongPoll(uint)),m_Settings,SLOT(setTs_LongPoll(uint)));
    connect(model,SIGNAL(ptsLongPoll(int)),m_Settings,SLOT(setPts_LongPoll(int)));
    connect(model,&Parser::longPollData,m_Settings,&GlobalSettings::setLongPollData);

    //newsfeed - Сохранить start_from
    connect(model,SIGNAL(setStart(QString)),m_Settings,SLOT(setStart_FROM(QString)));


    connect(model,SIGNAL(setUnreadCount(int)),m_Settings,SLOT(setUnreadCount(int)));
    //Long Poll - При получении нового Long Poll pts начать запросы
    connect(m_Settings,SIGNAL(Ts_LongPollChanged(uint)),this,SLOT(startLongPollTimer()));
    connect(&Timer_LongPoll,SIGNAL(timeout()),this,SLOT(initiateLongPoll()));


    connect(model,SIGNAL(reconnectToLongPoll()),this,SLOT(LongPollServer_get()));

    if(checkToken()) //Если true, то получать новый токен не нужно
        setTokenFromDatabase();
    else
        emit browserForAuthNeeded();


}

vkapi::~vkapi()
{
    delete tryofdb;
}

bool vkapi::RefreshToken() const
{
    return m_RefreshToken;
}

bool vkapi::hasInternetConnection() const
{
    return m_hasInternetConnection;
}

void vkapi::startLongPoll()
{
    QTimer *timer = new QTimer(this);
    timer->setInterval(1000*26);
    timer->start();
    qDebug()<<"timer starts";
}

void vkapi::authorize()
{
    //qDebug()<<manager->cache()->
    QString uri =QString("https://oauth.vk.com/authorize?")+
            "client_id=4351707&"+
            "scope=wall,photos,messages,audio,groups,friends,video,offline&"+
            "redirect_uri=https://oauth.vk.com/blank.html&"+
            "display=mobile&"+
            "v=5.52&"+
            "response_type=token";

    QNetworkRequest req;
    req.setUrl(QUrl(uri));
    QNetworkReply* Answer = manager->get(req);
    Replies[Answer]=Method_Id::AUTHORIZE;

}

void vkapi::onAuthorize(QNetworkReply *reply)
{
    int statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    if(statusCode!=302)
    {
        //Очень странная ошибка
        qDebug()<<reply->readAll()<<statusCode<<"StatusError";
        emit browserForAuthNeeded();

    }
    else if(statusCode==302)
    {
        QUrl redirectUrl = reply->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl();
        qDebug()<<redirectUrl<<statusCode;

        QNetworkRequest req;
        req.setUrl(redirectUrl);

        reply->deleteLater();
        QNetworkReply* Answer = manager->get(req);
        Replies[Answer]=Method_Id::AUTHORIZE_REDIRECT;
    }

}

void vkapi::onAuthorizeRedirect(QNetworkReply *reply)
{
    int statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    if(statusCode!=302)
    {
        //Очень странная ошибка
        qDebug()<<reply->readAll()<<statusCode<<"StatusError";
        emit browserForAuthNeeded();

    }
    else if(statusCode==302)
    {
        QUrl redirectUrl = reply->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl();
        qDebug()<<redirectUrl<<statusCode;

        QNetworkRequest req;
        req.setUrl(redirectUrl);
        setToken(redirectUrl.toString());

        reply->deleteLater();
        QNetworkReply* Answer=manager->get(req);
        Replies[Answer]=Method_Id::AUTHORIZE_FINALE;
    }

}

void vkapi::onReplyFinished(QNetworkReply * reply)
{
    switch(Replies[reply])
    {
    case Method_Id::WALL_GET:
        onWall_get(reply);
        break;
    case Method_Id::WALL_GETBYID:
        onWall_getbyid(reply);
        break;
    case Method_Id::WALL_GETCOMMENTS:
        onWall_getComments(reply);
        break;
    case Method_Id::WALL_POST:
        onWall_post(reply);
        break;
    case Method_Id::FRIENDS_GET:
        onFriends_get(reply);
        break;
    case Method_Id::FRIENDS_DELETE:
        onFriends_delete(reply);
        break;
    case Method_Id::FRIENDS_ADD:
        qDebug()<<reply->readAll();
        reply->deleteLater();
        break;
    case Method_Id::USER_GET:
        onUser_Get(reply);
        break;
    case Method_Id::NEWSFEED_GET:
        onNewsfeed_get(reply);
        break;
    case Method_Id::USER_GET_PERSONAL:
        onUser_Get(reply);
        break;
    case Method_Id::AUDIO_GET:
        onAudio_get(reply);
        break;
    case Method_Id::AUDIO_SEARCH:
        onAudio_get(reply);
        break;
    case Method_Id::VIDEO_GET:
        onVideo_get(reply);
        break;
    case Method_Id::IMAGE_GET:
        onSaveImageFinished(reply);
        break;
    case Method_Id::MESSAGES_GETDIALOGS:
        onMessages_getDialogs(reply);
        break;
    case Method_Id::MESSAGES_SEARCHDIALOGS:
        onMessages_searchDialogs(reply);
        break;
    case Method_Id::MESSAGES_GETHISTORY:
        //qDebug()<<reply->readAll();
        onMessages_getHistory(reply);
        break;
    case Method_Id::MESSAGES_GETBYID:
        onMessages_getById(reply);
        break;
    case Method_Id::LIKES_ADD:
        onLikes_add(reply);
        break;
    case Method_Id::LIKES_DELETE:
        onLikes_delete(reply);
        break;
    case Method_Id::LIKES_ISLIKED:
        onLikes_isLiked(reply);
        break;

    case Method_Id::GET_LONGSERVER:
        onLongPollServer_get(reply);
        break;
    case Method_Id::LONGPOLL:
        onInitiateLongPoll(reply);
        break;
    case Method_Id::AUTHORIZE:
        onAuthorize(reply);
        break;
    case Method_Id::GROUP_GET:
        onGroup_get(reply);
        break;
    case Method_Id::GROUP_GETBYID:
        onGroup_getbyid(reply);
        break;
    case Method_Id::AUTHORIZE_REDIRECT:
        onAuthorizeRedirect(reply);
        break;
    case Method_Id::AUTHORIZE_FINALE:
        qDebug()<<"Authorize finale";
        reply->deleteLater();
        break;
    case Method_Id::WALL_CREATECOMMENT:
        onWall_createComment(reply);
        break;
    default:
        qDebug()<<reply->readAll()<<reply->url();
        reply->deleteLater();
        break;
    }
    Replies.remove(reply);
}

void vkapi::wall_get(QString U_id,int count,int offset)
{
    QNetworkRequest req;
    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/wall.get?");
    req_url.addQueryItem("owner_id",U_id);
    req_url.addQueryItem("filter","all");
    if(offset>0)
        req_url.addQueryItem("offset",QString::number(offset));

    req_url.addQueryItem("count",QString::number(count));
    req_url.addQueryItem("extended",QString::number(1));
    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);

    req.setUrl(QUrl(req_url.query()));

    queueRequest(req,Method_Id::WALL_GET);

    //    QNetworkReply* Answer = manager->get(req);
    //    Replies[Answer]=Method_Id::WALL_GET;

}

void vkapi::newsfeed_get(int count,int start_from)
{
    QNetworkRequest req;
    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/newsfeed.get?");
    req_url.addQueryItem("filters","post");
    req_url.addQueryItem("count",QString::number(count));
    if(start_from>0)
    {

        appendNewsfeed=true;
        req_url.addQueryItem("start_from",m_Settings->start_FROM());
    }
    else
        appendNewsfeed=false;
    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);
    req.setUrl(QUrl(req_url.query()));

    queueRequest(req,Method_Id::NEWSFEED_GET);


    //    QNetworkReply* Answer = manager->get(req);
    //    Replies[Answer]=Method_Id::NEWSFEED_GET;

}

void vkapi::onNewsfeed_get(QNetworkReply *reply)
{
    qDebug()<<"Newsfeed ended";
    QJsonDocument response = QJsonDocument::fromJson(reply->readAll());
    newsfeed_get_end(response,appendNewsfeed);
    reply->deleteLater();
}

void vkapi::friends_get(int u_id, int count, int offset, QString name_case)
{
    QNetworkRequest req;
    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/friends.get?");
    req_url.addQueryItem("user_id",QString::number(u_id));
    req_url.addQueryItem("count",QString::number(count));
    req_url.addQueryItem("offset",QString::number(offset));
    req_url.addQueryItem("fields","photo_50,photo_100");
    req_url.addQueryItem("name_case",name_case);
    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);

    req.setUrl(QUrl(req_url.query()));
    queueRequest(req,Method_Id::FRIENDS_GET);

    //    QNetworkReply* Answer = manager->get(req);
    //    Replies[Answer]=Method_Id::FRIENDS_GET;
}

void vkapi::friends_search(int u_id, int count, int offset, QString name_case, QString q)
{
    QNetworkRequest req;
    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/friends.search?");
    req_url.addQueryItem("user_id",QString::number(u_id));
    req_url.addQueryItem("count",QString::number(count));
    req_url.addQueryItem("offset",QString::number(offset));
    req_url.addQueryItem("q",q);
    req_url.addQueryItem("fields","photo_50,photo_100");
    req_url.addQueryItem("name_case",name_case);
    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);

    req.setUrl(QUrl(req_url.query()));

    queueRequest(req,Method_Id::FRIENDS_GET);

    //    QNetworkReply* Answer = manager->get(req);
    //    Replies[Answer]=Method_Id::FRIENDS_GET;
}

void vkapi::onFriends_get(QNetworkReply *reply)
{
    QJsonDocument response = QJsonDocument::fromJson(reply->readAll());
    friends_get_end(response);
    reply->deleteLater();
}

void vkapi::friends_add(int u_id,QString text,int follow)
{
    QNetworkRequest req;
    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/friends.add?");
    req_url.addQueryItem("user_id",QString::number(u_id));
    req_url.addQueryItem("text",text);
    req_url.addQueryItem("follow",QString::number(follow));
    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);
    req.setUrl(QUrl(req_url.query()));
    queueRequest(req,Method_Id::FRIENDS_ADD);

    //    QNetworkReply* Answer = manager->get(req);
    //    Replies[Answer]=Method_Id::FRIENDS_ADD;

}

void vkapi::onFriends_add(QNetworkReply *reply)
{
    Q_UNUSED(reply)
    qWarning()<<"//!-- Not Implemented --!//";

}

void vkapi::friends_delete(int u_id)
{
    QNetworkRequest req;
    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/friends.delete?");
    req_url.addQueryItem("user_id",QString::number(u_id));
    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);
    req.setUrl(QUrl(req_url.query()));

    queueRequest(req,Method_Id::FRIENDS_DELETE);

    //    QNetworkReply* Answer = manager->get(req);
    //    Replies[Answer]=Method_Id::FRIENDS_DELETE;

}

void vkapi::onFriends_delete(QNetworkReply *reply)
{
    qWarning()<<"Не реализовано";
    //    После успешного выполнения начиная с версии 5.28 возвращается объект с полями:

    //        success — удалось успешно удалить друга
    //        friend_deleted — был удален друг
    //        out_request_deleted — отменена исходящая заявка
    //        in_request_deleted — отклонена входящая заявка
    //        suggestion_deleted — отклонена рекомендация друга

    reply->deleteLater();
}

void vkapi::onWall_get(QNetworkReply *reply)
{
    QJsonDocument response = QJsonDocument::fromJson(reply->readAll());
    wall_get_end(response,true);
    reply->deleteLater();
}

void vkapi::wall_getComments(QString wall_id, QString post_id, int start_comment_id)
{
    QNetworkRequest req;
    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/execute.wall_getComments?");
    req_url.addQueryItem("owner_id",wall_id);
    req_url.addQueryItem("post_id",post_id);
    if(start_comment_id!=0)
        req_url.addQueryItem("start_comment_id",QString::number(start_comment_id));
    req_url.addQueryItem("count",QString::number(20));
    req_url.addQueryItem("sort","desc");
    req_url.addQueryItem("extended","1");
    req_url.addQueryItem("need_likes","1");
    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);

    req.setUrl(QUrl(req_url.query()));

    queueRequest(req,Method_Id::WALL_GETCOMMENTS);


    //    QNetworkReply* Answer = manager->get(req);
    //    Replies[Answer]=Method_Id::WALL_GETCOMMENTS;

}

void vkapi::onWall_getComments(QNetworkReply *reply)
{
    QJsonDocument response = QJsonDocument::fromJson(reply->readAll());
    emit wall_getComments_end(response);
    reply->deleteLater();
}

void vkapi::wall_post(int owner_id, bool friends_only, bool from_group, QString message, QString attachments, QString services, bool is_signed, bool mark_as_ads, int publish_date, double lat, double _long, int place_id, int post_id)
{
    Q_UNUSED(lat)
    Q_UNUSED(_long)
    Q_UNUSED(publish_date)
    Q_UNUSED(place_id)
    Q_UNUSED(post_id)
    QNetworkRequest req;
    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/wall.post?");
    req_url.addQueryItem("owner_id",QString::number(owner_id));
    if(friends_only)
        req_url.addQueryItem("friends_only","1");
    else
        req_url.addQueryItem("friends_only","0");
    if(from_group)
        req_url.addQueryItem("from_group","1");
    else
        req_url.addQueryItem("from_group","0");
    req_url.addQueryItem("message",message);
    if(attachments.size()>0)
    req_url.addQueryItem("attachments",attachments);
    if(services.size()>0)
        req_url.addQueryItem("services",services);
    if(is_signed)
        req_url.addQueryItem("signed","1");
    else
        req_url.addQueryItem("signed","0");
    if(mark_as_ads)
        req_url.addQueryItem("mark_as_ads","1");
    else
        req_url.addQueryItem("mark_as_ads","0");

    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);

    req.setUrl(QUrl(req_url.query()));

    queueRequest(req,Method_Id::WALL_POST);

}

void vkapi::onWall_post(QNetworkReply *reply)
{
    QUrlQuery query(reply->url());
    QString owner_id=query.queryItemValue("owner_id");
    QJsonDocument response = QJsonDocument::fromJson(reply->readAll());
    auto doc = response.object();
    auto post_object = doc["response"].toObject();
    int post_id = post_object["post_id"].toInt();
    qDebug()<<owner_id+"_"+QString::number(post_id);

    wall_getbyid(owner_id+"_"+QString::number(post_id));
    reply->deleteLater();

}

void vkapi::wall_delete(int owner_id, int post_id)
{
    QNetworkRequest req;
    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/wall.delete?");
    req_url.addQueryItem("owner_id",QString::number(owner_id));
    req_url.addQueryItem("post_id",QString::number(post_id));
    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);

    req.setUrl(QUrl(req_url.query()));

    queueRequest(req,Method_Id::WALL_DELETE);

}

void vkapi::wall_getbyid(QString W_id)
{
    QNetworkRequest req;
    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/wall.getById?");
    req_url.addQueryItem("posts",W_id);
    req_url.addQueryItem("extended","1");
    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);

    req.setUrl(QUrl(req_url.query()));

    queueRequest(req,Method_Id::WALL_GETBYID);

    //    QNetworkReply* Answer = manager->get(req);
    //    Replies[Answer]=Method_Id::WALL_GETBYID;

}

void vkapi::onWall_getbyid(QNetworkReply *reply)
{
    QJsonDocument response = QJsonDocument::fromJson(reply->readAll());
    emit wall_get_end(response,false);
    reply->deleteLater();
}

void vkapi::wall_createComment(int owner_id, int post_id, QString message, QString attachments, int reply_id)
{
    QNetworkRequest req;
    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/wall.createComment?");
    req_url.addQueryItem("owner_id",QString::number(owner_id));
    req_url.addQueryItem("post_id",QString::number(post_id));

    req_url.addQueryItem("message",message);
    if(attachments.length()>0)
        req_url.addQueryItem("attachments",attachments);
    if(reply_id!=-1)
        req_url.addQueryItem("reply_to_comment",QString::number(reply_id));

    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);

    qDebug()<<req_url.query();
    req.setUrl(QUrl(req_url.query()));

    queueRequest(req,Method_Id::WALL_CREATECOMMENT);

    //    QNetworkReply* Answer = manager->get(req);
    //    Replies[Answer]=Method_Id::WALL_CREATECOMMENT;
}

void vkapi::onWall_createComment(QNetworkReply *reply)
{
    QJsonDocument response = QJsonDocument::fromJson(reply->readAll());
    emit wall_createComment_end(response);
    reply->deleteLater();

}

void vkapi::wall_deleteComment(int owner_id, int comment_id)
{
    QNetworkRequest req;
    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/wall.deleteComment?");
    req_url.addQueryItem("owner_id",QString::number(owner_id));
    req_url.addQueryItem("comment_id",QString::number(comment_id));
    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);

    req.setUrl(QUrl(req_url.query()));

    queueRequest(req,Method_Id::OTHER);

    //    QNetworkReply* Answer = manager->get(req);
    //    Replies[Answer]=Method_Id::OTHER;

    qWarning()<<"Not parsed method";

}

void vkapi::photo_getUploadServer()
{
    QNetworkRequest req;
    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/photo.getMessagesUploadServer?");
    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);

    req.setUrl(QUrl(req_url.query()));
    queueRequest(req,Method_Id::PHOTO_GETUPLOADSERVER);


    //    QNetworkReply* Answer = manager->get(req);
    //    Replies[Answer]=Method_Id::PHOTO_GETUPLOADSERVER;

}



void vkapi::user_get(QString User_id, QString filter, QString Name_nom)
{
    qDebug()<<User_id.toInt()<<filter;
    if(User_id.toInt()>0 && !User_id.contains(',')){
        QNetworkRequest req;
        QUrlQuery req_url;
        req_url.setQuery("https://api.vk.com/method/users.get?");
        if(User_id!="0")
            req_url.addQueryItem("user_ids",User_id);
        else
            req_url.addQueryItem("user_ids",QString::number(user_id));
        req_url.addQueryItem("fields",filter);
        req_url.addQueryItem("name_case",Name_nom);
        req_url.addQueryItem("v",api_version);
        req_url.addQueryItem("lang",lang);
        req_url.addQueryItem("access_token",token_id);

        req.setUrl(QUrl(req_url.query()));

        //        QNetworkReply* Answer = manager->get(req);
        if(User_id!="0")
            queueRequest(req,Method_Id::USER_GET);
        else
            queueRequest(req,Method_Id::USER_GET_PERSONAL);
        //            Replies[Answer]=Method_Id::USER_GET;

        //            Replies[Answer]=Method_Id::USER_GET_PERSONAL;
    }
    else if(User_id.contains(','))
    {
        QNetworkRequest req;
        QUrlQuery req_url;
        req_url.setQuery("https://api.vk.com/method/users.get?");
        req_url.addQueryItem("user_ids",QString::number(user_id));
        req_url.addQueryItem("fields",filter);
        req_url.addQueryItem("name_case",Name_nom);
        req_url.addQueryItem("v",api_version);
        req_url.addQueryItem("lang",lang);
        req_url.addQueryItem("access_token",token_id);

        req.setUrl(QUrl(req_url.query()));

        queueRequest(req,Method_Id::USER_GET);

        //        QNetworkReply* Answer = manager->get(req);
        //        Replies[Answer]=Method_Id::USER_GET;
    }
    else
        group_getbyid(User_id.toInt()*-1,"is_member,photo_max,photo_200");


}

void vkapi::audio_get(QString Owner_id, int count)
{
    QNetworkRequest req;
    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/audio.get?");
    req_url.addQueryItem("owner_id",Owner_id);
    req_url.addQueryItem("count",QString::number(count));
    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);

    req.setUrl(QUrl(req_url.query()));

    queueRequest(req,Method_Id::AUDIO_GET);

    //    QNetworkReply* Answer = manager->get(req);
    //    Replies[Answer]=Method_Id::AUDIO_GET;

}

void vkapi::onAudio_get(QNetworkReply *reply)
{
    QJsonDocument response = QJsonDocument::fromJson(reply->readAll());
    audio_get_end(response);
    reply->deleteLater();


}

void vkapi::video_get(QString VideoUrl)
{
    QNetworkRequest req;
    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/execute.video_get?");
    req_url.addQueryItem("videos",VideoUrl);
    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);

    req.setUrl(QUrl(req_url.query()));
    qDebug()<<req_url.query();

    queueRequest(req,Method_Id::VIDEO_GET);

    //    QNetworkReply* Answer = manager->get(req);
    //    Replies[Answer]=Method_Id::VIDEO_GET;

}

void vkapi::onVideo_get(QNetworkReply *reply)
{

    QJsonDocument response = QJsonDocument::fromJson(reply->readAll());
    emit video_get_end(response);
    reply->deleteLater();
}

void vkapi::video_GetComments(QString owner_id, QString video_id, int start_comment_id)
{
    QNetworkRequest req;
    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/execute.video_getComments?");
    req_url.addQueryItem("owner_id",owner_id);
    req_url.addQueryItem("video_id",video_id);
    if(start_comment_id!=0)
        req_url.addQueryItem("start_comment_id",QString::number(start_comment_id));
    req_url.addQueryItem("count",QString::number(20));
    req_url.addQueryItem("sort","desc");
    req_url.addQueryItem("extended","1");
    req_url.addQueryItem("need_likes","1");
    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);

    req.setUrl(QUrl(req_url.query()));

    queueRequest(req,Method_Id::WALL_GETCOMMENTS);

    //    QNetworkReply* Answer = manager->get(req);
    //    Replies[Answer]=Method_Id::WALL_GETCOMMENTS;

}

void vkapi::onVideo_GetComments(QNetworkReply *reply)
{
    QJsonDocument response = QJsonDocument::fromJson(reply->readAll());
    emit video_getcomments_end(response);
    reply->deleteLater();

}

void vkapi::audio_Search(QString q, bool performer_only, int count)
{
    QNetworkRequest req;
    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/audio.search?");
    req_url.addQueryItem("q",q);
    req_url.addQueryItem("auto_complete",QString::number(1));
    req_url.addQueryItem("lyrics",QString::number(1));
    req_url.addQueryItem("lyrics",QString::number(1));
    req_url.addQueryItem("performer_only",QString::number(performer_only));
    req_url.addQueryItem("count",QString::number(count));

    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);

    req.setUrl(QUrl(req_url.query()));

    queueRequest(req,Method_Id::AUDIO_SEARCH);

    //    QNetworkReply* Answer = manager->get(req);
    //    Replies[Answer]=Method_Id::AUDIO_SEARCH;
}

void vkapi::group_get(int user_id, QString filter, QString fields, int offset, int count)
{
    QNetworkRequest req;
    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/groups.get?");
    req_url.addQueryItem("user_id",QString::number(user_id));
    if(filter.size()>0)
        req_url.addQueryItem("filter",filter);
    if(fields.size()>0)
        req_url.addQueryItem("fields",fields);
    if(offset>0)
        req_url.addQueryItem("offset",QString::number(offset));
    req_url.addQueryItem("count",QString::number(count));
    req_url.addQueryItem("extended",QString::number(1));

    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);

    req.setUrl(QUrl(req_url.query()));
    qDebug()<<QUrl(req_url.query());

    queueRequest(req,Method_Id::GROUP_GET);

}

void vkapi::onGroup_get(QNetworkReply *reply)
{
    QJsonDocument response = QJsonDocument::fromJson(reply->readAll());
    group_get_end(response);
    reply->deleteLater();
}

void vkapi::group_getbyid(int group_id, QString filter)
{
    QNetworkRequest req;
    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/groups.getById?");
    req_url.addQueryItem("group_ids",QString::number(group_id));
    if(filter.size()>0)
        req_url.addQueryItem("fields",filter);

    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);

    req.setUrl(QUrl(req_url.query()));
    qDebug()<<QUrl(req_url.query());

    queueRequest(req,Method_Id::GROUP_GETBYID);

    //    QNetworkReply* Answer = manager->get(req);
    //    Replies[Answer]=Method_Id::GROUP_GETBYID;

}

void vkapi::onGroup_getbyid(QNetworkReply *reply)
{
    QJsonDocument response = QJsonDocument::fromJson(reply->readAll());
    group_getbyid_end(response);
    reply->deleteLater();
}

void vkapi::group_leave(int group_id)
{
    QNetworkRequest req;

    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/groups.leave?");
    req_url.addQueryItem("group_id",QString::number(group_id));

    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);

    req.setUrl(QUrl(req_url.query()));
    qDebug()<<   req_url.query();
    manager->get(req);

}

void vkapi::group_join(int group_id, bool not_sure)
{
    QNetworkRequest req;

    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/groups.join?");
    req_url.addQueryItem("group_id",QString::number(group_id));

    if(not_sure==true)
        req_url.addQueryItem("not_sure",QString::number(1));
    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);

    req.setUrl(QUrl(req_url.query()));
    qDebug()<<   req_url.query();
    manager->get(req);
}

void vkapi::messages_getDialogs(int offset, int count, int preview_length, int unread)
{
    Q_UNUSED(preview_length)
    QNetworkRequest req;

    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/execute.getDialogs?");
    req_url.addQueryItem("offset",QString::number(offset));
    req_url.addQueryItem("count",QString::number(count));
    req_url.addQueryItem("unread",QString::number(unread));

    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);

    req.setUrl(QUrl(req_url.query()));
    qDebug()<<   req_url.query();
    queueRequest(req,Method_Id::MESSAGES_GETDIALOGS);
    //    QNetworkReply* Answer = manager->get(req);
    //    Replies[Answer]=Method_Id::MESSAGES_GETDIALOGS;

}

void vkapi::onMessages_getDialogs(QNetworkReply *reply)
{

    QJsonDocument response = QJsonDocument::fromJson(reply->readAll());
    messages_getDialogs_end(response);
    reply->deleteLater();
}

void vkapi::messages_searchDialogs(QString q, int limit, QString fields)
{
    QNetworkRequest req;

    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/messages.searchDialogs?");
    req_url.addQueryItem("limit",QString::number(limit));
    req_url.addQueryItem("fields",fields);
    req_url.addQueryItem("q",q);

    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);

    req.setUrl(QUrl(req_url.query()));
    qDebug()<<   req_url.query();

    queueRequest(req,Method_Id::MESSAGES_SEARCHDIALOGS);

    //    QNetworkReply* Answer = manager->get(req);
    //    Replies[Answer]=Method_Id::MESSAGES_SEARCHDIALOGS;


}

void vkapi::onMessages_searchDialogs(QNetworkReply *reply)
{
    QJsonDocument response = QJsonDocument::fromJson(reply->readAll());
    messages_searchDialogs_end(response);
    reply->deleteLater();

}

void vkapi::messages_getHistory(int chat_id,int offset,int count,int last_message_id)
{
    Q_UNUSED(last_message_id)
    QNetworkRequest req;
    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/execute.getMessageHistory?");

    //     if(chat_id>2000000000)
    //        req_url.addQueryItem("peer_id",QString::number(chat_id));
    //      else
    req_url.addQueryItem("user_id",QString::number(chat_id));

    req_url.addQueryItem("offset",QString::number(offset));
    //  req_url.addQueryItem("start_message_id",QString::number(last_message_id));
    req_url.addQueryItem("count",QString::number(count));
    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);

    req.setUrl(QUrl(req_url.query()));

    queueRequest(req,Method_Id::MESSAGES_GETHISTORY);

    //    QNetworkReply* Answer = manager->get(req);
    //    Replies[Answer]=Method_Id::MESSAGES_GETHISTORY;

}

void vkapi::onMessages_getHistory(QNetworkReply *reply)
{
    //       qDebug()<<reply->readAll();

    QJsonDocument response = QJsonDocument::fromJson(reply->readAll());
    messages_getHistory_end(response);
    reply->deleteLater();

}

void vkapi::messages_getById(QStringList message_ids)
{
    QNetworkRequest req;
    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/execute.getMessageById?");

    QString m_ids="";
    for(int i=0;i<message_ids.length();i++)
        m_ids+=message_ids.at(i)+",";

    m_ids.remove(m_ids.length()-1,1);
    qDebug()<<"Mess_getById "+m_ids;
    req_url.addQueryItem("message_ids",m_ids.trimmed());


    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);

    req.setUrl(QUrl(req_url.query()));

    queueRequest(req,Method_Id::MESSAGES_GETBYID);

    //    QNetworkReply* Answer = manager->get(req);
    //    Replies[Answer]=Method_Id::MESSAGES_GETBYID;
}

void vkapi::onMessages_getById(QNetworkReply *reply)
{
    QJsonDocument response = QJsonDocument::fromJson(reply->readAll());
    messages_getById_end(response);
    reply->deleteLater();
}

void vkapi::messages_send(int chat_id, QString message,QString attachment)
{
    QNetworkRequest req;
    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/messages.send?");
    req_url.addQueryItem("user_id",QString::number(chat_id));
    req_url.addQueryItem("message",message);
    if(attachment.length()>0)
        req_url.addQueryItem("attachment",attachment);

    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);

    req.setUrl(QUrl(req_url.query()));

    queueRequest(req,Method_Id::MESSAGES_SEND);

    //    QNetworkReply* Answer = manager->get(req);
    //    Replies[Answer]=Method_Id::MESSAGES_SEND;

}

void vkapi::likes_add(QString type, int owner_id, int item_id)
{
    QNetworkRequest req;
    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/likes.add?");
    req_url.addQueryItem("type",type);
    req_url.addQueryItem("owner_id",QString::number(owner_id));
    req_url.addQueryItem("item_id",QString::number(item_id));

    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);

    req.setUrl(QUrl(req_url.query()));
    queueRequest(req,Method_Id::LIKES_ADD);

    //    QNetworkReply* Answer = manager->get(req);
    //    Replies[Answer]=Method_Id::LIKES_ADD;

}

void vkapi::onLikes_add(QNetworkReply *reply)
{
    QJsonDocument response = QJsonDocument::fromJson(reply->readAll());
    emit likes_add_end(response);
}

void vkapi::likes_delete(QString type, int owner_id, int item_id)
{
    QNetworkRequest req;
    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/likes.delete?");
    req_url.addQueryItem("type",type);
    req_url.addQueryItem("owner_id",QString::number(owner_id));
    req_url.addQueryItem("item_id",QString::number(item_id));

    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);

    req.setUrl(QUrl(req_url.query()));
    queueRequest(req,Method_Id::LIKES_DELETE);

    //    QNetworkReply* Answer = manager->get(req);
    //    Replies[Answer]=Method_Id::LIKES_DELETE;

}

void vkapi::onLikes_delete(QNetworkReply *reply)
{
    QJsonDocument response = QJsonDocument::fromJson(reply->readAll());
    emit likes_delete_end(response);
}

void vkapi::likes_isLiked(int user_id,QString type, int owner_id, int item_id)
{
    QNetworkRequest req;
    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/likes.isLiked?");
    req_url.addQueryItem("user_id",QString::number(user_id));

    req_url.addQueryItem("type",type);
    req_url.addQueryItem("owner_id",QString::number(owner_id));
    req_url.addQueryItem("item_id",QString::number(item_id));

    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);

    req.setUrl(QUrl(req_url.query()));
    queueRequest(req,Method_Id::LIKES_ISLIKED);


    //    QNetworkReply* Answer = manager->get(req);
    //    Replies[Answer]=Method_Id::LIKES_ISLIKED;

}

void vkapi::onLikes_isLiked(QNetworkReply *reply)
{
    QJsonDocument response = QJsonDocument::fromJson(reply->readAll());
    emit likes_isLikes_end(response);

}


void vkapi::onUser_Get(QNetworkReply *reply)
{
    switch(Replies[reply])
    {
    case Method_Id::USER_GET:
    {
        qWarning()<<"ВЫЗВАН onUserGET!!!!";
        QJsonDocument response = QJsonDocument::fromJson(reply->readAll());
        emit user_get_end(response);
        break;
    }
    case Method_Id::USER_GET_PERSONAL:
    {
        qDebug()<<"PERSONAL!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";
        QVariantMap result;
        //   qDebug()<<reply->readAll();
        QJsonDocument response = QJsonDocument::fromJson(reply->readAll());
        emit user_get_end(response);

    }

    }
    reply->deleteLater();

}

void vkapi::setToken(QString url)
{
    QUrlQuery query(url.replace("#","?"));
    token_id=query.queryItems().at(0).second;
    expires_in=QDateTime::currentMSecsSinceEpoch()/1000 + query.queryItems().at(1).second.toInt();
    user_id=query.queryItems().at(2).second.toInt();
    m_Settings->setUser_Id(user_id);
    m_Settings->setaccessToken(token_id);
    qDebug()<<QDateTime::currentMSecsSinceEpoch()<<token_id<<expires_in<<user_id;
    //Запись в БД
    tryofdb->createTokensTable(token_id,expires_in,user_id);
    // LongPollServer_get();
    LongPollServerHistory_get();
}

void vkapi::initiateLongPoll()
{
    QString Url_toLongPollServer;
    if(m_Settings->UseSSL())
        Url_toLongPollServer="https://%1?act=a_check&key=%2&ts=%3&wait=25&mode=2";
    else
        Url_toLongPollServer="http://%1?act=a_check&key=%2&ts=%3&wait=25&mode=2";

    Url_toLongPollServer=Url_toLongPollServer.arg(m_Settings->Server_LongPoll()).arg(m_Settings->Key_LongPoll()).arg(QString::number(m_Settings->Ts_LongPoll()));
    qDebug()<<"Url_toLongPollServer"<<Url_toLongPollServer<<m_Settings->Server_LongPoll()<<m_Settings->Key_LongPoll();

    QNetworkRequest req;
    req.setUrl(QUrl(Url_toLongPollServer));
    queueRequest(req,Method_Id::LONGPOLL);
    //    QNetworkReply* Answer = manager->get(req);
    //    Replies[Answer]=Method_Id::LONGPOLL;
    Log.writeToNetwork(Url_toLongPollServer);
}

void vkapi::onInitiateLongPoll(QNetworkReply *reply)
{

    QJsonDocument response = QJsonDocument::fromJson(reply->readAll());
    emit longpoll_get_end(response);
    Log.writeToNetwork(QString(response.toJson()));
    reply->deleteLater();
}

void vkapi::LongPollServer_get()
{

    QNetworkRequest req;
    QString need_pts="1";
    QString use_ssl="0";
    //    if(pts)
    //        need_pts="1";
    if(m_Settings->UseSSL())
        use_ssl="1";

    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/messages.getLongPollServer?");
    req_url.addQueryItem("use_ssl",use_ssl);
    req_url.addQueryItem("need_pts",need_pts);

    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);

    req.setUrl(QUrl(req_url.query()));

    queueRequest(req,Method_Id::GET_LONGSERVER);

    //    QNetworkReply* Answer = manager->get(req);
    //    Replies[Answer]=Method_Id::GET_LONGSERVER;
    Log.writeToNetwork(req.url().toString());


}

void vkapi::onLongPollServer_get(QNetworkReply *reply)
{
    qDebug()<<"LongPollHistory";

    QJsonDocument response = QJsonDocument::fromJson(reply->readAll());
    emit longpollServer_get_end(response);
    Log.writeToNetwork(QString(response.toJson()));
    reply->deleteLater();
    qDebug()<<"LongPollHistory";

}

void vkapi::LongPollServerHistory_get()
{
    if(m_Settings->Ts_LongPoll()>0)
    {
        QNetworkRequest req;

        QUrlQuery req_url;
        req_url.setQuery("https://api.vk.com/method/messages.getLongPollServerHistory?");
        req_url.addQueryItem("ts",QString::number(m_Settings->Ts_LongPoll()));
        req_url.addQueryItem("pts",QString::number(m_Settings->Pts_LongPoll()));

        req_url.addQueryItem("v",api_version);
        req_url.addQueryItem("lang",lang);
        req_url.addQueryItem("access_token",token_id);

        req.setUrl(QUrl(req_url.query()));

        queueRequest(req,Method_Id::LONGPOLL);

        //        QNetworkReply* Answer = manager->get(req);
        //        Replies[Answer]=Method_Id::LONGPOLL;
    }

    LongPollServer_get();
}

void vkapi::startLongPollTimer()
{
    Timer_LongPoll.start(m_Settings->LongPollTimeout());
}



void vkapi::onErrorOccurred(int ErrorCode)
{
    switch(ErrorCode)
    {
    case 5:
        //        setRefreshToken(true);
        if(QGuiApplication::allWindows().count()>0)
            emit browserForAuthNeeded();
        else
            qApp->quit();
        qDebug()<<ErrorCode;
        break;
    default:
        //        setRefreshToken(true);
        qDebug()<<ErrorCode;
        break;
    }
}

void vkapi::stat_trackVisitor()
{
    QNetworkRequest req;

    QUrlQuery req_url;
    req_url.setQuery("https://api.vk.com/method/stats.trackVisitor?");

    req_url.addQueryItem("v",api_version);
    req_url.addQueryItem("lang",lang);
    req_url.addQueryItem("access_token",token_id);

    req.setUrl(QUrl(req_url.query()));

    queueRequest(req,Method_Id::STAT_TRACKVISITOR);

    //    QNetworkReply* Answer = manager->get(req);
    //    Replies[Answer]=Method_Id::STAT_TRACKVISITOR;


}

QUrl vkapi::img_save(QString src,int type)
{
    QFileInfo a(src);
    QFile* file = new QFile;
    switch (type) {
    case 0:
        file->setFileName(QStandardPaths::writableLocation(QStandardPaths::PicturesLocation)+"/SK/"+a.fileName().split('?')[0]);
        break;
    case 1:
        file->setFileName(QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation)+"/SK/"+a.fileName().split('?')[0]);
        break;
    case 4:
        file->setFileName(QStandardPaths::writableLocation(QStandardPaths::PicturesLocation)+"/SK/"+a.fileName().split('?')[0]);
        break;
    case 5:
        file->setFileName(QStandardPaths::writableLocation(QStandardPaths::MusicLocation)+"/SK/"+a.fileName().split('?')[0]);
        break;
    case 6:
        file->setFileName(QStandardPaths::writableLocation(QStandardPaths::MoviesLocation)+"/SK/"+a.fileName().split('?')[0]);
        break;
    default:
        file->setFileName(QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation)+"/SK/"+a.fileName().split('?')[0]);
        break;
    }

    QUrl imageUrl(src);
    QNetworkRequest request;
    request.setUrl(imageUrl);
    qDebug()<<src<<type;

    QNetworkReply* reply = manager->get(request);
    //Похоже здесь супер костыль \ /
    //                            |
    Replies[reply]=Method_Id::IMAGE_GET;
    FilesToSave[reply]=file;
    file->open(QIODevice::WriteOnly);

    return QUrl::fromLocalFile(QStandardPaths::writableLocation(QStandardPaths::PicturesLocation)+"/SK/"+a.fileName());
}

void vkapi::onSaveImageFinished(QNetworkReply* reply) {
    auto file = FilesToSave[reply];
    int statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();

    if(statusCode==302)
    {
        file->close();
        QString redirectUrl = reply->attribute(QNetworkRequest::RedirectionTargetAttribute).toString();
        qDebug()<<redirectUrl<<statusCode;

        QNetworkRequest req;
        req.setUrl(redirectUrl);
        delete file;

        FilesToSave.remove(reply);
        reply->deleteLater();
        img_save(redirectUrl,1);

    }
    else{
        if (!reply->error()) {
            file->write(reply->readAll());
            file->close();
            qDebug()<<"Downloaded";
            delete file;
        }
        else{
            qDebug()<<reply->errorString();
            file->close();
            delete file;
        }
        FilesToSave.remove(reply);
        reply->deleteLater();
    }
}

bool vkapi::checkToken()
{
    int time_db=tryofdb->selectToken();
    qDebug()<<"From DB"<<time_db;
    if(time_db<=0)
    {
        qDebug()<<"Ne budu brat iz bazi";
        return false;
    }
    else
    {
        qDebug()<<"Vozmu iz bazi";
        return true;
    }
}

void vkapi::setTokenFromDatabase()
{
    QVariantList temp = tryofdb->getToken();
    token_id=temp.at(0).toString();
    expires_in=temp.at(1).toInt();
    user_id=temp.at(2).toInt();
    m_Settings->setUser_Id(user_id);
    m_Settings->setaccessToken(token_id);
    qDebug()<<token_id<<expires_in<<user_id;

    LongPollServer_get();
    stat_trackVisitor();

}


void vkapi::setRefreshToken(bool arg)
{
    qDebug()<<"setRefresh"<<m_RefreshToken<<arg;
    if (m_RefreshToken != arg) {
        m_RefreshToken = arg;
        emit refreshTokenChanged(arg);
    }
}

void vkapi::setHasInternetConnection(bool hasInternetConnection)
{

    if (m_hasInternetConnection == hasInternetConnection)
        return;
    if(hasInternetConnection)
        Log.writeToNetwork(QString("internet is on"));
    else
        Log.writeToNetwork(QString("internet is off"));

    m_hasInternetConnection = hasInternetConnection;
    if(m_hasInternetConnection)
    {
        emit RequestQueueSizeIncreased();
        Timer_Queue.stop();
    }
    else
        Timer_Queue.start();
    emit hasInternetConnectionChanged(hasInternetConnection);
}

void vkapi::queueRequest(QNetworkRequest req, vkapi::Method_Id Method)
{

    if(Method==Method_Id::LONGPOLL)
        foreach (auto item, RequestQueue) {
            if(item.second==Method)
                return;
        }

    QPair<QNetworkRequest,Method_Id> itm;
    itm.first =req;
    itm.second=Method;

    if(RequestQueue.size()>10)
        RequestQueue.dequeue();

    RequestQueue.enqueue(itm);
    Log.writeToNetwork(QString("Queue enqueued"));
    emit RequestQueueSizeIncreased();

}

void vkapi::startQueueRequest()
{

    if (hasInternetConnection()) {
        for (int i = 0; i < 3 && RequestQueue.size()!=0; ++i) {
            auto itm = RequestQueue.dequeue();
            Log.writeToNetwork(QString("queue dequeued")+itm.first.url().toString());
            qDebug()<<"queue dequeued"<<itm.first.url();
            QNetworkReply* Answer = manager->get(itm.first);
            Replies[Answer]=itm.second;
        }
    }else {
        Log.writeToNetwork(QString("No internet"));
        qDebug()<<"// it's dead";
    }

}

void vkapi::checkInternet()
{
    QFuture<void> future = QtConcurrent::run(this,&vkapi::checkInternetConcurrently,this);
}

void vkapi::checkInternetConcurrently(vkapi* parent)
{
    connect(this,&vkapi::newInternetConnection,parent,&vkapi::setHasInternetConnection);

    int exitCode = QProcess::execute("ping", QStringList() << "-c1" << "yandex.ru");
    if (0 == exitCode)
        emit newInternetConnection(true);
    else
        emit newInternetConnection(false);


}
