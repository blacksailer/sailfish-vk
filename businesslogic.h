#ifndef BUSINESSLOGIC_H
#define BUSINESSLOGIC_H

#include <QObject>
#include "model/audiomodel.h"
#include "model/wallmodel.h"
#include "model/dialogsmodel.h"
#include "model/messagesmodel.h"
#include "model/newsfeedmodel.h"
#include "databasesqlite.h"
#include "parser.h"
#include "vkapi.h"


class BusinessLogic : public QObject
{
    Q_OBJECT
public:
    explicit BusinessLogic(QObject *parent = 0);

    void startLongPoll();
    BusinessLogic(Parser *prsr,vkapi *_Api,WallModel *model,NewsFeedModel *newsmodel,AudioModel* audiomodel,DialogsModel* dialmodel,MessagesModel* messmodel);

private:
    vkapi *Api;
    QString Ids;
    Parser *Parsr;
    WallModel* parserModel;
    NewsFeedModel* Newsfeed_Model;
    AudioModel *Audio_model;
    DialogsModel *Dialogs_model;
    MessagesModel *Messages_model;
    QList<WallItem> *Wallposts;
    DatabaseSqlite* db;


signals:

public slots:
    void get_Wall_Answer(QString U_id, int count);
    void on_getWall(QJsonDocument doc);


//    void user_get_answered();
//    void get_Newsfeed_answer();
//    void get_Audio_answer();
//    void getDialogs_Messages_answer();
//    void getHistory_Messages_answer();

};

#endif // BUSINESSLOGIC_H
