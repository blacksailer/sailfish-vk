#ifndef PARSER_H
#define PARSER_H

#include <QObject>
#include <QJsonArray>
#include <QtCore>

#include "databasesqlite.h"
#include "notificationshelper.h"
#include "globalsettings.h"

#include "model/audiomodel.h"
#include "model/items/messageitem.h"
#include "model/items/commentitem.h"

#include "model/items/recursingwallitem.h"

#include "basiciteminfo.h"
#include "model/items/useritem.h"
#include "model/items/groupitem.h"
#include "model/items/chatitem.h"



class Parser : public QObject
{
    struct PostponedNotification{
    public:
         QString text;
         int  sender_id;
         QString name;
    };
    Q_OBJECT
public:
    explicit Parser(QObject *parent = 0);
    Parser(AudioModel *audiomodel,GlobalSettings *GlSett);
    enum class ATTACHMENTS_OBJECT
    {
      PHOTO,
      VIDEO,
      AUDIO,
      DOC,
      WALL,
      GIF,
        STICKER
    };
    enum LONGPOLL_FLAGS {
//        +1 	UNREAD 	сообщение не прочитано
//        +2 	OUTBOX 	исходящее сообщение
//        +4 	REPLIED 	на сообщение был создан ответ
//        +8 	IMPORTANT 	помеченное сообщение
//        +16 	CHAT 	сообщение отправлено через чат
//        +32 	FRIENDS 	сообщение отправлено другом
//        +64 	SPAM 	сообщение помечено как "Спам"
//        +128 	DELЕTЕD 	сообщение удалено (в корзине)
//        +256 	FIXED 	сообщение проверено пользователем на спам
//        +512 	MEDIA
            UNREAD = 0x01,
            OUTBOX  = 0x02,
            REPLIED  = 0x04,
            IMPORTANT = 0x08,
            CHAT = 0x10,
            FRIENDS = 0x20,
            SPAM  = 0x40,
            DELETED  = 0x80,
            FIXED = 0x100,
            MEDIA = 0x200
    };
    ~Parser();
private:
    QString Ids;
 QList<PostponedNotification> PostponedNotificationList;
QList<int> ActiveDialogsIds;
    AudioModel *Audio_model;
    QList<RecursingWallItem*> *RecWallposts;
    DatabaseSqlite* db;
    QHash<int,QVariantList> PhotoNdx; //Пара ключ-запись,значение-массив прикрепленных фото
    QHash<int,QVariantList> AudioNdx; //Пара ключ-запись,значение-массив прикрепленных фото
QVariantList Audios;
GlobalSettings* GlobalSett;
NotificationsHelper* Notifications;

//Получить атомарный объект
/*!
 * \brief Parser::getPostItem Возвращает вложенный пост. Используется в Parser::get_RecNewsfeed_answer
 * для получения copy_history
 * \param copy_History_array - copy history
 * \param Profiles - массив информации о профилях
 * \param Groups - массив информации о группах
 * \return
 */
RecursingWallItem *getPostItem(QJsonArray copy_History_array, QJsonArray Profiles, QJsonArray Groups);

/*!
 * \brief Parser::getPostItem Возвращает вложенный пост. Используется в Parser::get_RecNewsfeed_answer
 * \param copy_History_array - copy history
 * \param Profiles - массив информации о профилях
 * \param Groups - массив информации о группах
 * \return
 */
RecursingWallItem *getPostItem(QJsonObject Wall_object, QJsonArray Profiles, QJsonArray Groups);

/*!
 * \brief getFwdMsgItem Возвращает вложенное сообщение
 * \param FwdPosts - JsonArray пересланных сообщений
 * \return
 */
MessageItem *getFwdMsgItem(QJsonArray FwdPosts, QJsonArray Users, QJsonArray Groups);

/*!
 * \brief getFwdMsgItem Возвращает вложенное сообщение
 * \param MessageObj - JsonObject  сообщение
 * \return
 */
MessageItem *getFwdMsgItem(QJsonObject MessageObj,QJsonArray Users, QJsonArray Groups);
/*!
 * \brief getCommentItem Возвращает указатель на объект типа CommentItem
 * \param itm - Json объекта комментарий
 * \param Users - Массив пользователей
 * \param Groups - Массив групп
 * \return
 */
CommentItem* getCommentItem(QJsonObject itm, QJsonArray Users, QJsonArray Groups, QJsonArray DatUsers);

QMap<ATTACHMENTS_OBJECT,QVariantList> getAttachments(QJsonArray attachments);

signals:

//Start from newsfeed set
void setStart(QString value);
//Start from Recnewsfeed set
void setRecStart(QString value);

void userGetInfo(QString txt);
void getMessageById(QStringList mess_id);
//Добавить сообщение
void addMessagetoDb(QStringList Col,QVariantList Val);

void itemReady(WallItem note,QVariantList att);
void need_user_data(QString User_id,QString filter,QString Name_nom);
void errorOccurred(int Error_Code);

//Long Poll сигналы для сохранения параметров
void keyLongPoll(QString arg);
void serverLongPoll(QString arg);
void tsLongPoll(unsigned int arg);
void ptsLongPoll(int arg);
void longPollData(QString key,QString server, unsigned int ts,int pts);
void reconnectToLongPoll();

//Результаты LongPoll
void setUnreadCount(int arg);

//GROUP
void groupsGenerated(QVariantList value);

//Video ready
void videoGenerated(QVariantMap value);

void LongPollMesageGenerated(MessageItem *msg);
//!- сигналы в модели данных
void commentGenerated(CommentItem* cmnt,bool appended);
void newsfeedGenerated(RecursingWallItem* post,bool appended);
void MessageGenerated(MessageItem *msg);
void newCommentIdGenerated(int comment_id);
void dialogGenerated(MessageItem* post);
void wallGenerated(RecursingWallItem* post, bool appended);

void searchDialogsGenerated(BasicItemInfo *value);

void friendsGenerated(QVariantList value);
void userInfoGenerated(QVariantMap value);
void groupInfoGenerated(QVariantMap value);

public slots:
bool checkError(QJsonObject doc);

void get_Wall_Answer(QJsonDocument doc,bool appended);
void get_Wall_Comments(QJsonDocument doc);
void getWallCreateCommentAnswer(QJsonDocument doc);

void user_get_answered(QJsonDocument ans);

//---- GROUPS -----
void group_get_answered(QJsonDocument ans);
void group_getbyid_answered(QJsonDocument ans);

//---- FRIENDS ----
void friends_get_asnwer(QJsonDocument doc);

//Reccursing newsfeed item
void get_RecNewsfeed_answer(QJsonDocument doc,bool appended);

void get_Audio_answer(QJsonDocument doc);

//----  VIDEO   ----
void get_Video_answer(QJsonDocument doc);
void getComments_Video_answer(QJsonDocument doc);

void getDialogs_Messages_answer(QJsonDocument doc);
void searchDialogs_Messages_answer(QJsonDocument doc);

void getHistory_Messages_answer(QJsonDocument doc);

void getMessage_ById_answer(QJsonDocument doc);

void getLikes_add_answer(QJsonDocument doc);
void getLikes_delete_answer(QJsonDocument doc);
void getLikes_isLiked_answer(QJsonDocument doc);

void getNotifications(QJsonDocument doc);

//Long Poll методы
void getLongPollServer_answer(QJsonDocument doc);
void getInitiateLongPoll_answer(QJsonDocument doc);

//Работа с диалогами
void DialogIdDo(int userId,int type);


private slots:
void sendPostponedNotification(QVariantMap user);
};


#endif // PARSER_H
