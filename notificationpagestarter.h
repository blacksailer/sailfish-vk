#ifndef NOTIFICATIONPAGESTARTER_H
#define NOTIFICATIONPAGESTARTER_H

#include <QObject>
#include <QtCore>


class NotificationPageStarter : public QObject
{
    Q_OBJECT
public:
    explicit NotificationPageStarter(QObject *parent = 0);
    enum PAGETYPE {
        DIALOG,
        GROUP,
        BIRTHDAY
    };
    void addPage(PAGETYPE type,QVariant params);
signals:

public slots:
    QVariantList getPages();

private:
    QQueue<QVariantMap> m_Pages;
};

#endif // NOTIFICATIONPAGESTARTER_H
