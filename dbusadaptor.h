#ifndef DBUSADAPTOR_H
#define DBUSADAPTOR_H

#include <QQuickView>
#include <QQmlEngine>
#include <QGuiApplication>
#include <QDBusAbstractAdaptor>
#include <QDBusConnection>
#include <QDBusMessage>
#include <QDBusInterface>
#include <QDebug>
#include "skadaptor.h"

#include "utils/guinetworkaccessmanagerfactory.h"
#include "notificationpagestarter.h"

#include "vkapi.h"
#include "grouppage.h"
#include "parser.h"
#include "databasesqlite.h"

#include "model/commentsmodel.h"
#include "model/proxydialogsmodel.h"
#include "model/recursingnewsfeedmodel.h"
#include "model/attachmentstosendmodel.h"
#include "model/searchdialogsmodel.h"
#include "model/messagesmodel.h"
#include "model/wallmodel.h"
#include "model/friendsmodel.h"
#include "model/groupsmodel.h"
#include "sailfishapp.h"

class DBusAdaptor : public QObject
{
    Q_OBJECT
 //   Q_CLASSINFO("D-Bus Interface", "workshop.sk")

    Q_PROPERTY(QString organizationName READ organizationName)
    Q_PROPERTY(QString organizationDomain READ organizationDomain)
private:
    QGuiApplication *app;
    QQuickView *view;
    vkapi *a;
    NotificationPageStarter *PageStarter;
    GlobalSettings* Settings;

    Parser *JsonParser;
    AudioModel MusicModel;

public:
DBusAdaptor(QGuiApplication *application);
~DBusAdaptor();
signals:

public slots:
Q_NOREPLY void quit()
    {
    app->quit();
}

    Q_NOREPLY void ShowApp(QStringList item);
    Q_NOREPLY void openConversation(int id);
private slots:
void onViewDestroyed();
void onViewClosing(QQuickCloseEvent *v);

signals:
    void aboutToQuit();
    void mainWindowHasFocus();

public:

    QString organizationName()
    {
        return app->organizationName();
    }

    QString organizationDomain()
    {
        return app->organizationDomain();
    }
};




#endif // DBUSADAPTOR_H
