#ifndef GROUPPAGE_H
#define GROUPPAGE_H

#include <QObject>
#include <QtCore>
#include "POCO_classes/city.h"
#include"POCO_classes/country.h"
#include "POCO_classes/contacts.h"
#include "POCO_classes/cover.h"
#include "POCO_classes/baninfo.h"
#include "POCO_classes/market.h"
#include "POCO_classes/place.h"
#include"POCO_classes/counters.h"
#include "parser.h"

class GroupPage : public QObject
{
    Q_OBJECT

    /*
     * id
integer 	идентификатор сообщества.
*/
    Q_PROPERTY(int id READ id WRITE setId NOTIFY idChanged)
    /*
name
*/
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)

    /*
string 	название сообщества.
screen_name*/
    Q_PROPERTY(QString screen_name READ screen_name WRITE setScreen_name NOTIFY screen_nameChanged)
    /*
string 	короткий адрес, например, apiclub.
*/
    Q_PROPERTY(int is_closed READ is_closed  WRITE setIs_closed  NOTIFY is_closedChanged)
    /*
is_closed
integer 	является ли сообщество закрытым. Возможные значения:

    0 — открытое;
    1 — закрытое;
    2 — частное.

deactivated*/
    Q_PROPERTY(QString deactivated READ deactivated WRITE setDeactivated NOTIFY deactivatedChanged)
    /*
string 	возвращается в случае, если сообщество удалено или заблокировано. Возможные значения:

    deleted — сообщество удалено;
    banned — сообщество заблокировано;

is_admin
*/
    Q_PROPERTY(bool is_admin READ is_admin WRITE setIs_admin NOTIFY is_adminChanged)
    /*
integer, [0, 1]
Требуется scope = groups 	информация о том, является ли текущий пользователь руководителем. Возможные значения:

    1 — является;
    0 — не является.

admin_level
*/
    Q_PROPERTY(bool admin_level READ admin_level WRITE setAdmin_level NOTIFY admin_levelChanged)
    /*
integer
Требуется scope = groups 	уровень полномочий текущего пользователя (если is_admin = 1):

    1 — модератор;
    2 — редактор;
    3 — администратор.

is_member*/
    Q_PROPERTY(bool is_member READ is_member WRITE setIs_member NOTIFY is_memberChanged)
    /*
integer, [0, 1]
Требуется scope = groups 	информация о том, является ли текущий пользователь участником. Возможные значения:

    1 — является;
    0 — не является.

invited_by*/
    Q_PROPERTY(int invited_by READ invited_by WRITE setInvited_by NOTIFY invited_byChanged)
    /*
integer
Требуется scope = groups 	идентификатор пользователя, который отправил приглашение в сообщество.
Поле возвращается только для метода groups.getInvites.
type
*/
    Q_PROPERTY(QString type READ type WRITE setType NOTIFY typeChanged)
    /*
string 	тип сообщества:

    group — группа;
    page — публичная страница;
    event — мероприятие.

has_photo*/
    Q_PROPERTY(bool has_photo READ has_photo WRITE setHas_photo NOTIFY has_photoChanged)
    /*
integer, [0, 1]	информация о том, установлена ли у сообщества главная фотография. Возможные значения:

    1 — установлена;
    0 — не установлена.

photo_50*/
    Q_PROPERTY(QString photo_50 READ photo_50 WRITE setPhoto_50 NOTIFY photo_50Changed)
    /*
string 	URL главной фотографии с размером 50x50px.
photo_100*/
    Q_PROPERTY(QString photo_100 READ photo_100 WRITE setPhoto_100 NOTIFY photo_100Changed)
    /*
string 	URL главной фотографии с размером 100х100px.*/
    Q_PROPERTY(QString photo_200 READ photo_200 WRITE setPhoto_200 NOTIFY photo_200Changed)
    /*
photo_200
string 	URL главной фотографии в максимальном размере.
2. Опциональные поля A-O
activity*/
    Q_PROPERTY(QString activity READ activity WRITE setActivity NOTIFY activityChanged)
    /*
string 	строка состояния публичной страницы. У групп возвращается строковое значение, открыта ли группа или нет, а у событий дата начала.
age_limits*/
    Q_PROPERTY(int age_limits READ age_limits WRITE setAge_limits NOTIFY age_limitsChanged)
/*
integer	возрастное ограничение.

    1 — нет;
    2 — 16+;
    3 — 18+.


can_create_topic*/
    Q_PROPERTY(bool can_create_topic READ can_create_topic WRITE setCan_create_topic NOTIFY can_create_topicChanged)
    /*
integer, [0, 1] 	информация о том, может ли текущий пользователь создать новое обсуждение в группе. Возможные значения:

    1 — может;
    0 — не может.

    Поле возвращается только при передаче access_token в запросе

can_message*/
    Q_PROPERTY(bool can_message READ can_message WRITE setCan_message NOTIFY can_messageChanged)
    /*
integer, [0, 1] 	информация о том, может ли текущий пользователь написать сообщение сообществу. Возможные значения:

    1 — может;
    0 — не может.

    Поле возвращается только при передаче access_token в запросе

can_post
*/
    Q_PROPERTY(bool can_post READ can_post WRITE setCan_post NOTIFY can_postChanged)
    /*
integer, [0, 1] 	информация о том, может ли текущий пользователь оставлять записи на стене сообщества. Возможные значения:

    1 — может;
    0 — не может.

    Поле возвращается только при передаче access_token в запросе

can_see_all_posts*/
    Q_PROPERTY(bool can_see_all_posts READ can_see_all_posts WRITE setCan_see_all_posts NOTIFY can_see_all_postsChanged)
    /*
integer, [0, 1] 	информация о том, разрешено ли видеть чужие записи на стене группы. Возможные значения:

    1 — может;
    0 — не может.

    Поле возвращается только при передаче access_token в запросе

can_upload_doc*/
    Q_PROPERTY(bool can_upload_doc READ can_upload_doc WRITE setCan_upload_doc NOTIFY can_upload_docChanged)
    /*
integer, [0, 1] 	информация о том, может ли текущий пользователь загружать документы в группу. Возможные значения:

    1 — может;
    0 — не может.

    Поле возвращается только при передаче access_token в запросе

can_upload_video*/
    Q_PROPERTY(bool can_upload_video READ can_upload_video WRITE setCan_upload_video NOTIFY can_upload_videoChanged)
    /*
integer, [0, 1] 	информация о том, может ли текущий пользователь загружать видеозаписи в группу. Возможные значения:

    1 — может;
    0 — не может.

    Поле возвращается только при передаче access_token в запросе


description*/
    Q_PROPERTY(QString description READ description WRITE setDescription NOTIFY descriptionChanged)
    /*
string 	текст описания сообщества.
fixed_post*/
    Q_PROPERTY(int fixed_post READ fixed_post WRITE setFixed_post NOTIFY fixed_postChanged)
    /*
integer 	идентификатор закрепленной записи. Получить дополнительные данные о записи можно методом wall.getById, передав в поле posts {group_id}_{post_id}.
is_favorite*/
    Q_PROPERTY(bool is_favorite READ is_favorite WRITE setIs_favorite NOTIFY is_favoriteChanged)
    /*
integer, [0, 1]	информация о том, находится ли сообщество в закладках у текущего пользователя. Возможные значения:

    1 — находится ;
    0 — не находится.

    Поле возвращается только при передаче access_token в запросе

is_hidden_from_feed*/
    Q_PROPERTY(bool is_hidden_from_feed READ is_hidden_from_feed WRITE setIs_hidden_from_feed NOTIFY is_hidden_from_feedChanged)
    /*
integer, [0, 1]	информация о том, скрыто ли сообщество из ленты новостей текущего пользователя. Возможные значения:

    1 — скрыто ;
    0 — не скрыто.

    Поле возвращается только при передаче access_token в запросе

is_messages_allowed*/
    Q_PROPERTY(bool is_messages_allowed READ is_messages_allowed WRITE setIs_messages_allowed NOTIFY is_messages_allowedChanged)
    /*
integer, [0, 1] 	информация о том, разрешено ли сообществу отправлять сообщения текущему пользователю.

main_album_id*/
    Q_PROPERTY(int main_album_id READ main_album_id WRITE setMain_album_id NOTIFY main_album_idChanged)
    /*
integer 	идентификатор основного фотоальбома.
main_section*/
    Q_PROPERTY(int main_section READ main_section WRITE setMain_section NOTIFY main_sectionChanged)
    /*
integer 	информация о главной секции. Возможные значения:

    0 — отсутствует;
    1 — фотографии;
    2 — обсуждения;
    3 — аудиозаписи;
    4 — видеозаписи;
    5 — товары.

member_status*/
    Q_PROPERTY(int member_status READ member_status WRITE setMember_status NOTIFY member_statusChanged)
    /*
integer
Требуется scope = groups 	статус участника текущего пользователя. Возможные значения:

    0 — не является участником;
    1 — является участником;
    2 — не уверен, что посетит мероприятие;
    3 — отклонил приглашение;
    4 — подал заявку на вступление;
    5 — приглашен.

members_count*/
    Q_PROPERTY(int members_count READ members_count WRITE setMembers_count NOTIFY members_countChanged)
    /*
integer 	количество участников.

public_date_label*/
    Q_PROPERTY(QString public_date_label READ public_date_label WRITE setPublic_date_label NOTIFY public_date_labelChanged)
    /*
string 	возвращается для публичных страниц. Текст описания для поля start_date.
site*/
    Q_PROPERTY(QString site READ site WRITE setSite NOTIFY siteChanged)
    /*
string 	адрес сайта из поля «веб-сайт» в описании сообщества.
start_date и finish_date 	для встреч содержат время начала и окончания встречи в формате unixtime. Для публичных страниц содержит только start_date — дата основания в формате YYYYMMDD.
*/Q_PROPERTY(int start_date READ start_date WRITE setStart_date NOTIFY start_dateChanged)
    Q_PROPERTY(int finish_date READ finish_date WRITE setFinish_date NOTIFY finish_dateChanged)
/*status*/
    Q_PROPERTY(QString status READ status WRITE setStatus NOTIFY statusChanged)
    /*
string 	статус сообщества.
verified*/
    Q_PROPERTY(bool verified READ verified WRITE setVerified NOTIFY verifiedChanged)
    /*
integer, [0, 1] 	информация о том, верифицировано ли сообщество. Возможные значения:

    1 — является;
    0 — не является.

wiki_page*/
    Q_PROPERTY(QString wiki_page READ wiki_page WRITE setWiki_page NOTIFY wiki_pageChanged)
    Q_PROPERTY(City* city READ city WRITE setCity NOTIFY cityChanged)
    Q_PROPERTY(Counters* counters READ counters NOTIFY countersChanged)
    Q_PROPERTY(Country* country READ country NOTIFY countryChanged)

    /*
string 	название главной вики-страницы.


city
object 	город, указанный в информации о сообществе. Объект, содержащий следующие поля:

    id (integer) — идентификатор города;
    title (string) — название города.

counters
object 	объект, содержащий счётчики сообщества, может включать любой набор из следующих полей: photos, albums, audios, videos, topics, docs.

Поле возвращается только при запросе данных об одном сообществе
country
object 	страна, указанная в информации о сообществе. Объект, содержащий следующие поля:

    id (integer) — идентификатор страны;
    title (string) — название страны.

contacts*/
    Q_PROPERTY(QVariantList contacts READ contacts WRITE setContacts NOTIFY contactsChanged)
    /*
array 	информация из блока контактов публичной страницы. Массив объектов, каждый из которых может содержать поля:

    user_id (integer) — идентификатор пользователя;
    desc (string) — должность;
    phone (string) — номер телефона;
    email (string) — адрес e-mail.

links*/
    Q_PROPERTY(QVariantList links READ links WRITE setLinks NOTIFY linksChanged)
    /*
array 	информация из блока ссылок сообщества. Массив объектов, каждый из которых содержит следующие поля:

    id (integer) — идентификатор ссылки;
    url (string) — URL;
    name (string) — название ссылки;
    desc (string) — описание ссылки;
    photo_50 (string) — URL изображения-превью шириной 50px;
    photo_100 (string) — URL изображения-превью шириной 100px.

    Поле возвращается только при запросе данных об одном сообществе
ban_info*/
    Q_PROPERTY(BanInfo* ban_info READ ban_info NOTIFY ban_infoChanged)
    /*
object 	информация о занесении в черный список сообщества (поле возвращается только при запросе информации об одном сообществе). Объект, содержащий следующие поля:

    end_date (integer) — срок окончания блокировки в формате unixtime;
    comment (string) — комментарий к блокировке.

    Поле возвращается только при передаче access_token в запросе
cover*/
    Q_PROPERTY(Cover* cover READ cover NOTIFY coverChanged)
    /*
object 	обложка сообщества. Объект, который содержит следующие поля:

    enabled (integer,[0,1]) — информация о том, включена ли обложка (1 — да, 0 — нет);
    images (array) — копии изображений обложки. Массив объектов, каждый из которых содержит следующие поля:
        url (string) — URL копии;
        width (integer) — ширина копии;
        height (integer) — высота копии.


market*/
    Q_PROPERTY(Market* market READ market NOTIFY marketChanged)
    /*
object 	информация о магазине. Объект, содержащий следующие поля:

    enabled (integer, [0 1]) — информация о том, включен ли блок товаров в сообществе. Возможные значения:
        1 — включен;
        0 — выключен. Если enabled = 0, объект не содержит других полей.
    price_min (integer) — минимальная цена товаров;
    price_max (integer)— максимальная цена товаров;
    main_album_id (integer) — идентификатор главной подборки товаров;
    contact_id (integer) — идентификатор контактного лица для связи с продавцом. Возвращается отрицательное значение, если для связи с продавцом используются сообщения сообщества;
    currency (object) — информация о валюте. Объект, содержащий поля:
        id (integer) — идентификатор валюты;
        name (string) — символьное обозначение;
    currency_text (string) — строковое обозначение.
place*/
    Q_PROPERTY(Place* place READ place NOTIFY placeChanged)
    /*
object 	место, указанное в информации о сообществе. Объект, содержащий следующие поля:

    id (integer) — идентификатор места;
    title (string) — название места;
    latitude (number) — географическая широта в градусах (от -90 до 90);
    longitude (number) — географическая долгота в градусах (от -180 до 180);
    type (string) — тип места;
    country (integer) — идентификатор страны;
    city (integer) — идентификатор города;
    address (string) — адрес.


     */
    QString m_Nameplate="";

    QString m_Avatar_path="";

bool m_IsAdmin;

Parser* m_DataProvider=nullptr;

bool m_IsMember;

bool m_isActive;

int m_id;

QString m_name="";

QString m_screen_name="";

int m_is_closed;

QString m_deactivated="";

bool m_is_admin;

bool m_admin_level;

bool m_is_member;

int m_invited_by;

QString m_type="";

bool m_has_photo;

QString m_photo_50="";

QString m_photo_100="";

QString m_photo_200="";

QString m_activity="";

int m_age_limits;

bool m_can_create_topic;

bool m_can_message;

bool m_can_post;

bool m_can_see_all_posts;

bool m_can_upload_doc;

bool m_can_upload_video;

QString m_description="";

int m_fixed_post;

bool m_is_favorite;

bool m_is_hidden_from_feed;

bool m_is_messages_allowed;

int m_main_album_id;

int m_main_section;

int m_member_status;

int m_members_count;

QString m_public_date_label="";

QString m_site="";

int m_start_date;

int m_finish_date;

QString m_status="";

bool m_verified;

QString m_wiki_page="";

City* m_city=nullptr;

Counters* m_counters=nullptr;

Country* m_country=nullptr;

BanInfo* m_ban_info=nullptr;

Market* m_market=nullptr;

Place* m_place=nullptr;

Cover* m_cover=nullptr;

QVariantList m_contacts;

QVariantList m_links;

public:
    explicit GroupPage(QObject *parent = 0);
////--deprecated
//Q_PROPERTY(QString Avatar_path READ Avatar_path WRITE setAvatar_path NOTIFY Avatar_pathChanged)
//    Q_PROPERTY(QString Nameplate READ Nameplate WRITE setNameplate NOTIFY NameplateChanged)
//    Q_PROPERTY(bool IsMember READ IsMember WRITE setIsMember NOTIFY IsMemberChanged)
//    Q_PROPERTY(bool IsAdmin READ IsAdmin WRITE setIsAdmin NOTIFY IsAdminChanged)    //qml Привязка
////--deprecated


Q_PROPERTY(Parser* DataProvider READ DataProvider WRITE setDataProvider NOTIFY dataProviderChanged)
    Q_PROPERTY(bool isActive READ isActive WRITE setActive NOTIFY isActiveChanged)
void getResult(QVariantMap answer);
QString Nameplate() const
{
    return m_Nameplate;
}

QString Avatar_path() const
{
    return m_Avatar_path;
}

bool IsAdmin() const
{
    return m_IsAdmin;
}

Parser* DataProvider() const
{
    return m_DataProvider;
}

bool IsMember() const
{
    return m_IsMember;
}

bool isActive() const
{
    return m_isActive;
}

int id() const
{
    return m_id;
}

QString name() const
{
    return m_name;
}

QString screen_name() const
{
    return m_screen_name;
}

int is_closed() const
{
    return m_is_closed;
}

QString deactivated() const
{
    return m_deactivated;
}

bool is_admin() const
{
    return m_is_admin;
}

bool admin_level() const
{
    return m_admin_level;
}

bool is_member() const
{
    return m_is_member;
}

int invited_by() const
{
    return m_invited_by;
}

QString type() const
{
    return m_type;
}

bool has_photo() const
{
    return m_has_photo;
}

QString photo_50() const
{
    return m_photo_50;
}

QString photo_100() const
{
    return m_photo_100;
}

QString photo_200() const
{
    return m_photo_200;
}

QString activity() const
{
    return m_activity;
}

int age_limits() const
{
    return m_age_limits;
}

bool can_create_topic() const
{
    return m_can_create_topic;
}

bool can_message() const
{
    return m_can_message;
}

bool can_post() const
{
    return m_can_post;
}

bool can_see_all_posts() const
{
    return m_can_see_all_posts;
}

bool can_upload_doc() const
{
    return m_can_upload_doc;
}

bool can_upload_video() const
{
    return m_can_upload_video;
}

QString description() const
{
    return m_description;
}

int fixed_post() const
{
    return m_fixed_post;
}

bool is_favorite() const
{
    return m_is_favorite;
}

bool is_hidden_from_feed() const
{
    return m_is_hidden_from_feed;
}

bool is_messages_allowed() const
{
    return m_is_messages_allowed;
}

int main_album_id() const
{
    return m_main_album_id;
}

int main_section() const
{
    return m_main_section;
}

int member_status() const
{
    return m_member_status;
}

int members_count() const
{
    return m_members_count;
}

QString public_date_label() const
{
    return m_public_date_label;
}

QString site() const
{
    return m_site;
}

int start_date() const
{
    return m_start_date;
}

int finish_date() const
{
    return m_finish_date;
}

QString status() const
{
    return m_status;
}

bool verified() const
{
    return m_verified;
}

QString wiki_page() const
{
    return m_wiki_page;
}

City* city() const
{
    return m_city;
}

Counters* counters() const
{
    return m_counters;
}

Country* country() const
{
    return m_country;
}

BanInfo* ban_info() const
{
    return m_ban_info;
}

Market* market() const
{
    return m_market;
}

Place* place() const
{
    return m_place;
}

Cover* cover() const
{
    return m_cover;
}

QVariantList contacts() const
{
    return m_contacts;
}

QVariantList links() const
{
    return m_links;
}

signals:

void NameplateChanged(QString Nameplate);

void Avatar_pathChanged(QString Avatar_path);

void IsAdminChanged(bool IsAdmin);

void dataProviderChanged(Parser* DataProvider);

void IsMemberChanged(bool IsMember);

void isActiveChanged(bool isActive);

void idChanged(int id);

void nameChanged(QString name);

void screen_nameChanged(QString screen_name);

void is_closedChanged(int is_closed);

void deactivatedChanged(QString deactivated);

void is_adminChanged(bool is_admin);

void admin_levelChanged(bool admin_level);

void is_memberChanged(bool is_member);

void invited_byChanged(int invited_by);

void typeChanged(QString type);

void has_photoChanged(bool has_photo);

void photo_50Changed(QString photo_50);

void photo_100Changed(QString photo_100);

void photo_200Changed(QString photo_200);

void activityChanged(QString activity);

void age_limitsChanged(int age_limits);

void can_create_topicChanged(bool can_create_topic);

void can_messageChanged(bool can_message);

void can_postChanged(bool can_post);

void can_see_all_postsChanged(bool can_see_all_posts);

void can_upload_docChanged(bool can_upload_doc);

void can_upload_videoChanged(bool can_upload_video);

void descriptionChanged(QString description);

void fixed_postChanged(int fixed_post);

void is_favoriteChanged(bool is_favorite);

void is_hidden_from_feedChanged(bool is_hidden_from_feed);

void is_messages_allowedChanged(bool is_messages_allowed);

void main_album_idChanged(int main_album_id);

void main_sectionChanged(int main_section);

void member_statusChanged(int member_status);

void members_countChanged(int members_count);

void public_date_labelChanged(QString public_date_label);

void siteChanged(QString site);

void start_dateChanged(int start_date);

void finish_dateChanged(int finish_date);

void statusChanged(QString status);

void verifiedChanged(bool verified);

void wiki_pageChanged(QString wiki_page);

void cityChanged(City* city);

void countersChanged(Counters* counters);

void countryChanged(Country* country);

void marketChanged(Market* market);

void placeChanged(Place* place);

void coverChanged(Cover* cover);

void contactsChanged(QVariantList contacts);

void linksChanged(QVariantList links);

void ban_infoChanged(BanInfo* ban_info);

public slots:
void setNameplate(QString Nameplate)
{
    if (m_Nameplate == Nameplate)
        return;

    m_Nameplate = Nameplate;
    emit NameplateChanged(Nameplate);
}
void setAvatar_path(QString Avatar_path)
{
    if (m_Avatar_path == Avatar_path)
        return;

    m_Avatar_path = Avatar_path;
    emit Avatar_pathChanged(Avatar_path);
}
void setIsAdmin(bool IsAdmin)
{
    if (m_IsAdmin == IsAdmin)
        return;

    m_IsAdmin = IsAdmin;
    emit IsAdminChanged(IsAdmin);
}
void setDataProvider(Parser* DataProvider)
{
    if (m_DataProvider == DataProvider)
        return;

    m_DataProvider = DataProvider;
    connect(m_DataProvider,&Parser::groupInfoGenerated,this,&GroupPage::getResult);

    emit dataProviderChanged(DataProvider);
}
void setIsMember(bool IsMember)
{
    if (m_IsMember == IsMember)
        return;

    m_IsMember = IsMember;
    emit IsMemberChanged(IsMember);
}
void setActive(bool isActive)
{
    if (m_isActive == isActive)
        return;

    m_isActive = isActive;
    emit isActiveChanged(isActive);
}
void setId(int id)
{
    if (m_id == id)
        return;

    m_id = id;
    emit idChanged(id);
}
void setName(QString name)
{
    if (m_name == name)
        return;

    m_name = name;
    emit nameChanged(name);
}
void setScreen_name(QString screen_name)
{
    if (m_screen_name == screen_name)
        return;

    m_screen_name = screen_name;
    emit screen_nameChanged(screen_name);
}
void setIs_closed(int is_closed)
{
    if (m_is_closed == is_closed)
        return;

    m_is_closed = is_closed;
    emit is_closedChanged(is_closed);
}
void setDeactivated(QString deactivated)
{
    if (m_deactivated == deactivated)
        return;

    m_deactivated = deactivated;
    emit deactivatedChanged(deactivated);
}
void setIs_admin(bool is_admin)
{
    if (m_is_admin == is_admin)
        return;

    m_is_admin = is_admin;
    emit is_adminChanged(is_admin);
}
void setAdmin_level(bool admin_level)
{
    if (m_admin_level == admin_level)
        return;

    m_admin_level = admin_level;
    emit admin_levelChanged(admin_level);
}
void setIs_member(bool is_member)
{
    if (m_is_member == is_member)
        return;

    m_is_member = is_member;
    emit is_memberChanged(is_member);
}
void setInvited_by(int invited_by)
{
    if (m_invited_by == invited_by)
        return;

    m_invited_by = invited_by;
    emit invited_byChanged(invited_by);
}
void setType(QString type)
{
    if (m_type == type)
        return;

    m_type = type;
    emit typeChanged(type);
}
void setHas_photo(bool has_photo)
{
    if (m_has_photo == has_photo)
        return;

    m_has_photo = has_photo;
    emit has_photoChanged(has_photo);
}
void setPhoto_50(QString photo_50)
{
    if (m_photo_50 == photo_50)
        return;

    m_photo_50 = photo_50;
    emit photo_50Changed(photo_50);
}
void setPhoto_100(QString photo_100)
{
    if (m_photo_100 == photo_100)
        return;

    m_photo_100 = photo_100;
    emit photo_100Changed(photo_100);
}
void setPhoto_200(QString photo_200)
{
    if (m_photo_200 == photo_200)
        return;

    m_photo_200 = photo_200;
    emit photo_200Changed(photo_200);
}
void setActivity(QString activity)
{
    if (m_activity == activity)
        return;

    m_activity = activity;
    emit activityChanged(activity);
}
void setAge_limits(int age_limits)
{
    if (m_age_limits == age_limits)
        return;

    m_age_limits = age_limits;
    emit age_limitsChanged(age_limits);
}
void setCan_create_topic(bool can_create_topic)
{
    if (m_can_create_topic == can_create_topic)
        return;

    m_can_create_topic = can_create_topic;
    emit can_create_topicChanged(can_create_topic);
}
void setCan_message(bool can_message)
{
    if (m_can_message == can_message)
        return;

    m_can_message = can_message;
    emit can_messageChanged(can_message);
}
void setCan_post(bool can_post)
{
    if (m_can_post == can_post)
        return;

    m_can_post = can_post;
    emit can_postChanged(can_post);
}
void setCan_see_all_posts(bool can_see_all_posts)
{
    if (m_can_see_all_posts == can_see_all_posts)
        return;

    m_can_see_all_posts = can_see_all_posts;
    emit can_see_all_postsChanged(can_see_all_posts);
}
void setCan_upload_doc(bool can_upload_doc)
{
    if (m_can_upload_doc == can_upload_doc)
        return;

    m_can_upload_doc = can_upload_doc;
    emit can_upload_docChanged(can_upload_doc);
}
void setCan_upload_video(bool can_upload_video)
{
    if (m_can_upload_video == can_upload_video)
        return;

    m_can_upload_video = can_upload_video;
    emit can_upload_videoChanged(can_upload_video);
}
void setDescription(QString description)
{
    if (m_description == description)
        return;

    m_description = description;
    emit descriptionChanged(description);
}
void setFixed_post(int fixed_post)
{
    if (m_fixed_post == fixed_post)
        return;

    m_fixed_post = fixed_post;
    emit fixed_postChanged(fixed_post);
}
void setIs_favorite(bool is_favorite)
{
    if (m_is_favorite == is_favorite)
        return;

    m_is_favorite = is_favorite;
    emit is_favoriteChanged(is_favorite);
}
void setIs_hidden_from_feed(bool is_hidden_from_feed)
{
    if (m_is_hidden_from_feed == is_hidden_from_feed)
        return;

    m_is_hidden_from_feed = is_hidden_from_feed;
    emit is_hidden_from_feedChanged(is_hidden_from_feed);
}
void setIs_messages_allowed(bool is_messages_allowed)
{
    if (m_is_messages_allowed == is_messages_allowed)
        return;

    m_is_messages_allowed = is_messages_allowed;
    emit is_messages_allowedChanged(is_messages_allowed);
}
void setMain_album_id(int main_album_id)
{
    if (m_main_album_id == main_album_id)
        return;

    m_main_album_id = main_album_id;
    emit main_album_idChanged(main_album_id);
}
void setMain_section(int main_section)
{
    if (m_main_section == main_section)
        return;

    m_main_section = main_section;
    emit main_sectionChanged(main_section);
}
void setMember_status(int member_status)
{
    if (m_member_status == member_status)
        return;

    m_member_status = member_status;
    emit member_statusChanged(member_status);
}
void setMembers_count(int members_count)
{
    if (m_members_count == members_count)
        return;

    m_members_count = members_count;
    emit members_countChanged(members_count);
}
void setPublic_date_label(QString public_date_label)
{
    if (m_public_date_label == public_date_label)
        return;

    m_public_date_label = public_date_label;
    emit public_date_labelChanged(public_date_label);
}
void setSite(QString site)
{
    if (m_site == site)
        return;

    m_site = site;
    emit siteChanged(site);
}
void setStart_date(int start_date)
{
    if (m_start_date == start_date)
        return;

    m_start_date = start_date;
    emit start_dateChanged(start_date);
}
void setFinish_date(int finish_date)
{
    if (m_finish_date == finish_date)
        return;

    m_finish_date = finish_date;
    emit finish_dateChanged(finish_date);
}
void setStatus(QString status)
{
    if (m_status == status)
        return;

    m_status = status;
    emit statusChanged(status);
}
void setVerified(bool verified)
{
    if (m_verified == verified)
        return;

    m_verified = verified;
    emit verifiedChanged(verified);
}
void setWiki_page(QString wiki_page)
{
    if (m_wiki_page == wiki_page)
        return;

    m_wiki_page = wiki_page;
    emit wiki_pageChanged(wiki_page);
}
void setContacts(QVariantList contacts)
{
    if (m_contacts == contacts)
        return;

    m_contacts = contacts;
    emit contactsChanged(contacts);
}
void setLinks(QVariantList links)
{
    if (m_links == links)
        return;

    m_links = links;
    emit linksChanged(links);
}
void setCity(City* city)
{
    if (m_city == city)
        return;

    m_city = city;
    emit cityChanged(city);
}
};

#endif // GROUPPAGE_H
