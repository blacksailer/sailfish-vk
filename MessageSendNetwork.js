//Используется для выдачи ответа от getUploadServer
var internalQmlObject = Qt.createQmlObject('import QtQuick 2.0; QtObject { signal ownerUserIdReceived(int value) }', Qt.application, 'InternalQmlObject');

function request(type,wall_id,model) {
    var getUploadServer = "https://api.vk.com/method/photos.getMessagesUploadServer?v=5.62&access_token="+globalSettings.accessToken;
    if(type=="wall"& wall_id<0)
        getUploadServer = "https://api.vk.com/method/photos.getWallUploadServer?group_id="+wall_id*(-1)+"&v=5.62&access_token="+globalSettings.accessToken;
    else if(type=="wall")
        getUploadServer = "https://api.vk.com/method/photos.getWallUploadServer?v=5.62&access_token="+globalSettings.accessToken;
    //+vk api token
    //POST photo to returned address
    //экранирование в поле  photo  убрать
    // https://learn.javascript.ru/ajax-xmlhttprequest
    console.log(getUploadServer)

    get_Upload(getUploadServer, function() { // (3)
        var rspns= JSON.parse(this.responseText)
        console.log(this.responseText)
        model.UploadPhotoServer=rspns.response.upload_url
        //   c_attachmentsModelToSend.UploadPhotoServer=rspns.response.upload_url
        internalQmlObject.ownerUserIdReceived(rspns.response.user_id);
    }
    );

}


function get_Upload(url,callback){
    var xhr = new XMLHttpRequest();

    xhr.open('GET', url, true);

    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
            // defensive check
            if (typeof callback == "function") {
                // apply() sets the meaning of "this" in the callback
                callback.apply(xhr);
            }
        }
    }
    // send the request *after* the event handler is defined
    xhr.send();
}

//    xhr.upload.onprogress = function(event) {
//      alert( 'Загружено на сервер ' + event.loaded + ' байт из ' + event.total );
//    }


function photoSave( url, photo,hash,index){
    var saveMessagesPhoto = "https://api.vk.com/method/photos.saveMessagesPhoto?server="+url+"&photo="+photo+"&hash="+hash+"&access_token="+globalSettings.accessToken;

    var xhr = new XMLHttpRequest();


    xhr.onreadystatechange = function() { // (3)
        if (xhr.readyState != 4) return;

        if (xhr.status != 200) {
            console.log(xhr.status + ': ' + xhr.statusText);
        } else {
            console.log(xhr.status + ":" +xhr.responseText);
            attachDrawer.addPhotoId(xhr.responseText,index);
            // var rspns= JSON.parse(xhr.responseText)
        }

    }
    xhr.open('GET', saveMessagesPhoto, true);

    xhr.send(); // (1)

}
function photoSave( url, photo,hash,index,wall_id,type){
    var saveMessagesPhoto = "https://api.vk.com/method/photos.saveMessagesPhoto?server="+url+"&photo="+photo+"&hash="+hash+"&access_token="+globalSettings.accessToken;
    if(type==="wall")
        if(wall_id>0)
            saveMessagesPhoto = "https://api.vk.com/method/photos.saveWallPhoto?user_id="+wall_id+"&server="+url+"&photo="+photo+"&hash="+hash+"&access_token="+globalSettings.accessToken;
        else if(wall_id<0)
            saveMessagesPhoto = "https://api.vk.com/method/photos.saveWallPhoto?group_id="+wall_id*-1+"&server="+url+"&photo="+photo+"&hash="+hash+"&access_token="+globalSettings.accessToken;
        else
            saveMessagesPhoto = "https://api.vk.com/method/photos.saveWallPhoto?server="+url+"&photo="+photo+"&hash="+hash+"&access_token="+globalSettings.accessToken;
    var xhr = new XMLHttpRequest();

    console.log(saveMessagesPhoto)

    xhr.onreadystatechange = function() { // (3)
        if (xhr.readyState != 4) return;

        if (xhr.status != 200) {
            console.log(xhr.status + ': ' + xhr.statusText);
        } else {
            console.log(xhr.status + ":" +xhr.responseText);
            attachDrawer.addPhotoId(xhr.responseText,index);
            // var rspns= JSON.parse(xhr.responseText)
        }

    }
    xhr.open('GET', saveMessagesPhoto, true);

    xhr.send(); // (1)

}
function wallpost_photoSave( url, photo,hash,index,user_id,group_id,type){
    var saveMessagesPhoto = "https://api.vk.com/method/photos.saveMessagesPhoto?server="+url+"&photo="+photo+"&hash="+hash+"&access_token="+globalSettings.accessToken;
    if(type==="wall")
        if(group_id!==0)
            saveMessagesPhoto = "https://api.vk.com/method/photos.saveWallPhoto?user_id="+user_id+"&group_id="+group_id*-1+"&server="+url+"&photo="+photo+"&hash="+hash+"&access_token="+globalSettings.accessToken;
         else
            saveMessagesPhoto = "https://api.vk.com/method/photos.saveWallPhoto?server="+url+"&photo="+photo+"&hash="+hash+"&access_token="+globalSettings.accessToken;

    var xhr = new XMLHttpRequest();

    console.log(saveMessagesPhoto)

    xhr.onreadystatechange = function() { // (3)
        if (xhr.readyState != 4) return;

        if (xhr.status != 200) {
            console.log(xhr.status + ': ' + xhr.statusText);
        } else {
            console.log(xhr.status + ":" +xhr.responseText);
            attachDrawer.addPhotoId(xhr.responseText,index);
            // var rspns= JSON.parse(xhr.responseText)
        }

    }
    xhr.open('GET', saveMessagesPhoto, true);

    xhr.send(); // (1)

}

//VIDEO
//user.get
function user_get(owner_id)
{
    var url ;
    if(owner_id>0)
        url = "https://api.vk.com/method/users.get?user_ids="+owner_id+"&fields=photo_50,city,verified&name_case=Nom&v=5.62&access_token="+globalSettings.accessToken;
    else
        url = "https://api.vk.com/method/groups.getById?group_ids="+owner_id*-1+"&v=5.62&access_token="+globalSettings.accessToken;
    var xhr = new XMLHttpRequest();

    console.log(url)
    xhr.open('GET', url, true);

    xhr.send(); // (1)

    xhr.onreadystatechange = function() { // (3)
        if (xhr.readyState != 4) return;

        if (xhr.status != 200) {
            console.log(xhr.status + ': ' + xhr.statusText);
        } else {
            console.log(xhr.responseText)
            var json = JSON.parse(xhr.responseText);
            if(!json.response[0].hasOwnProperty("first_name"))
                videoPage.setUserInfo(json.response[0].photo_50,json.response[0].name)
            else
            {
                var name = json.response[0].first_name+" "+json.response[0].last_name;
                console.log(name)
                videoPage.setUserInfo(json.response[0].photo_50,name)
            }
        }

    }
}
