#include "notificationshelper.h"

NotificationsHelper::NotificationsHelper(QObject *parent, GlobalSettings *Gl) :
    QObject(parent),GlobalSet(Gl)
{
}

void NotificationsHelper::notifyMessage(int authorId, QString From, QString text)
{
    if(true)//GlobalSet->User_Id()!=authorId){
    {
        QVariantList arguments;
        arguments.append(authorId);
        QVariantList actions;
    actions.append(Notification::remoteAction("default","openApp","org.blacksailer.workshop.sk","/SK","workshop.sk","openConversation",arguments));
    Notification* NotificationObject = new Notification();
    NotificationObject->setItemCount(1);

    if(Published.keys().contains(authorId))
    {
        NotificationObject->setItemCount(Published[authorId]->itemCount()+1);
        NotificationObject->setReplacesId(Published[authorId]->replacesId());

}
        NotificationObject->setPreviewSummary(From);
        NotificationObject->setPreviewBody(text);

    NotificationObject->setAppName("SK");
    NotificationObject->setMaxContentLines(2);
    NotificationObject->setBody(text);
    NotificationObject->setOrigin("Message");
    NotificationObject->setTimestamp(QDateTime::currentDateTime());
//    NotificationObject->setHintValue("x-nemo-feedback","chat");
    NotificationObject->setCategory("x-harbour.SK.activity");
    NotificationObject->setRemoteActions(actions);

    NotificationObject->publish();
  //  NotificationId=NotificationObject->replacesId();
  //  qDebug()<<NotificationId;

    connect(NotificationObject,&Notification::closed,this,&NotificationsHelper::notificationCLosed);
    Published[authorId]=NotificationObject;
    }
}

void NotificationsHelper::notificationCLosed(uint Reason)
{
    NotificationId=0;
    Published.remove(Published.key((Notification*)sender()));
    sender()->deleteLater();
}
