#ifndef OCCUPATION_H
#define OCCUPATION_H
#include <QObject>

class Occupation: public QObject{
Q_OBJECT
    Q_PROPERTY(QString type READ type WRITE setType NOTIFY typeChanged)
    Q_PROPERTY(int id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)

public:
    QString type() const
    {
        return m_type;
    }

    int id() const
    {
        return m_id;
    }

    QString name() const
    {
        return m_name;
    }

public slots:
    void setType(QString type)
    {
        if (m_type == type)
            return;

        m_type = type;
        emit typeChanged(type);
    }

    void setId(int id)
    {
        if (m_id == id)
            return;

        m_id = id;
        emit idChanged(id);
    }

    void setName(QString name)
    {
        if (m_name == name)
            return;

        m_name = name;
        emit nameChanged(name);
    }

signals:
    void typeChanged(QString type);

    void idChanged(int id);

    void nameChanged(QString name);

private:
    QString m_type;
    int m_id;
    QString m_name;
};
#endif //OCCUPATION_H
