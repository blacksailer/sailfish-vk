#ifndef BANINFO_H
#define BANINFO_H
#include <QObject>
class BanInfo : public QObject
{
    Q_OBJECT
public:
/*
 *
 *
    end_date (integer) — срок окончания блокировки в формате unixtime;
    comment (string) — комментарий к блокировке.
*/
    Q_PROPERTY(int end_date READ end_date WRITE setEnd_date NOTIFY end_dateChanged)
    Q_PROPERTY(QString comment READ comment WRITE setComment NOTIFY commentChanged)

    int end_date() const
    {
        return m_end_date;
    }
    QString comment() const
    {
        return m_comment;
    }

public slots:
    void setEnd_date(int end_date)
    {
        if (m_end_date == end_date)
            return;

        m_end_date = end_date;
        emit end_dateChanged(end_date);
    }
    void setComment(QString comment)
    {
        if (m_comment == comment)
            return;

        m_comment = comment;
        emit commentChanged(comment);
    }

signals:
    void end_dateChanged(int end_date);
    void commentChanged(QString comment);

private:
    int m_end_date;
    QString m_comment;
};
#endif // BANINFO_H
