#ifndef MARKET_H
#define MARKET_H
#include <QObject>
class Currency;
class Market : public QObject
{
    Q_OBJECT
    bool m_enabled;

    int m_price_min;

int m_price_max;

int m_main_album_id;

int m_contact_id;

Currency* m_currency;

QString m_currency_name;

public:
    Q_PROPERTY(bool enabled READ enabled WRITE setEnabled NOTIFY enabledChanged)
    Q_PROPERTY(int price_min READ price_min WRITE setPrice_min NOTIFY price_minChanged)
    Q_PROPERTY(int price_max READ price_max WRITE setPrice_max NOTIFY price_maxChanged)
    Q_PROPERTY(int main_album_id READ main_album_id WRITE setMain_album_id NOTIFY main_album_idChanged)
    Q_PROPERTY(int contact_id READ contact_id WRITE setContact_id NOTIFY contact_idChanged)
    Q_PROPERTY(Currency* currency READ currency)
    Q_PROPERTY(QString currency_name READ currency_name WRITE setCurrency_name NOTIFY currency_nameChanged)

bool enabled() const
{
    return m_enabled;
}
int price_min() const
{
    return m_price_min;
}

int price_max() const
{
    return m_price_max;
}

int main_album_id() const
{
    return m_main_album_id;
}

int contact_id() const
{
    return m_contact_id;
}

Currency* currency() const
{
    return m_currency;
}

QString currency_name() const
{
    return m_currency_name;
}

public slots:
void setEnabled(bool enabled)
{
    if (m_enabled == enabled)
        return;

    m_enabled = enabled;
    emit enabledChanged(enabled);
}
void setPrice_min(int price_min)
{
    if (m_price_min == price_min)
        return;

    m_price_min = price_min;
    emit price_minChanged(price_min);
}

void setPrice_max(int price_max)
{
    if (m_price_max == price_max)
        return;

    m_price_max = price_max;
    emit price_maxChanged(price_max);
}

void setMain_album_id(int main_album_id)
{
    if (m_main_album_id == main_album_id)
        return;

    m_main_album_id = main_album_id;
    emit main_album_idChanged(main_album_id);
}

void setContact_id(int contact_id)
{
    if (m_contact_id == contact_id)
        return;

    m_contact_id = contact_id;
    emit contact_idChanged(contact_id);
}

void setCurrency_name(QString currency_name)
{
    if (m_currency_name == currency_name)
        return;

    m_currency_name = currency_name;
    emit currency_nameChanged(currency_name);
}

signals:
void enabledChanged(bool enabled);
void price_minChanged(int price_min);
void price_maxChanged(int price_max);
void main_album_idChanged(int main_album_id);
void contact_idChanged(int contact_id);
void currency_nameChanged(QString currency_name);
};

class Currency : public QObject
{
    Q_OBJECT
    int m_id;

    QString m_name;

public:
    Q_PROPERTY(int id READ id WRITE setId NOTIFY idChanged)
Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
int id() const
{
    return m_id;
}
QString name() const
{
    return m_name;
}

public slots:
void setId(int id)
{
    if (m_id == id)
        return;

    m_id = id;
    emit idChanged(id);
}
void setName(QString name)
{
    if (m_name == name)
        return;

    m_name = name;
    emit nameChanged(name);
}

signals:
void idChanged(int id);
void nameChanged(QString name);
};
/*
 *

market
object 	информация о магазине. Объект, содержащий следующие поля:

    enabled (integer, [0 1]) — информация о том, включен ли блок товаров в сообществе. Возможные значения:
        1 — включен;
        0 — выключен. Если enabled = 0, объект не содержит других полей.
    price_min (integer) — минимальная цена товаров;
    price_max (integer)— максимальная цена товаров;
    main_album_id (integer) — идентификатор главной подборки товаров;
    contact_id (integer) — идентификатор контактного лица для связи с продавцом. Возвращается отрицательное значение, если для связи с продавцом используются сообщения сообщества;
    currency (object) — информация о валюте. Объект, содержащий поля:
        id (integer) — идентификатор валюты;
        name (string) — символьное обозначение;
    currency_text (string) — строковое обозначение.*/
#endif // MARKET_H
