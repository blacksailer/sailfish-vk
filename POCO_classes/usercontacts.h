#ifndef USERCONTACTS_H
#define USERCONTACTS_H
#include <QObject>
class UserContacts : public QObject
{
    Q_OBJECT
    QString m_mobile_phone;

    QString m_home_phone;

public:
    UserContacts();
    Q_PROPERTY(QString mobile_phone READ mobile_phone WRITE setMobile_phone NOTIFY mobile_phoneChanged)
    Q_PROPERTY(QString home_phone READ home_phone WRITE setHome_phone NOTIFY home_phoneChanged)
/*    mobile_phone (string) — номер мобильного телефона пользователя (только для Standalone-приложений);
    home_phone (string) — дополнительный номер телефона пользователя.*/
    QString mobile_phone() const
    {
        return m_mobile_phone;
    }
    QString home_phone() const
    {
        return m_home_phone;
    }

public slots:
    void setMobile_phone(QString mobile_phone)
    {
        if (m_mobile_phone == mobile_phone)
            return;

        m_mobile_phone = mobile_phone;
        emit mobile_phoneChanged(mobile_phone);
    }
    void setHome_phone(QString home_phone)
    {
        if (m_home_phone == home_phone)
            return;

        m_home_phone = home_phone;
        emit home_phoneChanged(home_phone);
    }

signals:
    void mobile_phoneChanged(QString mobile_phone);
    void home_phoneChanged(QString home_phone);
};


#endif // USERCONTACTS_H
