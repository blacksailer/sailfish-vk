#ifndef RELATIVE_H
#define RELATIVE_H
#include <QObject>


class Relative : public QObject
{
Q_OBJECT
    Q_PROPERTY(int id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString type READ type WRITE setType NOTIFY typeChanged)

public:
    int id() const
    {
        return m_id;
    }

    QString name() const
    {
        return m_name;
    }

    QString type() const
    {
        return m_type;
    }

public slots:
    void setId(int id)
    {
        if (m_id == id)
            return;

        m_id = id;
        emit idChanged(id);
    }

    void setName(QString name)
    {
        if (m_name == name)
            return;

        m_name = name;
        emit nameChanged(name);
    }

    void setType(QString type)
    {
        if (m_type == type)
            return;

        m_type = type;
        emit typeChanged(type);
    }

signals:
    void idChanged(int id);

    void nameChanged(QString name);

    void typeChanged(QString type);

private:
    int m_id;
    QString m_name;
    QString m_type;
};
#endif // RELATIVE_H
