#ifndef COVER_H
#define COVER_H
#include <QObject>
#include <QVariant>
class Cover : public QObject
{
    Q_OBJECT
public:
    Q_PROPERTY(bool enabled READ enabled WRITE setEnabled NOTIFY enabledChanged)
    Q_PROPERTY(QVariantList images READ images WRITE setImages NOTIFY imagesChanged)
    bool enabled() const
    {
        return m_enabled;
    }
    QVariantList images() const
    {
        return m_images;
    }

public slots:
    void setEnabled(bool enabled)
    {
        if (m_enabled == enabled)
            return;

        m_enabled = enabled;
        emit enabledChanged(enabled);
    }
    void setImages(QVariantList images)
    {
        if (m_images == images)
            return;

        m_images = images;
        emit imagesChanged(images);
    }

signals:
    void enabledChanged(bool enabled);
    void imagesChanged(QVariantList images);

private:
    bool m_enabled;
    QVariantList m_images;
};/*cover
object 	обложка сообщества. Объект, который содержит следующие поля:

    enabled (integer,[0,1]) — информация о том, включена ли обложка (1 — да, 0 — нет);
    images (array) — копии изображений обложки. Массив объектов, каждый из которых содержит следующие поля:
        url (string) — URL копии;
        width (integer) — ширина копии;
        height (integer) — высота копии.
*/

#endif // COVER_H
