#ifndef INCLUDEPOCO_H
#define INCLUDEPOCO_H
#include "baninfo.h"
#include "career.h"
#include "city.h"
#include "contacts.h"
#include "counters.h"
#include "country.h"
#include "cover.h"
#include "education.h"
#include "lastseen.h"
#include "market.h"
#include "military.h"
#include "occupation.h"
#include "personal.h"
#include "place.h"
#include "relative.h"
#include "school.h"
#include "university.h"
#include "usercontacts.h"

class IncludePoco: public QObject
{
    Q_OBJECT
public:

};
#endif // INCLUDEPOCO_H
