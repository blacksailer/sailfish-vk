#ifndef UNIVERSITY_H
#define UNIVERSITY_H
#include <QObject>

class University : public QObject
{
Q_OBJECT
    Q_PROPERTY(int id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(int country READ country WRITE setCountry NOTIFY countryChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(int faculty READ faculty WRITE setFaculty NOTIFY facultyChanged)
    Q_PROPERTY(QString faculty_name READ faculty_name WRITE setFaculty_name NOTIFY faculty_nameChanged)
    Q_PROPERTY(QString chair_name READ chair_name WRITE setChair_name NOTIFY chair_nameChanged)
    Q_PROPERTY(int chair READ chair WRITE setChair NOTIFY chairChanged)
    Q_PROPERTY(int graduation READ graduation WRITE setGraduation NOTIFY graduationChanged)
    Q_PROPERTY(QString education_form READ education_form WRITE setEducation_form NOTIFY education_formChanged)
    Q_PROPERTY(QString education_status READ education_status WRITE setEducation_status NOTIFY education_statusChanged)

public:
    int id() const
    {
        return m_id;
    }

    int country() const
    {
        return m_country;
    }

    QString name() const
    {
        return m_name;
    }

    int faculty() const
    {
        return m_faculty;
    }

    QString faculty_name() const
    {
        return m_faculty_name;
    }

    QString chair_name() const
    {
        return m_chair_name;
    }

    int chair() const
    {
        return m_chair;
    }

    int graduation() const
    {
        return m_graduation;
    }

    QString education_form() const
    {
        return m_education_form;
    }

    QString education_status() const
    {
        return m_education_status;
    }

public slots:
    void setId(int id)
    {
        if (m_id == id)
            return;

        m_id = id;
        emit idChanged(id);
    }

    void setCountry(int country)
    {
        if (m_country == country)
            return;

        m_country = country;
        emit countryChanged(country);
    }

    void setName(QString name)
    {
        if (m_name == name)
            return;

        m_name = name;
        emit nameChanged(name);
    }

    void setFaculty(int faculty)
    {
        if (m_faculty == faculty)
            return;

        m_faculty = faculty;
        emit facultyChanged(faculty);
    }

    void setFaculty_name(QString faculty_name)
    {
        if (m_faculty_name == faculty_name)
            return;

        m_faculty_name = faculty_name;
        emit faculty_nameChanged(faculty_name);
    }

    void setChair_name(QString chair_name)
    {
        if (m_chair_name == chair_name)
            return;

        m_chair_name = chair_name;
        emit chair_nameChanged(chair_name);
    }

    void setChair(int chair)
    {
        if (m_chair == chair)
            return;

        m_chair = chair;
        emit chairChanged(chair);
    }

    void setGraduation(int graduation)
    {
        if (m_graduation == graduation)
            return;

        m_graduation = graduation;
        emit graduationChanged(graduation);
    }

    void setEducation_form(QString education_form)
    {
        if (m_education_form == education_form)
            return;

        m_education_form = education_form;
        emit education_formChanged(education_form);
    }

    void setEducation_status(QString education_status)
    {
        if (m_education_status == education_status)
            return;

        m_education_status = education_status;
        emit education_statusChanged(education_status);
    }

signals:
    void idChanged(int id);

    void countryChanged(int country);

    void nameChanged(QString name);

    void facultyChanged(int faculty);

    void faculty_nameChanged(QString faculty_name);

    void chair_nameChanged(QString chair_name);

    void chairChanged(int chair);

    void graduationChanged(int graduation);

    void education_formChanged(QString education_form);

    void education_statusChanged(QString education_status);

private:
    int m_id;
    int m_country;
    QString m_name;
    int m_faculty;
    QString m_faculty_name;
    QString m_chair_name;
    int m_chair;
    int m_graduation;
    QString m_education_form;
    QString m_education_status;
};
#endif // UNIVERSITY_H
