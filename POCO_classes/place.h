#ifndef PLACE_H
#define PLACE_H
#include <QObject>
class Place : public QObject
{
    Q_OBJECT
public:
    Q_PROPERTY(int id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(QString title READ title WRITE setTitle NOTIFY titleChanged)
    Q_PROPERTY(double latitude READ latitude WRITE setLatitude NOTIFY latitudeChanged)
    Q_PROPERTY(double longitude READ longitude WRITE setLongitude NOTIFY longitudeChanged)
    Q_PROPERTY(QString type READ type WRITE setType NOTIFY typeChanged)
    Q_PROPERTY(QString icon READ icon WRITE setIcon NOTIFY iconChanged)
    Q_PROPERTY(int country READ country WRITE setCountry NOTIFY countryChanged)
    Q_PROPERTY(int city READ city WRITE setCity NOTIFY cityChanged)
    Q_PROPERTY(QString address READ address WRITE setAddress NOTIFY addressChanged)
    Q_PROPERTY(int checkins READ checkins WRITE setCheckins NOTIFY checkinsChanged)
    Q_PROPERTY(int updated READ updated WRITE setUpdated NOTIFY updatedChanged)

    /*
 * place
object 	место, указанное в информации о сообществе. Объект, содержащий следующие поля:

    id (integer) — идентификатор места;
    title (string) — название места;
    latitude (number) — географическая широта в градусах (от -90 до 90);
    longitude (number) — географическая долгота в градусах (от -180 до 180);
    type (string) — тип места;
    country (integer) — идентификатор страны;
    city (integer) — идентификатор города;
    address (string) — адрес.

 */int id() const
    {
        return m_id;
    }
    QString title() const
    {
        return m_title;
    }

    double latitude() const
    {
        return m_latitude;
    }

    double longitude() const
    {
        return m_longitude;
    }

    QString type() const
    {
        return m_type;
    }

    int country() const
    {
        return m_country;
    }

    int city() const
    {
        return m_city;
    }

    QString address() const
    {
        return m_address;
    }

    QString icon() const
    {
        return m_icon;
    }

    int updated() const
    {
        return m_updated;
    }

    int checkins() const
    {
        return m_checkins;
    }

public slots:
    void setId(int id)
    {
        if (m_id == id)
            return;

        m_id = id;
        emit idChanged(id);
    }
    void setTitle(QString title)
    {
        if (m_title == title)
            return;

        m_title = title;
        emit titleChanged(title);
    }

    void setLatitude(double latitude)
    {
        if (m_latitude == latitude)
            return;

        m_latitude = latitude;
        emit latitudeChanged(latitude);
    }

    void setLongitude(double longitude)
    {
        if (m_longitude == longitude)
            return;

        m_longitude = longitude;
        emit longitudeChanged(longitude);
    }

    void setType(QString type)
    {
        if (m_type == type)
            return;

        m_type = type;
        emit typeChanged(type);
    }

    void setCountry(int country)
    {
        if (m_country == country)
            return;

        m_country = country;
        emit countryChanged(country);
    }

    void setCity(int city)
    {
        if (m_city == city)
            return;

        m_city = city;
        emit cityChanged(city);
    }

    void setAddress(QString address)
    {
        if (m_address == address)
            return;

        m_address = address;
        emit addressChanged(address);
    }

    void setIcon(QString icon)
    {
        if (m_icon == icon)
            return;

        m_icon = icon;
        emit iconChanged(icon);
    }

    void setUpdated(int updated)
    {
        if (m_updated == updated)
            return;

        m_updated = updated;
        emit updatedChanged(updated);
    }

    void setCheckins(int checkins)
    {
        if (m_checkins == checkins)
            return;

        m_checkins = checkins;
        emit checkinsChanged(checkins);
    }

signals:
    void idChanged(int id);
    void titleChanged(QString title);

    void latitudeChanged(double latitude);

    void longitudeChanged(double longitude);

    void typeChanged(QString type);

    void countryChanged(int country);

    void cityChanged(int city);

    void addressChanged(QString address);

    void iconChanged(QString icon);

    void updatedChanged(int updated);

    void checkinsChanged(int checkins);

private:
    int m_id;
    QString m_title;
    double m_latitude;
    double m_longitude;
    QString m_type;
    int m_country;
    int m_city;
    QString m_address;
    QString m_icon;
    int m_updated;
    int m_checkins;
};
#endif // PLACE_H
