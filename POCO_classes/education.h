#ifndef EDUCATION_H
#define EDUCATION_H
#include <QObject>
class Education : public QObject
{
Q_OBJECT
    Q_PROPERTY(int university READ university WRITE setUniversity NOTIFY universityChanged)
    Q_PROPERTY(QString university_name READ university_name WRITE setUniversity_name NOTIFY university_nameChanged)
    Q_PROPERTY(int faculty READ faculty WRITE setFaculty NOTIFY facultyChanged)
    Q_PROPERTY(QString faculty_name READ faculty_name WRITE setFaculty_name NOTIFY faculty_nameChanged)
    Q_PROPERTY(int graduation READ graduation WRITE setGraduation NOTIFY graduationChanged)

public:
    int university() const
    {
        return m_university;
    }

    QString university_name() const
    {
        return m_university_name;
    }

    int faculty() const
    {
        return m_faculty;
    }

    QString faculty_name() const
    {
        return m_faculty_name;
    }

    int graduation() const
    {
        return m_graduation;
    }

public slots:
    void setUniversity(int university)
    {
        if (m_university == university)
            return;

        m_university = university;
        emit universityChanged(university);
    }

    void setUniversity_name(QString university_name)
    {
        if (m_university_name == university_name)
            return;

        m_university_name = university_name;
        emit university_nameChanged(university_name);
    }

    void setFaculty(int faculty)
    {
        if (m_faculty == faculty)
            return;

        m_faculty = faculty;
        emit facultyChanged(faculty);
    }

    void setFaculty_name(QString faculty_name)
    {
        if (m_faculty_name == faculty_name)
            return;

        m_faculty_name = faculty_name;
        emit faculty_nameChanged(faculty_name);
    }

    void setGraduation(int graduation)
    {
        if (m_graduation == graduation)
            return;

        m_graduation = graduation;
        emit graduationChanged(graduation);
    }

signals:
    void universityChanged(int university);

    void university_nameChanged(QString university_name);

    void facultyChanged(int faculty);

    void faculty_nameChanged(QString faculty_name);

    void graduationChanged(int graduation);

private:
    int m_university;
    QString m_university_name;
    int m_faculty;
    QString m_faculty_name;
    int m_graduation;
};
#endif // EDUCATION_H
