#ifndef CONTACTS_H
#define CONTACTS_H

#include <QObject>

class Contacts:public QObject
{

    Q_OBJECT
    int m_user_id;

    QString m_desc;

QString m_phone;

QString m_email;

public:
    Contacts();
    Q_PROPERTY(int user_id READ user_id WRITE setUser_id NOTIFY user_idChanged)
    Q_PROPERTY(QString desc READ desc WRITE setDesc NOTIFY descChanged)
    Q_PROPERTY(QString phone READ phone WRITE setPhone NOTIFY phoneChanged)
    Q_PROPERTY(QString email READ email WRITE setEmail NOTIFY emailChanged)
    /*
     * 8user_id (integer) — идентификатор пользователя;
    desc (string) — должность;
    phone (string) — номер телефона;
    email (string) — адрес e-mail.*/
    int user_id() const
    {
        return m_user_id;
    }
    QString desc() const
    {
        return m_desc;
    }

    QString phone() const
    {
        return m_phone;
    }

    QString email() const
    {
        return m_email;
    }

public slots:
    void setUser_id(int user_id)
    {
        if (m_user_id == user_id)
            return;

        m_user_id = user_id;
        emit user_idChanged(user_id);
    }
    void setDesc(QString desc)
    {
        if (m_desc == desc)
            return;

        m_desc = desc;
        emit descChanged(desc);
    }

    void setPhone(QString phone)
    {
        if (m_phone == phone)
            return;

        m_phone = phone;
        emit phoneChanged(phone);
    }

    void setEmail(QString email)
    {
        if (m_email == email)
            return;

        m_email = email;
        emit emailChanged(email);
    }

signals:
    void user_idChanged(int user_id);
    void descChanged(QString desc);
    void phoneChanged(QString phone);
    void emailChanged(QString email);
};

#endif // CONTACTS_H
