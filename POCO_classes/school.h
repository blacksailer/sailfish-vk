#ifndef SCHOOL_H
#define SCHOOL_H
#include <QObject>
class School : public QObject
{
Q_OBJECT
    Q_PROPERTY(QString id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(int country READ country WRITE setCountry NOTIFY countryChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(int year_from READ year_from WRITE setYear_from NOTIFY year_fromChanged)
    Q_PROPERTY(int year_to READ year_to WRITE setYear_to NOTIFY year_toChanged)
    Q_PROPERTY(int year_graduated READ year_graduated WRITE setYear_graduated NOTIFY year_graduatedChanged)
    Q_PROPERTY(QString class_ READ class_ WRITE setClass_ NOTIFY class_Changed)
    Q_PROPERTY(QString speciality READ speciality WRITE setSpeciality NOTIFY specialityChanged)
    Q_PROPERTY(int type READ type WRITE setType NOTIFY typeChanged)
    Q_PROPERTY(QString type_str READ type_str WRITE setType_str NOTIFY type_strChanged)


public:
    QString id() const
    {
        return m_id;
    }

    int country() const
    {
        return m_country;
    }

    QString name() const
    {
        return m_name;
    }

    int year_from() const
    {
        return m_year_from;
    }

    int year_to() const
    {
        return m_year_to;
    }

    int year_graduated() const
    {
        return m_year_graduated;
    }

    QString class_() const
    {
        return m_class_;
    }

    QString speciality() const
    {
        return m_speciality;
    }

    int type() const
    {
        return m_type;
    }

    QString type_str() const
    {
        return m_type_str;
    }

public slots:
    void setId(QString id)
    {
        if (m_id == id)
            return;

        m_id = id;
        emit idChanged(id);
    }

    void setCountry(int country)
    {
        if (m_country == country)
            return;

        m_country = country;
        emit countryChanged(country);
    }

    void setName(QString name)
    {
        if (m_name == name)
            return;

        m_name = name;
        emit nameChanged(name);
    }

    void setYear_from(int year_from)
    {
        if (m_year_from == year_from)
            return;

        m_year_from = year_from;
        emit year_fromChanged(year_from);
    }

    void setYear_to(int year_to)
    {
        if (m_year_to == year_to)
            return;

        m_year_to = year_to;
        emit year_toChanged(year_to);
    }

    void setYear_graduated(int year_graduated)
    {
        if (m_year_graduated == year_graduated)
            return;

        m_year_graduated = year_graduated;
        emit year_graduatedChanged(year_graduated);
    }

    void setClass_(QString m_class)
    {
        if (m_class_ == m_class)
            return;

        m_class_ = m_class;
        emit class_Changed(m_class);
    }

    void setSpeciality(QString speciality)
    {
        if (m_speciality == speciality)
            return;

        m_speciality = speciality;
        emit specialityChanged(speciality);
    }

    void setType(int type)
    {
        if (m_type == type)
            return;

        m_type = type;
        emit typeChanged(type);
    }

    void setType_str(QString type_str)
    {
        if (m_type_str == type_str)
            return;

        m_type_str = type_str;
        emit type_strChanged(type_str);
    }

signals:
    void idChanged(QString id);

    void countryChanged(int country);

    void nameChanged(QString name);

    void year_fromChanged(int year_from);

    void year_toChanged(int year_to);

    void year_graduatedChanged(int year_graduated);

    void class_Changed(QString m_class);

    void specialityChanged(QString speciality);

    void typeChanged(int type);

    void type_strChanged(QString type_str);

private:
    QString m_id;
    int m_country;
    QString m_name;
    int m_year_from;
    int m_year_to;
    int m_year_graduated;
    QString m_class_;
    QString m_speciality;
    int m_type;
    QString m_type_str;
};
#endif // SCHOOL_H
