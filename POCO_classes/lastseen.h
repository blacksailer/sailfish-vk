#ifndef LASTSEEN_H
#define LASTSEEN_H
#include <QObject>
class LastSeen : public QObject {
Q_OBJECT
    Q_PROPERTY(int time READ time WRITE setTime NOTIFY timeChanged)
    Q_PROPERTY(QString platform READ platform WRITE setPlatform NOTIFY platformChanged)

public:
    QString platform() const
    {
        return m_platform;
    }

    int time() const
    {
        return m_time;
    }

public slots:
    void setPlatform(QString platform)
    {
        if (m_platform == platform)
            return;

        m_platform = platform;
        emit platformChanged(platform);
    }

    void setTime(int time)
    {
        if (m_time == time)
            return;

        m_time = time;
        emit timeChanged(time);
    }

signals:
    void platformChanged(QString platform);

    void timeChanged(int time);

private:
    QString m_platform;
    int m_time;
};
#endif // LASTSEEN_H
