#ifndef CITY_H
#define CITY_H
#include <QObject>
class City : public QObject{
Q_OBJECT
    Q_PROPERTY(int id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(QString title READ title WRITE setTitle NOTIFY titleChanged)

public:
    int id() const
    {
        return m_id;
    }

    QString title() const
    {
        return m_title;
    }

public slots:
    void setId(int id)
    {
        if (m_id == id)
            return;

        m_id = id;
        emit idChanged(id);
    }

    void setTitle(QString title)
    {
        if (m_title == title)
            return;

        m_title = title;
        emit titleChanged(title);
    }

signals:
    void idChanged(int id);

    void titleChanged(QString title);

private:
    int m_id;
    QString m_title;
};
#endif // CITY_H
