#ifndef PERSONAL_H
#define PERSONAL_H
#include <QStringList>
#include <QObject>
class Personal : public QObject
{
Q_OBJECT
    Q_PROPERTY(QString political READ political WRITE setPolitical NOTIFY politicalChanged)
    Q_PROPERTY(QString langs READ langs WRITE setLangs NOTIFY langsChanged)
    Q_PROPERTY(QString religion READ religion WRITE setReligion NOTIFY religionChanged)
    Q_PROPERTY(QString inspired_by READ inspired_by WRITE setInspired_by NOTIFY inspired_byChanged)
    Q_PROPERTY(QString people_main READ people_main WRITE setPeople_main NOTIFY people_mainChanged)
    Q_PROPERTY(QString life_main READ life_main WRITE setLife_main NOTIFY life_mainChanged)
    Q_PROPERTY(QString smoking READ smoking WRITE setSmoking NOTIFY smokingChanged)
\
public:
    QString political() const
    {
        return m_political;
    }

    QString langs() const
    {

        return m_langs;
    }

    QString religion() const
    {
        return m_religion;
    }

    QString inspired_by() const
    {
        return m_inspired_by;
    }

    QString people_main() const
    {
        return m_people_main;
    }

    QString life_main() const
    {
        return m_life_main;
    }

    QString smoking() const
    {
        return m_smoking;
    }

public slots:
    void setPolitical(QString political)
    {
        if (m_political == political)
            return;

        m_political = political;
        emit politicalChanged(political);
    }

    void setLangs(QString langs)
    {
        if (m_langs == langs)
            return;

        m_langs = langs;
        emit langsChanged(langs);
    }

    void setReligion(QString religion)
    {
        if (m_religion == religion)
            return;

        m_religion = religion;
        emit religionChanged(religion);
    }

    void setInspired_by(QString inspired_by)
    {
        if (m_inspired_by == inspired_by)
            return;

        m_inspired_by = inspired_by;
        emit inspired_byChanged(inspired_by);
    }

    void setPeople_main(QString people_main)
    {
        if (m_people_main == people_main)
            return;

        m_people_main = people_main;
        emit people_mainChanged(people_main);
    }

    void setLife_main(QString life_main)
    {
        if (m_life_main == life_main)
            return;

        m_life_main = life_main;
        emit life_mainChanged(life_main);
    }

    void setSmoking(QString smoking)
    {
        if (m_smoking == smoking)
            return;

        m_smoking = smoking;
        emit smokingChanged(smoking);
    }

signals:
    void politicalChanged(QString political);

    void langsChanged(QString langs);

    void religionChanged(QString religion);

    void inspired_byChanged(QString inspired_by);

    void people_mainChanged(QString people_main);

    void life_mainChanged(QString life_main);

    void smokingChanged(QString smoking);

private:
    QString m_political;
    QString m_langs;
    QString m_religion;
    QString m_inspired_by;
    QString m_people_main;
    QString m_life_main;
    QString m_smoking;
};
#endif // PERSONAL_H
