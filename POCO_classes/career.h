#ifndef CAREER_H
#define CAREER_H
#include <QObject>

class Career : public QObject {
Q_OBJECT
    Q_PROPERTY(int group_id READ group_id WRITE setGroup_id NOTIFY group_idChanged)
    Q_PROPERTY(QString company READ company WRITE setCompany NOTIFY companyChanged)
    Q_PROPERTY(int country_id READ country_id WRITE setCountry_id NOTIFY country_idChanged)
    Q_PROPERTY(int city_id READ city_id WRITE setCity_id NOTIFY city_idChanged)
    Q_PROPERTY(QString city_name READ city_name WRITE setCity_name NOTIFY city_nameChanged)
    Q_PROPERTY(int from READ from WRITE setFrom NOTIFY fromChanged)
    Q_PROPERTY(int until READ until WRITE setUntil NOTIFY untilChanged)
    Q_PROPERTY(QString position READ position WRITE setPosition NOTIFY positionChanged)

public:
    int group_id() const
    {
        return m_group_id;
    }

    QString company() const
    {
        return m_company;
    }

    int country_id() const
    {
        return m_country_id;
    }

    int city_id() const
    {
        return m_city_id;
    }

    QString city_name() const
    {
        return m_city_name;
    }

    int from() const
    {
        return m_from;
    }

    int until() const
    {
        return m_until;
    }

    QString position() const
    {
        return m_position;
    }

public slots:
    void setGroup_id(int group_id)
    {
        if (m_group_id == group_id)
            return;

        m_group_id = group_id;
        emit group_idChanged(group_id);
    }

    void setCompany(QString company)
    {
        if (m_company == company)
            return;

        m_company = company;
        emit companyChanged(company);
    }

    void setCountry_id(int country_id)
    {
        if (m_country_id == country_id)
            return;

        m_country_id = country_id;
        emit country_idChanged(country_id);
    }

    void setCity_id(int city_id)
    {
        if (m_city_id == city_id)
            return;

        m_city_id = city_id;
        emit city_idChanged(city_id);
    }

    void setCity_name(QString city_name)
    {
        if (m_city_name == city_name)
            return;

        m_city_name = city_name;
        emit city_nameChanged(city_name);
    }

    void setFrom(int from)
    {
        if (m_from == from)
            return;

        m_from = from;
        emit fromChanged(from);
    }

    void setUntil(int until)
    {
        if (m_until == until)
            return;

        m_until = until;
        emit untilChanged(until);
    }

    void setPosition(QString position)
    {
        if (m_position == position)
            return;

        m_position = position;
        emit positionChanged(position);
    }

signals:
    void group_idChanged(int group_id);

    void companyChanged(QString company);

    void country_idChanged(int country_id);

    void city_idChanged(int city_id);

    void city_nameChanged(QString city_name);

    void fromChanged(int from);

    void untilChanged(int until);

    void positionChanged(QString position);

private:
    int m_group_id;
    QString m_company;
    int m_country_id;
    int m_city_id;
    QString m_city_name;
    int m_from;
    int m_until;
    QString m_position;
};
#endif // CAREER_H
