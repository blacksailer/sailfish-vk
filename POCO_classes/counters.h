#ifndef COUNTERS_H
#define COUNTERS_H
#include <QObject>
class Counters : public QObject
{Q_OBJECT

    Q_PROPERTY(int albums READ albums WRITE setAlbums NOTIFY albumsChanged)
    Q_PROPERTY(int videos READ videos WRITE setVideos NOTIFY videosChanged)
    Q_PROPERTY(int audios READ audios WRITE setAudios NOTIFY audiosChanged)
    Q_PROPERTY(int photos READ photos WRITE setPhotos NOTIFY photosChanged)
    Q_PROPERTY(int docs READ docs WRITE setDocs NOTIFY docsChanged)
    Q_PROPERTY(int notes READ notes WRITE setNotes NOTIFY notesChanged)
    Q_PROPERTY(int friends READ friends WRITE setFriends NOTIFY friendsChanged)
    Q_PROPERTY(int groups READ groups WRITE setGroups NOTIFY groupsChanged)
    Q_PROPERTY(int online_friends READ online_friends WRITE setOnline_friends NOTIFY online_friendsChanged)
    Q_PROPERTY(int mutual_friends READ mutual_friends WRITE setMutual_friends NOTIFY mutual_friendsChanged)
    Q_PROPERTY(int user_videos READ user_videos WRITE setUser_videos NOTIFY user_videosChanged)
    Q_PROPERTY(int followers READ followers WRITE setFollowers NOTIFY followersChanged)
    Q_PROPERTY(int pages READ pages WRITE setPages NOTIFY pagesChanged)
    Q_PROPERTY(int topics READ topics WRITE setTopics NOTIFY topicsChanged)

public:
    int albums() const
    {
        return m_albums;
    }

    int videos() const
    {
        return m_videos;
    }

    int audios() const
    {
        return m_audios;
    }

    int photos() const
    {
        return m_photos;
    }

    int notes() const
    {
        return m_notes;
    }

    int friends() const
    {
        return m_friends;
    }

    int groups() const
    {
        return m_groups;
    }

    int online_friends() const
    {
        return m_online_friends;
    }

    int mutual_friends() const
    {
        return m_mutual_friends;
    }

    int user_videos() const
    {
        return m_user_videos;
    }

    int followers() const
    {
        return m_followers;
    }

    int pages() const
    {
        return m_pages;
    }

    int docs() const
    {
        return m_docs;
    }


    int topics() const
    {
        return m_topics;
    }

public slots:
    void setAlbums(int albums)
    {
        if (m_albums == albums)
            return;

        m_albums = albums;
        emit albumsChanged(albums);
    }

    void setVideos(int videos)
    {
        if (m_videos == videos)
            return;

        m_videos = videos;
        emit videosChanged(videos);
    }

    void setAudios(int audios)
    {
        if (m_audios == audios)
            return;

        m_audios = audios;
        emit audiosChanged(audios);
    }

    void setPhotos(int photos)
    {
        if (m_photos == photos)
            return;

        m_photos = photos;
        emit photosChanged(photos);
    }

    void setNotes(int notes)
    {
        if (m_notes == notes)
            return;

        m_notes = notes;
        emit notesChanged(notes);
    }

    void setFriends(int friends)
    {
        if (m_friends == friends)
            return;

        m_friends = friends;
        emit friendsChanged(friends);
    }

    void setGroups(int groups)
    {
        if (m_groups == groups)
            return;

        m_groups = groups;
        emit groupsChanged(groups);
    }

    void setOnline_friends(int online_friends)
    {
        if (m_online_friends == online_friends)
            return;

        m_online_friends = online_friends;
        emit online_friendsChanged(online_friends);
    }

    void setMutual_friends(int mutual_friends)
    {
        if (m_mutual_friends == mutual_friends)
            return;

        m_mutual_friends = mutual_friends;
        emit mutual_friendsChanged(mutual_friends);
    }

    void setUser_videos(int user_videos)
    {
        if (m_user_videos == user_videos)
            return;

        m_user_videos = user_videos;
        emit user_videosChanged(user_videos);
    }

    void setFollowers(int followers)
    {
        if (m_followers == followers)
            return;

        m_followers = followers;
        emit followersChanged(followers);
    }

    void setPages(int pages)
    {
        if (m_pages == pages)
            return;

        m_pages = pages;
        emit pagesChanged(pages);
    }

    void setDocs(int docs)
    {
        if (m_docs == docs)
            return;

        m_docs = docs;
        emit docsChanged(docs);
    }


    void setTopics(int topics)
    {
        if (m_topics == topics)
            return;

        m_topics = topics;
        emit topicsChanged(topics);
    }

signals:
    void albumsChanged(int albums);

    void videosChanged(int videos);

    void audiosChanged(int audios);

    void photosChanged(int photos);

    void notesChanged(int notes);

    void friendsChanged(int friends);

    void groupsChanged(int groups);

    void online_friendsChanged(int online_friends);

    void mutual_friendsChanged(int mutual_friends);

    void user_videosChanged(int user_videos);

    void followersChanged(int followers);

    void pagesChanged(int pages);

    void docsChanged(int docs);


    void topicsChanged(int topics);

private:
    int m_albums;
    int m_videos;
    int m_audios;
    int m_photos;
    int m_notes;
    int m_friends;
    int m_groups;
    int m_online_friends;
    int m_mutual_friends;
    int m_user_videos;
    int m_followers;
    int m_pages;
    int m_docs;
    int m_topics;
};
#endif // COUNTERS_H
