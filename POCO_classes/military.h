#ifndef MILITARY_H
#define MILITARY_H
#include <QObject>
class Military: public QObject{
Q_OBJECT
    Q_PROPERTY(QString unit READ unit WRITE setUnit NOTIFY unitChanged)
    Q_PROPERTY(int unit_id READ unit_id WRITE setUnit_id NOTIFY unit_idChanged)
    Q_PROPERTY(int country_id READ country_id WRITE setCountry_id NOTIFY country_idChanged)
    Q_PROPERTY(int from READ from WRITE setFrom NOTIFY fromChanged)
    Q_PROPERTY(int until READ until WRITE setUntil NOTIFY untilChanged)

public:
    QString unit() const
    {
        return m_unit;
    }

    int unit_id() const
    {
        return m_unit_id;
    }

    int country_id() const
    {
        return m_country_id;
    }

    int from() const
    {
        return m_from;
    }

    int until() const
    {
        return m_until;
    }

public slots:
    void setUnit(QString unit)
    {
        if (m_unit == unit)
            return;

        m_unit = unit;
        emit unitChanged(unit);
    }

    void setUnit_id(int unit_id)
    {
        if (m_unit_id == unit_id)
            return;

        m_unit_id = unit_id;
        emit unit_idChanged(unit_id);
    }

    void setCountry_id(int country_id)
    {
        if (m_country_id == country_id)
            return;

        m_country_id = country_id;
        emit country_idChanged(country_id);
    }

    void setFrom(int from)
    {
        if (m_from == from)
            return;

        m_from = from;
        emit fromChanged(from);
    }

    void setUntil(int until)
    {
        if (m_until == until)
            return;

        m_until = until;
        emit untilChanged(until);
    }

signals:
    void unitChanged(QString unit);

    void unit_idChanged(int unit_id);

    void country_idChanged(int country_id);

    void fromChanged(int from);

    void untilChanged(int until);

private:
    QString m_unit;
    int m_unit_id;
    int m_country_id;
    int m_from;
    int m_until;
};
#endif // MILITARY_H
