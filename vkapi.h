#ifndef VKAPI_H
#define VKAPI_H

#include <QObject>
#include <QGuiApplication>
#include <QTimer>
#include <QSettings>
#include <QNetworkAccessManager>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>

#include <QNetworkRequest>
#include <QNetworkReply>
#include <QEventLoop>
#include <QVariantMap>
#include <QUrlQuery>

#include <QStandardPaths>
#include <QImage>
#include <QtConcurrent/QtConcurrent>

#include "userpage.h"
#include "parser.h"
#include "databasesqlite.h"
#include "globalsettings.h"

#include "logger.h"
class vkapi : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool RefreshToken READ RefreshToken WRITE setRefreshToken NOTIFY refreshTokenChanged)
    Q_PROPERTY(bool hasInternetConnection READ hasInternetConnection WRITE setHasInternetConnection NOTIFY hasInternetConnectionChanged)
public:
    explicit vkapi(QObject *parent = 0);
    vkapi(Parser *model, GlobalSettings* Settings);
    ~vkapi();
    enum Method_Id{
        AUTHORIZE,
        AUTHORIZE_REDIRECT,
        AUTHORIZE_FINALE,
        CHECK_INTERNET,
        WALL_GET,
        WALL_GETBYID,
        WALL_GETCOMMENTS,
        WALL_CREATECOMMENT,
        WALL_POST,
        WALL_DELETE,
        USER_GET,
        NEWSFEED_GET,
        USER_GET_PERSONAL,
        FRIENDS_GET,
        FRIENDS_DELETE,
        FRIENDS_ADD,
        AUDIO_GET,
        AUDIO_SEARCH,
        VIDEO_GET,
        VIDEO_GETCOMMENTS,
        GROUP_GET,
        GROUP_GETBYID,
        IMAGE_GET,
        MESSAGES_GETDIALOGS,
        MESSAGES_SEARCHDIALOGS,
        MESSAGES_GETHISTORY,
        MESSAGES_GETBYID,
        MESSAGES_SEND,
        LIKES_ADD,
        LIKES_DELETE,
        LIKES_ISLIKED,
        LONGPOLL,
        GET_LONGSERVER,
        PHOTO_GETUPLOADSERVER,
        PHOTO_SAVEMESSAGESPHOTO,
        STAT_TRACKVISITOR,
        OTHER
    };

    bool RefreshToken() const;

    bool hasInternetConnection() const;

private:
    static  QString lang;
    static QString api_version;
    QString token_id;
    int expires_in;
    int user_id;
    QNetworkAccessManager *manager;
    QMap<QNetworkReply*,QFile*> FilesToSave;
    QQueue<QPair<QNetworkRequest,Method_Id>> RequestQueue;
    Logger Log;

    GlobalSettings* m_Settings;
    DatabaseSqlite* tryofdb;
    QTimer Timer_LongPoll;
    QTimer Timer_Queue;
     QTimer Timer_Internet;
    //Если добавляем новые записи
    bool appendNewsfeed;

    QHash<QNetworkReply*,int> Replies;

    void startLongPoll();
    bool m_RefreshToken=false;

    bool m_hasInternetConnection=false;

signals:
    void browserForAuthNeeded();

    void user_get_end(QJsonDocument answer);
    void user_get_personal(QJsonDocument answer);

    void friends_get_end(QJsonDocument answer);

    void wall_get_end(QJsonDocument answer,bool appended);
    void wall_getbyid_end(QJsonDocument answer);
    void wall_getComments_end(QJsonDocument answer);
    void wall_createComment_end(QJsonDocument answer);
    void newsfeed_get_end(QJsonDocument answer,bool appended);

    void audio_get_end(QJsonDocument answer);

    void video_get_end(QJsonDocument answer);
    void video_getcomments_end(QJsonDocument answer);

    void messages_getDialogs_end(QJsonDocument answer);
    void messages_searchDialogs_end(QJsonDocument answer);
    void messages_getHistory_end(QJsonDocument answer);
    void messages_getById_end(QJsonDocument answer);

    //-- SIGNALS - group --
    void group_get_end(QJsonDocument answer);
    void group_getbyid_end(QJsonDocument answer);

    //--- SIGNAL Likes ---
    void likes_add_end(QJsonDocument doc);
    void likes_delete_end(QJsonDocument doc);
    void likes_isLikes_end(QJsonDocument doc);

    void refreshTokenChanged(bool arg);

    //Long_Poll Server
    void longpoll_get_end(QJsonDocument answer);
    void longpollServer_get_end(QJsonDocument answer);

    //--- PHOTOS ---
    void photo_savemessagesphoto_end(QJsonDocument answer);

    void RequestQueueSizeIncreased();

    void newInternetConnection(bool value);
    void hasInternetConnectionChanged(bool hasInternetConnection);

public slots:
    void authorize();
    void onAuthorize(QNetworkReply *reply);
    void onAuthorizeRedirect(QNetworkReply* reply);
    void onReplyFinished(QNetworkReply*reply);

    //----   NEWSFEED   ----
    void newsfeed_get(int count, int start_from=0);
    void onNewsfeed_get(QNetworkReply *reply);

    //---- FRIENDS ----
    void friends_get(int u_id,int count,int offset =0,QString name_case="nom");
    void friends_search(int u_id,int count,int offset =0,QString name_case="nom",QString q="");
    void onFriends_get(QNetworkReply *reply);

    void friends_add(int u_id, QString text, int follow=0);
    void onFriends_add(QNetworkReply *reply);

    void friends_delete(int u_id);
    void onFriends_delete(QNetworkReply *reply);
    //----   WALL   ----
    void wall_get(QString U_id, int count, int offset);
    void onWall_get(QNetworkReply *reply);
    void wall_getComments(QString wall_id, QString post_id, int start_comment_id=0);
    void onWall_getComments(QNetworkReply* reply);

    void wall_post(int owner_id,bool friends_only,bool from_group,QString message,QString attachments,
                   QString services,bool is_signed,bool mark_as_ads,int publish_date,double lat,double _long,
                   int place_id,int post_id);
    void onWall_post(QNetworkReply *reply);
    void wall_delete(int owner_id,int post_id);
    void wall_getbyid(QString W_id);
    void onWall_getbyid(QNetworkReply *reply);
    void wall_createComment(int owner_id,int post_id,QString message,QString attachments,int reply_id=-1);
    void onWall_createComment(QNetworkReply* reply);
    void wall_deleteComment(int owner_id,int comment_id);

    //----  PHOTO   ----
    void photo_getUploadServer();
    //    void photo_saveMessagesPhoto(QString server, QString photo, QString hash);
    //    void onPhoto_saveMessagesPhoto(QNetworkReply *reply);


    //----   USER   ----
    void user_get(QString User_id,QString filter,QString Name_nom);
    void onUser_Get(QNetworkReply* reply);

    //----   AUDIO   ----
    void audio_get(QString Owner_id,int count);
    void onAudio_get(QNetworkReply *reply);

    //----  VIDEO   ----
    void video_get(QString VideoUrl);
    void onVideo_get(QNetworkReply *reply);
    void video_GetComments(QString owner_id, QString video_id, int start_comment_id=0);
    void onVideo_GetComments(QNetworkReply *reply);
    void audio_Search(QString q,bool performer_only,int count);

    //----   GROUPS   ----
    void group_get(int user_id,QString filter,QString fields,int offset,int count);
    void onGroup_get(QNetworkReply *reply);

    void group_getbyid(int group_id,QString filter);
    void onGroup_getbyid(QNetworkReply *reply);

    void group_leave(int group_id);
    void group_join(int group_id,bool not_sure=false);
    //----   MESSAGES   ----
    void messages_getDialogs(int offset = 0,int count =20,int preview_length = 0,int unread=1);
    void onMessages_getDialogs(QNetworkReply *reply);
    void messages_searchDialogs(QString q,int limit,QString fields);
    void onMessages_searchDialogs(QNetworkReply *reply);

    void messages_getHistory(int chat_id=0,int offset=0, int count=20,int last_message_id=0);
    void onMessages_getHistory(QNetworkReply *reply);

    void messages_getById(QStringList message_ids);
    void onMessages_getById(QNetworkReply *reply);

    void messages_send(int chat_id, QString message, QString attachment="");
    //    void onMessages_send(QNetworkReply *reply);

    //----   LIKES   ----
    void likes_add(QString type,int owner_id,int item_id);
    void onLikes_add(QNetworkReply* reply);

    void likes_delete(QString type,int owner_id,int item_id);
    void onLikes_delete(QNetworkReply* reply);

    void likes_isLiked(int user_id, QString type, int owner_id, int item_id);
    void onLikes_isLiked(QNetworkReply* reply);

    //----- Служебное ------
    void onErrorOccurred(int ErrorCode);
    void stat_trackVisitor();

    void setToken(QString url);
    void initiateLongPoll();
    void onInitiateLongPoll(QNetworkReply *reply);

    void LongPollServer_get();
    void onLongPollServer_get(QNetworkReply *reply);

    void LongPollServerHistory_get();

    void startLongPollTimer();


    //Не должен быть тут
    QUrl img_save(QString src, int type);
    void onSaveImageFinished(QNetworkReply* reply);

    bool checkToken();
    void setTokenFromDatabase();
    void setRefreshToken(bool arg);


    void setHasInternetConnection(bool hasInternetConnection);

private slots:
    void queueRequest(QNetworkRequest req,Method_Id Method);
    void startQueueRequest();
    void checkInternet();
    void checkInternetConcurrently(vkapi* parent);
};

#endif // VKAPI_H
