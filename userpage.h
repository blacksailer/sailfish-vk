#ifndef USERPAGE_H
#define USERPAGE_H
#include <QObject>
#include <QtCore>
#include <QQmlListProperty>
#include "parser.h"
#include "POCO_classes/career.h"
#include "POCO_classes/city.h"
#include "POCO_classes/university.h"
#include "POCO_classes/school.h"
#include "POCO_classes/relative.h"
#include "POCO_classes/personal.h"
#include "POCO_classes/occupation.h"
#include "POCO_classes/usercontacts.h"
#include "POCO_classes/counters.h"
#include "POCO_classes/country.h"
#include "POCO_classes/education.h"
#include "POCO_classes/lastseen.h"
#include "POCO_classes/military.h"


class UserPage : public QObject
{
    Q_OBJECT



    Q_PROPERTY(QString about READ about WRITE setAbout NOTIFY aboutChanged)/*string	содержимое поля «О себе» из профиля пользователя.*/
    Q_PROPERTY(QString activities READ activities WRITE setActivities NOTIFY activitiesChanged)/*string	содержимое поля «Деятельность» из профиля пользователя.*/
    Q_PROPERTY(QString bdate READ bdate WRITE setBdate NOTIFY bdateChanged)/*string	дата рождения. Возвращается в формате DD.MM.YYYY или DD.MM (если год рождения скрыт). Если дата рождения скрыта целиком, поле отсутствует в ответе.*/
    Q_PROPERTY(bool blacklisted READ blacklisted WRITE setBlacklisted NOTIFY blacklistedChanged)
/*
integer, [0,1]	информация о том, находится ли текущий пользователь в черном списке. Возможные значения:

    1 — находится;
    0 — не находится.
*/
    Q_PROPERTY(bool blacklisted_by_me READ blacklisted_by_me WRITE setBlacklisted_by_me NOTIFY blacklisted_by_meChanged)
    /*
integer, [0,1]	информация о том, находится ли пользователь в черном списке у текущего пользователя. Возможные значения:

    1 — находится;
    0 — не находится.

*/
    Q_PROPERTY(QString books READ books WRITE setBooks NOTIFY booksChanged)
    /*
string	содержимое поля «Любимые книги» из профиля пользователя.
*/
    Q_PROPERTY(bool can_post READ can_post WRITE setCan_post NOTIFY can_postChanged)
    /*
integer, [0,1]	информация о том, может ли текущий пользователь оставлять записи на стене. Возможные значения:

    1 — может;
    0 — не может.
*/
    Q_PROPERTY(bool can_see_all_posts READ can_see_all_posts WRITE setCan_see_all_posts NOTIFY can_see_all_postsChanged)
    /*
integer, [0,1]	информация о том, может ли текущий пользователь видеть чужие записи на стене. Возможные значения:

    1 — может;
    0 — не может.
*/
    Q_PROPERTY(bool can_see_audio READ can_see_audio WRITE setCan_see_audio NOTIFY can_see_audioChanged)
    /*
integer, [0,1]	информация о том, может ли текущий пользователь видеть аудиозаписи. Возможные значения:

    1 — может;
    0 — не может.

*/
    Q_PROPERTY(bool can_send_friend_request READ can_send_friend_request WRITE setCan_send_friend_request NOTIFY can_send_friend_requestChanged)
    /*
integer, [0,1]	информация о том, будет ли отправлено уведомление пользователю о заявке в друзья от текущего пользователя. Возможные значения:

    1 — уведомление будет отправлено;
    0 — уведомление не будет отправлено.

*/
    Q_PROPERTY(bool can_write_private_message READ can_write_private_message WRITE setCan_write_private_message NOTIFY can_write_private_messageChanged)
    /*
integer, [0,1]	информация о том, может ли текущий пользователь отправить личное сообщение. Возможные значения:

    1 — может;
    0 — не может.

*/
    Q_PROPERTY(int common_count READ common_count WRITE setCommon_count NOTIFY common_countChanged)
    /*
integer	количество общих друзей с текущим пользователем.
*/
    Q_PROPERTY(QString domain READ domain WRITE setDomain NOTIFY domainChanged)
    /*
string	короткий адрес страницы. Возвращается строка, содержащая короткий адрес страницы (например, andrew). Если он не назначен, возвращается "id"+user_id, например, id35828305.
*/
    Q_PROPERTY(QString maiden_name READ maiden_name WRITE setMaiden_name NOTIFY maiden_nameChanged)
    /*
string	девичья фамилия.
*/
    Q_PROPERTY(QString screen_name READ screen_name WRITE setScreen_name NOTIFY screen_nameChanged)
    /*
string	короткое имя страницы пользователя.
*/
    Q_PROPERTY(QString sex READ sex WRITE setSex NOTIFY sexChanged)
    /*
integer, [0,1,2]	пол пользователя. Возможные значения:

    1 — женский;
    2 — мужской;
    0 — пол не указан.

photo_50*/

    Q_PROPERTY(QString photo_50 READ photo_50 WRITE setPhoto_50 NOTIFY photo_50Changed)
    /*
string	url квадратной фотографии пользователя, имеющей ширину 50 пикселей. В случае отсутствия у пользователя фотографии возвращается http://vk.com/images/camera_c.gif.
photo_100*/
    Q_PROPERTY(QString photo_100 READ photo_100 WRITE setPhoto_100 NOTIFY photo_100Changed)
    /*
string	url квадратной фотографии пользователя, имеющей ширину 100 пикселей. В случае отсутствия у пользователя фотографии возвращается http://vk.com/images/camera_b.gif.
photo_200_orig*/
    Q_PROPERTY(QString photo_200_orig READ photo_200_orig WRITE setPhoto_200_orig NOTIFY photo_200_origChanged)
    /*
string	url фотографии пользователя, имеющей ширину 200 пикселей. В случае отсутствия у пользователя фотографии возвращается http://vk.com/images/camera_a.gif.
photo_200*/
    Q_PROPERTY(QString photo_200 READ photo_200 WRITE setPhoto_200 NOTIFY photo_200Changed)
    /*
string	url квадратной фотографии пользователя, имеющей ширину 200 пикселей. Если у пользователя отсутствует фотография таких размеров, в ответе вернется https://vk.com/images/camera_200.png
photo_400_orig*/
    Q_PROPERTY(QString photo_400_orig READ photo_400_orig WRITE setPhoto_400_orig NOTIFY photo_400_origChanged)
    /*
string	url фотографии пользователя, имеющей ширину 400 пикселей. Если у пользователя отсутствует фотография такого размера, в ответе вернется https://vk.com/images/camera_400.png.
photo_id*/
    Q_PROPERTY(QString photo_id READ photo_id WRITE setPhoto_id NOTIFY photo_idChanged)
    /*
string	строковый идентификатор главной фотографии профиля пользователя в формате {user_id}_{photo_id}, например, 6492_192164258. Обратите внимание, это поле может отсутствовать в ответе.
photo_max*/
    Q_PROPERTY(QString photo_max READ photo_max WRITE setPhoto_max NOTIFY photo_maxChanged)
    /*
string	url квадратной фотографии пользователя с максимальной шириной. Может быть возвращена фотография, имеющая ширину как 200, так и 100 пикселей. В случае отсутствия у пользователя фотографии возвращается http://vk.com/images/camera_b.gif.
photo_max_orig*/
    Q_PROPERTY(QString photo_max_orig READ photo_max_orig WRITE setPhoto_max_orig NOTIFY photo_max_origChanged)
  /*
string	url фотографии пользователя максимального размера. Может быть возвращена фотография, имеющая ширину как 400, так и 200 пикселей. В случае отсутствия у пользователя фотографии возвращается http://vk.com/images/camera_a.gif.
quotes*/
    Q_PROPERTY(QString quotes READ quotes WRITE setQuotes NOTIFY quotesChanged)
    /*
string	любимые цитаты.

site*/
    Q_PROPERTY(QString site READ site WRITE setSite NOTIFY siteChanged)
    /*
string	адрес сайта, указанный в профиле сайт пользователя.

timezone*/
    Q_PROPERTY(int timezone READ timezone WRITE setTimezone NOTIFY timezoneChanged)
    /*
integer	временная зона пользователя. Возвращается только при запросе информации о текущем пользователе.
tv*/
    Q_PROPERTY(QString tv READ tv WRITE setTv NOTIFY tvChanged)
    /*
string	любимые телешоу.
verified*/
    Q_PROPERTY(bool verified READ verified WRITE setVerified NOTIFY verifiedChanged)
    /*
integer, [0,1]	возвращается 1, если страница пользователя верифицирована, 0 — если не верифицирована.
wall_comments*/
    Q_PROPERTY(bool wall_comments READ wall_comments WRITE setWall_comments NOTIFY wall_commentsChanged)
    /*
integer, [0,1]	информация о том, включены ли комментарии на стене. Возможные значения:

    1 — включены;
    0 — отключены.
movies*/
    Q_PROPERTY(QString movies READ movies WRITE setMovies NOTIFY moviesChanged)
    /*
string	содержимое поля «Любимые фильмы» из профиля пользователя.
music*/
    Q_PROPERTY(QString music READ music WRITE setMusic NOTIFY musicChanged)
    /*
string	содержимое поля «Любимая музыка» из профиля пользователя.
nickname*/
    Q_PROPERTY(QString nickname READ nickname WRITE setNickname NOTIFY nicknameChanged)
    /*
string	никнейм (отчество) пользователя.
строка
*/
    Q_PROPERTY(bool online READ online WRITE setOnline NOTIFY onlineChanged)
    /*
online
integer, [0,1]	информация о том, находится ли пользователь сейчас на сайте. Если пользователь использует мобильное приложение либо мобильную версию сайта, возвращается дополнительное поле online_mobile, содержащее 1. При этом, если используется именно приложение, дополнительно возвращается поле online_app, содержащее его идентификатор.

relation*/
    Q_PROPERTY(QString relation READ relation WRITE setRelation NOTIFY relationChanged)
    /*
integer	семейное положение пользователя. Возможные значения:

    1 — не женат/не замужем;
    2 — есть друг/есть подруга;
    3 — помолвлен/помолвлена;
    4 — женат/замужем;
    5 — всё сложно;
    6 — в активном поиске;
    7 — влюблён/влюблена;
    8 — в гражданском браке;
    0 — не указано.

    Если в семейном положении указан другой пользователь, дополнительно возвращается объект relation_partner, содержащий id и имя этого человека.



exports	внешние сервисы, в которые настроен экспорт из ВК (twitter, facebook, livejournal, instagram).

connections	возвращает данные об указанных в профиле сервисах пользователя, таких как: skype, facebook, twitter, livejournal, instagram.

*/
    Q_PROPERTY(QString twitter READ twitter WRITE setTwitter NOTIFY twitterChanged)
    Q_PROPERTY(QString facebook READ facebook WRITE setFacebook NOTIFY facebookChanged)
    Q_PROPERTY(QString livejournal READ livejournal WRITE setLivejournal NOTIFY livejournalChanged)
    Q_PROPERTY(QString instagram READ instagram WRITE setInstagram NOTIFY instagramChanged)
    /*
first_name_{case}*/
    Q_PROPERTY(QString first_name_nom READ first_name_nom WRITE setFirst_name_nom NOTIFY first_name_nomChanged)
    Q_PROPERTY(QString first_name_gen READ first_name_gen WRITE setFirst_name_gen NOTIFY first_name_genChanged)
    Q_PROPERTY(QString first_name_dat READ first_name_dat WRITE setFirst_name_dat NOTIFY first_name_datChanged)
    Q_PROPERTY(QString first_name_acc READ first_name_acc WRITE setFirst_name_acc NOTIFY first_name_accChanged)
    Q_PROPERTY(QString first_name_ins READ first_name_ins WRITE setFirst_name_ins NOTIFY first_name_insChanged)
    Q_PROPERTY(QString first_name_abl READ first_name_abl WRITE setFirst_name_abl NOTIFY first_name_ablChanged)
    /*
string	имя в заданном падеже. Возможные значения для {case}:

    nom — именительный;
    gen — родительный;
    dat — дательный;
    acc — винительный;
    ins — творительный;
    abl — предложный.

В запросе можно передать несколько значений
followers_count */
    Q_PROPERTY(int followers_count READ followers_count WRITE setFollowers_count NOTIFY followers_countChanged)
/*
integer	количество подписчиков пользователя.
friend_status*/
    Q_PROPERTY(QString friend_status READ friend_status WRITE setFriend_status NOTIFY friend_statusChanged)
    /*
integer	статус дружбы с пользователем. Возможные значения:

    0 — не является другом,
    1 — отправлена заявка/подписка пользователю,
    2 — имеется входящая заявка/подписка от пользователя,
    3 — является другом.

games*/
    Q_PROPERTY(QString games READ games WRITE setGames NOTIFY gamesChanged)
    /*
string	содержимое поля «Любимые игры» из профиля пользователя.

has_mobile*/
    Q_PROPERTY(bool has_mobile READ has_mobile WRITE setHas_mobile NOTIFY has_mobileChanged)
    /*информация о том, известен ли номер мобильного телефона пользователя. Возвращаемые значения: 1 — известен, 0 — не известен.
has_photo*/
    Q_PROPERTY(bool has_photo READ has_photo WRITE setHas_photo NOTIFY has_photoChanged)
    /*
integer, [0,1]	1, если пользователь установил фотографию для профиля.
home_town*/
    Q_PROPERTY(QString home_town READ home_town WRITE setHome_town NOTIFY home_townChanged)
    /*
string	название родного города пользователя.
interests*/
    Q_PROPERTY(QString interests READ interests WRITE setInterests NOTIFY interestsChanged)
    /*
string	содержимое поля «Интересы» из профиля.
is_favorite*/
    Q_PROPERTY(bool is_favorite READ is_favorite WRITE setIs_favorite NOTIFY is_favoriteChanged)
    /*
integer, [0,1]	информация о том, есть ли пользователь в закладках у текущего пользователя. Возможные значения:

    1 — есть;
    0 — нет.

is_friend*/
    Q_PROPERTY(bool is_friend READ is_friend WRITE setIs_friend NOTIFY is_friendChanged)
    /*
integer	информация о том, является ли пользователь другом текущего пользователя. Возможные значения:

    1 — пользователь есть в друзьях;
    0 — пользователя нет в друзьях.

is_hidden_from_feed*/
    Q_PROPERTY(bool is_hidden_from_feed READ is_hidden_from_feed WRITE setIs_hidden_from_feed NOTIFY is_hidden_from_feedChanged)
    /*
integer, [0,1]	информация о том, скрыт ли пользователь из ленты новостей текущего пользователя. Возможные значения:

    1 — скрыт;
    0 — не скрыт.

last_name_{case}*/

    Q_PROPERTY(QString last_name_nom READ last_name_nom WRITE setLast_name_nom NOTIFY last_name_nomChanged)
    Q_PROPERTY(QString last_name_gen READ last_name_gen WRITE setLast_name_gen NOTIFY last_name_genChanged)
    Q_PROPERTY(QString last_name_dat READ last_name_dat WRITE setLast_name_dat NOTIFY last_name_datChanged)
    Q_PROPERTY(QString last_name_acc READ last_name_acc WRITE setLast_name_acc NOTIFY last_name_accChanged)
    Q_PROPERTY(QString last_name_ins READ last_name_ins WRITE setLast_name_ins NOTIFY last_name_insChanged)
    Q_PROPERTY(QString last_name_abl READ last_name_abl WRITE setLast_name_abl NOTIFY last_name_ablChanged)
    /*
string	фамилия в заданном падеже. Возможные значения для {case}:

    nom — именительный;
    gen — родительный;
    dat — дательный;
    acc — винительный;
    ins — творительный;
    abl — предложный.

lists*/
    Q_PROPERTY(QString lists READ lists WRITE setLists NOTIFY listsChanged)
    /*
string	разделенные запятой идентификаторы списков друзей, в которых состоит пользователь. Поле доступно только для метода friends.get.


occupation*/


    Q_PROPERTY(Occupation* occupation READ occupation WRITE setOccupation NOTIFY occupationChanged)/*
object	информация о текущем роде занятия пользователя. Объект, содержащий следующие поля:

    type (string) — тип. Возможные значения:
        work — работа;
        school — среднее образование;
        university — высшее образование.
    id (integer) — идентификатор школы, вуза, сообщества компании (в которой пользователь работает);
    name (string) — название школы, вуза или места работы;

military*/

    Q_PROPERTY(Military* military READ military WRITE setMilitary NOTIFY militaryChanged)
    /*
object	информация о военной службе пользователя. Объект, содержащий следующие поля:

    unit (string) — номер части;
    unit_id (integer) — идентификатор части в базе данных;
    country_id (integer) — идентификатор страны, в которой находится часть;
    from (integer) — год начала службы;
    until (integer) — год окончания службы.

last_seen*/

    Q_PROPERTY(LastSeen* last_seen READ last_seen WRITE setLast_seen NOTIFY last_seenChanged)
    /*
object	время последнего посещения. Объект, содержащий следующие поля:

    time (integer) — время последнего посещения в формате Unixtime.
    platform (integer) — тип платформы, через которую был осуществлён последний вход. Возможные значения:
        1 — мобильная версия;
        2 — приложение для iPhone;
        3 — приложение для iPad;
        4 — приложение для Android;
        5 — приложение для Windows Phone;
        6 — приложение для Windows 8;
        7 — полная версия сайта.

career*/

    Q_PROPERTY(Career* career READ career WRITE setCareer NOTIFY careerChanged)

    /*
object	информация о карьере пользователя. Объект, содержащий следующие поля:

    group_id (integer) — идентификатор сообщества (если доступно, иначе company);
    company (string) — название компании (если доступно, иначе group_id);
    country_id (integer) — идентификатор страны;
    city_id (integer) — идентификатор города (если доступно, иначе city_name);
    city_name (string) — название города (если доступно, иначе city_id);
    from (integer) — год начала работы;
    until (integer) — год окончания работы;
    position (string) — должность.

city*/
    Q_PROPERTY(City* city READ city WRITE setCity NOTIFY cityChanged)
    /*
object	информация о городе, указанном на странице пользователя в разделе «Контакты». Возвращаются следующие поля:

    id (integer) — идентификатор города, который можно использовать для получения его названия с помощью метода database.getCitiesById;
    title (string) — название города.


contacts*/

    Q_PROPERTY(UserContacts* contacts READ contacts)
    /*
object	информация о телефонных номерах пользователя. Если данные указаны и не скрыты настройками приватности, возвращаются следующие поля:

    mobile_phone (string) — номер мобильного телефона пользователя (только для Standalone-приложений);
    home_phone (string) — дополнительный номер телефона пользователя.

counters*/

    Q_PROPERTY(Counters* counters READ counters WRITE setCounters NOTIFY countersChanged)

/*
object	количество различных объектов у пользователя. Поле возвращается только в методе users.get при запросе информации об одном пользователе, с передачей access_token.
Объект, содержащий следующие поля:

    albums (integer) — количество фотоальбомов;
    videos (integer) — количество видеозаписей;
    audios (integer) — количество аудиозаписей;
    photos (integer) — количество фотографий;
    notes (integer) — количество заметок;
    friends (integer) — количество друзей;
    groups (integer) — количество сообществ;
    online_friends (integer) — количество друзей онлайн;
    mutual_friends (integer) — количество общих друзей;
    user_videos (integer) — количество видеозаписей с пользователем;
    followers (integer) — количество подписчиков;
    pages (integer) — количество объектов в блоке «Интересные страницы».

country*/

    Q_PROPERTY(Country* country READ country WRITE setCountry NOTIFY countryChanged)
    /*
object	информация о стране, указанной на странице пользователя в разделе «Контакты». Возвращаются следующие поля:

    id (integer) — идентификатор страны, который можно использовать для получения ее названия с помощью метода database.getCountriesById;
    title (string) — название страны.

education*/

    Q_PROPERTY(Education* education READ education )
    /*информация о высшем учебном заведении пользователя. Возвращаются поля:


    university (integer) — идентификатор университета;
    university_name (string) — название университета;
    faculty (integer) — идентификатор факультета;
    faculty_name (string)— название факультета;
    graduation (integer) — год окончания.


personal*/

    Q_PROPERTY(Personal* personal READ personal WRITE setPersonal NOTIFY personalChanged)
    /*
object	информация о полях из раздела «Жизненная позиция».

    political (integer) — политические предпочтения. Возможные значения:
        1 — коммунистические;
        2 — социалистические;
        3 — умеренные;
        4 — либеральные;
        5 — консервативные;
        6 — монархические;
        7 — ультраконсервативные;
        8 — индифферентные;
        9 — либертарианские.
    langs (array) — языки.
    religion (string) — мировоззрение.
    inspired_by (string) — источники вдохновения.
    people_main (integer) — главное в людях. Возможные значения:
        1 — ум и креативность;
        2 — доброта и честность;
        3 — красота и здоровье;
        4 — власть и богатство;
        5 — смелость и упорство;
        6 — юмор и жизнелюбие.
    life_main (integer) — главное в жизни. Возможные значения:
        1 — семья и дети;
        2 — карьера и деньги;
        3 — развлечения и отдых;
        4 — наука и исследования;
        5 — совершенствование мира;
        6 — саморазвитие;
        7 — красота и искусство;
        8 — слава и влияние;
    smoking (integer) — отношение к курению. Возможные значения:
        1 — резко негативное;
        2 — негативное;
        3 — компромиссное;
        4 — нейтральное;
        5 — положительное.
    alcohol (integer) — отношение к алкоголю. Возможные значения:
        1 — резко негативное;
        2 — негативное;
        3 — компромиссное;
        4 — нейтральное;
        5 — положительное.

relatives*/
    Q_PROPERTY(QQmlListProperty<Relative> relatives READ relatives NOTIFY relativesChanged)
    QQmlListProperty<Relative> relatives() const;
    static Relative *relativeAt(QQmlListProperty<Relative> *list, int index);
    static int countRelative(QQmlListProperty<Relative> *list);

    QList<Relative*> m_relatives;

    /*
array	список родственников текущего пользователя. Массив объектов, каждый из которых содержит поля:

    id (integer) — идентификатор пользователя;
    name (string) — имя родственника (в том случае, если родственник не является пользователем ВКонтакте, в этом случае id не возвращается);
    type (string) — тип родственной связи. Возможные значения:
        child — сын/дочь;
        sibling — брат/сестра;
        parent — отец/мать;
        grandparent — дедушка/бабушка;
        grandchild — внук/внучка.

schools*/
    Q_PROPERTY(QVariantList schools READ schools WRITE setSchools NOTIFY schoolsChanged)
  //  QQmlListProperty<School> schools() const;
//    static void append_school(QQmlListProperty<School> *list, School *schl);
//    static School *schoolAt(QQmlListProperty<School> *prop, int index);
//    static int count_school(QQmlListProperty<School> *lis);

//    QList<School*> m_schools;
    /*
     *
array	список школ, в которых учился пользователь. Массив объектов, описывающих школы. Каждый объект содержит следующие поля:

    id (string) — идентификатор школы;
    country (integer) — идентификатор страны, в которой расположена школа;
    city (integer) — идентификатор города, в котором расположена школа;
    name (string) — наименование школы
    year_from (integer) — год начала обучения;
    year_to (integer) — год окончания обучения;
    year_graduated (integer) — год выпуска;
    class (string) — буква класса;
    speciality (string) — специализация;
    type (integer) — идентификатор типа;
    type_str (string) — название типа. Возможные значения для пар type-typeStr:
        0 — "школа";
        1 — "гимназия";
        2 —"лицей";
        3 — "школа-интернат";
        4 — "школа вечерняя";
        5 — "школа музыкальная";
        6 — "школа спортивная";
        7 — "школа художественная";
        8 — "колледж";
        9 — "профессиональный лицей";
        10 — "техникум";
        11 — "ПТУ";
        12 — "училище";
        13 — "школа искусств".

universities*/

    Q_PROPERTY(QQmlListProperty<University> universities READ universities NOTIFY universitiesChanged)
    QQmlListProperty<University> universities() const;
    static University *universityAt(QQmlListProperty<University> *list, int index);
    static int countUniversity(QQmlListProperty<University> *list);

    QList<University*> m_universities;


    /*
array	список вузов, в которых учился пользователь. Массив объектов, описывающих университеты. Каждый объект содержит следующие поля:

    id (integer)— идентификатор университета;
    country (integer) — идентификатор страны, в которой расположен университет;
    city (integer) — идентификатор города, в котором расположен университет;
    name (string) — наименование университета;
    faculty (integer) — идентификатор факультета;
    faculty_name (string) — наименование факультета;
    chair (integer) — идентификатор кафедры;
    chair_name (string) — наименование кафедры;
    graduation (integer) — год окончания обучения;
    education_form (string) — форма обучения;
    education_status (string) — статус (например, «Выпускник (специалист)»).



     *
     *
     */


    Q_PROPERTY(QString mobile_phone READ mobile_phone WRITE setMobile_phone NOTIFY mobile_phoneChanged)
    Q_PROPERTY(QString home_phone READ home_phone WRITE setHome_phone NOTIFY home_phoneChanged)

private:
    QString m_Avatar_path;
    QString m_Nameplate;
    QString m_Age;
    QString m_Status;
    bool m_isOnline;

    Parser* m_DataProvider;

    bool m_isActive;

    bool m_is_Friend;

    QString m_about="";

    QString m_bdate="";

QString m_activities="";

bool m_blacklisted;

bool m_blacklisted_by_me;

QString m_books="";

bool m_can_post;

bool m_can_see_all_posts;

bool m_can_see_audio;

bool m_can_send_friend_request;

bool m_can_write_private_message;

int m_common_count;

QString m_domain="";

QString m_maiden_name="";

QString m_screen_name="";

QString m_sex="";
int m_sex_int=1;

QString m_photo_50="";

QString m_photo_100="";

QString m_photo_200_orig="";

QString m_photo_200="";

QString m_photo_400_orig="";

QString m_photo_id="";

QString m_photo_max="";


QString m_quotes="";

QString m_site="";

int m_timezone;

QString m_tv="";

bool m_verified;

bool m_wall_comments;

QString m_movies="";

QString m_music="";

QString m_nickname="";

bool m_online;

QString m_relation="";

QString m_twitter="";

QString m_facebook="";

QString m_livejournal="";

QString m_instagram="";

QString m_first_name_nom="";

QString m_first_name_gen="";

QString m_first_name_dat="";

QString m_first_name_acc="";

QString m_first_name_ins="";

QString m_first_name_abl="";

int m_followers_count;

bool m_has_mobile;

bool m_has_photo;

QString m_home_town="";

QString m_interests="";

bool m_is_favorite;

bool m_is_friend;

bool m_is_hidden_from_feed;

QString m_last_name_nom="";

QString m_last_name_gen="";

QString m_last_name_dat="";

QString m_last_name_acc="";

QString m_last_name_ins="";

QString m_last_name_abl="";

QString m_friend_status="";

QString m_photo_max_orig="";

QString m_games="";

QString m_lists="";

Personal* m_personal=nullptr;

Education* m_education=nullptr;

Country* m_country=nullptr;

Counters* m_counters=nullptr;

UserContacts* m_contacts=nullptr;

City* m_city=nullptr;

Career* m_career=nullptr;

LastSeen* m_last_seen=nullptr;

Military* m_military=nullptr;

Occupation* m_occupation=nullptr;



QVariantList m_schools;

QString m_mobile_phone="";

QString m_home_phone="";

public:
    explicit UserPage(QObject *parent = 0);

    Q_PROPERTY(QString Avatar_path READ Avatar_path WRITE setAvatar_path NOTIFY Avatar_pathChanged)
    Q_PROPERTY(QString Nameplate READ Nameplate WRITE setNameplate NOTIFY NameplateChanged)
    Q_PROPERTY(QString Age READ Age WRITE setAge NOTIFY AgeChanged)
    Q_PROPERTY(QString Status READ Status WRITE setStatus NOTIFY StatusChanged)
    Q_PROPERTY(bool is_Friend READ is_Friend WRITE setIs_Friend NOTIFY is_FriendChanged)
    Q_PROPERTY(bool isOnline READ isOnline WRITE setIsOnline NOTIFY isOnlineChanged)

    //qml Привязка
    Q_PROPERTY(Parser* DataProvider READ DataProvider WRITE setDataProvider NOTIFY dataProviderChanged)
    Q_PROPERTY(bool isActive READ isActive WRITE setActive NOTIFY isActiveChanged)

    QString Avatar_path() const
    {
        return m_Avatar_path;
    }

    QString Nameplate() const
    {
        return m_Nameplate;
    }

    QString Age() const
    {
        return m_Age;
    }

    QString Status() const
    {
        return m_Status;
    }

    bool isOnline() const
    {
        return m_isOnline;
    }

    Parser* DataProvider() const
    {
        return m_DataProvider;
    }

    bool isActive() const
    {
        return m_isActive;
    }

    bool is_Friend() const
    {
        return m_is_Friend;
    }

    QString about() const
    {
        return m_about;
    }

    QString bdate() const
    {
        return m_bdate;
    }

    QString activities() const
    {
        return m_activities;
    }

    bool blacklisted() const
    {
        return m_blacklisted;
    }

    bool blacklisted_by_me() const
    {
        return m_blacklisted_by_me;
    }

    QString books() const
    {
        return m_books;
    }

    bool can_post() const
    {
        return m_can_post;
    }

    bool can_see_all_posts() const
    {
        return m_can_see_all_posts;
    }

    bool can_see_audio() const
    {
        return m_can_see_audio;
    }

    bool can_send_friend_request() const
    {
        return m_can_send_friend_request;
    }

    bool can_write_private_message() const
    {
        return m_can_write_private_message;
    }

    int common_count() const
    {
        return m_common_count;
    }

    QString domain() const
    {
        return m_domain;
    }

    QString maiden_name() const
    {
        return m_maiden_name;
    }

    QString screen_name() const
    {
        return m_screen_name;
    }

    QString sex() const
    {
        return m_sex;
    }

    QString photo_50() const
    {
        return m_photo_50;
    }

    QString photo_100() const
    {
        return m_photo_100;
    }

    QString photo_200_orig() const
    {
        return m_photo_200_orig;
    }

    QString photo_200() const
    {
        return m_photo_200;
    }

    QString photo_400_orig() const
    {
        return m_photo_400_orig;
    }

    QString photo_id() const
    {
        return m_photo_id;
    }

    QString photo_max() const
    {
        return m_photo_max;
    }

    QString quotes() const
    {
        return m_quotes;
    }

    QString site() const
    {
        return m_site;
    }

    int timezone() const
    {
        return m_timezone;
    }

    QString tv() const
    {
        return m_tv;
    }

    bool verified() const
    {
        return m_verified;
    }

    bool wall_comments() const
    {
        return m_wall_comments;
    }

    QString movies() const
    {
        return m_movies;
    }

    QString music() const
    {
        return m_music;
    }

    QString nickname() const
    {
        return m_nickname;
    }

    bool online() const
    {
        return m_online;
    }

    QString relation() const
    {
        return m_relation;
    }

    QString twitter() const
    {
        return m_twitter;
    }

    QString facebook() const
    {
        return m_facebook;
    }

    QString livejournal() const
    {
        return m_livejournal;
    }

    QString instagram() const
    {
        return m_instagram;
    }

    QString first_name_nom() const
    {
        return m_first_name_nom;
    }

    QString first_name_gen() const
    {
        return m_first_name_gen;
    }

    QString first_name_dat() const
    {
        return m_first_name_dat;
    }

    QString first_name_acc() const
    {
        return m_first_name_acc;
    }

    QString first_name_ins() const
    {
        return m_first_name_ins;
    }

    QString first_name_abl() const
    {
        return m_first_name_abl;
    }

    int followers_count() const
    {
        return m_followers_count;
    }

    bool has_mobile() const
    {
        return m_has_mobile;
    }

    bool has_photo() const
    {
        return m_has_photo;
    }

    QString home_town() const
    {
        return m_home_town;
    }

    QString interests() const
    {
        return m_interests;
    }

    bool is_favorite() const
    {
        return m_is_favorite;
    }

    bool is_friend() const
    {
        return m_is_friend;
    }

    bool is_hidden_from_feed() const
    {
        return m_is_hidden_from_feed;
    }

    QString last_name_nom() const
    {
        return m_last_name_nom;
    }

    QString last_name_gen() const
    {
        return m_last_name_gen;
    }

    QString last_name_dat() const
    {
        return m_last_name_dat;
    }

    QString last_name_acc() const
    {
        return m_last_name_acc;
    }

    QString last_name_ins() const
    {
        return m_last_name_ins;
    }

    QString last_name_abl() const
    {
        return m_last_name_abl;
    }

    QString lists() const
    {
        return m_lists;
    }

    Personal* personal() const
    {
        return m_personal;
    }

    Education* education() const
    {
        return m_education;
    }

    Country* country() const
    {
        return m_country;
    }

    Counters* counters() const
    {
        return m_counters;
    }

    UserContacts* contacts() const
    {
        return m_contacts;
    }

    City* city() const
    {
        return m_city;
    }

    Career* career() const
    {
        return m_career;
    }

    LastSeen* last_seen() const
    {
        return m_last_seen;
    }

    Military* military() const
    {
        return m_military;
    }

    Occupation* occupation() const
    {
        return m_occupation;
    }

    QString friend_status() const
    {
        return m_friend_status;
    }

    QString photo_max_orig() const
    {
        return m_photo_max_orig;
    }

    QString games() const
    {
        return m_games;
    }

    QVariantList schools() const
    {
        return m_schools;
    }

    QString mobile_phone() const
    {
        return m_mobile_phone;
    }

    QString home_phone() const
    {
        return m_home_phone;
    }

signals:
    void universitiesChanged();
    void get_user_data(QString,QString,QString);
    void Avatar_pathChanged(QString arg);

    void NameplateChanged(QString arg);

    void AgeChanged(QString arg);

    void StatusChanged(QString arg);

    void isOnlineChanged(bool arg);

    void dataProviderChanged(Parser* DataProvider);

    void isActiveChanged(bool isActive);

    void is_FriendChanged(bool is_Friend);

    void aboutChanged(QString about);

    void bdateChanged(QString bdate);

    void activitiesChanged(QString activities);

    void blacklistedChanged(bool blacklisted);

    void blacklisted_by_meChanged(bool blacklisted_by_me);

    void booksChanged(QString books);

    void can_postChanged(bool can_post);

    void can_see_all_postsChanged(bool can_see_all_posts);

    void can_see_audioChanged(bool can_see_audio);

    void can_send_friend_requestChanged(bool can_send_friend_request);

    void can_write_private_messageChanged(bool can_write_private_message);

    void common_countChanged(int common_count);

    void domainChanged(QString domain);

    void maiden_nameChanged(QString maiden_name);

    void screen_nameChanged(QString screen_name);

    void sexChanged(QString sex);

    void photo_50Changed(QString photo_50);

    void photo_100Changed(QString photo_100);

    void photo_200_origChanged(QString photo_200_orig);

    void photo_200Changed(QString photo_200);

    void photo_400_origChanged(QString photo_400_orig);

    void photo_idChanged(QString photo_id);

    void photo_maxChanged(QString photo_max);

    void quotesChanged(QString quotes);

    void siteChanged(QString site);

    void timezoneChanged(int timezone);

    void tvChanged(QString tv);

    void verifiedChanged(bool verified);

    void wall_commentsChanged(bool wall_comments);

    void moviesChanged(QString movies);

    void musicChanged(QString music);

    void nicknameChanged(QString nickname);

    void onlineChanged(bool online);

    void relationChanged(QString relation);

    void twitterChanged(QString twitter);

    void facebookChanged(QString facebook);

    void livejournalChanged(QString livejournal);

    void instagramChanged(QString instagram);

    void first_name_nomChanged(QString first_name_nom);

    void first_name_genChanged(QString first_name_gen);

    void first_name_datChanged(QString first_name_dat);

    void first_name_accChanged(QString first_name_acc);

    void first_name_insChanged(QString first_name_ins);

    void first_name_ablChanged(QString first_name_abl);

    void followers_countChanged(int followers_count);

    void has_mobileChanged(bool has_mobile);

    void has_photoChanged(bool has_photo);

    void home_townChanged(QString home_town);

    void interestsChanged(QString interests);

    void is_favoriteChanged(bool is_favorite);

    void is_friendChanged(bool is_friend);

    void is_hidden_from_feedChanged(bool is_hidden_from_feed);

    void last_name_nomChanged(QString last_name_nom);

    void last_name_genChanged(QString last_name_gen);

    void last_name_datChanged(QString last_name_dat);

    void last_name_accChanged(QString last_name_acc);

    void last_name_insChanged(QString last_name_ins);

    void last_name_ablChanged(QString last_name_abl);

    void listsChanged(QString lists);

    void friend_statusChanged(QString friend_status);

    void photo_max_origChanged(QString photo_max_orig);

    void gamesChanged(QString games);

    void careerChanged(Career* career);

    void cityChanged(City* city);

    void countersChanged(Counters* counters);

    void schoolsChanged(QVariantList listcv);

    void countryChanged(Country* country);

    void mobile_phoneChanged(QString mobile_phone);

    void home_phoneChanged(QString home_phone);

    void last_seenChanged(LastSeen* last_seen);

    void occupationChanged(Occupation* occupation);

    void personalChanged(Personal* personal);

    void relativesChanged();

    void militaryChanged(Military* military);

public slots:
    void getResult(QVariantMap answer);

    void setAvatar_path(QString arg);
    void setNameplate(QString arg);
    void setAge(QString arg);
    void setStatus(QString arg);
    void setIsOnline(bool arg);
    void setDataProvider(Parser* DataProvider)
    {
        if (m_DataProvider == DataProvider)
            return;

        m_DataProvider = DataProvider;
        connect(m_DataProvider,&Parser::userInfoGenerated,this,&UserPage::getResult);

        emit dataProviderChanged(DataProvider);
    }
    void setActive(bool isActive)
    {
        if (m_isActive == isActive)
            return;

        m_isActive = isActive;
        emit isActiveChanged(isActive);
    }
    void setIs_Friend(bool is_Friend)
    {
        if (m_is_Friend == is_Friend)
            return;

        m_is_Friend = is_Friend;
        emit is_FriendChanged(is_Friend);
    }
    void setAbout(QString about)
    {
        if (m_about == about)
            return;

        m_about = about;
        emit aboutChanged(about);
    }
    void setBdate(QString bdate)
    {
        if (m_bdate == bdate)
            return;

        m_bdate = bdate;
        emit bdateChanged(bdate);
    }
    void setActivities(QString activities)
    {
        if (m_activities == activities)
            return;

        m_activities = activities;
        emit activitiesChanged(activities);
    }
    void setBlacklisted(bool blacklisted)
    {
        if (m_blacklisted == blacklisted)
            return;

        m_blacklisted = blacklisted;
        emit blacklistedChanged(blacklisted);
    }
    void setBlacklisted_by_me(bool blacklisted_by_me)
    {
        if (m_blacklisted_by_me == blacklisted_by_me)
            return;

        m_blacklisted_by_me = blacklisted_by_me;
        emit blacklisted_by_meChanged(blacklisted_by_me);
    }
    void setBooks(QString books)
    {
        if (m_books == books)
            return;

        m_books = books;
        emit booksChanged(books);
    }
    void setCan_post(bool can_post)
    {
        if (m_can_post == can_post)
            return;

        m_can_post = can_post;
        emit can_postChanged(can_post);
    }
    void setCan_see_all_posts(bool can_see_all_posts)
    {
        if (m_can_see_all_posts == can_see_all_posts)
            return;

        m_can_see_all_posts = can_see_all_posts;
        emit can_see_all_postsChanged(can_see_all_posts);
    }
    void setCan_see_audio(bool can_see_audio)
    {
        if (m_can_see_audio == can_see_audio)
            return;

        m_can_see_audio = can_see_audio;
        emit can_see_audioChanged(can_see_audio);
    }
    void setCan_send_friend_request(bool can_send_friend_request)
    {
        if (m_can_send_friend_request == can_send_friend_request)
            return;

        m_can_send_friend_request = can_send_friend_request;
        emit can_send_friend_requestChanged(can_send_friend_request);
    }
    void setCan_write_private_message(bool can_write_private_message)
    {
        if (m_can_write_private_message == can_write_private_message)
            return;

        m_can_write_private_message = can_write_private_message;
        emit can_write_private_messageChanged(can_write_private_message);
    }
    void setCommon_count(int common_count)
    {
        if (m_common_count == common_count)
            return;

        m_common_count = common_count;
        emit common_countChanged(common_count);
    }
    void setDomain(QString domain)
    {
        if (m_domain == domain)
            return;

        m_domain = domain;
        emit domainChanged(domain);
    }
    void setMaiden_name(QString maiden_name)
    {
        if (m_maiden_name == maiden_name)
            return;

        m_maiden_name = maiden_name;
        emit maiden_nameChanged(maiden_name);
    }
    void setScreen_name(QString screen_name)
    {
        if (m_screen_name == screen_name)
            return;

        m_screen_name = screen_name;
        emit screen_nameChanged(screen_name);
    }

    void setPhoto_50(QString photo_50)
    {
        if (m_photo_50 == photo_50)
            return;

        m_photo_50 = photo_50;
        emit photo_50Changed(photo_50);
    }
    void setPhoto_100(QString photo_100)
    {
        if (m_photo_100 == photo_100)
            return;

        m_photo_100 = photo_100;
        emit photo_100Changed(photo_100);
    }
    void setPhoto_200_orig(QString photo_200_orig)
    {
        if (m_photo_200_orig == photo_200_orig)
            return;

        m_photo_200_orig = photo_200_orig;
        emit photo_200_origChanged(photo_200_orig);
    }
    void setPhoto_200(QString photo_200)
    {
        if (m_photo_200 == photo_200)
            return;

        m_photo_200 = photo_200;
        emit photo_200Changed(photo_200);
    }
    void setPhoto_400_orig(QString photo_400_orig)
    {
        if (m_photo_400_orig == photo_400_orig)
            return;

        m_photo_400_orig = photo_400_orig;
        emit photo_400_origChanged(photo_400_orig);
    }
    void setPhoto_id(QString photo_id)
    {
        if (m_photo_id == photo_id)
            return;

        m_photo_id = photo_id;
        emit photo_idChanged(photo_id);
    }
    void setPhoto_max(QString photo_max)
    {
        if (m_photo_max == photo_max)
            return;

        m_photo_max = photo_max;
        emit photo_maxChanged(photo_max);
    }

    void setQuotes(QString quotes)
    {
        if (m_quotes == quotes)
            return;

        m_quotes = quotes;
        emit quotesChanged(quotes);
    }
    void setSite(QString site)
    {
        if (m_site == site)
            return;

        m_site = site;
        emit siteChanged(site);
    }
    void setTimezone(int timezone)
    {
        if (m_timezone == timezone)
            return;

        m_timezone = timezone;
        emit timezoneChanged(timezone);
    }
    void setTv(QString tv)
    {
        if (m_tv == tv)
            return;

        m_tv = tv;
        emit tvChanged(tv);
    }
    void setVerified(bool verified)
    {
        if (m_verified == verified)
            return;

        m_verified = verified;
        emit verifiedChanged(verified);
    }
    void setWall_comments(bool wall_comments)
    {
        if (m_wall_comments == wall_comments)
            return;

        m_wall_comments = wall_comments;
        emit wall_commentsChanged(wall_comments);
    }
    void setMovies(QString movies)
    {
        if (m_movies == movies)
            return;

        m_movies = movies;
        emit moviesChanged(movies);
    }
    void setMusic(QString music)
    {
        if (m_music == music)
            return;

        m_music = music;
        emit musicChanged(music);
    }
    void setNickname(QString nickname)
    {
        if (m_nickname == nickname)
            return;

        m_nickname = nickname;
        emit nicknameChanged(nickname);
    }
    void setOnline(bool online)
    {
        if (m_online == online)
            return;

        m_online = online;
        emit onlineChanged(online);
    }

    void setTwitter(QString twitter)
    {
        if (m_twitter == twitter)
            return;

        m_twitter = twitter;
        emit twitterChanged(twitter);
    }
    void setFacebook(QString facebook)
    {
        if (m_facebook == facebook)
            return;

        m_facebook = facebook;
        emit facebookChanged(facebook);
    }
    void setLivejournal(QString livejournal)
    {
        if (m_livejournal == livejournal)
            return;

        m_livejournal = livejournal;
        emit livejournalChanged(livejournal);
    }
    void setInstagram(QString instagram)
    {
        if (m_instagram == instagram)
            return;

        m_instagram = instagram;
        emit instagramChanged(instagram);
    }
    void setFirst_name_nom(QString first_name_nom)
    {
        if (m_first_name_nom == first_name_nom)
            return;

        m_first_name_nom = first_name_nom;
        emit first_name_nomChanged(first_name_nom);
    }
    void setFirst_name_gen(QString first_name_gen)
    {
        if (m_first_name_gen == first_name_gen)
            return;

        m_first_name_gen = first_name_gen;
        emit first_name_genChanged(first_name_gen);
    }
    void setFirst_name_dat(QString first_name_dat)
    {
        if (m_first_name_dat == first_name_dat)
            return;

        m_first_name_dat = first_name_dat;
        emit first_name_datChanged(first_name_dat);
    }
    void setFirst_name_acc(QString first_name_acc)
    {
        if (m_first_name_acc == first_name_acc)
            return;

        m_first_name_acc = first_name_acc;
        emit first_name_accChanged(first_name_acc);
    }
    void setFirst_name_ins(QString first_name_ins)
    {
        if (m_first_name_ins == first_name_ins)
            return;

        m_first_name_ins = first_name_ins;
        emit first_name_insChanged(first_name_ins);
    }
    void setFirst_name_abl(QString first_name_abl)
    {
        if (m_first_name_abl == first_name_abl)
            return;

        m_first_name_abl = first_name_abl;
        emit first_name_ablChanged(first_name_abl);
    }
    void setFollowers_count(int followers_count)
    {
        if (m_followers_count == followers_count)
            return;

        m_followers_count = followers_count;
        emit followers_countChanged(followers_count);
    }
    void setHas_mobile(bool has_mobile)
    {
        if (m_has_mobile == has_mobile)
            return;

        m_has_mobile = has_mobile;
        emit has_mobileChanged(has_mobile);
    }
    void setHas_photo(bool has_photo)
    {
        if (m_has_photo == has_photo)
            return;

        m_has_photo = has_photo;
        emit has_photoChanged(has_photo);
    }
    void setHome_town(QString home_town)
    {
        if (m_home_town == home_town)
            return;

        m_home_town = home_town;
        emit home_townChanged(home_town);
    }
    void setInterests(QString interests)
    {
        if (m_interests == interests)
            return;

        m_interests = interests;
        emit interestsChanged(interests);
    }
    void setIs_favorite(bool is_favorite)
    {
        if (m_is_favorite == is_favorite)
            return;

        m_is_favorite = is_favorite;
        emit is_favoriteChanged(is_favorite);
    }
    void setIs_friend(bool is_friend)
    {
        if (m_is_friend == is_friend)
            return;

        m_is_friend = is_friend;
        emit is_friendChanged(is_friend);
    }
    void setIs_hidden_from_feed(bool is_hidden_from_feed)
    {
        if (m_is_hidden_from_feed == is_hidden_from_feed)
            return;

        m_is_hidden_from_feed = is_hidden_from_feed;
        emit is_hidden_from_feedChanged(is_hidden_from_feed);
    }
    void setLast_name_nom(QString last_name_nom)
    {
        if (m_last_name_nom == last_name_nom)
            return;

        m_last_name_nom = last_name_nom;
        emit last_name_nomChanged(last_name_nom);
    }
    void setLast_name_gen(QString last_name_gen)
    {
        if (m_last_name_gen == last_name_gen)
            return;

        m_last_name_gen = last_name_gen;
        emit last_name_genChanged(last_name_gen);
    }
    void setLast_name_dat(QString last_name_dat)
    {
        if (m_last_name_dat == last_name_dat)
            return;

        m_last_name_dat = last_name_dat;
        emit last_name_datChanged(last_name_dat);
    }
    void setLast_name_acc(QString last_name_acc)
    {
        if (m_last_name_acc == last_name_acc)
            return;

        m_last_name_acc = last_name_acc;
        emit last_name_accChanged(last_name_acc);
    }
    void setLast_name_ins(QString last_name_ins)
    {
        if (m_last_name_ins == last_name_ins)
            return;

        m_last_name_ins = last_name_ins;
        emit last_name_insChanged(last_name_ins);
    }
    void setLast_name_abl(QString last_name_abl)
    {
        if (m_last_name_abl == last_name_abl)
            return;

        m_last_name_abl = last_name_abl;
        emit last_name_ablChanged(last_name_abl);
    }
    void setLists(QString lists)
    {
        if (m_lists == lists)
            return;

        m_lists = lists;
        emit listsChanged(lists);
    }
    void setFriend_status(QString friend_status)
    {
        if (m_friend_status == friend_status)
            return;

        m_friend_status = friend_status;
        emit friend_statusChanged(friend_status);
    }
    void setPhoto_max_orig(QString photo_max_orig)
    {
        if (m_photo_max_orig == photo_max_orig)
            return;

        m_photo_max_orig = photo_max_orig;
        emit photo_max_origChanged(photo_max_orig);
    }
    void setGames(QString games)
    {
        if (m_games == games)
            return;

        m_games = games;
        emit gamesChanged(games);
    }
    void setSex(QString sex)
    {
        if (m_sex == sex)
            return;

        m_sex = sex;
        emit sexChanged(sex);
    }
    void setRelation(QString relation)
    {
        if (m_relation == relation)
            return;

        m_relation = relation;
        emit relationChanged(relation);
    }
    void setCareer(Career* career)
    {
        m_career = career;
    }
    void setCity(City* city)
    {
        if (m_city == city)
            return;

        m_city = city;
        emit cityChanged(city);
    }
    void setCounters(Counters* counters)
    {
        if (m_counters == counters)
            return;

        m_counters = counters;
        emit countersChanged(counters);
    }
    void setSchools(QVariantList schools)
    {
        if (m_schools == schools)
            return;

        m_schools = schools;
        emit schoolsChanged(schools);
    }
    void setCountry(Country* country)
    {
        if (m_country == country)
            return;

        m_country = country;
        emit countryChanged(country);
    }
    void setMobile_phone(QString mobile_phone)
    {
        if (m_mobile_phone == mobile_phone)
            return;

        m_mobile_phone = mobile_phone;
        emit mobile_phoneChanged(mobile_phone);
    }
    void setHome_phone(QString home_phone)
    {
        if (m_home_phone == home_phone)
            return;

        m_home_phone = home_phone;
        emit home_phoneChanged(home_phone);
    }
    void setLast_seen(LastSeen* last_seen)
    {
        if (m_last_seen == last_seen)
            return;

        m_last_seen = last_seen;
        emit last_seenChanged(last_seen);
    }
    void setOccupation(Occupation* occupation)
    {
        if (m_occupation == occupation)
            return;

        m_occupation = occupation;
        emit occupationChanged(occupation);
    }
    void setPersonal(Personal* personal)
    {
        if (m_personal == personal)
            return;

        m_personal = personal;
        emit personalChanged(personal);
    }
    void setMilitary(Military* military)
    {
        if (m_military == military)
            return;

        m_military = military;
        emit militaryChanged(military);
    }
};


/*
 *     void setRelation(int relation)
    {

    1 — не женат/не замужем;
    2 — есть друг/есть подруга;
    3 — помолвлен/помолвлена;
    4 — женат/замужем;
    5 — всё сложно;
    6 — в активном поиске;
    7 — влюблён/влюблена;
    8 — в гражданском браке;
    0 — не указано.

        QString relation1;
        QString relation2;
        QString relation3;
        QString relation4;
        QString relation7;

        if(m_sex_int==2)
        {
            relation1 =QT_TR_NOOP("не замужем");
            relation2 =QT_TR_NOOP("есть друг");
            relation3 =QT_TR_NOOP("помолвлена");
            relation4 =QT_TR_NOOP("замужем");
            relation7 =QT_TR_NOOP("влюблёна");
        }
        else
        {
            relation1 =QT_TR_NOOP("не женат");
            relation2 =QT_TR_NOOP("есть подруга");
            relation3 =QT_TR_NOOP("помолвлен");
            relation4 =QT_TR_NOOP(" женат");
            relation7 =QT_TR_NOOP("влюблён");
        }
        QString relation5 =QT_TR_NOOP("всё сложно");
        QString relation6 =QT_TR_NOOP("в активном поиске");
        QString relation8 =QT_TR_NOOP("в гражданском браке");
        QString relation0 =QT_TR_NOOP("не указано");

        switch (relation) {
        case 1:
            if (m_relation == relation1)
                return;

            m_relation = relation1;

            break;
        case 2:
            if (m_relation == relation2)
                return;

            m_relation = relation2;

            break;
        case 3:
            if (m_relation == relation3)
                return;

            m_relation = relation3;

            break;
        case 4:
            if (m_relation == relation4)
                return;

            m_relation = relation4;

            break;
        case 5:
            if (m_relation == relation5)
                return;

            m_relation = relation5;

            break;
        case 6:
            if (m_relation == relation6)
                return;

            m_relation = relation6;

            break;
        case 7:
            if (m_relation == relation7)
                return;

            m_relation = relation7;

            break;
        case 8:
            if (m_relation == relation8)
                return;

            m_relation = relation8;

            break;
        default:
            if (m_relation == relation0)
                return;

            m_relation = relation0;

            break;
        }
        emit relationChanged(m_relation);
    }

    void setSex(int sex)
    {
        m_sex_int=sex;
        QString female= QT_TR_NOOP("женский");
        QString male= QT_TR_NOOP("мужской");
        QString uknown= QT_TR_NOOP("мужской");
        switch (sex) {
        case 1:
            if (m_sex == female)
                return;
            m_sex = female;

            break;
        case 2:
            if (m_sex ==male)
                return;
            m_sex = male;

        default:
            if (m_sex == uknown)
                return;
            m_sex = uknown;
            break;
        }
        emit sexChanged(m_sex);
    }
 *
 */

#endif // USERPAGE_H
