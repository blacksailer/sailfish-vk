

function insertLinks(text)
{
    var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
      return text.replace(exp,"<a href='$1'>$1</a>");
}

function insertToVK(text)
{
    var exp=/\[(club\w*)\|(.*?)\]/g;
   return text.replace(exp,"<a href='$1'>$2</a>");
}

function insertIDToVK(text)
{
    var exp=/\[(id\w*)\|(.*?)\]/g;
   return text.replace(exp,"<a href='$1'>$2</a>");
}
