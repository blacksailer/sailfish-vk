/*
  Copyright (C) 2013 Jolla Ltd.
  Contact: Thomas Perl <thomas.perl@jollamobile.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import QtMultimedia 5.0
import "pages"

ApplicationWindow
{
    id:root
    property bool isToken: true

    initialPage:  Component{
        Page{
            BusyIndicator{
            anchors.centerIn: parent
            running: true
            size: BusyIndicatorSize.Large
            }
        }
    }

    cover: Qt.resolvedUrl("cover/CoverPage.qml")

    TextField{
        id:clipboardText
        visible:false
    }


    Audio {
        id:playMusic

        property bool repeat: false
        property string songName
        property string gangName
        property  int  id: 0
        onStopped: {
            if(repeat)
                //   playMusic.play()
                playMusic.seek(0)

        }
        function nextSong(){
            playMusic.source = musicModel[id].source;
            playMusic.songName = musicModel[id].songName;
            playMusic.gangName = musicModel[id].gangName;
        }
    }

    ListModel{
        id:playlistModel

    }

    Component.onCompleted: {
        if(vkapi.checkToken()) //Если true, то получать новый токен не нужно
                    {
//                        vkapi.setTokenFromDatabase()
//                        pageStack.replace(Qt.resolvedUrl("pages/MenuPage.qml"),PageStackAction.Animated)
            var pages = []
            var page1 =Qt.resolvedUrl("pages/MenuPage.qml");

            pages.push(page1);
            var listPages = c_PageStarter.getPages();
            for(var i =0; i< listPages.length;i++)
            {
                /* pageType caption
                  0 - Dialog
                  1 - Group
                  2 - Birthday
                  */
                switch(listPages[i].pageType)
                {
                case 0:
                    var page_dialog=Qt.resolvedUrl("pages/MessagesHistoryPage.qml")
                    pages.push({page:page_dialog,properties:{chat_id:listPages[i].pageParam}});
                    break;
                }
            }

            pageStack.replace(pages)//,{page: page_dialog, properties: {chat_id: 6520452}}])

        }
        else
        {
            console.log("Token is False")
            isToken = false
//                        pageStack.push(Qt.resolvedUrl("pages/MenuPage.qml"),PageStackAction.Animated)
            if(pageStack.depth===1)
                   pageStack.replace(Qt.resolvedUrl("pages/AuthorizePage.qml"),PageStackAction.Animated)
            else
                pageStack.push(Qt.resolvedUrl("pages/AuthorizePage.qml"),PageStackAction.Animated)

        }
    }

    Connections {
        target:vkapi
        onRefreshTokenChanged:
        {
            if(vkapi.RefreshToken)
            {
                pageStack.push(Qt.resolvedUrl("pages/AuthorizePage.qml"),PageStackAction.Animated)
                vkapi.setRefreshToken(false);
            }
        }
        onBrowserForAuthNeeded:
        {
            console.log("Browser needed")
            pageStack.push(Qt.resolvedUrl("pages/AuthorizePage.qml"),PageStackAction.Animated)
        }
    }
    onApplicationActiveChanged: {
        if(!Qt.application.active)
        {
            console.log("Timeout is 20 seconds")
            globalSettings.setLongPollTimeout(20*1000)
        }
        else
        {
            var pages =[];
            var listPages = c_PageStarter.getPages();
            for(var i =0; i< listPages.length;i++)
            {
                /* pageType caption
                  0 - Dialog
                  1 - Group
                  2 - Birthday
                  */
                switch(listPages[i].pageType)
                {
                case 0:
                    var page_dialog=Qt.resolvedUrl("pages/MessagesHistoryPage.qml")
                    pages.push({page:page_dialog,properties:{chat_id:listPages[i].pageParam}});
                    break;
                }
            }
            if(pages.length>0)
                pageStack.push(pages)


            console.log("Timeout is 1 second")
            globalSettings.setLongPollTimeout(1000)
        }

    }
}


