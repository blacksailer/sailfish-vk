import QtQuick 2.0
import Sailfish.Silica 1.0
import VKPoco 1.0
import VKModels 1.0


Item {

    property UsersInfo userInformation
//    property string avatarSource
//    property string nameplate
//    property string status
    property int user_id
//    property string cityName
    property UsersInfo userInfo
//    property var counters
    property var schools:[]
    property var univers
//    property bool isOnline: false
//    property bool isFriend: false
    property bool isMyself: false

    id:page_info
    y: Theme.paddingLarge
    x: Theme.paddingLarge
    opacity: 1
    width: parent.width - 2*Theme.paddingLarge
    height:page_avatar.height+name_info.height+status_info.contentHeight+addFriendButton.height+infoGroup.height+wallHeader.height+Theme.itemSizeExtraSmall// +infoGroup.currentSection.height

    Image{
        id:page_avatar
        height: 200
        width: height
        fillMode: Image.PreserveAspectCrop
        source: userInfo.photo_100
        anchors{
            topMargin: 20
            top:parent.top
            horizontalCenter: parent.horizontalCenter
        }
    }

    Label{
        id:name_info
        text:userInfo.first_name_nom +" "+ userInfo.last_name_nom
        color:Theme.highlightColor
        anchors {
            leftMargin: 10
            top:page_avatar.bottom
            horizontalCenter: parent.horizontalCenter
        }

    }
    Label{
        id:status_info
        text:userInfo.Status
        color:Theme.secondaryHighlightColor

        font.pixelSize: Theme.fontSizeSmall
        wrapMode:Text.WordWrap
        width: parent.width
        horizontalAlignment: Text.AlignHCenter
        anchors{
            top: name_info.bottom
            horizontalCenter: parent.horizontalCenter
        }
    }
    Button{
        id:addFriendButton
        height:isMyself?0:Theme.itemSizeSmall
        visible: !isMyself
        anchors {
            top: status_info.bottom
            horizontalCenter: parent.horizontalCenter

            topMargin:5
        }

        text: userInfo.is_Friend ? qsTr("Убрать из друзей") :qsTr("Добавить в друзья")
        onClicked: {
            if(userInfo.is_Friend)
                //Из родителя вызывается areYouSureDialogPage и addFriendDialogPage
                onClicked: pageStack.push(areYouSureDialogPage)
            else
                onClicked: pageStack.push(addFriendDialogPage)
        }
    }
    ExpandingSectionGroup {
        id: infoGroup
        anchors.top:addFriendButton.bottom
        animateToExpandedSection:false
        anchors.topMargin: Theme.paddingMedium
        //  currentIndex: 0

        Repeater {
            model: 2

            ExpandingSection {
                id: section
                buttonHeight: Theme.itemSizeSmall
                property int sectionIndex: model.index
                title: model.index == 0 ?qsTr("Общая информация"):qsTr("Медиа")

                content.sourceComponent:
                    Loader{
                    sourceComponent: model.index == 0 ? userInfoComponent:mediaComponent
                }
            }
        }
    }
    ListItem{
        id:wallHeader
        anchors.top:infoGroup.bottom
        contentHeight:  Theme.itemSizeExtraSmall
        SectionHeader{
            text: qsTr("Стена")
        }
        menu:ContextMenu{
            MenuItem{
                text:qsTr("Написать на стене")
                visible:userInfo.can_post
                onClicked: {
                    var dialog = pageStack.push("../pages/PostWriterDialog.qml",{owner_id:user_id})
                    userInformation.isActive = true
                    vkapi.wall_post(dialog.owner_id,dialog.friends_only,dialog.from_group,dialog.text_to_send,
                                    dialog.attachments_to_send,"",dialog.signed,
                                    dialog.is_ads,0,0,0,0,0)

                }
            }
        }
    }
    Component{
        id:userInfoComponent
        Column {
            width: parent.width
            Row {
                visible:userInfo.bdate==""? false : true
                width: parent.width
                spacing: Theme.paddingMedium
                anchors.leftMargin: Theme.horizontalPageMargin
                Label{
                    text: qsTr("День рождения")
                    color: Theme.highlightColor

                }
                Label{
                    text: userInfo.bdate
                    color: Theme.secondaryHighlightColor

                }
            }
            Row {
                visible:userInfo.city ? false : true

                width: parent.width
                spacing: Theme.paddingMedium
                anchors.leftMargin: Theme.horizontalPageMargin
                Label{
                    text: qsTr("Город")
                    color: Theme.highlightColor
                }
                Label{
                    text: userInfo.city.title
                    color: Theme.secondaryHighlightColor

                }
            }
            Row {
                visible:userInfo.instagram==""? false : true


                width: parent.width
                spacing: Theme.paddingMedium
                anchors.leftMargin: Theme.horizontalPageMargin
                Label{
                    text: qsTr("Instagram")
                    color: Theme.highlightColor
                }
                Label{
                    text: userInfo.instagram
                    color: Theme.secondaryHighlightColor

                }
            }
            Row {        
                visible:userInfo.facebook==""? false : true

                width: parent.width
                spacing: Theme.paddingMedium
                anchors.leftMargin: Theme.horizontalPageMargin
                Label{
                    text: qsTr("Facebook")
                    color: Theme.highlightColor
                }
                Label{
                    text: userInfo.facebook
                    color: Theme.secondaryHighlightColor

                }
            }
            Row {
                visible:userInfo.twitter==""? false : true

                width: parent.width
                spacing: Theme.paddingMedium
                anchors.leftMargin: Theme.horizontalPageMargin
                Label{
                    text: qsTr("Twitter")
                    color: Theme.highlightColor
                }
                Label{
                    text: userInfo.twitter
                    color: Theme.secondaryHighlightColor

                }
            }
            Row {
                visible:userInfo.site==""? false : true

                width: parent.width
                spacing: Theme.paddingMedium
                anchors.leftMargin: Theme.horizontalPageMargin
                Label{
                    text: qsTr("Веб-сайт")
                    color: Theme.highlightColor
                }
                Label{
                    text: userInfo.site
                    color: Theme.secondaryHighlightColor

                }
            }
            BackgroundItem{
                visible:false


                id:fullInfoItem
                anchors.leftMargin: Theme.horizontalPageMargin
                width: parent.width-2*x
                Label{
                    anchors.centerIn: parent
                    text:qsTr("Полная информация")
                    color: fullInfoItem.highlighted ? Theme.highlightColor :Theme.primaryColor
                }
            }
        }
    }
    Component{
        id:mediaComponent
        Column {
            width: parent.width
            BackgroundItem {
                id:friendsItem
                Label{
                    anchors.left: parent.left
                    anchors.leftMargin: Theme.horizontalPageMargin
                    text: qsTr("Друзья")
                    color:friendsItem.highlighted ? Theme.highlightColor : Theme.primaryColor
                    anchors.verticalCenter: parent.verticalCenter

                }
                Label{
                    anchors{
                        right: parent.right
                        rightMargin: Theme.horizontalPageMargin

                    }
                    anchors.verticalCenter: parent.verticalCenter

                    text: userInfo.counters.friends
                    color:friendsItem.highlighted ? Theme.highlightColor : Theme.primaryColor


                }
                onClicked: pageStack.push(Qt.resolvedUrl("qrc:///qml/pages/FriendsPage.qml"),{user_id:user_id})

            }
            BackgroundItem
            {
                id:mutualItem
                Label{
                    anchors.left: parent.left
                    anchors.leftMargin: Theme.horizontalPageMargin

                    text: qsTr("Общие друзья")
                    color:mutualItem.highlighted ? Theme.highlightColor : Theme.primaryColor

                    anchors.verticalCenter: parent.verticalCenter

                }
                Label{
                    anchors{
                        rightMargin: Theme.horizontalPageMargin
                        right: parent.right
                    }
                    anchors.verticalCenter: parent.verticalCenter


                    text: userInfo.counters.mutual_friends
                    color:mutualItem.highlighted ? Theme.highlightColor : Theme.primaryColor



                }
            }
            BackgroundItem
            {
                id:followersItem
                Label{
                    anchors.left: parent.left
                    anchors.leftMargin: Theme.horizontalPageMargin

                    text: qsTr("Подписчики")
                    color:followersItem.highlighted ? Theme.highlightColor : Theme.primaryColor

                    anchors.verticalCenter: parent.verticalCenter

                }
                Label{
                    anchors{
                        right: parent.right
                        rightMargin: Theme.horizontalPageMargin

                    }
                    anchors.verticalCenter: parent.verticalCenter

                    text: userInfo.counters.followers
                    color:followersItem.highlighted?  Theme.secondaryHighlightColor : Theme.primaryColor


                }

            }
            BackgroundItem {
                id:groupItem
                Label{
                    anchors.left: parent.left
                    anchors.leftMargin: Theme.horizontalPageMargin

                    text: qsTr("Группы")
                    anchors.verticalCenter: parent.verticalCenter
                    color:groupItem.highlighted? Theme.highlightColor : Theme.primaryColor

                }
                Label{
                    anchors{
                        right: parent.right
                        rightMargin: Theme.horizontalPageMargin

                    }
                    anchors.verticalCenter: parent.verticalCenter

                    text: userInfo.counters.groups
                    color:groupItem.highlighted? Theme.highlightColor : Theme.primaryColor


                }
                onClicked: pageStack.push(Qt.resolvedUrl("qrc:///qml/pages/GroupsPage.qml"),{user_id:user_id})


            }
            BackgroundItem {
                id:photoItem
                Label{
                    anchors.left: parent.left
                    anchors.leftMargin: Theme.horizontalPageMargin

                    text: qsTr("Фото")
                    anchors.verticalCenter: parent.verticalCenter
                    color:photoItem.highlighted? Theme.highlightColor : Theme.primaryColor

                }
                Label{
                    anchors{
                        right: parent.right
                        rightMargin: Theme.horizontalPageMargin

                    }
                    anchors.verticalCenter: parent.verticalCenter

                    text: userInfo.counters.photos
                    color:photoItem.highlighted? Theme.highlightColor : Theme.primaryColor


                }

            }
            BackgroundItem  {
                id:videoItem
                Label{
                    anchors.left: parent.left
                    anchors.leftMargin: Theme.horizontalPageMargin

                    text: qsTr("Видео")
                    anchors.verticalCenter: parent.verticalCenter
                    color:videoItem.highlighted?Theme.highlightColor: Theme.primaryColor
                }
                Label{
                    anchors{
                        right: parent.right
                        rightMargin: Theme.horizontalPageMargin

                    }
                    anchors.verticalCenter: parent.verticalCenter

                    text: userInfo.counters.videos
                    color:videoItem.highlighted? Theme.highlightColor : Theme.primaryColor


                }

            }
            BackgroundItem
            {
                id:audioItem
                Label{
                    anchors.left: parent.left
                    anchors.leftMargin: Theme.horizontalPageMargin

                    text: qsTr("Аудио")
                    color:audioItem.highlighted? Theme.highlightColor : Theme.primaryColor

                    anchors.verticalCenter: parent.verticalCenter

                }
                Label{
                    anchors{
                        right: parent.right
                        rightMargin: Theme.horizontalPageMargin
                    }
                    anchors.verticalCenter: parent.verticalCenter
                    text: userInfo.counters.audios
                    color:audioItem.highlighted? Theme.highlightColor : Theme.primaryColor


                }
            }


        }
    }
    Component {
        id: fullInfoComponent
        Dialog{
            SilicaFlickable{
                anchors.fill: parent
                Column{
                    width:parent.width

                    Repeater{
                        model:schools
                        delegate: Column
                        {
                        width:parent.parent

                        Label{
                            text:modelData.name
                        }
                        Label{
                            text:modelData.type_str
                        }
                    }
                }
                Repeater{
                    model:univers
                    delegate: Column
                    {
                    width:parent.parent

                    Label{
                        text:name
                    }
                    Label{
                        text:modelData.faculty_name
                    }
                    Label{
                        text:modelData.chair_name
                    }
                    Label{
                        text:modelData.graduation
                    }
                }
            }
        }
    }
}
}
}

