import QtQuick 2.0
import Sailfish.Silica 1.0
import VKPoco 1.0
import VKModels 1.0
import "qrc:/helpers.js" as Helper


Item {
    property GroupInfoC groupInformation

    id:page_info
    y: Theme.paddingLarge
    x: Theme.paddingLarge
    width: parent.width - 2*Theme.paddingLarge
    height:page_avatar.height+ name_info.height+ status_info.height+addSubscriptionButton.height+ infoGroup.height+wallHeader.height+2*Theme.paddingLarge

    Image{
        id:page_avatar
        height: 200
        width: height
        fillMode: Image.PreserveAspectCrop
        source: groupInformation.photo_100
        anchors{
            topMargin: 20
            top:parent.top
            horizontalCenter: parent.horizontalCenter
        }
            }
    Label{
        id:name_info
        text:groupInformation.name
        anchors {
            leftMargin: 10
            top:page_avatar.bottom
            horizontalCenter: parent.horizontalCenter
        }
    }
    Label{
        id:status_info
        text:groupInformation.status
        color:Theme.secondaryHighlightColor

        font.pixelSize: Theme.fontSizeSmall
        wrapMode:Text.WordWrap
        width: parent.width
        horizontalAlignment: Text.AlignHCenter
        anchors{
            top: name_info.bottom
            horizontalCenter: parent.horizontalCenter
        }
    }
    Button{
        id:addSubscriptionButton
        //width: 100
        height:50
        anchors {
            top: status_info.bottom
            horizontalCenter: parent.horizontalCenter
            topMargin:Theme.paddingMedium
            bottomMargin: Theme.paddingMedium

        }

        text: groupInformation.is_member ? "Отписаться" :"Подписаться"
        onClicked: {
            if(groupInformation.is_member)
                //Из родителя вызывается areYouSureDialogPage и addSubsDialogPage
                 pageStack.push(areYouSureDialogPage)
            else
                 pageStack.push(addSubsDialogPage)
        }
    }
    ExpandingSectionGroup {
        id: infoGroup
        anchors.top:addSubscriptionButton.bottom
        animateToExpandedSection:false
        anchors.topMargin: Theme.paddingMedium
        //  currentIndex: 0

        Repeater {
            model: 2

            ExpandingSection {
                id: section
                buttonHeight: Theme.itemSizeSmall
                property int sectionIndex: model.index
                title: model.index === 0 ?qsTr("Общая информация"):qsTr("Медиа")

                content.sourceComponent:
                    Loader{
                    sourceComponent: model.index === 0 ? groupInfoComponent:mediaComponent
                }
            }
        }
    }
    ListItem{
        id:wallHeader
        anchors.top:infoGroup.bottom
        contentHeight:  Theme.itemSizeExtraSmall
        SectionHeader{
            text: qsTr("Стена")
        }
        menu:ContextMenu{
            MenuItem{
                text:qsTr("Написать на стене")
                visible: groupInformation.can_post
                onClicked:{
                    var dialog = pageStack.push("../pages/PostWriterDialog.qml",{owner_id:groupInformation.id*-1})
                dialog.accepted.connect(function(){
                    groupInformation.isActive = true
                    vkapi.wall_post(dialog.owner_id,dialog.friends_only,dialog.from_group,dialog.text_to_send,
                                    dialog.attachments_to_send,"",dialog.signed,
                                    dialog.is_ads,0,0,0,0,0)
                })
                }

            }
        }
    }

    Component{
        id:groupInfoComponent
        Column {
            width: parent.width
            Row {
                visible:groupInformation.description==""? false : true
                width: parent.width
                spacing: Theme.paddingMedium
                anchors.leftMargin: Theme.horizontalPageMargin
                Label{
                    id:infoItem
                    text: qsTr("Информация")
                    color: Theme.highlightColor
                }
                Label{
                    width: parent.width-infoItem.width
                    text:Helper.insertLinks(groupInformation.description)
                    wrapMode: Text.WordWrap
                    textFormat: Text.StyledText
                    linkColor: Theme.highlightColor

                    color: Theme.secondaryHighlightColor
                    onLinkActivated: Qt.openUrlExternally(link)
                }
            }
            Row {
                visible:groupInformation.site==""? false : true
                width: parent.width
                spacing: Theme.paddingMedium
                anchors.leftMargin: Theme.horizontalPageMargin
                Label{
                    text: qsTr("Веб-сайт")
                    color: Theme.highlightColor

                }
                Label{
                    text:Helper.insertLinks(groupInformation.site)
                    textFormat: Text.StyledText
                    linkColor: Theme.highlightColor
                    color: Theme.secondaryHighlightColor
                    onLinkActivated: Qt.openUrlExternally(link)
                }
            }
            Row {
                visible:groupInformation.type=="event"?  true:false
                width: parent.width
                spacing: Theme.paddingMedium
                anchors.leftMargin: Theme.horizontalPageMargin
                Label{
                    text: qsTr("Начало встречи")
                    color: Theme.highlightColor

                }
                Label{
                    text:Format.formatDate(new Date(groupInformation.start_date*1000), Formatter.DateMedium)
                    color: Theme.secondaryHighlightColor
                }
            }
            Row {
                visible:groupInformation.type=="event"? true : false
                width: parent.width
                spacing: Theme.paddingMedium
                anchors.leftMargin: Theme.horizontalPageMargin
                Label{
                    text: qsTr("Конец встречи")
                    color: Theme.highlightColor

                }
                Label{
                    text:Format.formatDate(new Date(groupInformation.finish_date*1000), Formatter.DateMedium)
                    color: Theme.secondaryHighlightColor
                }
            }
            Row {
                visible:groupInformation.place==null? false : true
                width: parent.width
                spacing: Theme.paddingMedium
                anchors.leftMargin: Theme.horizontalPageMargin
                Label{
                    text: qsTr("Адрес")
                    color: Theme.highlightColor

                }
                Label{
                    text:groupInformation.place==null?"":groupInformation.place.address
                    color: Theme.secondaryHighlightColor
                }
            }
            BackgroundItem{
                visible:false
                id:fullInfoItem
                anchors.leftMargin: Theme.horizontalPageMargin
                width: parent.width-2*x
                Label{
                    anchors.centerIn: parent
                    text:qsTr("Полная информация")
                    color: fullInfoItem.highlighted ? Theme.highlightColor :Theme.primaryColor
                }
            }
        }
    }
    Component{
        id:mediaComponent
        Column {
            width: parent.width
            BackgroundItem
            {
                id:followersItem
                Label{
                    anchors.left: parent.left
                    anchors.leftMargin: Theme.horizontalPageMargin

                    text: qsTr("Подписчики")
                    color:followersItem.highlighted ? Theme.highlightColor : Theme.primaryColor

                    anchors.verticalCenter: parent.verticalCenter

                }
                Label{
                    anchors{
                        right: parent.right
                        rightMargin: Theme.horizontalPageMargin

                    }
                    anchors.verticalCenter: parent.verticalCenter

                    text: groupInformation.members_count
                    color:followersItem.highlighted?  Theme.secondaryHighlightColor : Theme.primaryColor


                }

            }
            BackgroundItem {
                id:photoItem
                Label{
                    anchors.left: parent.left
                    anchors.leftMargin: Theme.horizontalPageMargin

                    text: qsTr("Фото")
                    anchors.verticalCenter: parent.verticalCenter
                    color:photoItem.highlighted? Theme.highlightColor : Theme.primaryColor

                }
                Label{
                    anchors{
                        right: parent.right
                        rightMargin: Theme.horizontalPageMargin

                    }
                    anchors.verticalCenter: parent.verticalCenter

                    text: groupInformation.counters.photos
                    color:photoItem.highlighted? Theme.highlightColor : Theme.primaryColor


                }

            }
            BackgroundItem  {
                id:videoItem
                Label{
                    anchors.left: parent.left
                    anchors.leftMargin: Theme.horizontalPageMargin

                    text: qsTr("Видео")
                    anchors.verticalCenter: parent.verticalCenter
                    color:videoItem.highlighted?Theme.highlightColor: Theme.primaryColor
                }
                Label{
                    anchors{
                        right: parent.right
                        rightMargin: Theme.horizontalPageMargin

                    }
                    anchors.verticalCenter: parent.verticalCenter

                    text: groupInformation.counters.videos
                    color:videoItem.highlighted? Theme.highlightColor : Theme.primaryColor


                }

            }
            BackgroundItem
            {
                id:audioItem
                Label{
                    anchors.left: parent.left
                    anchors.leftMargin: Theme.horizontalPageMargin

                    text: qsTr("Аудио")
                    color:audioItem.highlighted? Theme.highlightColor : Theme.primaryColor

                    anchors.verticalCenter: parent.verticalCenter

                }
                Label{
                    anchors{
                        right: parent.right
                        rightMargin: Theme.horizontalPageMargin
                    }
                    anchors.verticalCenter: parent.verticalCenter
                    text: groupInformation.counters.audios
                    color:audioItem.highlighted? Theme.highlightColor : Theme.primaryColor


                }
            }
            BackgroundItem
            {
                id:topicItem
                Label{
                    anchors.left: parent.left
                    anchors.leftMargin: Theme.horizontalPageMargin

                    text: qsTr("Обсуждение")
                    color:topicItem.highlighted? Theme.highlightColor : Theme.primaryColor

                    anchors.verticalCenter: parent.verticalCenter

                }
                Label{
                    anchors{
                        right: parent.right
                        rightMargin: Theme.horizontalPageMargin
                    }
                    anchors.verticalCenter: parent.verticalCenter
                    text: groupInformation.counters.topics
                    color:topicItem.highlighted? Theme.highlightColor : Theme.primaryColor


                }
            }
            BackgroundItem
            {
                id:docItem
                Label{
                    anchors.left: parent.left
                    anchors.leftMargin: Theme.horizontalPageMargin

                    text: qsTr("Документы")
                    color:docItem.highlighted? Theme.highlightColor : Theme.primaryColor

                    anchors.verticalCenter: parent.verticalCenter

                }
                Label{
                    anchors{
                        right: parent.right
                        rightMargin: Theme.horizontalPageMargin
                    }
                    anchors.verticalCenter: parent.verticalCenter
                    text: groupInformation.counters.docs
                    color:docItem.highlighted? Theme.highlightColor : Theme.primaryColor


                }
            }
        }
    }
}

