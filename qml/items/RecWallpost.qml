import QtQuick 2.0
import Sailfish.Silica 1.0
import VKItems 1.0
import "qrc:/helpers.js" as Helper

ListItem {
    id:rootWallpost
    property int post_id

    property string owner_n
    property int post_d
    property string owner_ava_source
    property int ownerid
    property int from_id

    signal remove()

    property bool isGroupAdmin:false
    property bool isChild: false
    property bool hasChild: false
    property alias model: attachments.pandvmodel
    property alias audiomodel: attachments.audiomodel
    property alias videomodel: attachments.videomodel
    property alias docmodel: attachments.docmodel
    property alias gifmodel: attachments.gifmodel
    property string text_d
    property bool fadetext
    property int mm: 4

    //Likes,Reposts
    property int like_count
    property int repost_count
    property bool can_like
    property bool user_likes
    property bool comments_canPost: commentsCanPost
    property int comments_count

    property alias childcopyWallpost : attachments.copyWallpost

    property color primcolor: Theme.primaryColor
    property color highlightColor: Theme.highlightColor
    property color highlightBackgroundColor: Theme.highlightBackgroundColor

    x: isChild?0:Theme.horizontalPageMargin
    width:isChild?parent.width - 2*x-repostLine.width-repostLine.anchors.leftMargin:parent.width - 2*x
    menu: usageMenu
    // height: text_data.height+owner_info.height+attachments.attachmentsHeight+socials.height
    //Иначе Rectangle е заполняет весь элемент
    contentHeight:  text_data.height+text_data.anchors.topMargin+owner_info.height+attachments.attachmentsHeight+attachments.anchors.topMargin+socials.height

    Rectangle{
        color: isChild ? Theme.rgba(primcolor, 0) :
                         _showPress ? Theme.rgba(highlightBackgroundColor, Theme.highlightBackgroundOpacity)
                                    : Theme.rgba(primcolor, 0.2)
        anchors.fill: parent
    }
    Rectangle{
        id:repostLine
        width:3
        height: parent.height
        visible: isChild
        color:Theme.secondaryHighlightColor
        anchors.left: parent.left
        anchors.leftMargin: 8
    }



    BackgroundItem {
        id: owner_info
        width:isChild? parent.width-Theme.paddingLarge:parent.width
        height:Theme.itemSizeSmall
        anchors.left: isChild?repostLine.right:undefined
        anchors.leftMargin: isChild?5:0

        Image {
            id: author_avatar
            source: owner_ava_source
            height: parent.height-2*Theme.paddingSmall
            width: height
            fillMode: Image.PreserveAspectCrop
            anchors{
                left:parent.left
                top:parent.top
                leftMargin:Theme.paddingSmall
                bottomMargin: Theme.paddingSmall
                topMargin: Theme.paddingSmall
            }
        }
        Label {
            id: owner_name
            elide:TruncationMode.Elide
            text: owner_n
            color: Theme.primaryColor
            font.pixelSize: Theme.fontSizeSmall

            anchors {
                left: author_avatar.right
                right: post_date.left
                leftMargin:  Theme.paddingSmall
                rightMargin: Theme.paddingSmall
            }
        }

        Label {
            id: post_date
            text: Format.formatDate(new Date(post_d*1000), Formatter.TimepointRelative)

            font.pixelSize: Theme.fontSizeExtraSmall
            color: Theme.primaryColor
            anchors.right: parent.right
            anchors.rightMargin: Theme.paddingSmall
            anchors.bottom: author_avatar.bottom
        }
        onClicked:{
            if(from_id>0)
                pageStack.push(Qt.resolvedUrl("qrc:///qml/pages/UserPage.qml"),{usr_id:from_id})
            else
                pageStack.push(Qt.resolvedUrl("qrc:///qml/pages/GroupPage.qml"),{group_id:from_id*-1})
        }

    }

    Label {
        id:text_data

        text: Helper.insertIDToVK(Helper.insertLinks(Helper.insertToVK(text_d)))
        textFormat: Text.StyledText
        linkColor: Theme.highlightColor

        color:Theme.primaryColor
        font.pixelSize: Theme.fontSizeSmall
        horizontalAlignment: Text.AlignLeft
        width: parent.width-anchors.leftMargin-anchors.rightMargin
        wrapMode: Text.Wrap
        anchors {
            top: owner_info.bottom
            right: parent.right
            left: isChild?repostLine.right:parent.left
            topMargin: text.length==0?0:Theme.paddingSmall
            leftMargin: isChild?10:5
            rightMargin: 5

        }
        onLinkActivated:{

            console.log("Link clicked");
            if(link.substring(0,4)=="club")
                pageStack.push(Qt.resolvedUrl("qrc:///qml/pages/GroupPage.qml"),{group_id:parseInt(link.substring(4,link.length))}) //умножаем на -1,т.к. является группой
            else if (link.substring(0,2)=="id")
                pageStack.push(Qt.resolvedUrl("qrc:///qml/pages/UserPage.qml"),{usr_id:parseInt(link.substring(2,link.length))}) //умножаем на -1,т.к. является группой
            else
                Qt.openUrlExternally(link)
        }
    }

    AttachmentsView {
        id:attachments

        fadetext: rootWallpost.fadetext
        parentWidth:isChild ? rootWallpost.width-Theme.paddingLarge:rootWallpost.width

        anchors {
            leftMargin: isChild?5:0
            topMargin: text_data.length==0?0:10

            top: text_data.bottom
            left: isChild?repostLine.right:parent.left
            right: parent.right
        }

    }


    Row {
        id:socials
        property int actionCount:  3
        width:owner_info.width
        visible: !isChild
        height:isChild?0:Theme.itemSizeSmall
        anchors.bottom:parent.bottom
        Item{
            id:likes
            height:parent.height
            width:parent.width/socials.actionCount
            Item{
                anchors.fill: parent
                anchors.centerIn: parent

                IconButton {
                    id:iconLike
                    icon.source: "image://theme/icon-s-like?" + (highlighted
                                                                 ? Theme.highlightColor
                                                                 : Theme.primaryColor)
                    highlighted:user_likes
                    enabled: can_like
                    onClicked: {
                        if(true)//an_like)
                        {
                            console.log("You can like");

                            if(!user_likes)
                            {
                                user_likes=!user_likes;
                                console.log("like++");
                                like_count=like_count+1;
                                vkapi.likes_add("post",from_id,post_id);

                            }
                            else
                            {
                                console.log("like--");
                                user_likes=!user_likes;
                                like_count=like_count-1;
                                vkapi.likes_delete("post",from_id,post_id);

                            }
                        }
                    }
                }
                Label {
                    text: like_count
                    anchors.left:iconLike.right
                    anchors.verticalCenter: iconLike.verticalCenter

                }
            }
        }
        Item{
            id:comments
            height:parent.height
            width:parent.width/socials.actionCount
            Item{
                anchors.fill: parent
                anchors.centerIn: parent
                visible:comments_canPost


                IconButton {
                    id:commentsImage
                    icon.source: "image://theme/icon-s-message"

                }
                Label {
                    text: comments_count
                    anchors.left:commentsImage.right
                    anchors.verticalCenter: commentsImage.verticalCenter
                }
            }
        }
        Item{
            id:reposts
            height:parent.height
            width:parent.width/socials.actionCount
            Item{
                anchors.fill: parent
                anchors.centerIn: parent

                IconButton {
                    id:iconRepost
                    icon.source: "image://theme/icon-s-retweet?" + (pressed
                                                                    ? Theme.highlightColor
                                                                    : Theme.primaryColor)
                    onClicked: {
                        rootWallpost.menu = shareMenu;
                        showMenu()
                    }
                }
                Label {
                    text: repost_count
                    anchors.left:iconRepost.right
                    anchors.verticalCenter: iconRepost.verticalCenter
                }
            }
        }
    }


    states: [
        State {
            name: "wide text"
            when:  fadetext==true
            PropertyChanges {
                target: text_data
                maximumLineCount: 10
                truncationMode: TruncationMode.Elide
            }


        },
        State {
            name: "not wide text"
            when: fadetext==false
            PropertyChanges {
                target: text_data
                truncationMode: TruncationMode.None
            }

        }
    ]
    onMenuOpenChanged:  {
        if(!menuOpen)
            menu = usageMenu
    }
    Component{
        id:shareMenu
        ContextMenu{
            MenuItem{
                text:qsTr("Личным сообщением")
                onClicked: {
                    pageStack.push(Qt.resolvedUrl("../pages/DialogsPage.qml"),{attachmentId:"wall"+rootWallpost.from_id+"_"+rootWallpost.post_id});
                }

            }
            MenuItem{
                text:qsTr("В группу (не реализ)")
                onClicked: {
                }
            }
            MenuItem{
                text:qsTr("На стену (не реализ)")
                onClicked: {
                }
            }
        }
    }
    Component{
        id:usageMenu
        ContextMenu{
            MenuItem{
                text:qsTr("Скопировать текст")
                onClicked: {
                    clipboardText.text = text_d;
                    clipboardText.selectAll();
                    clipboardText.copy();
                }

            }
            MenuItem{
                text:qsTr("Скопировать ссылку")
                onClicked: {
                    var text = "https://vk.com/wall"+from_id+"_"+post_id
                    clipboardText.text = text;
                    clipboardText.selectAll();
                    clipboardText.copy();
                }
            }
            MenuItem{
                text:qsTr("Удалить запись")
                visible: from_id == globalSettings.User_Id || isGroupAdmin
                onClicked: {
                        pageStack.push(areYouSureDialogPage)
                }
            }
        }
    }

    Component.onCompleted: {
        if(childcopyWallpost!=null)
        {
            //  childWallpost.setSource("../pages/RWallpostPage.qml",{"item":childcopyWallpost,"width":parent.width})
        }
    }

    Component {
        id: areYouSureDialogPage

        Dialog {
            acceptDestinationAction: PageStackAction.Pop
            Column
            {
                width:parent.width
                DialogHeader {
                    acceptText: qsTr("Удалить")
                    cancelText: qsTr("Отменить")
                }

                Label{
                    text:qsTr("Вы уверены что хотите удалить запись?")
                    width: parent.width-2*Theme.horizontalPageMargin
                    wrapMode:Text.WordWrap
                    x:Theme.horizontalPageMargin
                }
            }
            onAccepted: {
                vkapi.wall_delete(ownerid,post_id);
                remove()
            }
        }
    }
}
