import QtQuick 2.0
import Sailfish.Silica 1.0
import VKItems 1.0
import "../items"

MessageItem {
        property var item
 //       width:parent.width
        anchors.right: parent.right
        isOut: true
        avatar_photo: item.photo50
        author:item.firstName+" "+item.lastName
        bodylabel: item.body
        grAtt: item.GridAttachments
        ltAtt:item.ListAttachments
        videoAtt: item.VideoAttachments
        docAtt: item.DocAttachments
        gifAtt: item.GifAttachments
        stickAtt:item.stickerAttachments
        dt: Format.formatDate(new Date(item.date*1000), Formatter.TimepointRelative)
        fwdMsg: item.fwdMessage
        Component.onCompleted: {
        console.log("childQml",item.GridAttachments,item)
        }

    }


