import QtQuick 2.0
import Sailfish.Silica 1.0
import QtMultimedia 5.0
import "qrc:/helpers.js" as Helper

ListItem {
    id:rootComment
    x: Theme.paddingLarge
    width: parent.width - 2*Theme.paddingLarge
    contentWidth: width
    contentHeight: bodylabel.height+avatar.height+dateLabel.contentHeight+20+attachments.attachmentsHeight
    property color primcolor: Theme.primaryColor
    property color seccolor: Theme.secondaryColor
    property alias attachWallpost: attachments.copyWallpost


    property bool isOut

    property color highlightColor: Theme.highlightColor
    property color highlightBackgroundColor: Theme.highlightBackgroundColor

    Rectangle{
        id:rect
        color: isOut ? Theme.rgba(seccolor, 0.2) :
                       _showPress ? Theme.rgba(highlightBackgroundColor, Theme.highlightBackgroundOpacity)
                                  : Theme.rgba(primcolor, 0.2)

        width:  rootComment.width
        height: rootComment.contentHeight

        anchors.right: isOut?undefined:parent.right
        Image {
            id:avatar
            source: photo_50
            width:50
            height: 50
            fillMode: Image.PreserveAspectFit
            anchors.top: parent.top
            anchors.left: parent.left
        }
        Label{
            id:authorName
            font.pixelSize: Theme.fontSizeTiny
            color: Theme.secondaryHighlightColor
            text:replyToUserName=="" ? firstName+" "+lastName :
                                      sex!=1 ? firstName+" "+lastName+" "+qsTr("ответил")+" "+replyToUserName
                                             : firstName+" "+lastName+" "+qsTr("ответила")+" "+replyToUserName
            anchors {
                left:avatar.right
                verticalCenter: avatar.verticalCenter
                leftMargin: 5
            }
        }
        Label {
            id:bodylabel
            text: Helper.insertToVK(Helper.insertIDToVK(Helper.insertLinks(bodyText)))
            linkColor: Theme.highlightColor

            color: rootComment.highlighted ? Theme.highlightColor : Theme.secondaryColor
            anchors.top: avatar.bottom
            anchors.left: parent.left
            anchors.topMargin: 10
            width: Math.min(implicitWidth,rootComment.width)
            textFormat: Text.StyledText
            height: contentHeight
            horizontalAlignment: Text.AlignLeft
            wrapMode: Text.Wrap
            font.pixelSize:  Theme.fontSizeSmall

            onLinkActivated:{
                console.log("Link clicked");
                    if(link.substring(0,4)=="club")
                           pageStack.push(Qt.resolvedUrl("qrc:///qml/pages/GroupPage.qml"),{group_id:parseInt(link.substring(4,link.length))}) //умножаем на -1,т.к. является группой
                    else if (link.substring(0,2)=="id")
                        pageStack.push(Qt.resolvedUrl("qrc:///qml/pages/UserPage.qml"),{usr_id:parseInt(link.substring(2,link.length))}) //умножаем на -1,т.к. является группой
                    else
                     Qt.openUrlExternally(link)
            }
        }

        AttachmentsView {
            id:attachments
            pandvmodel:gridAttachments
            audiomodel: listAttachments
            videomodel: videoAttachments
            docmodel: docAttachments
            gifmodel: gifAttachments
            stickermodel: stickerAttachments
            width:rootComment.width
            anchors {
                top: bodylabel.bottom
                left: bodylabel.left
                //                right: parent.right
            }
        }
        Label {
            id:dateLabel
            text: Format.formatDate(new Date(date*1000), Formatter.TimepointRelative)
            color: rootComment.highlighted ? Theme.highlightColor : Theme.secondaryColor
            anchors.bottom:parent.bottom
            anchors.left: parent.left
            font.pixelSize: Theme.fontSizeExtraSmall
        }
    }

}
