import QtQuick 2.0
import Sailfish.Silica 1.0
import QtMultimedia 5.0
Item{
    /*
type
integer 	тип документа. Возможные значения:

    1 — текстовые документы;
    2 — архивы;
    3 — gif;
    4 — изображения;
    5 — аудио;
    6 — видео;
    7 — электронные книги;
    8 — неизвестно.
  */
    property alias pandvmodel: photosVideosGrid.model
    property alias videomodel: videosGrid.model
    property alias audiomodel: listView.model
    property alias docmodel: docView.model
    property alias gifmodel: gifGridView.model
    property alias stickermodel :stickerRepeater.model
    property bool hasAttachments: photosVideosGrid.count>0 || videosGrid.count>0 || listView.count>0 || docView.count>0||gifGridView.count|| copyWallpost==null>0 ||stickerRepeater.count>0? 1 : 0
    property bool hasStickers: stickerRepeater.count>0
    property var copyWallpost
    property bool fadetext:true

    property int attachmentsHeight: photosVideosGrid.height+videosGrid.height+docView.height+gifGridView.height + listView.height +childWallpost.height+stickerRepeater.height
    height: attachmentsHeight
    property int parentWidth: parent.width
    //width:parent.width-2*Theme.paddingSmall
    width:  copyWallpost==null?Math.max(photosVideosGrid.width,videosGrid.width,gifGridView.width,stickerRepeater.width):parentWidth//,docView.width,listView.width)
    signal stopPlay()
    signal startPlay()

    property bool playing: false

    onStopPlay:
    {
        playMusic.stop()
        playing=false;
        console.log(playing)
    }
    onStartPlay: {
        playMusic.play();
        playing=true;
        console.log(playing)
    }

    SilicaGridView{
        id: photosVideosGrid
        width: photosVideosGrid.count >2 ? imageWidth*3 :photosVideosGrid.count*imageWidth
        anchors {
            topMargin:photosVideosGrid.count==0?0:10
        }

        // flicking: false
        property int imageWidth: parentWidth/3
        cellWidth: imageWidth
        cellHeight: imageWidth
        //   height:photosGrid.count%3===0 ?  Math.floor(photosGrid.count/3)*imageWidth : (Math.floor(photosGrid.count/3)+1)*imageWidth
        height: Math.ceil(photosVideosGrid.count/3)*imageWidth
        delegate:    Item {
            id:imageWrapper
            width: photosVideosGrid.imageWidth
            height: width

            Image {
                width: parent.width-2*Theme.paddingSmall
                height: width
                anchors.centerIn: parent
                id:picture
                asynchronous: true
                opacity: 1
                //          opacity: status === Image.Ready ? 1 : 0.1 //hack for constant size
                source: modelData.photo_75 ? modelData.photo_130 : modelData.photo_75
                fillMode: Image.PreserveAspectCrop
                clip: true


                Behavior on opacity {
                    NumberAnimation {
                        easing.type: Easing.InOutQuad
                    }
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked:
                    {
                        pageStack.push("qrc:/qml/pages/ImageGalleryPage.qml",{model:photosVideosGrid.model,currentIndex:index})}
                }
            }
            BusyIndicator {
                running: picture.status != Image.Ready
                size:BusyIndicatorSize.Medium
                anchors.centerIn: imageWrapper

            }
        }
    }
    SilicaGridView{
        id: videosGrid
        width: videosGrid.count >2 ? imageWidth*3 :videosGrid.count*imageWidth
        anchors {
            top:photosVideosGrid.bottom
            topMargin:photosVideosGrid.height==0?0:10
        }

        // flicking: false
        property int imageWidth: parentWidth/3
       // spacing: Theme.paddingSmall
        //        columns: mm
        cellWidth: imageWidth
        cellHeight: imageWidth
        //   height:photosGrid.count%3===0 ?  Math.floor(photosGrid.count/3)*imageWidth : (Math.floor(photosGrid.count/3)+1)*imageWidth
        height: Math.ceil(videosGrid.count/3)*imageWidth
        delegate:    Item {
            width: videosGrid.imageWidth
            height: width
            Image {
                width: parent.width-2*Theme.paddingSmall
                height: width
                anchors.centerIn: parent
                asynchronous: true
                opacity: 1
                //          opacity: status === Image.Ready ? 1 : 0.1 //hack for constant size
                source: modelData.photo_130
                fillMode: Image.PreserveAspectCrop
                clip: true


                Behavior on opacity {
                    NumberAnimation {
                        easing.type: Easing.InOutQuad
                    }
                }
                IconButton{
                    anchors.centerIn: parent
                    icon.source: "image://theme/icon-l-play?" + (pressed
                                                                 ? Theme.highlightColor
                                                                 : Theme.primaryColor)
                    onClicked: {

                        console.log("player "+modelData.player)
                        pageStack.push("qrc:///qml/pages/VideoPage.qml",{
                                           thumbnail: modelData.photo_320,
                                           duration: modelData.duration,
                                           owner_id:modelData.owner_id,
                                           access_token:modelData.access_key,
                                           id:modelData.id,
                                           postDate:modelData.date,
                                           owner_n:"test",
                                           owner_ava_source:modelData.photo_130

                                       })

                    }
                }
                BusyIndicator {
                    running: parent.status != Image.Ready
                    size:BusyIndicatorSize.Medium
                    anchors.centerIn: parent

                }
            }

        }
    }
    SilicaGridView{
        id: gifGridView
        width: gifGridView.count >2 ?imageWidth*3 :gifGridView.count*imageWidth
        anchors {
            top:videosGrid.bottom
            topMargin:videosGrid.height==0?0:10
        }

        // flicking: false
        property int imageWidth: parentWidth/3
        cellWidth: imageWidth
        cellHeight: imageWidth
        height: Math.ceil(gifGridView.count/3)*imageWidth
        delegate:  Item{
            width: gifGridView.imageWidth
            height:width
            AnimatedImage{
                BusyIndicator{
                    size: BusyIndicatorSize.Medium
                    anchors.centerIn: parent
                    running: parent.status == Image.Loading
                }
                playing: false
                source: modelData.preview.photo.sizes[0].src
                anchors.centerIn:  parent
                width: parent.width-2*Theme.paddingSmall
                height:width

                MouseArea{
                    anchors.fill: parent
                    onClicked: pageStack.push(Qt.resolvedUrl("../pages/GifViewerPage.qml"),{gifValue:modelData,src:modelData.url})
                }
            }
            Rectangle{
                x:Theme.paddingSmall
                width: parent.width-2*Theme.paddingSmall
                height: parent.height/3
                anchors.bottom: parent.bottom
                color: "black"
                opacity: 0.5
                    Label{
                        font.pixelSize: Theme.fontSizeSmall
                        anchors.left: parent.left
                        width: parent.width-gifSize.width
                        text:modelData.title
                        elide:Text.ElideRight
                    }
                    Label{
                        id:gifSize
                        text:Format.formatFileSize(modelData.size)
                        font.pixelSize: Theme.fontSizeSmall
                        anchors.right: parent.right
                    }

            }
        }
    }
    SilicaListView {
        id: listView
        width: parent.width
        height: listView.count*Theme.itemSizeMedium

        anchors {
            top:videosGrid.bottom
            topMargin:listView.height==0?0:10
            leftMargin: listView.height==0?0:20
        }


        delegate: ListItem {
            id:listItem
            width:parent.width
            contentWidth: width
            contentHeight: Theme.itemSizeMedium
            IconButton {
                id: play
                anchors.leftMargin: Theme.paddingLarge
                icon.source: "image://theme/icon-m-play"
                //                anchors.horizontalCenter: parent.horizontalCenter
                onClicked:   {

                    playMusic.source = modelData.url;
                    playMusic.songName = modelData.title;
                    playMusic.gangName = modelData.artist;
                    if(playMusic.playbackState != Audio.PlayingState)
                    {
                        play.icon.source = "image://theme/icon-l-pause"
                        startPlay()
                    }
                    else
                    {
                        play.icon.source = "image://theme/icon-l-play"
                        stopPlay();
                    }
                    //           enabled: !iconButtons.playing
                }
            }
            Label {
                id:titlelabel
                text: modelData.title
                elide:TruncationMode.Elide
                color: listItem.highlighted ? Theme.highlightColor : Theme.secondaryColor
                width:parent.width
                anchors.left: play.right
                anchors.leftMargin: Theme.paddingLarge
                anchors.horizontalCenter: parent.horizontalCenter

            }
            Label {
                id:artistlabel
                text: modelData.artist
                elide:TruncationMode.Elide
                width:parent.width
                color: listItem.highlighted ? Theme.highlightColor : Theme.secondaryColor
                anchors.top: titlelabel.bottom
                anchors.left: titlelabel.left

            }
        }

    }
    SilicaListView {
        id: docView
        width: parent.width
        height: docView.count*Theme.itemSizeMedium

        anchors {
            top:listView.bottom
            topMargin:docView.height==0?0:10
            leftMargin: docView.height==0?0:20
        }


        delegate:ListItem{
            contentHeight: Theme.itemSizeMedium
            Row{
                width: parent.width
                Image {
                    id: iconTypeImage
                    Component.onCompleted: {
                        switch(modelData.type){
                        case 1:
                            source= "image://theme/icon-m-file-document"
                            break;
                        case 4:
                            source= "image://theme/icon-m-file-image"
                            break;
                        case 5:
                            source= "image://theme/icon-m-file-audio"
                            break;
                        case 6:
                            source= "image://theme/icon-m-file-video"
                            break;
                        case 7:
                            source= "image://theme/icon-m-file-formatted"
                            break;
                        default:
                            source= "image://theme/icon-m-file-other"
                            break;
                        }
                    }
                }
                Column{
                    width:parent.width-iconTypeImage.width
                    Label{
                        text:modelData.title
                        font.pixelSize: Theme.fontSizeSmall
                        width: parent.width
                        elide: Text.ElideMiddle
                    }
                    Label{
                        text:Format.formatFileSize(modelData.size)
                        font.pixelSize: Theme.fontSizeSmall

                    }
                }

            }
            menu: ContextMenu {
                x:Theme.paddingSmall
                MenuItem {
                    text: qsTr("Скачать")
                    onClicked: vkapi.img_save(modelData.url,modelData.type)
                }
            }
        }
    }
    Repeater{
    id:stickerRepeater

    anchors {
        top:docView.bottom
        topMargin:stickerRepeater.count==0?0:10
        leftMargin: stickerRepeater.count==0?0:20
    }

    delegate: Image{
        source: modelData.photo_256
        onWidthChanged: stickerRepeater.width=width
        onHeightChanged: stickerRepeater.height=height
    }
    }
    Loader {
        id:childWallpost
        anchors.top:listView.bottom
        width:parent.width
        height:childrenRect.height
    }
    onCopyWallpostChanged:  {
        if(copyWallpost!=null)
        {
            childWallpost.setSource("../pages/RWallpostPage.qml",{"item":copyWallpost,"fadetext":fadetext,"width":width-Theme.paddingSmall,"comments_canPost":false})
        }
    }
}
