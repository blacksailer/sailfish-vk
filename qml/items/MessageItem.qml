import QtQuick 2.0
import Sailfish.Silica 1.0
import QtMultimedia 5.0
import "qrc:/helpers.js" as Helper

ListItem {
    id:rootMessage
    width: parent.width
    property int contentColumnHeight: bodylabel.height+fwdMessage.height+attachments.attachmentsHeight+attachments.anchors.topMargin+attachments.anchors.bottomMargin
    contentWidth: width
    contentHeight: Math.max(contentColumnHeight,userInfoColumn.height)+dateLabel.height+Theme.paddingSmall

    highlighted: !read_state
    property color primcolor: Theme.primaryColor
    property color seccolor: Theme.secondaryColor
    property alias attachWallpost: attachments.copyWallpost

    property var fwdMsg:null

    property bool isOut
    property alias avatar_photo: avatar.source
    property alias author:authorName.text
    property alias bodylabel:bodylabel.text

    property alias grAtt: attachments.pandvmodel
    property alias ltAtt: attachments.audiomodel
    property alias videoAtt:attachments.videomodel
    property alias docAtt:attachments.docmodel
    property alias gifAtt:attachments.gifmodel
    property alias stickAtt:attachments.stickermodel

    property alias dt:dateLabel.text

    property color highlightColor: Theme.highlightColor
    property color highlightBackgroundColor: Theme.highlightBackgroundColor

Item{
    id:messageWrapper
    x: Theme.paddingLarge
    width: parent.width-2*x
    anchors.bottom:parent.bottom
    anchors.bottomMargin: dateLabel.height+dateLabel.anchors.topMargin

    Column{
        id:contentColumn
        width: fwdMsg==null? Math.max(bodylabel.width+2*Theme.paddingSmall,attachments.width):parent.width-userInfoColumn.width-Theme.paddingSmall
        anchors.left: isOut?userInfoColumn.right:undefined
        anchors.right: isOut?undefined:userInfoColumn.left
        anchors {
        leftMargin: isOut?Theme.paddingSmall:0
            rightMargin: isOut?0:Theme.paddingSmall
            bottom:parent.bottom

        }
        Rectangle{
            id:rect
            color: attachments.hasStickers ? "transparent":
                isOut ? Theme.rgba(seccolor, 0.2) :
                           _showPress ? Theme.rgba(highlightBackgroundColor, Theme.highlightBackgroundOpacity)
                                      : Theme.rgba(primcolor, 0.2)

            width: parent.width

            height: rootMessage.contentColumnHeight

            Label {
                id:bodylabel
                text: Helper.insertIDToVK(Helper.insertLinks(Helper.insertToVK(body)))
                x:Theme.paddingSmall
                textFormat: Text.StyledText
                horizontalAlignment: Text.AlignLeft
                linkColor: Theme.highlightColor
                font.pixelSize:  Theme.fontSizeSmall

                wrapMode: Text.Wrap
                color: rootMessage.highlighted ? Theme.highlightColor : Theme.secondaryColor

                //Почему 34? Да потому что посчитал я сам количество символов в виртуальной машине
                width: text.length<34?paintedWidth:messageWrapper.width-userInfoColumn.width-3*Theme.paddingSmall//Math.min((implicitWidth),messageWrapper.width-userInfoColumn.width-Theme.paddingSmall)

                onLinkActivated:{
                    console.log("Link clicked");
                    if(link.indexOf("vk.com")!==-1)
                    {
                        var urlArray;
                        urlArray=link.split("/");
                        console.log(urlArray);
                        if(urlArray[urlArray.length-1].substring(0,4)==="club")
                        {
                            console.log("group")
                            pageStack.push(Qt.resolvedUrl("qrc:///qml/pages/GroupPage.qml"),{group_id:parseInt(urlArray[urlArray.length-1].substring(4,urlArray[urlArray.length-1].length))}) //умножаем на -1,т.к. является группой
                        }
                        else if (urlArray[urlArray.length-1].substring(0,2)==="id")
                        {
                            console.log("user",parseInt(urlArray[urlArray.length-1].substring(2,urlArray[urlArray.length-1].length)))
                            pageStack.push(Qt.resolvedUrl("qrc:///qml/pages/UserPage.qml"),{usr_id:parseInt(urlArray[urlArray.length-1].substring(2,urlArray[urlArray.length-1].length))}) //умножаем на -1,т.к. является группой
                        }
                        else
                            Qt.openUrlExternally(link)
                    }
                    else
                        Qt.openUrlExternally(link)
                }


                states: [
                    State {
                        name: "no text"
                        when:  bodylabel.text.length===0
                        PropertyChanges {
                            target: bodylabel
                            height: 0
                        }

                    }
                ]
            }

            AttachmentsView {
                id:attachments
                pandvmodel:gridAttachments
                audiomodel: listAttachments
                videomodel: videoAttachments
                docmodel: docAttachments
                gifmodel: gifAttachments
                stickermodel: stickerAttachments
                parentWidth: messageWrapper.width-userInfoColumn.width-Theme.paddingSmall
                anchors {
                    top:bodylabel.bottom
                    topMargin:height==0?0:10
                    bottomMargin:height==0?0:10
                }
            }

            Loader {
                id:fwdMessage
                anchors.top:attachments.bottom
                width:parent.width
//                height:childrenRect.height
            }
        }
    }
    Column{
        id:userInfoColumn
        anchors.bottom: contentColumn.bottom
        anchors.right: isOut?undefined:parent.right
        width:50
        Image {
            id:avatar
            source: photo_50
            width:50
            height: 50
            fillMode: Image.PreserveAspectFit
            MouseArea{
            anchors.fill: parent
            onClicked: {
                if(user_id>0)
                    pageStack.push(Qt.resolvedUrl("qrc:///qml/pages/UserPage.qml"),{usr_id:user_id})
                else
                    pageStack.push(Qt.resolvedUrl("qrc:///qml/pages/GroupPage.qml"),{group_id:user_id})

            }
            }
        }
        Label{
            id:authorName
            font.pixelSize: Theme.fontSizeTiny
            color: Theme.secondaryHighlightColor
            visible: false
        }
    }
    Label {
        id:dateLabel
        text: Format.formatDate(new Date(date*1000), Formatter.TimepointRelative)
        color: rootMessage.highlighted ? Theme.highlightColor : Theme.secondaryColor
        font.pixelSize: Theme.fontSizeExtraSmall
        anchors.right: isOut?undefined:userInfoColumn.right
        anchors.left: isOut?userInfoColumn.left:undefined
        anchors.top:userInfoColumn.bottom
        anchors.topMargin: Theme.paddingSmall
    }



}
    Component.onCompleted: {
        if(fwdPost!=null)
        {
            attachments.attWallpost.setSource("../pages/RWallpostPage.qml",{"item":fwdPost,"width":parent.width})
        }
        if(fwdMsg!=null)
        {
            fwdMessage.setSource("FwdMessageItem.qml",{"item":fwdMsg})//,"width":messageWrapper.width})//parent.width-2*Theme.paddingLarge})

        }

    }
}
