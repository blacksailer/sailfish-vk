import QtQuick 2.0
import Sailfish.Silica 1.0
Page{
    id:page
    Column {
        anchors.topMargin: Theme.paddingLarge
        x: Theme.horizontalPageMargin
        width: parent.width-2*x
        spacing: 10
        Item{
            //Костылебаг?
            width: parent.width
            height:Theme.itemSizeMedium
        }
        Image{
            anchors.horizontalCenter: parent.horizontalCenter
            source: "qrc:/harbour-SK.png"
        }

        Label {
            id: label
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            wrapMode: Text.WordWrap
            text: qsTr("SK - приложение для социальной сети VK.com.
                        Разработчик - blacksailer.
                        Пишите issue в gitlab и taiga, нужен фидбек!
                        Заходите в группу, пишите пожелания.
                        ") +Qt.application.version
        }
        ListItem {
            contentHeight: Theme.itemSizeMedium
            Label{
                id:vkLabel
                text: qsTr("Группа ВК:")
                color: parent.highlighted ? Theme.highlightColor : Theme.primaryColor
            }
            Label{
                anchors.top:vkLabel.bottom
                text: "https://vk.com/club138396327"
                color: parent.highlighted ? Theme.highlightColor : Theme.primaryColor
            }
            onClicked:pageStack.replace("GroupPage.qml",{group_id:138396327});
        }
        ListItem {
            contentHeight: Theme.itemSizeMedium
            Label{
                id:contactLabel
                text: qsTr("Контакты:")
                color: parent.highlighted ? Theme.highlightColor : Theme.primaryColor
            }
            Label{
                anchors.top:contactLabel.bottom
                text: "blacksailer@yandex.ru"
                color: parent.highlighted ? Theme.highlightColor : Theme.primaryColor
            }
        }
        ListItem {
            contentHeight: Theme.itemSizeMedium
            Image{
                id:twitterImage
                height: parent.height
                width:height
                source: "qrc:/logo-twitter.png"
            }
            Label{
                anchors.left:twitterImage.right
                text: "@black_sailer"
                color: parent.highlighted ? Theme.highlightColor : Theme.primaryColor
            }
            onClicked: Qt.openUrlExternally("https://twitter.com/black_sailer")

        }
        ListItem {
            contentHeight: Theme.itemSizeMedium
            Image{
                id:taigaImage
                height: parent.height
                width:height
                source: "qrc:/logo-taiga.png"
            }
            Label{
                anchors.left:taigaImage.right
                text: "sailfish-vk"
                color: parent.highlighted ? Theme.highlightColor : Theme.primaryColor
            }
            onClicked: Qt.openUrlExternally("https://tree.taiga.io/project/blacksailer-sailfish-vk/")
        }
        ListItem {
            contentHeight: Theme.itemSizeMedium
            Image{
                id:gitlabImage
                height: parent.height
                width:height
                source: "qrc:/logo-gitlab.png"
            }
            Label{
                anchors.left:gitlabImage.right
                text: "sailfish-vk"
                color: parent.highlighted ? Theme.highlightColor : Theme.primaryColor
            }
            onClicked: Qt.openUrlExternally("https://gitlab.com/blacksailer/sailfish-vk")
        }
    }
}

