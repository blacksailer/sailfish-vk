import QtQuick 2.0
import Sailfish.Silica 1.0
import VKModels 1.0
import "../items"

Page{
    id: page
    property int group_id


    GroupInfoC{
        id:groupInfo
        Component.onCompleted: DataProvider = c_dataProvider
    }

    SilicaListView {
        id: listView
        model: groupWallModel
        anchors.fill: parent
        spacing:30
        NumberAnimation { id: anim; target: listView; property: "contentY"; duration: 500 }
        PullDownMenu{
            MenuItem{
                text:qsTr("Скопировать ссылку")
                onClicked: {
                    var text = "https://vk.com/club"+groupInfo.id
                    clipboardText.text = text;
                    clipboardText.selectAll();
                    clipboardText.copy();
                }
            }

            MenuItem{
                text: qsTr("Написать сообщение")
                visible: groupInfo.can_message
                onClicked:{
                    pageStack.push(Qt.resolvedUrl("MessagesHistoryPage.qml"),{chat_id:groupInfo.id*-1 , headertitle:groupInfo.name})
                }

            }
        }
        PushUpMenu {
            id:pulley
            MenuItem {
                text: qsTr("Добавить прошлые записи")
                onClicked:
                {
                    var pos = listView.contentY;
                    var destPos;
                    listView.positionViewAtIndex(listView.count-2,ListView.End)
                    destPos = listView.contentY;
                    anim.from = pos;
                    anim.to = destPos;
                    anim.running = true;
                    vkapi.wall_get(group_id*-1,20,listView.count);
                }
            }
        }


        header: GroupsInfo {
            groupInformation: groupInfo
        }

        delegate:
            RecWallpost{
            id:dataWall
            post_id: id
            owner_ava_source: from_photo
            isGroupAdmin:groupInfo.is_admin

            owner_n: from_name + " " + from_surname
            ownerid:owner_id
            from_id:source_id
            post_d: date
            text_d: datatext

            model:attachments
            audiomodel:listAttachments
            videomodel: videoAttachments
            docmodel: docAttachments
            gifmodel:gifAttachments
            fadetext: true

            like_count: likes_count
            repost_count: reposts_count
            user_likes: user_islikes
            can_like: user_canlike
            comments_count : commentsCount

            childcopyWallpost: groupWallModel.getChild(id)

            onClicked: {
                pageStack.push("RecSinglePostPage.qml",{
                                   post_id: id,
                                   ownerid:owner_id,
                                   owner_ava_source: from_photo,
                                   owner_n: from_name +" "+ from_surname,
                                   from_id:source_id,


                                   post_d: date,
                                   text_d: datatext,
                                   model:attachments,
                                   audiomodel:listAttachments,
                                   videomodel: videoAttachments,
                                   docmodel: docAttachments,
                                   gifmodel: gifAttachments,
                                   comments_count : commentsCount,
                                   comments_canPost:commentsCanPost,
                                   like_count: likes_count,
                                   repost_count: reposts_count,
                                   user_likes:user_islikes,
                                   can_like: user_canlike,
                                   childWallpost: groupWallModel.getChild(id)

                               })
            }
            onRemove:{            groupWallModel.remove(index)
}
        }

    }
    WallModel {
        id:groupWallModel
        Component.onCompleted: DataProvider = c_dataProvider
        isActive: groupInfo.isActive
    }
    Component.onCompleted:{
        groupWallModel.clear_Wall();
        var allFields ="id,is_closed,deactivated,is_admin,admin_level,is_member,has_photo,activity,age_limits,can_create_topic,can_message,can_post,can_see_all_posts,can_upload_doc,can_upload_video,description,fixed_post,is_favorite,is_hidden_from_feed,is_messages_allowed,main_album_id,main_section,member_status,members_count,public_date_label,site,start_date,finish_date,status,verified,wiki_page,city,counters,contacts,links,ban_info,cover,market,place";
        vkapi.group_getbyid(group_id,allFields);//"photo_200,photo_max,is_member,is_admin");

        vkapi.wall_get(group_id*-1,20,0);
    }
    onStatusChanged: {
        console.log(groupWallModel.isActive)
        if(status == PageStatus.Active)
            groupInfo.isActive = true;
        else if(status == PageStatus.Activating)
            groupInfo.isActive = true;
        else
            groupInfo.isActive = false;
    }
    Component {
        id: areYouSureDialogPage

        Dialog {
            acceptDestination: page
            acceptDestinationAction: PageStackAction.Pop
            Column
            {
                width:parent.width
                DialogHeader {
                    acceptText: "Отписаться"
                    cancelText: "Отменить"
                }

                Label{
                    text:"Вы уверены что хотите отписаться?"
                    width: parent.width-2*Theme.paddingLarge
                    wrapMode:Text.WordWrap
                    x:Theme.paddingLarge
                }

                Item {
                    y: Theme.paddingLarge
                    x: Theme.paddingLarge
                    opacity: 1
                    width: parent.width - 2*Theme.paddingLarge
                    height:page_avatar.height+ name_info.height+ status_info.height
                    Image{
                        id:page_avatar
                        height: 200
                        width: height
                        fillMode: Image.PreserveAspectCrop
                        source: groupInfo.photo_100
                        anchors{
                            topMargin: 20
                            top:parent.top
                            horizontalCenter: parent.horizontalCenter
                        }
                    }
                    Label{
                        id:name_info
                        text:groupInfo.name
                        anchors {
                            leftMargin: 10
                            top:page_avatar.bottom
                            horizontalCenter: parent.horizontalCenter
                        }
                    }
                    Label{
                        id:status_info
                        text:groupInfo.status
                        color:Theme.secondaryHighlightColor

                        font.pixelSize: Theme.fontSizeSmall
                        wrapMode:Text.WordWrap
                        width: parent.width
                        horizontalAlignment: Text.AlignHCenter
                        anchors{
                            top: name_info.bottom
                            horizontalCenter: parent.horizontalCenter
                        }
                    }
                }
            }
            onAccepted: {
                vkapi.group_leave(group_id);
                groupInfo.setIsMember(false);
            }
        }
    }
    Component {
        id: addSubsDialogPage

        Dialog {
            acceptDestination: page
            acceptDestinationAction: PageStackAction.Pop
            Column
            {
                width:parent.width
                DialogHeader {
                    acceptText: "Подписаться"
                    cancelText: "Отменить"
                }

                Item {
                    y: Theme.paddingLarge
                    x: Theme.paddingLarge
                    opacity: 1
                    width: parent.width - 2*Theme.paddingLarge
                    height:page_avatar.height+ name_info.height+ status_info.height
                    Image{
                        id:page_avatar
                        height: 200
                        width: height
                        fillMode: Image.PreserveAspectCrop
                        source: groupInfo.photo_100
                        anchors{
                            topMargin: 20
                            top:parent.top
                            horizontalCenter: parent.horizontalCenter
                        }
                    }
                    Label{
                        id:name_info
                        text:groupInfo.name
                        anchors {
                            leftMargin: 10
                            top:page_avatar.bottom
                            horizontalCenter: parent.horizontalCenter
                        }
                    }
                    Label{
                        id:status_info
                        text:groupInfo.status
                        color:Theme.secondaryHighlightColor

                        font.pixelSize: Theme.fontSizeSmall
                        wrapMode:Text.WordWrap
                        width: parent.width
                        horizontalAlignment: Text.AlignHCenter
                        anchors{
                            top: name_info.bottom
                            horizontalCenter: parent.horizontalCenter
                        }
                    }
                }


            }
            onAccepted: {
                vkapi.group_join(group_id,false);
                groupInfo.setIsMember(true);
            }
        }
    }
}
