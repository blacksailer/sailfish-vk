import QtQuick 2.0
import Sailfish.Silica 1.0
import VKModels 1.0
import "../items"

import "../../MessageSendNetwork.js" as Send
Page {
    id:videoPage
    //Шапка с данными владьца
    //*
    //*
    //*
    //  ------  ------  -----//
    //  ScreenshotVideo -----//
    //  ------  ------  -----//
    //*
    //*
    //------Comments------//

    property string thumbnail: thumbnailPhoto.source
    property int duration: 0
    property int id
    property int owner_id
    property int postDate
    property string owner_n
    property string owner_pic_link
    property string videoTitle: ""
    signal setInfo(var name,var pic);
    property string owner_ava_source
    property string access_token:""
    property url playerUrl
    // get author_avatar sorce
    // get owner_name text

    CommentsModel{
        id:commentsModel
        onCommentsCountChanged:console.log("commentsCount="+commentsCount)
        Component.onCompleted:    DataProvider=c_dataProvider

    }

    SilicaListView{
        header:headerComponent
        id:commentsView
        width: parent.width
        height:parent.height
        anchors.top:parent.top
        anchors.topMargin: Theme.paddingMedium
        spacing: Theme.paddingMedium
        model: commentsModel
        clip:true

        delegate: CommentItem{
            width: parent.width - 2*Theme.paddingLarge
            x:Theme.paddingLarge

        }
        NumberAnimation { id: anim; target: commentsView; property: "contentY"; duration: 500 }

        PushUpMenu{
            visible:commentsView.count!==commentsModel.commentsCount
            MenuItem {
                text: qsTr(("Загрузить еще 20 из %1 комментариев").arg(commentsModel.commentsCount-commentsView.count))
                onClicked:{
                    var pos = commentsView.contentY;
                    var destPos;
                    commentsView.positionViewAtIndex(commentsView.count-2, ListView.End);
                    destPos = commentsView.contentY;
                    anim.from = pos;
                    anim.to = destPos;
                    anim.running = true;
                    vkapi.video_GetComments(owner_id,id,commentsModel.lastId)
                }

            }
        }
    }


    Connections{
        target:c_dataProvider
        onVideoGenerated:{
            console.log(value.owner_name)
            playerUrl=value.player
            owner_n=value.owner_name
            owner_ava_source=value.photo_50
            videoTitle=value.title
            console.log(value.player)
        }
    }
    onStatusChanged: {
        if(status == PageStatus.Active)
            commentsModel.isActive = true;
        else if(status == PageStatus.Activating)
            commentsModel.isActive = true;
        else
            commentsModel.isActive = false;
    }
    Component.onCompleted: {
        if(access_token!="")
            vkapi.video_get(owner_id+"_"+id+"_"+access_token);
        else
            vkapi.video_get(owner_id+"_"+id);
            vkapi.video_GetComments(owner_id,id,0);

    }

    Component{
        id:headerComponent
        Column{
            id:headerColumn
            width: parent.width-2* Theme.paddingLarge
            x:Theme.paddingLarge
            y:Theme.paddingLarge
            spacing:Theme.paddingSmall
            BackgroundItem{
                id: owner_info
                height:Theme.itemSizeSmall
                Image {
                    id: author_avatar
                    source: owner_ava_source
                    height: parent.height
                    width: height
                    fillMode: Image.PreserveAspectCrop
                    anchors.left:parent.left
                }
                Label {
                    id: owner_name
                    elide:TruncationMode.Elide
                    text: owner_n
                    color: Theme.primaryColor
                    font.pixelSize: Theme.fontSizeSmall

                    anchors {
                        left: author_avatar.right
                        right: post_date.left
                        rightMargin: Theme.paddingSmall
                    }
                }

                Label {
                    id: post_date
                    text: Format.formatDate(new Date(postDate*1000), Formatter.TimepointRelative)

                    font.pixelSize: Theme.fontSizeExtraSmall
                    color: Theme.primaryColor
                    anchors.right: parent.right
                    anchors.bottom: author_avatar.bottom
                }
                onClicked:{
                    if(owner_id>0)
                        pageStack.push(Qt.resolvedUrl("qrc:///qml/pages/UserPage.qml"),{usr_id:owner_id})
                    else
                        pageStack.push(Qt.resolvedUrl("qrc:///qml/pages/GroupPage.qml"),{group_id:owner_id*-1})
                }

            }
            Label{
                id:titleVideo
                text:videoTitle
                width: parent.width
                elide:Text.ElideRight
                color:Theme.secondaryHighlightColor
                anchors.topMargin: Theme.paddingMedium
                anchors.bottomMargin:  Theme.paddingMedium
                anchors.leftMargin: Theme.paddingMedium
                font {
                    pixelSize: Theme.fontSizeSmall
                    family: Theme.fontFamilyHeading
                }
            }
            Image{
                id:thumbnailPhoto
                source: thumbnail
                width:parent.width
                Label{
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                    text:Format.formatDuration(duration,Formatter.DurationShort)
                }
                Image {
                    id: playButton
                    anchors.centerIn: parent
                    source: "image://theme/icon-l-play"
                }
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        //pageStack.push(Qt.resolvedUrl("VideoWebPlayer.qml"),{src:playerUrl})
                        Qt.openUrlExternally(playerUrl);
                    }
                }
            }

            PageHeader{
                title:qsTr("Комментарии")
            }
            Label
            {
                visible: commentsView.count==0
                anchors.horizontalCenter: parent.horizontalCenter
                color: Theme.secondaryHighlightColor
                font {
                    pixelSize: Theme.fontSizeLarge
                    family: Theme.fontFamilyHeading
                }

                text:qsTr("Нет комментариев")
            }

        }
    }

    function setUserInfo(img,nick)
    {
        setInfo(nick,img);
        owner_n = nick;
    }
}

