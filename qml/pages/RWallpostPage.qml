import QtQuick 2.0
import Sailfish.Silica 1.0
import VKItems 1.0
import "../items"


RecWallpost{
    property var item

    post_id: item.id
    owner_ava_source: item.fromPhoto
    isChild:true
    owner_n: item.fromName + " " + item.FromSurname
    ownerid:item.ownerId
    from_id: item.fromId
    post_d: item.date
    text_d: item.text

    model:item.GridAttachments
    audiomodel:item.ListAttachments
    videomodel: item.VideoAttachments
    docmodel: item.DocAttachments
    gifmodel:item.GifAttachments
    fadetext: true

    like_count: item.Likes_count
    repost_count: item.repostCount
    user_likes: item.userLikes
    can_like: item.userCanLikes
    childcopyWallpost: item.repostedPost

    onClicked: {
        pageStack.push("RecSinglePostPage.qml",{

                           post_id: item.id,
                           owner_ava_source: item.fromPhoto,
                           isChild:true,
                           owner_n: item.fromName + " " + item.FromSurname,
                           ownerid:item.ownerId,
                           from_id:item.fromId,
                           post_d: item.date,
                           text_d: item.text,

                           model:item.GridAttachments,
                           audiomodel:item.ListAttachments,
                           videomodel: item.VideoAttachments,
                           docmodel: item.DocAttachments,
                           gifmodel:item.GifAttachments,
                           fadetext: true,

                           like_count: item.Likes_count,
                           repost_count: item.repostCount,
                           user_likes: item.userLikes,
                           can_like: item.userCanLikes,
                           childcopyWallpost: item.repostedPost

                       })
    }
    Component.onCompleted: {
        //console.log("childQml",item.repostedPost,item.id,owner_n,item.fromName,item.text,height,width)
    }

}


