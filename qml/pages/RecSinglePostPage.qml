import QtQuick 2.0
import Sailfish.Silica 1.0
import VKModels 1.0
import "../items"
import "../../MessageSendNetwork.js" as Net
Page {
    id: wallpostPage
    property int post_id:     wallpost.post_id
    property string owner_ava_source:        wallpost.owner_ava_source
    property string owner_n:        wallpost.owner_n
    property int ownerid:        wallpost.ownerid
    property int from_id: wallpost.from_id
    property int   post_d:     wallpost.post_d
    property string   text_d:     wallpost.text_d

    property var  model:    wallpost.model
    property var  audiomodel:   wallpost.audiomodel
    property var  videomodel
    property var docmodel: wallpost.docmodel
    property var  gifmodel:   wallpost.gifmodel
    property int like_count: wallpost.like_count
    property int repost_count: wallpost.repost_count
    property int comments_count
    property bool comments_canPost:true
    property bool user_likes: wallpost.user_likes
    property bool can_like: wallpost.can_like
    property var childWallpost

    property RecWallpost wallpost:null;

    CommentsModel{
        id:commentsModel
        onCommentsCountChanged:console.log("commentsCount="+commentsCount)
        Component.onCompleted:    DataProvider=c_dataProvider
    }


    WritingItem{
        id:writer
        wall_id: ownerid
        typeWriter: "wall"
        bottomArea.visible:comments_canPost
        SilicaListView{
            id:listView
            width: parent.width
            height: parent.height-writer.sendAreaHeight
            anchors.topMargin: Theme.paddingMedium
            anchors.bottom: parent.bottomArea
            focus:true
            clip:true
            header: wallpostComponent
            spacing: Theme.paddingMedium
            model: commentsModel
            delegate: CommentItem{
                menu: fromId ==globalSettings.User_Id ? ownerContextMenu: contextMenu
                function remove(){

                    remorseAction(qsTr("Удаление"), function() {
                        vkapi.wall_deleteComment(ownerid,id);
                        listView.model.remove(index) })
                           }

                Component {
                    id: ownerContextMenu
                    ContextMenu {
                        MenuItem {
                            text: qsTr("Удалить комментарий")
                            onClicked: remove()
                        }
                        MenuItem {
                            text: qsTr("Ответить")
                            onClicked:
                            {
                                writer.reply_id=id
                                writer.author=firstName+" "+lastName
                                writer.authorsText=bodyText
                            }
                        }
                    }
                }
                Component{
                    id: contextMenu
                    ContextMenu {
                        MenuItem {
                            text: qsTr("Ответить")
                            onClicked:
                            {
                                writer.reply_id=id
                                writer.author=firstName+" "+lastName
                                writer.authorsText=bodyText
                            }
                        }

                    }
                }
            }

            NumberAnimation { id: anim; target: listView; property: "contentY"; duration: 500 }
            PushUpMenu{
                visible:listView.count!==commentsModel.commentsCount
                MenuItem {
                    text: qsTr(("Загрузить еще 20 из %1 комментариев").arg(commentsModel.commentsCount-listView.count))
                    onClicked:{
                        var pos = listView.contentY;
                        var destPos;
                        listView.positionViewAtIndex(listView.count-2, ListView.End);
                        destPos = listView.contentY;
                        anim.from = pos;
                        anim.to = destPos;
                        anim.running = true;
                        vkapi.wall_getComments(ownerid,post_id,commentsModel.lastId)
                    }
                }
            }

        }
        actionButton.onClicked: {
            var Attachments = c_Model.getAllSendUrls()
            console.log(Attachments)
            vkapi.wall_createComment(ownerid,post_id,text,Attachments,writer.reply_id)
            c_Model.clear();
            writer.author=writer.authorsText=""
            writer.reply_id=-1
            text=""
            listView.forceLayout()
        }
        Connections {
            target:commentsModel
            onNewCommentIdChanged:
            {
                vkapi.wall_getComments(ownerid,post_id,commentsModel.newCommentId)
            }
        }
        Component.onCompleted: {
            if(typeWriter=="wall")
            Net.request("wall",ownerid,c_Model);
            console.log(reply_id)
        }
    }


    Component.onCompleted:{
        vkapi.wall_getComments(ownerid,post_id,0)
    }
    onStatusChanged: {
        if(status == PageStatus.Active)
            commentsModel.isActive = true;
        else if(status == PageStatus.Activating)
            commentsModel.isActive = true;
        else
            commentsModel.isActive = false;
    }

    Component{
        id:wallpostComponent
        Column {
            width: parent.width
            RecWallpost{
                id:wallpostItem
                fadetext: false
                Component.onCompleted: {
                    wallpostPage.wallpost = wallpostItem
                    setupWallpost()
                }
            }
            PageHeader{
                title:qsTr("Комментарии")
            }
            Label
            {
                visible: listView.count==0
                anchors.horizontalCenter: parent.horizontalCenter
                color: Theme.secondaryHighlightColor
                font {
                    pixelSize: Theme.fontSizeLarge
                    family: Theme.fontFamilyHeading
                }

                text:qsTr("Нет комментариев")
            }
        }

    }
    function setupWallpost()
    {
        wallpost.post_id = post_id
        wallpost.owner_ava_source=owner_ava_source
        wallpost.owner_n=owner_n
        wallpost.ownerid=ownerid
        wallpost.from_id=from_id
        wallpost.post_d=post_d
        wallpost.text_d=text_d

        wallpost.model=model
        wallpost.audiomodel=audiomodel
        wallpost.videomodel=videomodel
        wallpost.docmodel=docmodel
        wallpost.gifmodel=gifmodel
        wallpost.like_count=like_count
        wallpost.can_like=can_like
        wallpost.comments_canPost=comments_canPost
        wallpost.comments_count = comments_count
        wallpost.repost_count=repost_count
        wallpost.user_likes=user_likes
        wallpost.can_like=can_like
        wallpost.childcopyWallpost=childWallpost


    }
}

