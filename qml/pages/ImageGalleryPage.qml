import QtQuick 2.0
import Sailfish.Silica 1.0
import "../items"


Page {
    id: imagePage
    property bool backStepping: false
    property alias currentIndex: listView.currentIndex
    property alias model: listView.model
    allowedOrientations: Orientation.All
    property url imageUrl;
    property var picData;

    onImageUrlChanged: {
        console.log(imageUrl)}
    backNavigation: backStepping

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent


        // PullDownMenu and PushUpMenu must be declared in SilicaFlickable, SilicaListView or SilicaGridView
        Drawer {
            id: drawer

            anchors.fill: parent
            dock: imagePage.isPortrait ? Dock.Top : Dock.Left

            background:ShareComponent{
                attachmentId: "photo"+listView.model[listView.currentIndex].owner_id+"_"+listView.model[listView.currentIndex].id+"_"+listView.model[listView.currentIndex].access_key;
                attachmentPreview:listView.model[listView.currentIndex].photo_130;
                PullDownMenu {
                    MenuItem {
                        text: qsTr("Сохранить картинку")
                        onClicked: {
                            var picUri = listView.model[listView.currentIndex].photo_1280 ? listView.model[listView.currentIndex].photo_1280 : listView.model[listView.currentIndex].photo_604
                            console.log(picUri)
                            vkapi.img_save(picUri,0)
                        }
                    }
                }
                urlOfSharingItem: imageUrl
            }


            // List view with Sailfish Silica specific behaviour
            SilicaListView {
                id: listView
                clip: true
                snapMode: ListView.SnapOneItem
                orientation: ListView.HorizontalFlick
                highlightRangeMode: ListView.StrictlyEnforceRange
                cacheBuffer: width

                anchors.fill: parent

                delegate: Item {
                    width: listView.width
                    height: listView.height
                    clip: true

                    Image {
                        id:picVk
                        source: modelData.photo_1280 ? modelData.photo_1280 : modelData.photo_604? modelData.photo_604:modelData.photo_130
                        fillMode: Image.PreserveAspectFit
                        sourceSize.height: window.height * 2
                        asynchronous: true
                        anchors.fill: parent

                        PinchArea {
                            anchors.fill: parent
                            pinch.target: parent
                            enabled: true
                            pinch.minimumScale: 1
                            pinch.maximumScale: 4
                        }
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                drawer.open = !drawer.open
                                imagePage.backStepping = !imagePage.backStepping

                            }
                        }
                    }
                    BusyIndicator {
                        running: picVk.status != Image.Ready
                        size:BusyIndicatorSize.Large
                        anchors.centerIn: picVk

                    }

                }

                Binding{
                    target: imagePage; property: "imageUrl"; value: model[listView.currentIndex].photo_1280? model[listView.currentIndex].photo_1280 : model[listView.currentIndex].photo_604;
                }
            }
        }
    }
}

