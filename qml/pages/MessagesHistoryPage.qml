
import QtQuick 2.0
import Sailfish.Silica 1.0
import QtMultimedia 5.0
import VKModels 1.0
import ".."
import "../items"
import "../../MessageSendNetwork.js" as Net
import QtDocGallery 5.0
import org.nemomobile.thumbnailer 1.0


Page{
    id:attachDrawer


    signal addAttach(var src,var type);
    signal addPhotoId(var json,var indx);

    property string attachId:"";
    property string attachPreview;
    property string headertitle;
    property int chat_id;
    property bool isMulti:false
    property string sobesed_photo;
    MessageModel{
        id:historyModel
        Component.onCompleted:
        {
            historyModel.DataProvider=c_dataProvider
            historyModel.userId=chat_id
        }
    }
    WritingItem{
        id:writer
        typeWriter:"message"
        bottomArea.visible:true

        PageHeader{
            id:name_info
            title:headertitle
            anchors {
                leftMargin: 10
            }
        }

                SilicaListView {
                    id: listView
                    model: historyModel
                    verticalLayoutDirection: ListView.BottomToTop
                    focus:!writer.textArea.focus

                    PullDownMenu {
                        id:pulley
                        MenuItem {
                            text: qsTr("добавить прошлые записи")
                            onClicked:
                            {
                                vkapi.messages_getHistory(chat_id,listView.count,20,-1)
                            }
                        }
                    }

                    width: parent.width
                    height: parent.height-writer.sendAreaHeight-name_info.height

                      spacing: 30
                    anchors {
                        top:name_info.bottom
                        bottom: writer.bottomArea.top
                    }
                    clip:true
                    delegate:MessageItem{
                        isOut:out
                        author: isMulti?first_name+ " "+last_name:""
                        fwdMsg: historyModel.getFwdMessage(id)
                        attachWallpost: historyModel.getFwdPost(id)
                    }

                }

        actionButton.onClicked: {
            var Attachments = c_Model.getAllSendUrls()
            console.log(Attachments)
            vkapi.messages_send(chat_id,text,Attachments)
            c_Model.clear();
            text=""
            highlighted=!highlighted
            writer.author=writer.authorsText=""
            writer.reply_id=-1

        }

    }

//    Drawer{
//        id:attachDrawer2
//        open:false
//        dock:Dock.Bottom
//        anchors.fill: parent
//        PageHeader{
//            id:name_info
//            title:headertitle
//            anchors {
//                leftMargin: 10
//            }
//        }

//        AttachmentsToSendModel{
//            id:c_attachmentsModelToSend
//            Component.onCompleted: Net.request("message",0,c_attachmentsModelToSend);
//        }

//        SilicaListView {
//            id: listView
//            model: historyModel
//            anchors.bottom: sendArea.top
//            verticalLayoutDirection: ListView.BottomToTop
//            focus:!messageArea.focus

//            PullDownMenu {
//                id:pulley
//                MenuItem {
//                    text: qsTr("добавить прошлые записи")
//                    onClicked:
//                    {
//                        vkapi.messages_getHistory(chat_id,listView.count,20,-1)
//                    }
//                }
//            }

//            width: parent.width
//            height: parent.height-name_info.height
//            spacing: 30
//            anchors {
//                top:name_info.bottom
//            }
//            clip:true
//            delegate:MessageItem{
//                isOut:out
//                author: isMulti?first_name+ " "+last_name:""
//                fwdMsg: historyModel.getFwdMessage(id)
//                attachWallpost: historyModel.getFwdPost(id)
//            }

//        }
//        Item{
//            id:sendArea
//            anchors.bottom: parent.bottom
//            width: parent.width
//            height: messageArea.height+attachmentstoSend.height+Theme.paddingSmall
//            SilicaListView{
//                id:attachmentstoSend
//                width:parent.width
//                height: attachmentstoSend.count>0?Theme.itemSizeMedium:0
//                anchors.bottom: messageArea.top
//                model:c_attachmentsModelToSend
//                orientation:Qt.Horizontal
//                spacing:5
//                delegate: Image{
//                    width:Theme.itemSizeMedium
//                    height:width
//                    sourceSize: Qt.size(width,height)
//                    IconButton{
//                        icon.source: "image://theme/icon-s-clear-opaque-cross?"+Theme.highlightColor
//                        anchors.centerIn: parent
//                        onClicked: c_attachmentsModelToSend.removeAttachment(index)
//                        Component.onCompleted: {
//                            switch(type) {
//                            case 0: //        PHOTO
//                                if(c_source.substring(0,4)!=="http")
//                                    source="image://nemoThumbnail/"+c_source
//                                else
//                                    source = c_source
//                                break;
//                            case 1: //        VIDEO
//                                if(c_source.substring(0,4)!=="http")
//                                    source="image://nemoThumbnail/"+c_source
//                                else
//                                    source = c_source
//                                break;
//                            case 2://        AUDIO
//                                source="image://theme/icon-m-file-audio"
//                                break;
//                            case 3://        DOC
//                                source="image://theme/icon-m-document"
//                                break;
//                            case 4://        WALL
//                                source="image://theme/icon-m-other"
//                                break;

//                            }

//                        }
//                    }
//                    Rectangle{
//                        color:"black"
//                        visible: c_status>0
//                        opacity: 0.5
//                        anchors.fill: parent
//                    }
//                    ProgressCircle {
//                        id: progressCircle

//                        anchors.centerIn: parent
//                        value: c_progress
//                        visible: c_status>0

//                    }
//                }
//            }
//            IconButton {
//                id:skrepkaWizard
//                icon.source: "image://theme/icon-m-attach"
//                highlighted: false
//                anchors.bottom: messageArea.bottom
//                anchors.right:messageArea.left
//                anchors.bottomMargin: 25
//                onClicked: attachDrawer2.open=true
//            }
//            TextArea {
//                id:messageArea

//                anchors.bottom: parent.bottom
//                anchors.right: sendButton.left

//                height:  Math.min(Theme.itemSizeExtraLarge,implicitHeight)//Math.max(page.height/5, Math.min(page.height/3,implicitHeight))
//                width:parent.width-sendButton.width- skrepkaWizard.width
//            }

//            IconButton {
//                id:sendButton
//                icon.source: "image://theme/icon-m-message"
//                highlighted: false
//                anchors.bottom: messageArea.bottom
//                //                anchors.left:messageArea.right
//                anchors.right:parent.right
//                anchors.bottomMargin: 25

//                onClicked: {
//                    var Attachments = c_attachmentsModelToSend.getAllSendUrls()
//                    console.log(Attachments)
//                    vkapi.messages_send(chat_id,messageArea.text,Attachments)
//                    c_attachmentsModelToSend.clear();
//                    messageArea.selectAll()
//                    messageArea.text=""
//                    highlighted=!highlighted
//                }
//            }
//        }


//        MouseArea {
//            enabled: attachDrawer2.open
//            anchors.fill: listView
//            onClicked: attachDrawer2.open = false
//        }


//        background:Loader{
//            id:attachLoader
//            anchors.fill: parent
//        }

//        Connections {
//            target: attachLoader.item
//            onS_items: {
//                console.log(items)
//                var types = ["photo","audio","doc","video","wall","place","photoVK","videoVK"];
//                for(var i=0;i<items.length;i++)
//                {
//                    console.log(items[i].url);
//                    var file = items[i].url;
//                    console.log(file)
//                    c_attachmentsModelToSend.addAttachment(file,types[items[i].type])
//                }
//            }
//        }
//        Connections {
//            target:c_attachmentsModelToSend
//            onPhotosavemessagesphoto:
//            {
//                var obj = JSON.parse(json);
//                Net.photoSave(obj.server,obj.photo,obj.hash,idx);
//            }
//        }

//        onOpenedChanged:
//        {
//            if(opened)
//                attachLoader.setSource("../items/AttachComponent.qml")
//            else
//                attachLoader.sourceComponent=undefined
//        }
//    }





    onAddPhotoId:
    {
        var obj = JSON.parse(json)
        c_attachmentsModelToSend.setPhotoAttachmentId(obj.response[0].id,indx)
    }
    onStatusChanged: {
        if(status == PageStatus.Active)
            historyModel.isActive = true;
        else if(status == PageStatus.Activating)
            historyModel.isActive = true;

        historyModel.isActive = false;
    }
    Component.onCompleted: {
        historyModel.clear_Messages();
        globalSettings.companionId=chat_id;
        vkapi.messages_getHistory(chat_id,0,20,-1);

        console.log(attachId,attachPreview)
        if(attachId.length>0)
        {
            console.log(attachId,attachPreview)
            if(attachId.substring(0,4)=="wall")
                writer.c_Model.addAttachment(attachId,4) //Add Wall
            else if(attachId.substring(0,5)=="photo")
                writer.c_Model.addAttachment(attachId,0,attachPreview) //Add Photo
            else if(attachId.substring(0,3)=="doc")
            {
                if(attachPreview.length>0)
                    writer.c_Model.addAttachment(attachId,0,attachPreview)
                else
                    writer.c_Model.addAttachment(attachId,3)
            }
        }
    }
    Component.onDestruction:
    {
        globalSettings.companionId=0;
    }



}
