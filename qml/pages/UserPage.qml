import QtQuick 2.0
import Sailfish.Silica 1.0
import VKModels 1.0
import VKPoco 1.0
import "../items"
Page{
    id: page

    property string usr_id: globalSettings.User_Id

    UsersInfo{
        id:userPage
        Component.onCompleted: DataProvider = c_dataProvider

    }

    Component.onCompleted:{
        customModel.clear_Wall();
        // vkapi.user_get(usr_id,"photo_200,photo_max,is_friend,online,status","Nom");
        var allFields = " counters,photo_id, verified, sex, bdate, city, country, home_town, has_photo, photo_50, photo_100, photo_200_orig, photo_200, photo_400_orig, photo_max, photo_max_orig, online, lists, domain, has_mobile, contacts, site, education, universities, schools, status, last_seen, followers_count, common_count, occupation, nickname, relatives, relation, personal, connections, exports, wall_comments, activities, interests, music, movies, tv, books, games, about, quotes, can_post, can_see_all_posts, can_see_audio, can_write_private_message, can_send_friend_request, is_favorite, is_hidden_from_feed, timezone, screen_name, maiden_name, crop_photo, is_friend, friend_status, career, military, blacklisted, blacklisted_by_me";
        vkapi.user_get(usr_id,allFields,"Nom");
        vkapi.wall_get(usr_id,20,0);
    }
    onStatusChanged: {
        if(status == PageStatus.Active)
            userPage.isActive = true;
        else if(status == PageStatus.Activating)
            userPage.isActive = true;
        else
            userPage.isActive = false;
    }


    SilicaListView {
        id: listView
        model: customModel
        anchors.fill: parent
        spacing:30
        bottomMargin: 20

        NumberAnimation { id: anim; target: listView; property: "contentY"; duration: 500 }
        PullDownMenu{
            MenuItem{
                text:qsTr("Скопировать ссылку")
                onClicked: {
                    var text = "https://vk.com/id"+usr_id
                    clipboardText.text = text;
                    clipboardText.selectAll();
                    clipboardText.copy();
                }
            }
//            MenuItem{
//                text:qsTr("Добавить в закладки")
//            }
            MenuItem{
                text:qsTr("Написать сообщение")
                visible: userPage.can_write_private_message
                onClicked:{
                    pageStack.push(Qt.resolvedUrl("MessagesHistoryPage.qml"),{chat_id:usr_id , headertitle:userPage.first_name_nom+" "+userPage.last_name_nom})
                }
            }
        }
        PushUpMenu {
            id:pulley
            MenuItem {
                text: qsTr("Добавить прошлые записи")
                onClicked:
                {
                    var pos = listView.contentY;
                    var destPos;
                    listView.positionViewAtIndex(listView.count-2,ListView.End)
                    destPos = listView.contentY;
                    anim.from = pos;
                    anim.to = destPos;
                    anim.running = true;
                    vkapi.wall_get(usr_id,20,listView.count);
                }
            }
        }

        header: UserInfo {
            user_id:usr_id
            userInformation: userPage
            //            avatarSource: userPage.Avatar_path
            //            nameplate: userPage.Nameplate
            //            status:userPage.Status
            //            isOnline: userPage.isOnline
            //            isFriend: userPage.is_Friend
            isMyself: usr_id==globalSettings.User_Id?true:false
            //cityName: userPage.city.title
            //            counters: userPage.counters
            schools: userPage.schools
            univers: userPage.universities
            userInfo:userPage
        }

        delegate:
            RecWallpost{
            id:dataWall
            post_id: id
            owner_ava_source: from_photo

            owner_n: from_name + " " + from_surname
            ownerid:owner_id
            from_id:source_id

            post_d: date
            text_d: datatext

            model:attachments
            audiomodel:listAttachments
            videomodel: videoAttachments
            fadetext: true

            like_count: likes_count
            repost_count: reposts_count
            user_likes: user_islikes
            can_like: user_canlike
            comments_count : commentsCount


            childcopyWallpost: customModel.getChild(id)

            onClicked: {
                pageStack.push("RecSinglePostPage.qml",{
                                              post_id: id,
ownerid:owner_id,
                                   owner_ava_source: from_photo,
                                   owner_n: from_name +" "+ from_surname,
                                   from_id:source_id,

                                   post_d: date,
                                   text_d: datatext,
                                   model:attachments,
                                   audiomodel:listAttachments,
                                   videomodel: videoAttachments,
                                   docmodel: docAttachments,
                                   gifmodel: gifAttachments,
                                   comments_count : commentsCount,
                                   comments_canPost:commentsCanPost,
                                   like_count: likes_count,
                                   repost_count: reposts_count,
                                   user_likes:user_islikes,
                                   can_like: user_canlike,
                                   childWallpost: customModel.getChild(id)

                               })
            }
            onRemove: {
            customModel.remove(index)
            }
        }

    }


    WallModel {
        id:customModel
        Component.onCompleted: DataProvider = c_dataProvider
        isActive:userPage.isActive
    }

    BusyIndicator {
        running: false
        size: BusyIndicatorSize.Large
        anchors.centerIn: parent
    }

    Component {
        id: areYouSureDialogPage

        Dialog {
            acceptDestination: page
            acceptDestinationAction: PageStackAction.Pop
            Column
            {
                width:parent.width
                DialogHeader {
                    acceptText: qsTr("Убрать")
                    cancelText: qsTr("Отменить")
                }

                Label{
                    text:qsTr("Вы уверены что хотите удалить из друзей?")
                    width: parent.width-2*Theme.paddingLarge
                    wrapMode:Text.WordWrap
                    x:Theme.paddingLarge

                }
                Item {

                    id:page_info
                    y: Theme.paddingLarge
                    x: Theme.paddingLarge
                    opacity: 1
                    width: parent.width - 2*Theme.paddingLarge
                    height:page_avatar.height+name_info.height+status_info.height
                    Image{
                        id:page_avatar
                        height: 200
                        width: height
                        fillMode: Image.PreserveAspectCrop
                        source: userPage.photo_100
                        anchors{
                            topMargin: 20
                            top:parent.top
                            horizontalCenter: parent.horizontalCenter
                        }
                    }

                    Label{
                        id:name_info
                        text:userPage.first_name_nom +" "+ userPage.last_name_nom
                        color:Theme.highlightColor
                        anchors {
                            leftMargin: 10
                            top:page_avatar.bottom
                            horizontalCenter: parent.horizontalCenter
                        }

                    }
                    Label{
                        id:status_info
                        text:userPage.Status
                        color:Theme.secondaryHighlightColor

                        font.pixelSize: Theme.fontSizeSmall
                        wrapMode:Text.WordWrap
                        width: parent.width
                        horizontalAlignment: Text.AlignHCenter
                        anchors{
                            top: name_info.bottom
                            horizontalCenter: parent.horizontalCenter
                        }
                    }
                }
            }
            onAccepted: {
                vkapi.friends_delete(usr_id);
                userPage.setIs_Friend(false);

            }
        }
    }
    Component {
        id: addFriendDialogPage

        Dialog {
            acceptDestination: page
            acceptDestinationAction: PageStackAction.Pop
            Column
            {
                width:parent.width
                DialogHeader {
                    acceptText: qsTr("Добавить")
                    cancelText: qsTr("Отменить")
                }

                Item {

                    id:page_info
                    y: Theme.paddingLarge
                    x: Theme.paddingLarge
                    opacity: 1
                    width: parent.width - 2*Theme.paddingLarge
                    height:page_avatar.height+name_info.height+status_info.height

                    Image{
                        id:page_avatar
                        height: 200
                        width: height
                        fillMode: Image.PreserveAspectCrop
                        source: userPage.photo_100
                        anchors{
                            topMargin: 20
                            top:parent.top
                            horizontalCenter: parent.horizontalCenter
                        }
                    }

                    Label{
                        id:name_info
                        text:userPage.first_name_nom +" "+ userPage.last_name_nom
                        color:Theme.highlightColor
                        anchors {
                            leftMargin: 10
                            top:page_avatar.bottom
                            horizontalCenter: parent.horizontalCenter
                        }

                    }
                    Label{
                        id:status_info
                        text:userPage.Status
                        color:Theme.secondaryHighlightColor

                        font.pixelSize: Theme.fontSizeSmall
                        wrapMode:Text.WordWrap
                        width: parent.width
                        horizontalAlignment: Text.AlignHCenter
                        anchors{
                            top: name_info.bottom
                            horizontalCenter: parent.horizontalCenter
                        }
                    }
                }

                TextArea {
                    id:textMessage
                    width:parent.width
                    placeholderText: qsTr("При желании, добавьте текст при добавлении в друзья")
                }
            }
            onAccepted: {
                vkapi.friends_add(usr_id,textMessage.text,0);
                //Костыль, на самом деле нужно ждать подтверждения добавления в друзья
                userPage.setIs_Friend(true);
            }
        }
    }
}


