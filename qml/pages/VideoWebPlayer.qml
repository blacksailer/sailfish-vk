import QtQuick 2.0
import Sailfish.Silica 1.0
//import Sailfish.WebView 1.0


Page{
    allowedOrientations: Orientation.Landscape
    orientation: Orientation.Landscape
    property alias src:webView.url
    SilicaWebView {
        id:webView
        anchors.fill: parent
        contentWidth: 300
        contentHeight: 200
//        experimental.preferredMinimumContentsWidth:300
//        experimental.preferences.developerExtrasEnabled: true
//        experimental.userAgent:"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.146 Safari/537.36"
        MouseArea{
            anchors.fill: parent
            onClicked:  pageStack.pushAttached(Qt.resolvedUrl("VideoWebPlayer.qml"),{src:"http://127.0.0.1:9999"})

        }

    }

}
