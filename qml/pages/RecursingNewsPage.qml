

import QtQuick 2.0
import Sailfish.Silica 1.0
import VKItems 1.0
import VKModels 1.0
import "../items"

Page{

    id: page
    property int currIndexShow: -1
    property int offset: 20
    property bool fromMessage:false
    onCurrIndexShowChanged: console.log("Show is --"+currIndexShow)
    onStatusChanged: {
        if(status == PageStatus.Active)
            feedModel.isActive = true;
        else if(status == PageStatus.Activating)
            feedModel.isActive = true;
        else
            feedModel.isActive = false;
    }
    Component.onCompleted:{
        if(!fromMessage)
        {
            feedModel.clear_Newsfeed();
            vkapi.newsfeed_get(20);
        }
    }


    SilicaListView {

        NumberAnimation { id: anim; target: listView; property: "contentY"; duration: 500 }
        PushUpMenu {
            id:pulley
            MenuItem {
                text: "добавить прошлые записи"
                onClicked:
                {
                    var pos = listView.contentY;
                    var destPos;
                    listView.positionViewAtIndex(offset-2, ListView.End);
                    destPos = listView.contentY;
                    anim.from = pos;
                    anim.to = destPos;
                    anim.running = true;
                    vkapi.newsfeed_get(20,offset)
                    offset+=20;

                }
            }
        }
        Binding {
            target: page
            property:"currIndexShow"
            value:listView.currentIndex
        }
        id: listView
        model:  feedModel
        anchors.fill: parent
        spacing:Theme.paddingLarge
        bottomMargin: Theme.paddingMedium

        header:
            PageHeader{
            id:name_info
            title:qsTr("Мои новости")
        }

        delegate:
            RecWallpost{
            id:dataWall
            post_id: id
            owner_ava_source: from_photo

            owner_n: from_name + " " + from_surname
            ownerid:owner_id
            from_id:source_id
            post_d: date
            text_d: datatext

            model:attachments
            audiomodel:listAttachments
            videomodel: videoAttachments
            docmodel: docAttachments
            gifmodel:gifAttachments
            fadetext: true

            like_count: likes_count
            repost_count: reposts_count
            user_likes: user_islikes
            can_like: user_canlike
            comments_count: commentsCount
            comments_canPost: commentsCanPost
            childcopyWallpost: feedModel.getChild(id)

            onClicked: {
                pageStack.push("RecSinglePostPage.qml",{
                                              post_id: id,
ownerid:owner_id,
                                   owner_ava_source: from_photo,
                                   owner_n: from_name +" "+ from_surname,
                                   from_id:source_id,

                                   post_d: date,
                                   text_d: datatext,
                                   model:attachments,
                                   audiomodel:listAttachments,
                                   videomodel: videoAttachments,
                                   docmodel: docAttachments,
                                   gifmodel: gifAttachments,
                                   like_count: likes_count,
                                   repost_count: reposts_count,
                                   comments_count : commentsCount,
                                   comments_canPost: commentsCanPost,
                                   user_likes:user_islikes,
                                   can_like: user_canlike,
                                   childWallpost: feedModel.getChild(id)

                               })
            }
        }
    }
    NewsfeedModel{
        id:feedModel
        Component.onCompleted:    DataProvider=c_dataProvider
    }
    BusyIndicator {
        running: listView.count==0 && Qt.application.active
        size: BusyIndicatorSize.Large
        anchors.centerIn: parent
    }

}



