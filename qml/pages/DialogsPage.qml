
import QtQuick 2.0
import Sailfish.Silica 1.0
import QtMultimedia 5.0
import VKModels 1.0
import ".."
Page{
    id:page
    property string attachmentId:""
    property string attachmentPreview
    DialogsModel{
        id:dialogsModel
        Component.onCompleted: DataProvider = c_dataProvider
    }
    SearchDialogsModel{
        id:searchDialogsModel
        Component.onCompleted: DataProvider = c_dataProvider
    }

    onStatusChanged: {
        if(page.status==PageStatus.Activating)
        {
            dialogsModel.isActive=true
            dialogsModel.clear_Dialogs();
            vkapi.messages_getDialogs(0,20,0,0);
        }

    }
    Timer{
        id:searchDelay
        interval:800
        repeat:false
        onTriggered: {
            if(searchField.text.trim()!="")
            {
                searchDialogsModel.clearAll();
                vkapi.messages_searchDialogs(searchField.text.trim(),20,"photo_50,photo_100,photo_200");
            }
        }
    }
    Loader {
        anchors.fill: parent
        sourceComponent: listViewComponent
    }

    Column {
        id: headerContainer

        width: parent.width

        PageHeader{
            id:name_info
            title:attachmentId.length==0?qsTr("Мои диалоги"):qsTr("Выберите получателя")
            anchors {
                leftMargin: 10
            }
        }
        SearchField {
            id: searchField
            width: parent.width
            onTextChanged: {
                searchDelay.restart()
                if(text.trim()=="")
                    searchDialogsModel.clearAll();

            }
        }
    }
    Component{
        id:listViewComponent
        Item{
            anchors.fill: parent
        SilicaListView {
            id: listView
            model: searchField.text.trim().length>0 ? searchDialogsModel: dialogsModel
            onModelChanged: searchField.focus=true
            anchors {
                fill: parent
            }
            PushUpMenu{
                visible: listView.model == dialogsModel
                MenuItem{
                    text: qsTr("загрузить еще диалоги")
                    onClicked: vkapi.messages_getDialogs(listView.count,20,0,0);
                }
            }
           // visible:!busy.running
            header:Item {
                id: header
                width: headerContainer.width
                height: headerContainer.height
                Component.onCompleted: headerContainer.parent = header
            }
            delegate:model==dialogsModel ? listItemDialog : listItemSearchDialog
        }
        BusyIndicator {
            id:busy
            running: listView.count==0 && Qt.application.active
            size: BusyIndicatorSize.Large
            anchors.centerIn: parent
        }
    }

    }
    Component{
    id:listItemDialog
    ListItem{
                    id:listItem
                    x: Theme.paddingLarge
                    width: parent.width - 2*Theme.paddingLarge
                    contentWidth: width
                    contentHeight: Theme.itemSizeMedium+10
                    highlighted: !read_state

                    Rectangle {
                        id:avatar
                        width:parent.height-5
                        height: width
                        anchors{
                            top: parent.top
                            topMargin: 5
                            left: parent.left
                            verticalCenter: parent.verticalCenter
                        }

                        Image {
                            id:avata
                            source: title==" ... " & title!="" ? user_photo : photo_50
                            fillMode: Image.PreserveAspectCrop
                            anchors.fill: parent
                        }
                    }
                    Label {
                        id:titlelabel
                        text: title==" ... " & title!="" ? first_name+" "+last_name : title
                        color: listItem.highlighted ? Theme.highlightColor : Theme.secondaryColor
                        truncationMode: TruncationMode.Fade

                        anchors.top: avatar.top
                        anchors.left: avatar.right
                        anchors.leftMargin: Theme.paddingLarge
                        width: parent.width-avatar.width

                    }
                    Label {
                        id:bodylabel
                        text: body
                        color: listItem.highlighted ? Theme.highlightColor : Theme.secondaryColor
                        width:parent.width-avata.width

                        anchors.top: titlelabel.bottom
                        anchors.left: titlelabel.left
                        anchors.right: dateLabel.left
                        anchors.topMargin: 10
                        truncationMode: TruncationMode.Fade
                        maximumLineCount: 1
                        //  width: parent.width-avatar.width
                        font.pixelSize:  Theme.fontSizeSmall

                    }
                    Label {
                        id:dateLabel
                        text: Format.formatDate(new Date(date*1000), Formatter.TimepointRelative)
                        color: listItem.highlighted ? Theme.highlightColor : Theme.secondaryColor
                        anchors.right: parent.right
                        anchors.baseline: bodylabel.baseline
                        font.pixelSize: Theme.fontSizeExtraSmall

                    }

                    onClicked: {
                        console.log(attachmentPreview)
                        if(chat_id>0)
                            pageStack.push(Qt.resolvedUrl("MessagesHistoryPage.qml"),{chat_id:2000000000+chat_id , headertitle:titlelabel.text,isMulti:true,attachPreview:attachmentPreview,attachId:attachmentId});
                        else
                            pageStack.push(Qt.resolvedUrl("MessagesHistoryPage.qml"),{chat_id:user_id , headertitle:titlelabel.text,attachPreview:attachmentPreview,attachId:attachmentId});
                        attachmentId="";
                        attachmentPreview="";
                    }
                    Separator {
                        // alignment: Qt.AlignHCenter
                        width:parent.width
                        color:Theme.secondaryColor
                        anchors.top:parent.bottom
                    }
                }
    }
    Component{
    id:listItemSearchDialog
    ListItem{
                    id:listItem
                    x: Theme.paddingLarge
                    width: parent.width - 2*Theme.paddingLarge
                    contentWidth: width
                    contentHeight: Theme.itemSizeMedium+10

                    Image {
                        id:avatar
                        width:parent.height-5
                        height: width

                        anchors{
                            top: parent.top
                            topMargin: 5
                            left: parent.left
                        }

                        anchors.verticalCenter: parent.verticalCenter
                        source: photo_50
                        fillMode: Image.PreserveAspectCrop

                    }
                    Label {
                        id:titlelabel
                        text: title ? title :firstName+" "+lastName
                        color: listItem.highlighted ? Theme.highlightColor : Theme.secondaryColor
                        truncationMode: TruncationMode.Fade

                        anchors.verticalCenter: parent.verticalCenter
                        anchors.leftMargin: Theme.paddingLarge
                        anchors.left: avatar.right
                        width: parent.width-avatar.width

                    }

                    onClicked: {
                        console.log(attachmentPreview)
                        if(itemType==2)
                            pageStack.push(Qt.resolvedUrl("MessagesHistoryPage.qml"),{chat_id:2000000000+id , headertitle:titlelabel.text,isMulti:true,attachPreview:attachmentPreview,attachId:attachmentId});
                        else
                            pageStack.push(Qt.resolvedUrl("MessagesHistoryPage.qml"),{chat_id:id , headertitle:titlelabel.text,attachPreview:attachmentPreview,attachId:attachmentId});
                        attachmentId="";
                        attachmentPreview="";
                    }
                    Separator {
                        // alignment: Qt.AlignHCenter
                        width:parent.width
                        color:Theme.secondaryColor
                        anchors.top:parent.bottom
                    }
                }
    }

}
