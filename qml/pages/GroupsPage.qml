
import QtQuick 2.0
import Sailfish.Silica 1.0
import VKModels 1.0
import ".."
Page{
    id:page
    property int user_id:globalSettings.User_Id

    Timer{
        id:searchDelay
        interval:800
        repeat:false
        onTriggered: {
            if(searchField.text.trim()!="")
            {
                _groupsModel.setIsSearchMode(true);
                _groupsModel.clearGroups();
                //Search
            }
            else
            {
                _groupsModel.setIsSearchMode(false)
            }

        }
    }

    GroupsModel{
    id:_groupsModel
    Component.onCompleted:    DataProvider=c_dataProvider
    }
    Component.onCompleted:{
        //_groupsModel.clearGroups();
        vkapi.group_get(user_id,"","status",0,30);
    }
    Loader {
        anchors.fill: parent
        sourceComponent: listViewComponent
    }

    Column {
        id: headerContainer

        width: parent.width

        PageHeader{
            id:name_info
            title:qsTr("Группы")
            anchors {
                leftMargin: 10
            }
        }
        SearchField {
            id: searchField
            width: parent.width
            visible: false
            onTextChanged: {
                searchDelay.restart()
            }
        }
    }
    Component{
        id:listViewComponent
        SilicaListView {
            id: listView
            header:  Item {
                id: header
                width: headerContainer.width
                height: headerContainer.height
                Component.onCompleted: headerContainer.parent = header
            }
            model:_groupsModel
            PullDownMenu{
                MenuItem{
                    text:qsTr("Поиск")
                    onClicked:searchField.visible=true
                }
            }
            PushUpMenu{
                visible:_groupsModel.totalCount!==listView.count
                MenuItem
                {
                    text:qsTr("загрузить ещё")
                    onClicked:
                    {
                            vkapi.group_get(user_id,"","status",30,listView.count);
                    }

                }
            }
            anchors {
                fill: parent
            }
            delegate: ListItem {
                id:listItem
                x: Theme.paddingLarge
                width: parent.width - 2*Theme.paddingLarge
                contentWidth: width
                contentHeight: Theme.itemSizeMedium+10

                Rectangle {
                    width:parent.height-5
                    height: width
                    anchors{
                        top: parent.top
                        topMargin: 5
                        left: parent.left
                    }
                    id:avatar
                    anchors.verticalCenter: parent.verticalCenter

                    Image {
                        id:avata
                        source:  photo_50
                        fillMode: Image.PreserveAspectCrop
                        anchors.fill: parent
                    }
                }
                Column{
width: parent.width-avatar.width-Theme.paddingSmall
                    anchors.leftMargin: Theme.paddingSmall
                    anchors.left: avatar.right
                    spacing:Theme.paddingLarge
                    Label{
                        text: name
                          }
                    Label{
                        text: status
                    }
                }


                Separator {
                    width:parent.width
                    color:Theme.primaryColor
                    anchors.top:parent.bottom
                }

                onClicked:{
                    pageStack.push(Qt.resolvedUrl("qrc:///qml/pages/GroupPage.qml"),{group_id:group_id})
                }
            }
            Connections{
                target:_groupsModel
                onIsSearchModeChanged:{
                listView.forceLayout()
                }
            }
        }
    }
}
