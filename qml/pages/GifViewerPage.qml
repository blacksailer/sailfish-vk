import QtQuick 2.0
import Sailfish.Silica 1.0
import "../items"
Dialog {
    id:rootDialog
    allowedOrientations: Orientation.All
    property var gifValue
    property alias src: animation.source

    SilicaFlickable{
        anchors.fill: parent

        Drawer {
            id: drawer

            open:!animation.playing
            anchors.fill: parent
            dock: rootDialog.isPortrait ? Dock.Top : Dock.Left

            background:ShareComponent{
                attachmentId: "doc"+gifValue.owner_id+"_"+gifValue.id+"_"+gifValue.access_key;
                attachmentPreview: gifValue.preview.photo.sizes[0].src
        PullDownMenu{
                    MenuItem {
                        text: qsTr("Скачать")
                        onClicked: vkapi.img_save(src,1)
                    }
                }
            }
        AnimatedImage{
            id:animation
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            playing: true

            MouseArea{
                anchors.fill: parent
                onClicked: parent.playing=!parent.playing
            }
            onStatusChanged: console.log(status)

        }
    }
        BusyIndicator{
            anchors.centerIn: parent
            size:BusyIndicatorSize.Large
            running: animation.status == AnimatedImage.Loading
        }
    }
    Component.onCompleted: console.log("doc"+gifValue.owner_id+"_"+gifValue.id+"_"+gifValue.access_key)

}

