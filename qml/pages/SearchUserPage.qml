import QtQuick 2.0
import Sailfish.Silica 1.0


Page {
    id:page

    TextField {
        id:textF
        placeholderText: "Введите ИД"
        width:parent.width
        anchors.top:parent.top
        anchors.topMargin: 50
        label: "Text field"
        EnterKey.onClicked: {
            pageStack.push("UserPage.qml",{usr_id:textF.text})
        }
    }
}
