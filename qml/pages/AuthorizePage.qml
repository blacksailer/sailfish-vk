import QtQuick 2.0
import QtWebKit 3.0
import Sailfish.Silica 1.0

Page {
    id: webViewPage

    onEntered:console.log("entered")
    Timer {
        interval: 500; running: true;
               onTriggered:{

                   if(1)//!isToken)
                   {
                       webView.url="https://oauth.vk.com/authorize?"+
                               "client_id=4351707&"+
                               "scope=photos,wall,messages,audio,groups,friends,video,offline&"+
                               "redirect_uri=https://oauth.vk.com/blank.html&"+
                               "display=mobile&"+
                               "v=5.52&"+
                               "response_type=token";
                   }
               }
    }

    SilicaWebView {
        id: webView
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        opacity: 1


        onLoadingChanged: {
            console.log("loadingchanged")
            switch (loadRequest.status)
            {
            case WebView.LoadSucceededStatus:
//                urlField.text = webView.url
                opacity = 1
                if (url.toString().match(/expires_in/)) {
                    console.log("url changed",url)
                    var result = url.toString()
                    vkapi.setToken(result)
                    vkapi.stat_trackVisitor();
                    if(pageStack.depth===1)
                        pageStack.replace(Qt.resolvedUrl("MenuPage.qml"),PageStackAction.Animated);
                    else
                    pageStack.pop();
                }
                break
            case WebView.LoadFailedStatus:
                opacity = 0
//                urlField.text = webView.url
                break
            default:
//                urlField.text = webView.url
                opacity = 0
                break
            }
        }



        FadeAnimation on opacity {}
        PullDownMenu {
            MenuItem {
                text: "Reload"
                onClicked: webView.reload()
            }
        }

    }
    BusyIndicator {
        running: webView.opacity==0 && Qt.application.active
        size: BusyIndicatorSize.Large
        anchors.centerIn: parent
    }
}
