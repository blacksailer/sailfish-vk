
import QtQuick 2.0
import Sailfish.Silica 1.0
import VKModels 1.0
import ".."
Page{
    id:page
    property int user_id: globalSettings.User_Id
    Timer{
        id:searchDelay
        interval:800
        repeat:false
        onTriggered: {
            if(searchField.text.trim()!="")
            {
                _friendsModel.setIsSearchMode(true);
                _friendsModel.clearFriends();
                vkapi.friends_search(user_id,30,0,"nom",searchField.text.trim())//_friendsModel.setFilterFixedString(text)
            }
            else
            {
                _friendsModel.setIsSearchMode(false)
            }
        }
    }

    Component.onCompleted:{
        //_friendsModel.clearFriends();
        vkapi.friends_get(user_id,30,0,"nom");
    }
    Loader {
        anchors.fill: parent
        sourceComponent: listViewComponent
    }

    Column {
        id: headerContainer

        width: parent.width

        PageHeader{
            id:name_info
            title:qsTr("Мои друзья")
            anchors {
                leftMargin: 10
            }
        }
        SearchField {
            id: searchField
            width: parent.width
            onTextChanged: {
                searchDelay.restart()
            }
        }
    }
    FriendsModel{
        id:_friendsModel
        Component.onCompleted:    DataProvider=c_dataProvider
    }
    Component{
        id:listViewComponent
        SilicaListView {
            id: listView
            header:  Item {
                id: header
                width: headerContainer.width
                height: headerContainer.height
                Component.onCompleted: headerContainer.parent = header
            }
            model:_friendsModel
            PullDownMenu{
                MenuItem{
                    text:qsTr("Поиск")
                    onClicked:searchField.visible=true
                }
            }
            PushUpMenu{
                visible:_friendsModel.totalCount!==listView.count
                MenuItem
                {
                    text:qsTr("загрузить ещё")
                    onClicked:
                    {
                        if(_friendsModel.isSearchMode)
                            vkapi.friends_search(user_id,30,listView.count,"nom",searchField.text.trim());
                        else
                            vkapi.friends_get(user_id,30,listView.count,"nom");
                    }

                }
            }
            anchors {
                fill: parent
            }
            delegate: ListItem {
                id:listItem
                x: Theme.paddingLarge
                width: parent.width - 2*Theme.paddingLarge
                contentWidth: width
                contentHeight: Theme.itemSizeMedium+10

                Rectangle {
                    width:parent.height-5
                    height: width
                    //            color:"red"
                    anchors{
                        top: parent.top
                        topMargin: 5
                        left: parent.left
                    }
                    id:avatar
                    anchors.verticalCenter: parent.verticalCenter

                    Image {
                        id:avata
                        source:  photo_50
                        fillMode: Image.PreserveAspectCrop
                        anchors.fill: parent
                    }
                }
                Label{
                    text: first_name+" "+surname
                    anchors.left: avatar.right
                    anchors.horizontalCenter: avatar.horizontalCenter
                }


                Separator {
                    // alignment: Qt.AlignHCenter
                    width:parent.width
                    color:Theme.primaryColor
                    anchors.top:parent.bottom
                }

                onClicked:{
                    pageStack.push(Qt.resolvedUrl("qrc:///qml/pages/UserPage.qml"),{usr_id:user_id})
                }

            }
            Connections{
                target:_friendsModel
                onIsSearchModeChanged:{
                    listView.forceLayout()
                }
            }
        }
    }
}
