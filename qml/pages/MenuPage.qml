import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id:page
    property int dialogsCount:0
    ListModel {
        id: pagesModel
//        ListElement {
//            page: "SearchUserPage.qml"
//            title: "Поиск"
//            count:0
//        }
        ListElement {
            page: "UserPage.qml"
            title: QT_TR_NOOP("Моя страница")
            count:0

        }
        ListElement {
            page: "FriendsPage.qml"
            title: QT_TR_NOOP("Мои друзья")
            count:0

        }
        ListElement {
            page: "RecursingNewsPage.qml"
            title: QT_TR_NOOP("Мои новости")
            count:0

        }
//Пока контакт не разблокирует
        //        ListElement {
//            page: "MyAudioPage.qml"
//            title: "Мои аудиозаписи"
//            count:0

//        }
        ListElement {
            page: "DialogsPage.qml"
            title: QT_TR_NOOP("Мои диалоги")
            count:0

        }
        ListElement {
            page: "GroupsPage.qml"
            title: QT_TR_NOOP("Мои группы")
            count:0

        }
        ListElement {
            page: "AboutPage.qml"
            title: QT_TR_NOOP("О программе")
            count:0

        }
}


        SilicaListView {
            id: listView
            anchors.fill: parent
            model: pagesModel
            header: PageHeader { title: qsTr("Меню") }
            delegate: BackgroundItem {
                width: listView.width
                Label {
                    id: firstName
                    text: qsTr(model.title)
                    color: highlighted ? Theme.highlightColor : Theme.primaryColor
                    anchors.verticalCenter: parent.verticalCenter
                    x: Theme.paddingLarge
                }
                Label {
                    id: countElement
                    text: model.count
                    visible: model.count>0 ? 1:0
                    color: highlighted ? Theme.highlightColor : Theme.primaryColor
                    anchors.right: parent.right
                     anchors.rightMargin: Theme.paddingLarge
                    anchors.verticalCenter: parent.verticalCenter
                        }
                onClicked: pageStack.push(Qt.resolvedUrl(page))
            }
            VerticalScrollDecorator {}
        }
        Timer{
            id:timer
         interval:3000
         repeat: false
         onTriggered: {
             console.log("start");

         }
        }
        Connections{
         target:globalSettings
         onUnreadCountChanged:
         {
             console.log("unread is "+globalSettings.unreadCount)
             pagesModel.setProperty(3,"count",globalSettings.unreadCount)
          //   dialogsCount=globalSettings.unreadCount
             pagesModel.set(3,{page: "DialogsPage.qml",
                                title: "Мои диалоги",
                                count:globalSettings.unreadCount})

         }

        }

}
