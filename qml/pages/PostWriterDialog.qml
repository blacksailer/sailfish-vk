import QtQuick 2.0
import Sailfish.Silica 1.0
import VKModels 1.0
import "../../MessageSendNetwork.js" as Net
Dialog{
    property int owner_id //Должен быть отрицательным если группа
    property int upload_user_id

    property bool isAdmin: true

    property bool friends_only:false
    property bool from_group:false
    property bool signed:false
    property bool is_ads:false
    property string text_to_send:""
    property string attachments_to_send:""

    property string typeWriter:"wall"

    acceptDestinationAction: PageStackAction.Pop

    Drawer{
        id:attachDrawer


        property bool isShareBackgroundOpen: false

        signal addPhotoId(var json,var indx);

        onAddPhotoId:
        {
            var obj = JSON.parse(json)
            c_attachmentsModelToSend.setPhotoAttachmentId(obj.response[0].id,indx)
        }
        open:false
        dock:Dock.Bottom
        anchors.fill: parent

        function setUploadUserId(v) {
            console.log("Signal received " + v);
            upload_user_id=v;
        }
        AttachmentsToSendModel{
            id:c_attachmentsModelToSend

            Component.onCompleted:{
                    Net.request("wall",owner_id,c_attachmentsModelToSend);
            }
        }

        DialogHeader  {
            id:dialogHeader
            title:qsTr("Публикация на стене")
            acceptText: qsTr("Опубликовать")
            cancelText: qsTr("Отменить")
        }

        TextArea {
            id:messageArea

            anchors.top: dialogHeader.bottom
            anchors.bottom: sendArea.top

            x:Theme.horizontalPageMargin
            height:  implicitHeight
            width:parent.width-2*Theme.horizontalPageMargin
        }

        Item{
            id:sendArea
            anchors.bottom: parent.bottom
            x:Theme.horizontalPageMargin
            width:parent.width-2*Theme.horizontalPageMargin
            height: attachmentstoSend.height+buttonsRow.height+Theme.paddingSmall
            SilicaListView{
                id:attachmentstoSend
                width:parent.width
                height: attachmentstoSend.count>0?Theme.itemSizeMedium:0
                anchors.bottom: buttonsRow.top
                model:c_attachmentsModelToSend
                orientation:Qt.Horizontal
                spacing:5
                delegate: Image{
                    width:Theme.itemSizeMedium
                    height:width
                    sourceSize: Qt.size(width,height)
                    IconButton{
                        icon.source: "image://theme/icon-s-clear-opaque-cross?"+Theme.highlightColor
                        anchors.centerIn: parent
                        onClicked: c_attachmentsModelToSend.removeAttachment(index)
                        Component.onCompleted: {
                            switch(type) {
                            case 0: //        PHOTO
                                if(c_source.substring(0,4)!=="http")
                                    source="image://nemoThumbnail/"+c_source
                                else
                                    source = c_source
                                break;
                            case 1: //        VIDEO
                                if(c_source.substring(0,4)!=="http")
                                    source="image://nemoThumbnail/"+c_source
                                else
                                    source = c_source
                                break;
                            case 2://        AUDIO
                                source="image://theme/icon-m-file-audio"
                                break;
                            case 3://        DOC
                                source="image://theme/icon-m-document"
                                break;
                            case 4://        WALL
                                source="image://theme/icon-m-other"
                                break;

                            }

                        }
                    }
                    Rectangle{
                        color:"black"
                        visible: c_status>0
                        opacity: 0.5
                        anchors.fill: parent
                    }
                    ProgressCircle {
                        id: progressCircle

                        anchors.centerIn: parent
                        value: c_progress
                        visible: c_status>0

                    }
                }
            }
            Row{
                id:buttonsRow
                width: parent.width
                anchors.bottom: parent.bottom
                anchors.bottomMargin: Theme.paddingLarge
                spacing: Theme.paddingLarge
                IconButton {
                    id:skrepkaWizard
                    width: parent.width/2
                    icon.source: "image://theme/icon-m-attach"
                    onClicked: {attachDrawer.isShareBackgroundOpen=true; attachDrawer.open=true}
                }
                IconButton {
                    id:settings
                    width: parent.width/2
                    enabled: owner_id<0 & isAdmin ?true : owner_id>0 ? true:false
                    icon.source: "image://theme/icon-m-developer-mode"
                    onClicked: attachDrawer.open=true
                }
            }
        }
        MouseArea {
            enabled: attachDrawer.open
            anchors.fill: messageArea
            onClicked: attachDrawer.open = false
        }


        background:Loader{
            id:attachLoader
            anchors.fill: parent
        }

        Connections {
            target: attachLoader.item
            onS_items: {
                console.log(items)
                var types = ["photo","audio","doc","video","wall","place","photoVK","videoVK"];
                for(var i=0;i<items.length;i++)
                {
                    console.log(items[i].url);
                    var file = items[i].url;
                    console.log(file)
                    c_attachmentsModelToSend.addAttachment(file,types[items[i].type])
                }
            }
        }
        Connections {
            target:c_attachmentsModelToSend
            onPhotosavemessagesphoto:
            {
                console.log(json)
                var obj = JSON.parse(json);
                console.log(upload_user_id)
                if(owner_id<0)
                Net.wallpost_photoSave(obj.server,obj.photo,obj.hash,idx,upload_user_id,owner_id,typeWriter);
                else
                    Net.wallpost_photoSave(obj.server,obj.photo,obj.hash,idx,upload_user_id,0,typeWriter);
                //Message
                //            Net.photoSave(obj.server,obj.photo,obj.hash,idx);
            }
        }

        onOpenedChanged:
        {
            if(opened & isShareBackgroundOpen)
                attachLoader.setSource("../items/AttachComponent.qml")
            else if(opened)
                attachLoader.sourceComponent=postSettingsComponent
            else
            {

                attachLoader.sourceComponent=undefined
                isShareBackgroundOpen=false
            }
        }
        Component.onCompleted: {
            Net.internalQmlObject.ownerUserIdReceived.connect(attachDrawer.setUploadUserId);
        }
    }

    Component{
        id:postSettingsComponent
        Column {
            width:parent.width
            //Не используется. Ради исключения ошибки в консоли
            signal s_items(var items)

            IconTextSwitch {
                id:fromGroup
                visible: owner_id<0 &isAdmin ?true : false
                width: parent.width-Theme.paddingMedium
                text: qsTr("От имени сообщества")
                icon.source: "image://theme/icon-m-dot"
                onCheckedChanged: from_group=checked
            }
            IconTextSwitch {
                text: qsTr("Подпись")
                width: parent.width-Theme.paddingMedium
                visible: owner_id<0 &isAdmin ?true : false
                enabled: fromGroup.checked
                icon.source: "image://theme/icon-m-media-artists"
                onEnabledChanged: {if(!enabled) checked=false}
                onCheckedChanged: signed=checked

            }
            IconTextSwitch {
                id:isAds
                visible: owner_id<0 &isAdmin ?true : false
                width: parent.width-Theme.paddingMedium
                enabled: fromGroup.checked
                text: qsTr("Это реклама")
                description: qsTr("Нельзя резместить больше трех рекламных сообщений")
                icon.source: "image://theme/icon-m-about"
                onCheckedChanged: is_ads=checked
            }
            IconTextSwitch {
                width: parent.width-Theme.paddingMedium
                visible: owner_id>0?true:false
                text: qsTr("Только для друзей")
                icon.source: "image://theme/icon-m-person"
                onCheckedChanged: friends_only=checked

            }
            IconTextSwitch {
                width: parent.width-Theme.paddingMedium
                visible: false
                text: qsTr("Отложенная публикация")
                icon.source: "image://theme/icon-m-backup"
            }
            ValueButton {
                property date selectedDate
                property int selectedHour
                property int selectedMinute
                visible: false
                function openDateDialog() {
                    var dialog = pageStack.push("Sailfish.Silica.DatePickerDialog", {
                                                    date: selectedDate,
                                                    acceptDestination:timeDialog,
                                                    acceptDestinationAction:PageStackAction.Replace,
                                                    forwardNavigation:true
                                                })

                    dialog.accepted.connect(function() {
                        console.log(dialog.dateText)
                    })}

                label: qsTr("Отложенная публикация")
                value: " - "
                width: parent.width
                onClicked: openDateDialog()
                TimePickerDialog{
                    id:timeDialog
                    onAccepted:{
                        console.log(timeText)
                    }
                }
            }

        }
    }
    onAccepted: {
        text_to_send=messageArea.text
        attachments_to_send=c_attachmentsModelToSend.getAllSendUrls()
//        vkapi.wall_post(owner_id,friends_only,from_group,messageArea.text,
//                        c_attachmentsModelToSend.getAllSendUrls(),"",signed,
//                        is_ads,0,0,0,0,0)
    }
}
