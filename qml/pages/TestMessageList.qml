
import QtQuick 2.0
import Sailfish.Silica 1.0
import QtMultimedia 5.0
import VKModels 1.0
import ".."
import "../items"
import "../../MessageSendNetwork.js" as Net
import QtDocGallery 5.0
import org.nemomobile.thumbnailer 1.0


Page{
    id:attachDrawer


    signal addAttach(var src,var type);
    signal addPhotoId(var json,var indx);

    property string attachId:"";
    property string attachPreview;
    property string headertitle;
    property int chat_id;
    property bool isMulti:false
    property string sobesed_photo;


    ListModel{
    id:testMessagesModel
    ListElement{

        first_name:"Ivan"
        last_name:"Ivanov"
        user_photo:"https://images.duckduckgo.com/iu/?u=http%3A%2F%2Fmedia01.money4invest.com%2F2010%2F01%2Ffaceyourmanga-female-avatar.jpg&f=1"
        date:19000000
        read_state:1
        out:1
        title:" ... "
        body:"Ipsum Lorem dolorum"
//        roles[PHOTOATTACHMENTS] = "gridAttachments";
//        roles[AUDIOATTACHMENTS] = "listAttachments";
//        roles[DOCATTACHMENTS] = "docAttachments";
//        roles[VIDEOATTACHMENTS] = "videoAttachments";
//        roles[GIFATTACHMENTS] = "gifAttachments";
//        roles[CHAT_ID]="chat_id";
//        roles[PHOTO_50]="photo_50";
//        roles[PHOTO_100]="photo_100";
//        roles[PHOTO_200]="photo_200";
    }
    }

    WritingItem{
        id:writer
        typeWriter:"message"
        bottomArea.visible:true

        PageHeader{
            id:name_info
            title:headertitle
            anchors {
                leftMargin: 10
            }
        }

                SilicaListView {
                    id: listView
                    model: historyModel
                    verticalLayoutDirection: ListView.BottomToTop
                    focus:!writer.textArea.focus

                    PullDownMenu {
                        id:pulley
                        MenuItem {
                            text: qsTr("добавить прошлые записи")
                            onClicked:
                            {
                                vkapi.messages_getHistory(chat_id,listView.count,20,-1)
                            }
                        }
                    }

                    width: parent.width
                    height: parent.height-writer.sendAreaHeight-name_info.height

                      spacing: 30
                    anchors {
                        top:name_info.bottom
                        bottom: writer.bottomArea.top
                    }
                    clip:true
                    delegate:MessageItem{
                        isOut:out
                        author: isMulti?first_name+ " "+last_name:""
                        fwdMsg: historyModel.getFwdMessage(id)
                        attachWallpost: historyModel.getFwdPost(id)
                    }

                }

        actionButton.onClicked: {
            var Attachments = c_Model.getAllSendUrls()
            console.log(Attachments)
  //          vkapi.messages_send(chat_id,text,Attachments)
            c_Model.clear();
            text=""
            highlighted=!highlighted
            writer.author=writer.authorsText=""
            writer.reply_id=-1

        }
    }
    onAddPhotoId:
    {
        var obj = JSON.parse(json)
        c_attachmentsModelToSend.setPhotoAttachmentId(obj.response[0].id,indx)
    }
    onStatusChanged: {
        if(status == PageStatus.Active)
            historyModel.isActive = true;
        else if(status == PageStatus.Activating)
            historyModel.isActive = true;

        historyModel.isActive = false;
    }
    Component.onCompleted: {
        historyModel.clear_Messages();
        globalSettings.companionId=chat_id;
        vkapi.messages_getHistory(chat_id,0,20,-1);

        console.log(attachId,attachPreview)
        if(attachId.length>0)
        {
            console.log(attachId,attachPreview)
            if(attachId.substring(0,4)=="wall")
                writer.c_Model.addAttachment(attachId,4) //Add Wall
            else if(attachId.substring(0,5)=="photo")
                writer.c_Model.addAttachment(attachId,0,attachPreview) //Add Photo
            else if(attachId.substring(0,3)=="doc")
            {
                if(attachPreview.length>0)
                    writer.c_Model.addAttachment(attachId,0,attachPreview)
                else
                    writer.c_Model.addAttachment(attachId,3)
            }
        }
    }
    Component.onDestruction:
    {
        globalSettings.companionId=0;
    }



}
