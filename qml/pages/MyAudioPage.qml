
import QtQuick 2.0
import Sailfish.Silica 1.0
import QtMultimedia 5.0
import ".."
Page{
    id:page

    signal stopPlay()
    signal startPlay()

    property bool playing: false
    property string searchString


    Timer{
        id:searchDelay
        interval:800
        repeat:false
        onTriggered: {
            searchString=searchField.text.toLowerCase().trim()
            musicModel.clear_Audios();
            vkapi.audio_Search(searchString,0,80)
        }
    }

    onStopPlay:
    {
        playMusic.stop()
        playlistModel.clear();
        playing=false;
        console.log(playing)
    }
    onStartPlay: {
        playMusic.play();
        playing=true;
    }
    Component.onCompleted:{
        musicModel.clear_Audios();
        vkapi.audio_get(globalSettings.User_Id.toString(),40);
    }
    Column
    {
        id:headerContainer
        width: page.width
        height:page.height
        PageHeader{
            id:name_info
            title:"Мои аудиозаписи"
        }
        SearchField {
            id: searchField
            width: parent.width
            height: Theme.itemSizeSmall
            anchors.top:name_info.bottom
            onTextChanged: {
                searchDelay.restart()
            }
        }
        SilicaListView {
            id: listView
            model: musicModel
            clip:true
            currentIndex: -1 // otherwise currentItem will steal focus
            width:parent.width
            height:parent.height
            anchors {
                top: searchField.bottom
                topMargin:5
                bottom:playerPanel.top
            }
            delegate: ListItem {
                id:listItem
                width:parent.width-2*Theme.paddingLarge
                contentWidth: width
                contentHeight: Theme.itemSizeMedium
                x:Theme.paddingLarge

                IconButton {
                    id: play
                    anchors.leftMargin: Theme.paddingLarge
                    icon.source: "image://theme/icon-l-play"
                    //                anchors.horizontalCenter: parent.horizontalCenter
                    onClicked:   {

                        playMusic.source = url;
                        playMusic.songName = title;
                        playMusic.gangName = artist;
                        playMusic.id = index;
                        if(playMusic.playbackState != Audio.PlayingState)
                        {
                            play.icon.source = "image://theme/icon-l-pause"
                            startPlay()
                        }
                        else
                        {
                            play.icon.source = "image://theme/icon-l-play"
                            stopPlay();
                        }
                        //           enabled: !iconButtons.playing
                    }
                }
                Label {
                    id:titlelabel
                    text: title
                    color: listItem.highlighted ? Theme.highlightColor : Theme.secondaryColor
                    anchors.left: play.right
                    anchors.leftMargin: Theme.paddingLarge
                    anchors.horizontalCenter: parent.horizontalCenter

                }
                Label {
                    id:artistlabel
                    text: artist
                    color: listItem.highlighted ? Theme.highlightColor : Theme.secondaryColor
                    anchors.top: titlelabel.bottom
                    anchors.left: titlelabel.left

                }
            }
        }
        DockedPanel {
            id: playerPanel
            open: playMusic.playbackState == Audio.PlayingState
            width: page.isPortrait ? parent.width : Theme.itemSizeExtraLarge + Theme.paddingLarge
            height:  Theme.itemSizeExtraLarge+Theme.itemSizeSmall + Theme.paddingLarge
            dock: page.isPortrait ? Dock.Bottom : Dock.Right
            IconButton {
                id: playPlayer
                icon.source: playMusic.playbackState != Audio.PlayingState ? "image://theme/icon-l-play" : "image://theme/icon-l-pause"
                anchors.left: parent.left
                anchors.leftMargin: Theme.paddingMedium
                anchors.topMargin: Theme.paddingLarge
                anchors.verticalCenter: parent.verticalCenter
                onClicked:   {
                    if(playMusic.playbackState != Audio.PlayingState)
                    {
                        startPlay()
                    }
                    else
                    {
                        stopPlay();
                    }
                }
            }
            ProgressBar {
                id: progressBar
                width: parent.width - playPlayer.width - replay.width
                maximumValue: playMusic.duration
                anchors.top: gangName.bottom
                anchors.leftMargin: Theme.paddingSmall
                anchors.left: playPlayer.right
                value:playMusic.position
                valueText: Format.formatDate(new Date(value), Formatter.DurationShort)
                label:Format.formatDate(new Date(progressBar.maximumValue), Formatter.DurationShort)

            }

            Label {
                id:songName
                text:playMusic.songName
                anchors{
                    left: playPlayer.right
                    leftMargin: 20
                    topMargin:  10
                }

            }
            Label {
                id:gangName
                text:playMusic.gangName
                anchors{
                    left: playPlayer.right
                    top:songName.bottom
                    leftMargin: 20

                }
            }

            IconButton {
                id: replay
                icon.source: "image://theme/icon-l-repeat"
                anchors.right: parent.right
                highlighted: playMusic.repeat
                onClicked:{
                    playMusic.repeat = !playMusic.repeat
                }
                visible: false
            }
        }
    }
}



