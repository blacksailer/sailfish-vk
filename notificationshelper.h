#ifndef NOTIFICATIONSHELPER_H
#define NOTIFICATIONSHELPER_H

#include <QObject>
#include <nemonotifications-qt5/notification.h>
#include "globalsettings.h"
class NotificationsHelper : public QObject
{
    Q_OBJECT
private:
    quint32 NotificationId=0;
    GlobalSettings* GlobalSet;
    QMap<int,Notification*> Published;
public:
    explicit NotificationsHelper(QObject *parent = 0,GlobalSettings* Gl=0);

signals:

public slots:
    void notifyMessage(int authorId, QString From, QString text);
    void notificationCLosed(uint Reason);

};

#endif // NOTIFICATIONSHELPER_H
