#include "logger.h"

Logger::Logger(QObject *parent) :
    QObject(parent)
{
    Network_Log.setFileName(QStandardPaths::writableLocation(QStandardPaths::DataLocation)+"/"+Network_Log_Name);


}

void Logger::writeToNetwork(const QString data)
{
 if(Network_Log.open(QIODevice::ReadWrite | QIODevice::Append))
 {
     QString ToFile = QDateTime::currentDateTime().toString("[dd/MM/yy hh:mm:ss]")+" - " +data+"\n";
Network_Log.write(ToFile.toUtf8());
Network_Log.close();
 }
 else
     qDebug()<<"File not open";
}
