<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name></name>
    <message id="Поделиться">
        <source>Share link</source>
        <extracomment>List header for link sharing method list</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message id="sailfish_browser-la-no_accounts">
        <source>No sharing accounts available. You can add accounts in settings</source>
        <extracomment>Empty state for share link page</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AboutPage</name>
    <message>
        <source>Группа ВК:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Контакты:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>SK - приложение для социальной сети VK.com.
                        Разработчик - blacksailer.
                        Пишите issue в gitlab и taiga, нужен фидбек!
                        Заходите в группу, пишите пожелания.
                        </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AttachComponent</name>
    <message>
        <source>Фото</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Отменить</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Прикрепить</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AttachmentsView</name>
    <message>
        <source>Скачать</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CommentItem</name>
    <message>
        <source>ответил</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ответила</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>SK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DialogsPage</name>
    <message>
        <source>загрузить еще диалоги</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Мои диалоги</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Выберите получателя</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FriendsPage</name>
    <message>
        <source>загрузить ещё</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Поиск</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Мои друзья</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GifViewerPage</name>
    <message>
        <source>Скачать</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GroupPage</name>
    <message>
        <source>Написать сообщение</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Добавить прошлые записи</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Скопировать ссылку</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GroupsInfo</name>
    <message>
        <source>Общая информация</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Медиа</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Информация</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Веб-сайт</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Начало встречи</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Конец встречи</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Полная информация</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Стена</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Написать на стене</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Адрес</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Подписчики</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Фото</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Видео</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Аудио</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Обсуждение</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Документы</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GroupsPage</name>
    <message>
        <source>Группы</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Поиск</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>загрузить ещё</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImageGalleryPage</name>
    <message>
        <source>Сохранить картинку</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MenuPage</name>
    <message>
        <source>Моя страница</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Мои друзья</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Мои новости</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Мои диалоги</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Мои группы</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>О программе</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Меню</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MessagesHistoryPage</name>
    <message>
        <source>добавить прошлые записи</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PostWriterDialog</name>
    <message>
        <source>Публикация на стене</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Опубликовать</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Отменить</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>От имени сообщества</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Подпись</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Это реклама</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Нельзя резместить больше трех рекламных сообщений</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Только для друзей</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Отложенная публикация</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecSinglePostPage</name>
    <message>
        <source>Комментарии</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Нет комментариев</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Удаление</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Удалить комментарий</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ответить</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecWallpost</name>
    <message>
        <source>Личным сообщением</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>В группу (не реализ)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>На стену (не реализ)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Скопировать текст</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Скопировать ссылку</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Удалить запись</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Удалить</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Отменить</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Вы уверены что хотите удалить запись?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecursingNewsPage</name>
    <message>
        <source>Мои новости</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TestMessageList</name>
    <message>
        <source>добавить прошлые записи</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UserInfo</name>
    <message>
        <source>Стена</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Общая информация</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Медиа</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>День рождения</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Город</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Instagram</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Facebook</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Twitter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Веб-сайт</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Полная информация</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Друзья</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Общие друзья</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Подписчики</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Фото</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Видео</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Аудио</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Написать на стене</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Группы</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Убрать из друзей</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Добавить в друзья</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UserPage</name>
    <message>
        <source>Добавить прошлые записи</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Скопировать ссылку</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Написать сообщение</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Вы уверены что хотите удалить из друзей?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Добавить</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Отменить</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>При желании, добавьте текст при добавлении в друзья</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Убрать</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VideoPage</name>
    <message>
        <source>Комментарии</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Нет комментариев</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
