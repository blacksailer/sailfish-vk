<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en" sourcelanguage="ru">
<context>
    <name></name>
    <message id="Поделиться">
        <source>Share link</source>
        <extracomment>List header for link sharing method list</extracomment>
        <translation>Share link</translation>
    </message>
    <message id="sailfish_browser-la-no_accounts">
        <source>No sharing accounts available. You can add accounts in settings</source>
        <extracomment>Empty state for share link page</extracomment>
        <translation>No sharing accounts available. You can add accounts in settings</translation>
    </message>
</context>
<context>
    <name>AboutPage</name>
    <message>
        <source>Группа ВК:</source>
        <translation>VK Group:</translation>
    </message>
    <message>
        <source>Контакты:</source>
        <translation>Contacts:</translation>
    </message>
    <message>
        <source>SK - приложение для социальной сети VK.com.
                        Разработчик - blacksailer.
                        Пишите issue в gitlab и taiga, нужен фидбек!
                        Заходите в группу, пишите пожелания.
                        </source>
        <translation>SK - application for social network VK.com.
                         The developer is blacksailer.
                         Write issue in gitlab and taiga, feedback is needed!
                         Come to the group, write your wishes.</translation>
    </message>
</context>
<context>
    <name>AttachComponent</name>
    <message>
        <source>Фото</source>
        <translation>Photo</translation>
    </message>
    <message>
        <source>Отменить</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <source>Прикрепить</source>
        <translation>Attach</translation>
    </message>
</context>
<context>
    <name>AttachmentsView</name>
    <message>
        <source>Скачать</source>
        <translation>Download</translation>
    </message>
</context>
<context>
    <name>CommentItem</name>
    <message>
        <source>ответил</source>
        <translation>answered to</translation>
    </message>
    <message>
        <source>ответила</source>
        <translation>answered to</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>SK</source>
        <translation>SK</translation>
    </message>
</context>
<context>
    <name>DialogsPage</name>
    <message>
        <source>загрузить еще диалоги</source>
        <translation>download more</translation>
    </message>
    <message>
        <source>Мои диалоги</source>
        <translation>My dialogs</translation>
    </message>
    <message>
        <source>Выберите получателя</source>
        <translation>Choose recipient</translation>
    </message>
</context>
<context>
    <name>FriendsPage</name>
    <message>
        <source>загрузить ещё</source>
        <translation>fetch more</translation>
    </message>
    <message>
        <source>Поиск</source>
        <translation>Search</translation>
    </message>
    <message>
        <source>Мои друзья</source>
        <translation>My friends</translation>
    </message>
</context>
<context>
    <name>GifViewerPage</name>
    <message>
        <source>Скачать</source>
        <translation>Download</translation>
    </message>
</context>
<context>
    <name>GroupPage</name>
    <message>
        <source>Написать сообщение</source>
        <translation>Write a message</translation>
    </message>
    <message>
        <source>Добавить прошлые записи</source>
        <translation>Fetch more</translation>
    </message>
    <message>
        <source>Скопировать ссылку</source>
        <translation>Copy link</translation>
    </message>
</context>
<context>
    <name>GroupsInfo</name>
    <message>
        <source>Общая информация</source>
        <translation>Main info</translation>
    </message>
    <message>
        <source>Медиа</source>
        <translation>Media</translation>
    </message>
    <message>
        <source>Информация</source>
        <translation>Information</translation>
    </message>
    <message>
        <source>Веб-сайт</source>
        <translation>Web-site</translation>
    </message>
    <message>
        <source>Начало встречи</source>
        <translation>Start date</translation>
    </message>
    <message>
        <source>Конец встречи</source>
        <translation>End date</translation>
    </message>
    <message>
        <source>Полная информация</source>
        <translation>Full information</translation>
    </message>
    <message>
        <source>Стена</source>
        <translation>Wall</translation>
    </message>
    <message>
        <source>Написать на стене</source>
        <translation>Write a wallpost</translation>
    </message>
    <message>
        <source>Адрес</source>
        <translation>Adress</translation>
    </message>
    <message>
        <source>Подписчики</source>
        <translation>Followers</translation>
    </message>
    <message>
        <source>Фото</source>
        <translation>Photo</translation>
    </message>
    <message>
        <source>Видео</source>
        <translation>Video</translation>
    </message>
    <message>
        <source>Аудио</source>
        <translation>Audio</translation>
    </message>
    <message>
        <source>Обсуждение</source>
        <translation>Threads</translation>
    </message>
    <message>
        <source>Документы</source>
        <translation>Documents</translation>
    </message>
</context>
<context>
    <name>GroupsPage</name>
    <message>
        <source>Группы</source>
        <translation>Groups</translation>
    </message>
    <message>
        <source>Поиск</source>
        <translation>Search</translation>
    </message>
    <message>
        <source>загрузить ещё</source>
        <translation>fetch more</translation>
    </message>
</context>
<context>
    <name>ImageGalleryPage</name>
    <message>
        <source>Сохранить картинку</source>
        <translation>Download picture</translation>
    </message>
</context>
<context>
    <name>MenuPage</name>
    <message>
        <source>Моя страница</source>
        <translation>My page</translation>
    </message>
    <message>
        <source>Мои друзья</source>
        <translation>My friends</translation>
    </message>
    <message>
        <source>Мои новости</source>
        <translation>My news</translation>
    </message>
    <message>
        <source>Мои диалоги</source>
        <translation>My dialogs</translation>
    </message>
    <message>
        <source>Мои группы</source>
        <translation>My groups</translation>
    </message>
    <message>
        <source>О программе</source>
        <translation>About</translation>
    </message>
    <message>
        <source>Меню</source>
        <translation>Menu</translation>
    </message>
</context>
<context>
    <name>MessagesHistoryPage</name>
    <message>
        <source>добавить прошлые записи</source>
        <translation>Fetch more</translation>
    </message>
</context>
<context>
    <name>PostWriterDialog</name>
    <message>
        <source>Публикация на стене</source>
        <translation>Wallpost</translation>
    </message>
    <message>
        <source>Опубликовать</source>
        <translation>Publish</translation>
    </message>
    <message>
        <source>Отменить</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <source>От имени сообщества</source>
        <translation>Post as group</translation>
    </message>
    <message>
        <source>Подпись</source>
        <translation>On my behalf</translation>
    </message>
    <message>
        <source>Это реклама</source>
        <translation>This is an ad</translation>
    </message>
    <message>
        <source>Нельзя резместить больше трех рекламных сообщений</source>
        <translation>You can not post more than three ads</translation>
    </message>
    <message>
        <source>Только для друзей</source>
        <translation>Friends only</translation>
    </message>
    <message>
        <source>Отложенная публикация</source>
        <translation>Timer</translation>
    </message>
</context>
<context>
    <name>RecSinglePostPage</name>
    <message>
        <source>Комментарии</source>
        <translation>Comments</translation>
    </message>
    <message>
        <source>Нет комментариев</source>
        <translation>No comments</translation>
    </message>
    <message>
        <source>Удаление</source>
        <translation>Delete</translation>
    </message>
    <message>
        <source>Удалить комментарий</source>
        <translation>Delete a comment</translation>
    </message>
    <message>
        <source>Ответить</source>
        <translation>Reply</translation>
    </message>
</context>
<context>
    <name>RecWallpost</name>
    <message>
        <source>Личным сообщением</source>
        <translation>Share via message</translation>
    </message>
    <message>
        <source>В группу (не реализ)</source>
        <translation>Share to group (not working yet)</translation>
    </message>
    <message>
        <source>На стену (не реализ)</source>
        <translation>Share to wall (not working yet)</translation>
    </message>
    <message>
        <source>Скопировать текст</source>
        <translation>Copy text</translation>
    </message>
    <message>
        <source>Скопировать ссылку</source>
        <translation>Copy link</translation>
    </message>
    <message>
        <source>Удалить запись</source>
        <translation>Delete wallpost</translation>
    </message>
    <message>
        <source>Удалить</source>
        <translation>Delete</translation>
    </message>
    <message>
        <source>Отменить</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <source>Вы уверены что хотите удалить запись?</source>
        <translation>Are you sure you want to delete this post?</translation>
    </message>
</context>
<context>
    <name>RecursingNewsPage</name>
    <message>
        <source>Мои новости</source>
        <translation>My news</translation>
    </message>
</context>
<context>
    <name>TestMessageList</name>
    <message>
        <source>добавить прошлые записи</source>
        <translation>Fetch more</translation>
    </message>
</context>
<context>
    <name>UserInfo</name>
    <message>
        <source>Стена</source>
        <translation>Wall</translation>
    </message>
    <message>
        <source>Общая информация</source>
        <translation>Main information</translation>
    </message>
    <message>
        <source>Медиа</source>
        <translation>Media</translation>
    </message>
    <message>
        <source>День рождения</source>
        <translation>Birthday</translation>
    </message>
    <message>
        <source>Город</source>
        <translation>Hometown</translation>
    </message>
    <message>
        <source>Instagram</source>
        <translation>Instagram</translation>
    </message>
    <message>
        <source>Facebook</source>
        <translation>Facebook</translation>
    </message>
    <message>
        <source>Twitter</source>
        <translation>Twitter</translation>
    </message>
    <message>
        <source>Веб-сайт</source>
        <translation>Website</translation>
    </message>
    <message>
        <source>Полная информация</source>
        <translation>Full information</translation>
    </message>
    <message>
        <source>Друзья</source>
        <translation>Friends</translation>
    </message>
    <message>
        <source>Общие друзья</source>
        <translation>Mutual friends</translation>
    </message>
    <message>
        <source>Подписчики</source>
        <translation>Followers</translation>
    </message>
    <message>
        <source>Фото</source>
        <translation>Photo</translation>
    </message>
    <message>
        <source>Видео</source>
        <translation>Video</translation>
    </message>
    <message>
        <source>Аудио</source>
        <translation>Audio</translation>
    </message>
    <message>
        <source>Написать на стене</source>
        <translation>Write a wallpost</translation>
    </message>
    <message>
        <source>Группы</source>
        <translation>Groups</translation>
    </message>
    <message>
        <source>Убрать из друзей</source>
        <translation>Remove from friends</translation>
    </message>
    <message>
        <source>Добавить в друзья</source>
        <translation>Add to friends</translation>
    </message>
</context>
<context>
    <name>UserPage</name>
    <message>
        <source>Добавить прошлые записи</source>
        <translation>Fetch more</translation>
    </message>
    <message>
        <source>Скопировать ссылку</source>
        <translation>Copy link</translation>
    </message>
    <message>
        <source>Написать сообщение</source>
        <translation>Write a message</translation>
    </message>
    <message>
        <source>Вы уверены что хотите удалить из друзей?</source>
        <translation>Are you sure you want to delete from friends?</translation>
    </message>
    <message>
        <source>Добавить</source>
        <translation>Add</translation>
    </message>
    <message>
        <source>Отменить</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <source>При желании, добавьте текст при добавлении в друзья</source>
        <translation>If desired, add the text when adding to a friend</translation>
    </message>
    <message>
        <source>Убрать</source>
        <translation>Remove</translation>
    </message>
</context>
<context>
    <name>VideoPage</name>
    <message>
        <source>Комментарии</source>
        <translation>Comments</translation>
    </message>
    <message>
        <source>Нет комментариев</source>
        <translation>No comments</translation>
    </message>
</context>
</TS>
