#ifndef GLOBALSETTINGS_H
#define GLOBALSETTINGS_H

#include <QObject>
#include <QQmlEngine>
#include <QJSEngine>
#include "databasesqlite.h"

class GlobalSettings : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(GlobalSettings)

    int m_LongPollTimeout=20000;
    bool m_UseSSL=true;
    QString m_Key_LongPoll;
    QString m_Server_LongPoll;

    DatabaseSqlite* db;

    unsigned int m_Ts_LongPoll=0;

    int m_Pts_LongPoll=0;

    int m_unreadCount=0;

    int m_User_Id=0;

    QString m_start_FROM;

    int m_companionId=0;

    QString m_FirstName;

    QString m_LastName;

QString m_AvatarPath;

QString m_accessToken;

public:
explicit GlobalSettings(QObject *parent = 0);

    static QObject *qmlInstance(QQmlEngine *engine, QJSEngine *scriptEngine) {

        Q_UNUSED(engine);
               Q_UNUSED(scriptEngine);

               return new GlobalSettings;
        }

    Q_PROPERTY(QString start_FROM READ start_FROM WRITE setStart_FROM NOTIFY start_FROMChanged)
    //user_id
    Q_PROPERTY(int User_Id READ User_Id WRITE setUser_Id NOTIFY User_IdChanged)
    Q_PROPERTY(QString FirstName READ FirstName WRITE setFirstName NOTIFY FirstNameChanged)
    Q_PROPERTY(QString LastName READ LastName WRITE setLastName NOTIFY LastNameChanged)
    Q_PROPERTY(QString AvatarPath READ AvatarPath WRITE setAvatarPath NOTIFY AvatarPathChanged)
    //
    Q_PROPERTY(bool UseSSL READ UseSSL WRITE setUseSSL NOTIFY UseSSLChanged)
    Q_PROPERTY(QString accessToken READ accessToken WRITE setaccessToken NOTIFY accessTokenChanged)
    Q_PROPERTY(int LongPollTimeout READ LongPollTimeout WRITE setLongPollTimeout NOTIFY LongPollTimeoutChanged)

    //Ответы от сервера на LongPoll
    Q_PROPERTY(QString Key_LongPoll READ Key_LongPoll WRITE setKey_LongPoll NOTIFY Key_LongPollChanged)
    Q_PROPERTY(QString Server_LongPoll READ Server_LongPoll WRITE setServer_LongPoll NOTIFY Server_LongPollChanged)

    //Свойства для запроса на LongPoll
    Q_PROPERTY(unsigned int Ts_LongPoll READ Ts_LongPoll WRITE setTs_LongPoll NOTIFY Ts_LongPollChanged)
    Q_PROPERTY(int Pts_LongPoll READ Pts_LongPoll WRITE setPts_LongPoll NOTIFY Pts_LongPollChanged)

    //Непрочитанные сообщения
    Q_PROPERTY(int unreadCount READ unreadCount WRITE setUnreadCount NOTIFY unreadCountChanged)

    //Id собеседника
    Q_PROPERTY(int companionId READ companionId WRITE setCompanionId NOTIFY companionIdChanged)


int LongPollTimeout() const;
bool UseSSL() const;
QString Key_LongPoll() const;
QString Server_LongPoll() const;

unsigned int Ts_LongPoll() const;

int Pts_LongPoll() const;

int unreadCount() const;

int User_Id() const;

QString start_FROM() const;

int companionId() const;

QString FirstName() const
{
    return m_FirstName;
}

QString LastName() const
{
    return m_LastName;
}

QString AvatarPath() const
{
    return m_AvatarPath;
}

QString accessToken() const
{
    return m_accessToken;
}

signals:
void LongPollTimeoutChanged(int arg);
void UseSSLChanged(bool arg);
void Key_LongPollChanged(QString arg);
void Server_LongPollChanged(QString arg);

void Ts_LongPollChanged(unsigned int arg);

void Pts_LongPollChanged(int arg);

void unreadCountChanged(int arg);

void User_IdChanged(int arg);

void start_FROMChanged(QString start_FROM);

void companionIdChanged(int companionId);

void FirstNameChanged(QString FirstName);

void LastNameChanged(QString LastName);

void AvatarPathChanged(QString AvatarPath);

void accessTokenChanged(QString accessToken);

public slots:

void setLongPollData(QString key,QString server,unsigned int ts, int pts);

void setLongPollTimeout(int arg);
void setUseSSL(bool arg);
void setKey_LongPoll(QString arg);
void setServer_LongPoll(QString arg);
void setTs_LongPoll(unsigned int arg);
void setPts_LongPoll(int arg);
void setUnreadCount(int arg);
void setUser_Id(int arg);
void setStart_FROM(QString arg);
void setCompanionId(int companionId);
void setFirstName(QString FirstName)
{
    if (m_FirstName == FirstName)
        return;

    m_FirstName = FirstName;
    emit FirstNameChanged(FirstName);
}
void setLastName(QString LastName)
{
    if (m_LastName == LastName)
        return;

    m_LastName = LastName;
    emit LastNameChanged(LastName);
}
void setAvatarPath(QString AvatarPath)
{
    if (m_AvatarPath == AvatarPath)
        return;

    m_AvatarPath = AvatarPath;
    emit AvatarPathChanged(AvatarPath);
}
void setaccessToken(QString accessToken)
{
    if (m_accessToken == accessToken)
        return;

    m_accessToken = accessToken;
    emit accessTokenChanged(accessToken);
}
};

#endif // GLOBALSETTINGS_H
