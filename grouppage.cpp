#include "grouppage.h"

GroupPage::GroupPage(QObject *parent) : QObject(parent)
{

}

void GroupPage::getResult(QVariantMap answer)
{
    qDebug()<<answer["name"].toString()<<answer<<isActive();
    if(isActive())
    {
        setId(answer["id"].toInt());
        setName(answer["name"].toString());
        setScreen_name(answer["screen_name"].toString());
        setIs_closed(answer["is_closed"].toInt());
        setType(answer["type"].toString());
        if(answer["is_admin"].toInt()>0)
            setIs_admin(true);
        else
            setIs_admin(false);
        if(answer["is_member"].toInt()>0)
            setIs_member(true);
        else
            setIs_member(false);
        setActivity(answer["activity"].toString());
        setAge_limits(answer["age_limits"].toInt());
        if(answer["can_create_topic"].toInt()>0)
            setCan_create_topic(true);
        else
            setCan_create_topic(false);
        if(answer["can_post"].toInt()>0)
            setCan_post(true);
        else
            setCan_post(false);
        if(answer["can_see_all_posts"].toInt()>0)
            setCan_see_all_posts(true);
        else
            setCan_see_all_posts(false);
        if(answer["can_upload_doc"].toInt()>0)
            setCan_upload_doc(true);
        else
            setCan_upload_doc(false);
        if(answer["can_upload_video"].toInt()>0)
            setCan_upload_video(true);
        else
            setCan_upload_video(false);
        setDescription(answer["description"].toString());
        setFixed_post(answer["fixed_post"].toInt());
        if(answer["is_favorite"].toInt()>0)
            setIs_favorite(true);
        else
            setIs_favorite(false);
        if(answer["is_hidden_from_feed"].toInt()>0)
            setIs_hidden_from_feed(true);
        else
            setIs_hidden_from_feed(false);
        setMain_album_id(answer["main_album_id"].toInt());
        setMain_section(answer["main_section"].toInt());
        setMember_status(answer["member_status"].toInt());
        setMembers_count(answer["members_count"].toInt());
        setSite(answer["site"].toString());
        setStatus(answer["status"].toString());
        if(answer["verified"].toInt()>0)
            setVerified(true);
        else
            setVerified(false);
        setWiki_page(answer["wiki_page"].toString());









        setPhoto_50(answer["photo_50"].toString());
        setPhoto_100(answer["photo_100"].toString());
        setPhoto_200(answer["photo_200"].toString());

        if(answer.contains("city"))
        {
            auto map = answer["city"].toMap();
            City *city = new City();
            city->setParent(this);
            city->setId(map["id"].toInt());
            city->setTitle(map["title"].toString());
            setCity(city);
        }

        if(answer.contains("counters"))
        {
            auto map = answer["counters"].toMap();
            Counters *counters = new Counters();
            counters->setParent(this);
            counters->setPhotos(map["photos"].toInt());
            counters->setAlbums(map["albums"].toInt());
            counters->setVideos(map["videos"].toInt());
            counters->setTopics(map["topics"].toInt());
            counters->setDocs(map["docs"].toInt());
            m_counters=counters;
            emit countersChanged(m_counters);
        }
        if(answer.contains("contacts"))
        {
            setContacts(answer["contacts"].toList());
        }
        if(answer.contains("links"))
        {
            setLinks(answer["links"].toList());
        }
        if(answer.contains("market"))
        {
            auto map = answer["market"].toMap();
            if(map["enabled"].toInt()>0)
            {
                Market *market = new Market();
                market->setParent(this);
                market->setEnabled(true);
                market->setContact_id(map["contact_id"].toInt());
                market->setCurrency_name(map["currency_name"].toString());
                market->setMain_album_id(map["main_album_id"].toInt());
                market->setPrice_max(map["price_max"].toInt());
                market->setPrice_min(map["price_min"].toInt());
                m_market=market;
                emit marketChanged(m_market);
            }
        }
        if(answer.contains("place"))
        {
            auto map = answer["place"].toMap();
            Place *place = new Place();
            place->setParent(this);
            place->setId(map["id"].toInt());
            place->setTitle(map["title"].toString());
            place->setLatitude(map["latitude"].toDouble());
            place->setLongitude(map["longitude"].toDouble());
            place->setIcon(map["icon"].toString());
            place->setType(map["type"].toString());
            place->setCountry(map["country"].toInt());
            place->setCity(map["city"].toInt());
            place->setCheckins(map["checkins"].toInt());
            place->setUpdated(map["updated"].toInt());
            m_place = place;
            emit placeChanged(m_place);
        }
        if(answer["has_photo"].toInt()>0)
            setHas_photo(true);
        else
            setHas_photo(false);
        if(answer["can_message"].toInt()>0)
            setCan_message(true);
        else
            setCan_message(false);




    }
}

