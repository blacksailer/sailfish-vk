#include "notificationpagestarter.h"

NotificationPageStarter::NotificationPageStarter(QObject *parent) : QObject(parent)
{

}

void NotificationPageStarter::addPage(NotificationPageStarter::PAGETYPE type, QVariant params)
{
    QVariantMap NewPage;

    NewPage["pageType"] = type;
    NewPage["pageParam"] = params;
    m_Pages.append(NewPage);
}

QVariantList NotificationPageStarter::getPages()
{
    QVariantList result;
    while(m_Pages.size()>0)
        result.append(m_Pages.dequeue());

    return result;
}
