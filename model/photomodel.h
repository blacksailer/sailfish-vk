#ifndef PHOTOMODEL_H
#define PHOTOMODEL_H

#include <QObject>
#include <QtCore>
#include <QJsonArray>
#include <QJsonObject>
#include "items/photoitem.h"
class PhotoModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit PhotoModel(QObject *parent = 0);
    enum TypeInputs {
        ID,
ALBUM_ID,
OWNER_ID,
DATE,
TEXT,
PHOTO_75,
PHOTO_130,
PHOTO_604,
PHOTO_807,
PHOTO_1280,
WIDTH,
HEIGHT,
ACCESS_KEY
    };
    int rowCount(const QModelIndex &parent) const;
     QVariant data(const QModelIndex &index, int role) const;
     Qt::ItemFlags flags(const QModelIndex &index) const;
         QHash<int, QByteArray> roleNames() const;

         void addPhotoItem(const QJsonObject &value);
         QList<PhotoItem> *PhotoAttachments;

private:
    QHash<int,QByteArray> roles;



};

#endif // PHOTOMODEL_H
