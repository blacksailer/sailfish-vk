#include "groupsmodel.h"

GroupsModel::GroupsModel(QObject *parent) : QAbstractListModel(parent)
{
    roles = roleNames();

}

int GroupsModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    if(isSearchMode())
        return SearchItems.size();
    else
        return Items.size();

}

QVariant GroupsModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();
    if(isSearchMode())
    {
        if(index.row()>=SearchItems.size())
            return QVariant();
        if(role==GROUP_ID)
            return SearchItems.at(index.row()).toMap()["id"];
        else if(role==PHOTO_50)
            return SearchItems.at(index.row()).toMap()["photo_50"];
        else if(role==PHOTO_100)
            return SearchItems.at(index.row()).toMap()["photo_100"];
        else if(role==NAME)
            return SearchItems.at(index.row()).toMap()["name"];
        else if(role==IS_CLOSED)
            return SearchItems.at(index.row()).toMap()["is_closed"];
        else if(role==TYPE)
            return SearchItems.at(index.row()).toMap()["type"];
        else if(role==PHOTO_200)
            return SearchItems.at(index.row()).toMap()["photo_200"];
        else if(role==STATUS)
            return SearchItems.at(index.row()).toMap()["status"];

        else if(role==IS_ADMIN)
            return SearchItems.at(index.row()).toMap()["is_admin"];
        else if(role==IS_MEMBER)
            return SearchItems.at(index.row()).toMap()["is_member"];
        else
            return QVariant();
    }
    else
    {
        if(index.row()>=Items.size())
            return QVariant();
        if(role==GROUP_ID)
            return Items.at(index.row()).toMap()["id"];
        else if(role==PHOTO_50)
            return Items.at(index.row()).toMap()["photo_50"];
        else if(role==PHOTO_100)
            return Items.at(index.row()).toMap()["photo_100"];
        else if(role==NAME)
            return Items.at(index.row()).toMap()["name"];
        else if(role==IS_CLOSED)
            return Items.at(index.row()).toMap()["is_closed"];
        else if(role==TYPE)
            return Items.at(index.row()).toMap()["type"];
        else if(role==PHOTO_200)
            return Items.at(index.row()).toMap()["photo_200"];
        else if(role==STATUS)
            return Items.at(index.row()).toMap()["status"];

        else if(role==IS_ADMIN)
            return Items.at(index.row()).toMap()["is_admin"];
        else if(role==IS_MEMBER)
            return Items.at(index.row()).toMap()["is_member"];
        else
            return QVariant();

    }
}

QHash<int, QByteArray> GroupsModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[GROUP_ID] = "group_id";
    roles[PHOTO_50] = "photo_50";
    roles[PHOTO_100]="photo_100";
    roles[PHOTO_100]="photo_200";
    roles[NAME] = "name";
    roles[TYPE]="type";
    roles[STATUS]="status";

    roles[IS_CLOSED] = "is_closed";
    roles[IS_ADMIN] = "is_admin";
    roles[IS_MEMBER] = "is_member";

    return roles;

}

void GroupsModel::addGroups(QVariantList value)
{
    foreach ( auto item, value) {
        setTotalCount(item.toMap()["totalCount"].toInt());
        beginInsertRows(QModelIndex(),rowCount(),rowCount());
        if(isSearchMode())
            SearchItems.append(item);
        else
            Items.append(item);
        endInsertRows();
    }

}

void GroupsModel::clearGroups()
{
    beginRemoveRows(QModelIndex(),0,rowCount());
    if(isSearchMode())
        SearchItems.clear();
    else
        Items.clear();
    endRemoveRows();
}

void GroupsModel::setDataProvider(Parser *DataProvider)
{
    if (m_DataProvider == DataProvider)
        return;

    m_DataProvider = DataProvider;
    connect(m_DataProvider,&Parser::groupsGenerated,this,&GroupsModel::addGroups);

    emit dataProviderChanged(DataProvider);
}

void GroupsModel::setIsSearchMode(bool isSearchMode)
{
    if (m_isSearchMode == isSearchMode)
        return;
    if(rowCount()>0)
    {
    beginRemoveRows(QModelIndex(),0,rowCount());
    endRemoveRows();
    }

    m_isSearchMode = isSearchMode;
    emit isSearchModeChanged(isSearchMode);
    if(rowCount()>0)
    {
        beginInsertRows(QModelIndex(),0,rowCount()-1);
        endInsertRows();
    }
}
