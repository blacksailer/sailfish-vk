#ifndef NEWSFEEDMODEL_H
#define NEWSFEEDMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include<QHash>

#include <QJsonValue>
#include <QJsonObject>
#include <QVariantMap>
#include <QDateTime>
#include "items/wallitem.h"
class NewsFeedModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit NewsFeedModel(QObject *parent = 0);
    enum TypeInputs {
        Id,
        SOURCE_ID,
        From_name,
        From_surname,
        From_photo,
        Owner_id,
        Date,
        Post_type,
        Text,
        Attachments,
        AudioAttachments,
        VIDEOATTACHMENTS,
        User_info,
        REPOST_FIRST_NAME,
        REPOST_SURNAME,
        REPOST_PHOTO,
        Repost_wall_id,
        Repost_owner_id,
        Repost_from_id,
         Repost_date,
         Repost_post_type,
         Repost_text,
        LIKES_COUNT,
        USER_LIKES,
        USER_CANLIKE,
        REPOSTS_COUNT

    };

    int rowCount(const QModelIndex &parent= QModelIndex()) const;
     QVariant data(const QModelIndex &index, int role) const;
     Qt::ItemFlags flags(const QModelIndex &index) const;
         bool setData(const QModelIndex & index, const QJsonValue &value,
                      int role = Qt::EditRole);
         QHash<int, QByteArray> roleNames() const;

         void addNewsItem(const WallItem &value, const QVariantList &attachments, const QVariantList &audioattachments,const bool appended);
signals:


public slots:
         QVariantList getAttach(int id);
         void clear_Newsfeed();

private:
    QList<WallItem> *Items;  //Записи стены пользователя
    QHash<int,QVariantList> PhotoNdx; //Пара ключ-запись,значение-массив прикрепленных фото
    QHash<int,QVariantList> AudioNdx; //Пара ключ-запись,значение-массив прикрепленных фото

    QHash<int,QVariantList> UserNdx;
    QHash<int,QByteArray> roles; //Роли


};

#endif // NEWSFEEDMODEL_H
