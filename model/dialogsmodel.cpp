#include "dialogsmodel.h"


DialogsModel::DialogsModel(QObject *parent)
{
    setParent(parent);
    roles = roleNames();

}



int DialogsModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return Items.count();

}

QVariant DialogsModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();
    if(index.row()>=Items.size())
        return QVariant();
     MessageItem* Wi = Items.at(index.row());
    if(role==ID)
        return Wi->id();
    else if(role==USER_ID)
        return Wi->userId();
    else if(role==FROM_ID)
        return Wi->fromId();
    else if(role==FIRST_NAME)
        return Wi->firstName();
    else if(role==LAST_NAME)
        return Wi->lastName();
    else if(role==USER_PHOTO)
        return Wi->userPhoto();

    else if(role==DATE)
        return Wi->date();
    else if(role==READ_STATE)
        return Wi->readState();
    else if(role==OUT)
        return Wi->out();
    else if(role==TITLE)
        return Wi->title();
    else if(role==BODY)
        return Wi->body();
    else if(role==PHOTO_NDX)
        return Wi->GridAttachments();
       else if(role==EMOJI)
        return Wi->emoji();
    else if(role==IMPORTANT)
        return Wi->important();
    else if(role==DELETED)
        return Wi->deleted();
    else if(role==CHAT_ID)
        return Wi->chatId();
    else if(role==CHAT_ACTIVE)
        return Wi->chatActive();
    else if(role==PUSH_SETTINGS)
        return Wi->pushSettings();
    else if(role==USER_COUNT)
        return Wi->userCount();
    else if(role==ACTION)
        return Wi->action();
    else if(role==ACTION_MID)
        return Wi->actionMid();
    else if(role==ACTION_EMAIL)
        return Wi->actionEmail();
    else if(role==ACTION_TEXT)
        return Wi->actionText();
    else if(role==PHOTO_50)
        return Wi->photo50();
    else if(role==PHOTO_100)
        return Wi->photo100();
    else if(role==PHOTO_200)
        return Wi->photo200();

    else
        return QVariant();

}

Qt::ItemFlags DialogsModel::flags(const QModelIndex &index) const
{
    if(!index.isValid())
        return Qt::ItemIsEnabled;

    return QAbstractListModel::flags(index) | Qt::ItemIsEditable;

}


QHash<int, QByteArray> DialogsModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[ID] = "id";
    roles[USER_ID] = "user_id";
    roles[FIRST_NAME]="first_name";
    roles[LAST_NAME]="last_name";
    roles[USER_PHOTO]="user_photo";

    roles[FROM_ID]="from_id";
    roles[DATE] = "date";
    roles[READ_STATE]="read_state";
    roles[OUT] = "out";
    roles[TITLE] = "title";
    roles[BODY] = "body";
    roles[PHOTO_NDX] = "photo_attachments";
    roles[FWDMSGNDX]="fwdmsg";
    roles[EMOJI]="emoji";
    roles[IMPORTANT]="important";
    roles[DELETED]="deleted";
    roles[CHAT_ID]="chat_id";
    roles[CHAT_ACTIVE]="chat_active";
    roles[PUSH_SETTINGS]="push_settings";
    roles[USER_COUNT]="user_count";
    roles[ACTION]="action";
    roles[ACTION_MID]="action_mid";
    roles[ACTION_EMAIL]="action_email";
    roles[ACTION_TEXT]="action_text";
    roles[PHOTO_50]="photo_50";
    roles[PHOTO_100]="photo_100";
    roles[PHOTO_200]="photo_200";

    return roles;

}

void DialogsModel::addDialogItem(MessageItem *value)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    Items.append(value);
    endInsertRows();

}

Parser *DialogsModel::DataProvider() const
{
    return m_DataProvider;
}

bool DialogsModel::isActive() const
{
    return m_isActive;
}

void DialogsModel::clear_Dialogs()
{
    for(int i=0;i<rowCount();i++)
    {
        Items.at(i)->deleteLater();
    }
    beginRemoveRows(QModelIndex(),0,Items.size()-1);
    Items.clear();
    endRemoveRows();
}

void DialogsModel::setDataProvider(Parser *DataProvider)
{
    m_DataProvider =DataProvider;
    connect(m_DataProvider,&Parser::dialogGenerated,this,&DialogsModel::addDialogItem);
    emit dataProviderChanged(DataProvider);
}

void DialogsModel::setActive(bool isActive)
{
    if (m_isActive == isActive)
        return;

    m_isActive = isActive;
    emit isActiveChanged(isActive);
}
