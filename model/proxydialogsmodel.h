#ifndef PROXYFRIENDSMODEL_H
#define PROXYFRIENDSMODEL_H

#include <QObject>
#include <QSortFilterProxyModel>
#include "dialogsmodel.h"

class ProxyDialogsModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(Parser* DataProvider READ DataProvider WRITE setDataProvider NOTIFY dataProviderChanged)
    Q_PROPERTY(bool isActive READ isActive WRITE setIsActive NOTIFY isActiveChanged)

    Parser* m_DataProvider;
    DialogsModel* FModel;

public:
ProxyDialogsModel();
Parser* DataProvider() const
{
    return m_DataProvider;
}
bool isActive() const
{
    return FModel->isActive();
}

public slots:
int modelCount();
void clear_Dialogs();
void setDataProvider(Parser* DataProvider)
{
    FModel->setDataProvider(DataProvider);
    if (m_DataProvider == DataProvider)
        return;

    m_DataProvider = DataProvider;
    emit dataProviderChanged(DataProvider);
}
void setIsActive(bool isActive)
{
   FModel->setActive(isActive);
}

signals:
void dataProviderChanged(Parser* DataProvider);
void isActiveChanged(bool isActive);
};

#endif // PROXYFRIENDSMODEL_H
