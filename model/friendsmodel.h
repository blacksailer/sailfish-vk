#ifndef FRIENDSMODEL_H
#define FRIENDSMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <QSortFilterProxyModel>
#include<QHash>
#include <QVariantMap>
#include <QDateTime>
#include "../parser.h"

class FriendsModel : public QAbstractListModel

{
    Q_PROPERTY(Parser* DataProvider READ DataProvider WRITE setDataProvider NOTIFY dataProviderChanged)
    Q_PROPERTY(bool isSearchMode READ isSearchMode WRITE setIsSearchMode NOTIFY isSearchModeChanged)
    Q_PROPERTY(int totalCount READ totalCount WRITE setTotalCount NOTIFY totalCountChanged)
    Q_OBJECT
public:
    explicit FriendsModel(QObject *parent = 0);

    enum TypeInputs{
        USER_ID,
        PHOTO_50,
        PHOTO_100,
        NAME,
        SURNAME,
        ONLINE
    };
    int rowCount(const QModelIndex &parent= QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;

    void addFriends(QVariantList value);
    Parser* DataProvider() const;

    bool isSearchMode() const
    {
        return m_isSearchMode;
    }

    int totalCount() const
    {
        return m_totalCount;
    }

signals:


    void dataProviderChanged(Parser* DataProvider);

    void isSearchModeChanged(bool isSearchMode);

    void totalCountChanged(int totalCount);

public slots:
void clearFriends();
void setDataProvider(Parser* DataProvider);

void setIsSearchMode(bool isSearchMode);

void setTotalCount(int totalCount)
{
    if (m_totalCount == totalCount)
        return;

    m_totalCount = totalCount;
    emit totalCountChanged(totalCount);
}

private:
    QVariantList Items;  //Записи стены пользователя
    QVariantList SearchItems;  //Записи стены пользователя

    QHash<int,QByteArray> roles; //Роли
    Parser* m_DataProvider;
    bool m_isSearchMode=false;
    int m_totalCount;
};

#endif // FRIENDSMODEL_H
