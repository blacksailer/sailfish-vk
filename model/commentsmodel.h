#ifndef COMMENTSMODEL_H
#define COMMENTSMODEL_H

#include <QObject>
#include <QAbstractListModel>

#include "items/commentitem.h"
#include "../parser.h"
class CommentsModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int lastId READ lastId NOTIFY lastIdChanged)
    Q_PROPERTY(int commentsCount READ commentsCount WRITE setCommentsCount NOTIFY commentsCountChanged)
    Q_PROPERTY(int newCommentId READ newCommentId WRITE setNewCommentId NOTIFY newCommentIdChanged)
    Q_PROPERTY(Parser* DataProvider READ DataProvider WRITE setDataProvider NOTIFY dataProviderChanged)
    Q_PROPERTY(bool isActive READ isActive WRITE setActive NOTIFY isActiveChanged)

public:
    CommentsModel();

    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &parent= QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;

    Parser *DataProvider() const;

    bool isActive() const;

    int lastId() const;

    int commentsCount() const
    {
        return m_commentsCount;
    }

    int newCommentId() const
    {
        return m_newCommentId;
    }

public slots:
    void addItem(CommentItem* Item, bool Appended);
    void remove(int row);

    void setDataProvider(Parser* DataProvider);

    void setActive(bool isActive);

    void setCommentsCount(int commentsCount)
    {
        if (m_commentsCount == commentsCount)
            return;

        m_commentsCount = commentsCount;
        emit commentsCountChanged(commentsCount);
    }

    void setNewCommentId(int newCommentId)
    {
        if (m_newCommentId == newCommentId)
            return;

        m_newCommentId = newCommentId;
        emit newCommentIdChanged(newCommentId);
    }

signals:


    void dataProviderChanged(Parser* DataProvider);

    void isActiveChanged(bool isActive);

    void lastIdChanged(int lastId);

    void commentsCountChanged(int commentsCount);

    void newCommentIdChanged(int newCommentId);

protected:
    QList<CommentItem*> Items;  //Записи стены пользователя
    QHash<int,QByteArray> roles; //Роли
    enum TYPE{
        ID,
        FROM_ID,
        FIRSTNAME,
        LASTNAME,
        PHOTO_50,
        DATE,
        TEXT,
        SEX,
        REPLY_TO_USER,
        REPLY_TO_USERNAME,
        REPLY_TO_COMMENT,
        GRIDATTACHMENTS,
        AUDIOATTACHMENTS,
        VIDEOATTACHMENTS,
        DOCATTACHMENTS,
        GIFATTACHMENTS,
        STICKERATTACHMENTS
    };

private:
    Parser* m_DataProvider;
    bool m_isActive;
    int m_lastId=10000000;
    int m_commentsCount=0;
    bool cmp(CommentItem* itm);
    int m_newCommentId;

    // QAbstractItemModel interface
public:
};



#endif // COMMENTSMODEL_H
