#ifndef ATTACHMENTSTOSENDMODEL_H
#define ATTACHMENTSTOSENDMODEL_H
#include <QAbstractListModel>
#include <QtNetwork>
#include <QtQml>
#include <QImage>
#include "items/attachmenttosenditem.h"
class AttachmentsToSendModel : public QAbstractListModel
{
    Q_OBJECT
private:
    QHash<int,QByteArray> roles; //Роли
    QNetworkAccessManager* mngr;
    QMap<QNetworkReply*,AttachmentToSendItem*> Requests;
    QMap<QNetworkReply*,QFile*> Files;
    QList<AttachmentToSendItem*> Items;

    void doConnect();

    void sendPhoto(AttachmentToSendItem* itm);

    Q_PROPERTY(QString UploadPhotoServer READ UploadPhotoServer WRITE setUploadPhotoServer NOTIFY UploadPhotoServerChanged)
    QString m_UploadPhotoServer;

public:
    explicit AttachmentsToSendModel(QObject *parent = 0);
    ~AttachmentsToSendModel();
    enum ObjectProperty {
        TYPE,
        SOURCE,
        STATUS,
        URL,
        PROGRESS,
        TOTAL,
        SENDURL
    };
    enum Network{
      GETUPLOAD
    };
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;
    int rowCount(const QModelIndex &parent= QModelIndex()) const;


    QString UploadPhotoServer() const
    {
        return m_UploadPhotoServer;
    }

signals:
    void errorOccured(QString text);
    void UploadPhotoServerChanged(QString UploadPhotoServer);
    void photosavemessagesphoto(QString json,int idx);

public slots:
    void addAttachment(QString source, int Type, QString value=QString());
    void removeAttachment(int index);
    void clear();
    void setPhotoAttachmentId(QString id, int indexIns);
    QString getAllSendUrls();


    // QAbstractItemModel interface
    void rqstEnd(QNetworkReply* reply);
    void qstEnd();
    void progressChanged(qint64 a, qint64 b);

    void setUploadPhotoServer(QString UploadPhotoServer)
    {
        if (m_UploadPhotoServer == UploadPhotoServer)
            return;

        m_UploadPhotoServer = UploadPhotoServer;
        emit UploadPhotoServerChanged(UploadPhotoServer);
    }
};



/*
 * attachment	медиавложения к личному сообщению,
 * перечисленные через запятую. Каждое прикрепление представлено в формате:

    <type><owner_id>_<media_id>



<type> — тип медиавложения:

    photo — фотография;
    video — видеозапись;
    audio — аудиозапись;
    doc — документ;
    wall — запись на стене;
    market — товар.




<owner_id> — идентификатор владельца медиавложения
 (обратите внимание, если объект находится в сообществе,
этот параметр должен быть отрицательным).
<media_id> — идентификатор медиавложения.

Например:

    photo100172_166443618



Параметр является обязательным, если не задан параметр message.

В случае, если прикрепляется объект, принадлежащий
другому пользователю следует добавлять
 к вложению его access_key в формате <type><owner_id>_<media_id>_<access_key>,


*/






#endif // ATTACHMENTSTOSENDMODEL_H
