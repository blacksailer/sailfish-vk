#include "wallmodel.h"


WallModel::WallModel(QObject *parent)
{
    setParent(parent);
    qDebug()<<"Setting roles";
    roles = roleNames();
}



QHash<int, QByteArray> WallModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[ID] = "id";
    roles[SOURCE_ID] = "source_id";
    roles[FROM_NAME]="from_name";
    roles[FROM_SURNAME] = "from_surname";
    roles[FROM_PHOTO]="from_photo";
    roles[OWNER_ID] = "owner_id";
    roles[DATE] = "date";
    roles[POST_TYPE] = "post_type";
    roles[TEXT] = "datatext";
    roles[ATTACHMENTS]="attachments";
    roles[AUDIOATTACHMENTS]="listAttachments";
    roles[VIDEOATTACHMENTS]="videoAttachments";
    roles[DOCATTACHMENTS]="docAttachments";
    roles[GIFATTACHMENTS]="gifAttachments";
    roles[LIKES_COUNT]="likes_count";
    roles[USER_LIKES]="user_islikes";
    roles[USER_CANLIKE]="user_canlike";
    roles[REPOSTS_COUNT]="reposts_count";
    roles[COMMENTS_COUNT]="commentsCount";
    roles[COMMENTS_CANPOST]="commentsCanPost";
    roles[CHILD_WALLITEM]="child_wallitem";

    return roles;

}


void WallModel::addWallItem( RecursingWallItem *value,const bool appended)
{
    qDebug()<<rowCount();
    if(isActive())
    {
        if(appended)
         {   beginInsertRows(QModelIndex(), rowCount(), rowCount());
        Items.append(value);
        endInsertRows();}
        else{
        beginInsertRows(QModelIndex(), 0, 0);
        Items.prepend(value);
        endInsertRows();
        }
        qDebug()<<rowCount();
    }
}

QObject *WallModel::getChild(int id)
{
    qDebug()<<"[WallModel] getChild="<<id;
    for(int i=0;i<rowCount();i++)
        if(Items.at(i)->id()==id)
            return static_cast<QObject*>(Items.at(i)->repostedPost());
    return nullptr;

}

void WallModel::remove(int row)
{
        beginRemoveRows(QModelIndex(),row,0);
        Items.removeAt(row);
        endRemoveRows();
}

void WallModel::clear_Wall()
{
    beginRemoveRows(QModelIndex(),0,Items.size()-1);
    Items.clear();
    qDebug()<<"clearing";
    endRemoveRows();
}

void WallModel::setDataProvider(Parser *DataProvider)
{
    if (m_DataProvider == DataProvider)
        return;

    m_DataProvider = DataProvider;
    connect(m_DataProvider,&Parser::wallGenerated,this,&WallModel::addWallItem);

    emit dataProviderChanged(DataProvider);
}

void WallModel::setActive(bool isActive)
{
    if (m_isActive == isActive)
        return;

    m_isActive = isActive;
    emit isActiveChanged(isActive);
}

int WallModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return Items.count();
}

QVariant WallModel::data(const QModelIndex &index,int role) const
{
    //    qDebug()<<" Data asked for "<<index.row()<<" and role "<<role;
    if(!index.isValid())
        return QVariant();
    if(index.row()>=Items.size())
        return QVariant();

    //   const RecursingWallItem* Wi = Items.at(index.row());

    if(role==ID)
        return Items.at(index.row())->id();
    else if(role==SOURCE_ID)
        return Items.at(index.row())->fromId();
    else if(role==OWNER_ID)
        return Items.at(index.row())->ownerId();
    else if(role==DATE)
        return Items.at(index.row())->date();
    else if(role==POST_TYPE)
        return Items.at(index.row())->postType();
    else if(role==TEXT)
        return Items.at(index.row())->text();
    else if(role==ATTACHMENTS)
        return Items.at(index.row())->GridAttachments();
    else if(role==AUDIOATTACHMENTS)
        return Items.at(index.row())->ListAttachments();
    else if(role==VIDEOATTACHMENTS)
        return Items.at(index.row())->VideoAttachments();
    else if(role==DOCATTACHMENTS)
        return Items.at(index.row())->DocAttachments();
    else if(role==GIFATTACHMENTS)
        return Items.at(index.row())->GifAttachments();
    else if(role ==COMMENTS_CANPOST)
        return Items.at(index.row())->canUserComment();
    else if(role ==COMMENTS_COUNT)
        return Items.at(index.row())->commentsCount();

    else if(role==FROM_NAME)
        return Items.at(index.row())->fromName();
    else if(role==FROM_SURNAME)
        return Items.at(index.row())->FromSurname();
    else if(role==FROM_PHOTO)
        return Items.at(index.row())->fromPhoto();
    else if(role==LIKES_COUNT)
        return Items.at(index.row())->Likes_count();
    else if(role==REPOSTS_COUNT)
        return Items.at(index.row())->repostCount();
    else if(role==USER_CANLIKE)
        return Items.at(index.row())->userCanLikes();
    else if(role==USER_LIKES)
        return Items.at(index.row())->userLikes();
    else
        return QVariant();
}

Qt::ItemFlags WallModel::flags(const QModelIndex &index) const
{
    if(!index.isValid())
        return Qt::ItemIsEnabled;

    return QAbstractListModel::flags(index) | Qt::ItemIsEditable;
}


