#include "audiomodel.h"

AudioModel::AudioModel(QObject *parent)
{
    setParent(parent);
    qDebug()<<"Setting roles for newsfeed";
    roles = roleNames();
    Items = new QList<QVariantMap>;

}

int AudioModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return Items->count();
}

QVariant AudioModel::data(const QModelIndex &index, int role) const
{
    qDebug()<<" Data asked for "<<index.row()<<" and role "<<role;
    if(!index.isValid())
        return QVariant();
    if(index.row()>=Items->size())
        return QVariant();
    const QVariantMap Wi = Items->at(index.row());
    if(role==ID)
        return Wi["id"];
    else if(role==OWNER_ID)
        return Wi["owner_id"];
    else if(role==URL)
        return Wi["url"];
    else if(role==GENRE_ID)
        return Wi["genre_id"];
    else if(role==DURATION)
        return Wi["duration"];
    else if(role==TITLE)
        return Wi["title"];
    else if(role==LYRICS_ID)
        return Wi["lyrics_id"];
    else if(role==ARTIST)
        return Wi["artist"];

    else
        return QVariant();

}

Qt::ItemFlags AudioModel::flags(const QModelIndex &index) const
{
    if(!index.isValid())
        return Qt::ItemIsEnabled;

    return QAbstractListModel::flags(index) | Qt::ItemIsEditable;

}


QHash<int, QByteArray> AudioModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[ID] = "id";
    roles[OWNER_ID] = "owner_id";
    roles[URL]="url";
    roles[GENRE_ID] = "genre_id";
    roles[DURATION]="duration";
    roles[TITLE] = "title";
    roles[LYRICS_ID] = "lyrics_id";
    roles[ARTIST] = "artist";

    return roles;

}

void AudioModel::addNewsItem(const QVariantMap &value)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    qDebug()<<value;
    Items->append(value);
    endInsertRows();

}

void AudioModel::clear_Audios()
{
    beginRemoveRows(QModelIndex(),0,Items->size()-1);
    Items->clear();
    endRemoveRows();
}
