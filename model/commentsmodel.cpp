#include "commentsmodel.h"

CommentsModel::CommentsModel()
{
    roles = roleNames();
}

int CommentsModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return Items.count();
}

QVariant CommentsModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();
    if(index.row()>=Items.size())
        return QVariant();
    if(role==ID)
        return Items.at(index.row())->id();
    else if(role ==FROM_ID)
        return Items.at(index.row())->from_id();
    else if(role==REPLY_TO_USERNAME)
        return Items.at(index.row())->reply_to_userName();
    else if(role==REPLY_TO_COMMENT)
        return Items.at(index.row())->reply_to_comment();
    else if(role==FIRSTNAME)
        return Items.at(index.row())->firstName();
    else if(role==LASTNAME)
        return Items.at(index.row())->lastName();
    else if(role==PHOTO_50)
        return Items.at(index.row())->fromPhoto();
    else if(role==REPLY_TO_USER)
        return Items.at(index.row())->reply_to_user();
    else if(role==DATE)
        return Items.at(index.row())->date();
    else if(role==TEXT)
        return Items.at(index.row())->Text();
    else if(role==SEX)
        return Items.at(index.row())->Sex();
    else if(role==GRIDATTACHMENTS)
        return Items.at(index.row())->GridAttachments();
    else if(role==AUDIOATTACHMENTS)
        return Items.at(index.row())->ListAttachments();
    else if(role==VIDEOATTACHMENTS)
        return Items.at(index.row())->VideoAttachments();
    else if(role==DOCATTACHMENTS)
        return Items.at(index.row())->DocAttachments();
    else if(role==GIFATTACHMENTS)
        return Items.at(index.row())->GifAttachments();
    else if(role==STICKERATTACHMENTS)
        return Items.at(index.row())->stickerAttachments();

    else
        return QVariant();
}

QHash<int, QByteArray> CommentsModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[ID] = "id";
    roles[FROM_ID]="fromId";
    roles[FIRSTNAME]="firstName";
    roles[LASTNAME]="lastName";
    roles[PHOTO_50]="photo_50";
    roles[REPLY_TO_USER]="replyToUser";
    roles[REPLY_TO_USERNAME]="replyToUserName";
    roles[REPLY_TO_COMMENT] = "replyToComment";
    roles[DATE] = "date";
    roles[SEX] = "sex";
    roles[TEXT] = "bodyText";
    roles[GRIDATTACHMENTS]="gridAttachments";
    roles[AUDIOATTACHMENTS]="listAttachments";
    roles[VIDEOATTACHMENTS]="videoAttachments";
    roles[DOCATTACHMENTS]="docAttachments";
    roles[GIFATTACHMENTS]="gifAttachments";
    roles[STICKERATTACHMENTS] = "stickerAttachments";
    return roles;

}

Parser *CommentsModel::DataProvider() const
{
    return m_DataProvider;
}

bool CommentsModel::isActive() const
{
    return m_isActive;
}

int CommentsModel::lastId() const
{
    return m_lastId;
}

void CommentsModel::addItem(CommentItem *Item, bool Appended)
{
    if(isActive())
    {
        if(m_lastId>Item->id())
        {
            m_lastId=Item->id();
            emit lastIdChanged(m_lastId);
        }
        else if(m_lastId<Item->id())
            Appended=false;

        setCommentsCount(Item->totalCommentsCount());
        //Qlist contains не работет, где-то слошал...
        if(!cmp(Item))
        {
            if(Appended)
            {
                beginInsertRows(QModelIndex(), rowCount(), rowCount());
                Items.append(Item);
                endInsertRows();

            }
            else
            {
                beginInsertRows(QModelIndex(), 0, 0);

                Items.prepend(Item);
                endInsertRows();

            }
        }

    }

}

void CommentsModel::setDataProvider(Parser *DataProvider)
{
    if (m_DataProvider == DataProvider)
        return;

    m_DataProvider = DataProvider;
    connect(m_DataProvider,&Parser::commentGenerated,this,&CommentsModel::addItem);
    connect(m_DataProvider,&Parser::newCommentIdGenerated,this,&CommentsModel::setNewCommentId);

    emit dataProviderChanged(DataProvider);
}

void CommentsModel::setActive(bool isActive)
{
    if (m_isActive == isActive)
        return;

    m_isActive = isActive;
    emit isActiveChanged(isActive);
}

bool CommentsModel::cmp(CommentItem *itm)
{
    foreach (auto Item, Items) {

        if(Item->id()==itm->id())
            return true;
    }
    return false;
}


void CommentsModel::remove(int row)
{
    beginRemoveRows(QModelIndex(),row,0);
    Items.removeAt(row);
    setCommentsCount(commentsCount()-1);
    endRemoveRows();
}
