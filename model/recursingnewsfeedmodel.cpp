#include "recursingnewsfeedmodel.h"

RecursingNewsFeedModel::RecursingNewsFeedModel(QObject *parent) :QAbstractListModel(parent)
{
    roles = roleNames();
}

RecursingNewsFeedModel::RecursingNewsFeedModel(RecursingWallItem *ChildItem)
{
    roles = roleNames();
Items.append(ChildItem);
}

RecursingNewsFeedModel::~RecursingNewsFeedModel()
{
}

void RecursingNewsFeedModel::updateItem()
{
    RecursingWallItem* item = static_cast<RecursingWallItem*>(sender());
    QModelIndex index = this->indexFromItem(item);
    if(index.isValid())
        emit(dataChanged(index,index));
}

Parser *RecursingNewsFeedModel::DataProvider() const
{
    return m_DataProvider;
}

bool RecursingNewsFeedModel::isActive() const
{
    return m_isActive;
}

void RecursingNewsFeedModel::clear_Newsfeed()
{
    for(int i=0;i<rowCount();i++)
    {
        Items.at(i)->deleteLater();
    }
    beginRemoveRows(QModelIndex(),0,Items.count()-1);
    Items.clear();
    endRemoveRows();
    qDebug()<<rowCount();
}

QModelIndex RecursingNewsFeedModel::indexFromItem(RecursingWallItem *item) const
{
    if(item!=nullptr)
    {
        for(int i =0;i<this->Items.size();i++)
            if(this->Items.at(i)==item)
                return index(i);
    }
    return QModelIndex();
}

QObject* RecursingNewsFeedModel::getChild(int id)
{
    qDebug()<<"[RecursingNewsFeedModel] getChild="<<id;
    for(int i=0;i<rowCount();i++)
        if(Items.at(i)->id()==id)
            return static_cast<QObject*>(Items.at(i)->repostedPost());
    return nullptr;
}

void RecursingNewsFeedModel::setDataProvider(Parser *DataProvider)
{
m_DataProvider =DataProvider;
qDebug()<<"[RecursingNewsFeedModel] setDataProvider";
    connect(m_DataProvider,&Parser::newsfeedGenerated,this,&RecursingNewsFeedModel::addNewsItem);
    emit dataProviderChanged(DataProvider);
}

void RecursingNewsFeedModel::setActive(bool isActive)
{
    if (m_isActive == isActive)
        return;

    m_isActive = isActive;
    emit isActiveChanged(isActive);
}




int RecursingNewsFeedModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return Items.count();
}

QVariant RecursingNewsFeedModel::data(const QModelIndex &index, int role) const
{
    //    qDebug()<<" Data asked for "<<index.row()<<" and role "<<role;
      if(!index.isValid())
          return QVariant();
      if(index.row()>=Items.size())
          return QVariant();

   //   const RecursingWallItem* Wi = Items.at(index.row());

      if(role==ID)
          return Items.at(index.row())->id();
      else if(role==SOURCE_ID)
          return Items.at(index.row())->fromId();
      else if(role==OWNER_ID)
          return Items.at(index.row())->ownerId();
      else if(role==DATE)
          return Items.at(index.row())->date();
      else if(role==POST_TYPE)
          return Items.at(index.row())->postType();
      else if(role==TEXT)
          return Items.at(index.row())->text();
      else if(role==ATTACHMENTS)
          return Items.at(index.row())->GridAttachments();
      else if(role==AUDIOATTACHMENTS)
          return Items.at(index.row())->ListAttachments();
      else if(role==VIDEOATTACHMENTS)
          return Items.at(index.row())->VideoAttachments();
      else if(role==DOCATTACHMENTS)
          return Items.at(index.row())->DocAttachments();
      else if(role==GIFATTACHMENTS)
          return Items.at(index.row())->GifAttachments();
      else if(role ==COMMENTS_CANPOST)
          return Items.at(index.row())->canUserComment();
      else if(role ==COMMENTS_COUNT)
          return Items.at(index.row())->commentsCount();
      else if(role==FROM_NAME)
          return Items.at(index.row())->fromName();
      else if(role==FROM_SURNAME)
          return Items.at(index.row())->FromSurname();
      else if(role==FROM_PHOTO)
          return Items.at(index.row())->fromPhoto();
      else if(role==LIKES_COUNT)
          return Items.at(index.row())->Likes_count();
      else if(role==REPOSTS_COUNT)
          return Items.at(index.row())->repostCount();
      else if(role==USER_CANLIKE)
          return Items.at(index.row())->userCanLikes();
      else if(role==USER_LIKES)
          return Items.at(index.row())->userLikes();
      else
          return QVariant();
}

Qt::ItemFlags RecursingNewsFeedModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
           return 0;

       return QAbstractListModel::flags(index);
}

QHash<int, QByteArray> RecursingNewsFeedModel::roleNames() const
{
        QHash<int, QByteArray> roles;
        roles[ID] = "id";
        roles[SOURCE_ID] = "source_id";
        roles[FROM_NAME]="from_name";
        roles[FROM_SURNAME] = "from_surname";
        roles[FROM_PHOTO]="from_photo";
        roles[OWNER_ID] = "owner_id";
        roles[DATE] = "date";
        roles[POST_TYPE] = "post_type";
        roles[TEXT] = "datatext";
        roles[ATTACHMENTS]="attachments";
        roles[AUDIOATTACHMENTS]="listAttachments";
        roles[VIDEOATTACHMENTS]="videoAttachments";
        roles[DOCATTACHMENTS]="docAttachments";
        roles[GIFATTACHMENTS]="gifAttachments";
        roles[LIKES_COUNT]="likes_count";
        roles[USER_LIKES]="user_islikes";
        roles[USER_CANLIKE]="user_canlike";
        roles[REPOSTS_COUNT]="reposts_count";
        roles[COMMENTS_COUNT]="commentsCount";
        roles[COMMENTS_CANPOST]="commentsCanPost";
        roles[CHILD_WALLITEM]="child_wallitem";

        return roles;


}

void RecursingNewsFeedModel::addNewsItem(RecursingWallItem *value, const bool appended)
{
    qDebug()<<rowCount();
    if(isActive())
    {
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    if(appended)
        Items.append(value);
    else
    Items.prepend(value);
    endInsertRows();
    qDebug()<<rowCount();
    }
}
