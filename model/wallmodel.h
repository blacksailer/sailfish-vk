#ifndef WALLMODEL_H
#define WALLMODEL_H
#include <QObject>
#include <QAbstractListModel>
#include<QHash>

#include <QJsonValue>
#include <QJsonObject>
#include <QVariantMap>
#include <QDateTime>
#include "../parser.h"
#include "items/recursingwallitem.h"

class WallModel: public QAbstractListModel

{
    Q_OBJECT
    Q_PROPERTY(Parser* DataProvider READ DataProvider WRITE setDataProvider NOTIFY dataProviderChanged)
    Q_PROPERTY(bool isActive READ isActive WRITE setActive NOTIFY isActiveChanged)

public:
    WallModel(QObject *parent = 0);
    enum TypeInputs {
        ID,
        SOURCE_ID,
        FROM_NAME,
        FROM_SURNAME,
        FROM_PHOTO,
        OWNER_ID,
        DATE,
        POST_TYPE,
        TEXT,
        ATTACHMENTS,
        AUDIOATTACHMENTS,
        VIDEOATTACHMENTS,
        DOCATTACHMENTS,
        GIFATTACHMENTS,
        USER_INFO,
        LIKES_COUNT,
        USER_LIKES,
        USER_CANLIKE,
        REPOSTS_COUNT,
        COMMENTS_COUNT,
        COMMENTS_CANPOST,
        CHILD_WALLITEM
    };

    int rowCount(const QModelIndex &parent= QModelIndex()) const;
     QVariant data(const QModelIndex &index, int role) const;
     Qt::ItemFlags flags(const QModelIndex &index) const;
         QHash<int, QByteArray> roleNames() const;

         void addWallItem( RecursingWallItem *value, const bool appended);
         Parser* DataProvider() const
         {
             return m_DataProvider;
         }

         bool isActive() const
         {
             return m_isActive;
         }

signals:

         void dataProviderChanged(Parser* DataProvider);

         void isActiveChanged(bool isActive);

public slots:
         QObject* getChild(int id);
         void remove(int row);
         void clear_Wall();

         void setDataProvider(Parser* DataProvider);

         void setActive(bool isActive);

private:
    QList<RecursingWallItem*> Items;  //Записи стены пользователя
    QHash<int,QByteArray> roles; //Роли

    Parser* m_DataProvider;
    bool m_isActive;
};

#endif // WALLMODEL_H
