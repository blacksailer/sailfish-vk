#include "messagesmodel.h"

MessagesModel::MessagesModel(QObject *parent) :QAbstractListModel(parent)
{
    qDebug()<<"Setting roles for newsfeed";
    roles = roleNames();

}

MessagesModel::~MessagesModel()
{
    emit userIdRegister(m_userId,1);
}


int MessagesModel::rowCount(const QModelIndex &parent) const
{
Q_UNUSED(parent)
    return Items.count();

}

QVariant MessagesModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();
    if(index.row()>=Items.size())
        return QVariant();
    MessageItem* Wi = Items.at(index.row());
    if(role==ID)
        return Wi->id();
    else if(role==USER_ID)
        return Wi->userId();
    else if(role==FROM_ID)
        return Wi->fromId();
    else if(role==FIRST_NAME)
        return Wi->firstName();
    else if(role==LAST_NAME)
        return Wi->lastName();
    else if(role==USER_PHOTO)
        return Wi->userPhoto();

    else if(role==DATE)
        return Wi->date();
    else if(role==READ_STATE)
        return Wi->readState();
    else if(role==OUT)
        return Wi->out();
    else if(role==TITLE)
        return Wi->title();
    else if(role==BODY)
        return Wi->body();
    else if(role==PHOTOATTACHMENTS)
        return Wi->GridAttachments();
    else if(role==AUDIOATTACHMENTS)
        return Wi->ListAttachments();
    else if(role==VIDEOATTACHMENTS)
        return Wi->VideoAttachments();
    else if(role==DOCATTACHMENTS)
        return Wi->DocAttachments();
    else if(role==GIFATTACHMENTS)
        return Wi->GifAttachments();
    else if(role==STICKERATTACHMENTS)
        return Wi->stickerAttachments();
    else if(role==EMOJI)
        return Wi->emoji();
    else if(role==IMPORTANT)
        return Wi->important();
    else if(role==DELETED)
        return Wi->deleted();
    else if(role==CHAT_ID)
        return Wi->chatId();
    else if(role==CHAT_ACTIVE)
        return Wi->chatActive();
    else if(role==PUSH_SETTINGS)
        return Wi->pushSettings();
    else if(role==USER_COUNT)
        return Wi->userCount();
    else if(role==ACTION)
        return Wi->action();
    else if(role==ACTION_MID)
        return Wi->actionMid();
    else if(role==ACTION_EMAIL)
        return Wi->actionEmail();
    else if(role==ACTION_TEXT)
        return Wi->actionText();
    else if(role==PHOTO_50)
        return Wi->photo50();
    else if(role==PHOTO_100)
        return Wi->photo100();
    else if(role==PHOTO_200)
        return Wi->photo200();

    else
        return QVariant();

}

Qt::ItemFlags MessagesModel::flags(const QModelIndex &index) const
{
    if(!index.isValid())
        return Qt::ItemIsEnabled;

    return QAbstractListModel::flags(index) | Qt::ItemIsEditable;


}



QHash<int, QByteArray> MessagesModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[ID] = "id";
    roles[USER_ID] = "user_id";
    roles[FIRST_NAME]="first_name";
    roles[LAST_NAME]="last_name";
    roles[USER_PHOTO]="user_photo";
    roles[FWDPOST]="fwdPost";
    roles[FROM_ID]="from_id";
    roles[DATE] = "date";
    roles[READ_STATE]="read_state";
    roles[OUT] = "out";
    roles[TITLE] = "title";
    roles[BODY] = "body";
    roles[PHOTOATTACHMENTS] = "gridAttachments";
    roles[AUDIOATTACHMENTS] = "listAttachments";
    roles[DOCATTACHMENTS] = "docAttachments";
    roles[VIDEOATTACHMENTS] = "videoAttachments";
    roles[GIFATTACHMENTS] = "gifAttachments";
    roles[STICKERATTACHMENTS] = "stickerAttachments";

    roles[FWDMSGNDX]="fwdmsg";
    roles[EMOJI]="emoji";
    roles[IMPORTANT]="important";
    roles[DELETED]="deleted";
    roles[CHAT_ID]="chat_id";
    roles[CHAT_ACTIVE]="chat_active";
    roles[PUSH_SETTINGS]="push_settings";
    roles[USER_COUNT]="user_count";
    roles[ACTION]="action";
    roles[ACTION_MID]="action_mid";
    roles[ACTION_EMAIL]="action_email";
    roles[ACTION_TEXT]="action_text";
    roles[PHOTO_50]="photo_50";
    roles[PHOTO_100]="photo_100";
    roles[PHOTO_200]="photo_200";

    return roles;

}

void MessagesModel::addMessageItem(MessageItem *value)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    Items.append(value);
    endInsertRows();

}

void MessagesModel::addLongPollMessageItem(MessageItem *value)
{
    if(value->userId()==userId())
    {
        beginInsertRows(QModelIndex(), 0, 0);
        Items.prepend(value);
        endInsertRows();
    }
}

Parser *MessagesModel::DataProvider() const
{
    return m_DataProvider;
}

bool MessagesModel::isActive() const
{
    return m_isActive;
}

int MessagesModel::userId() const
{
    return m_userId;
}


void MessagesModel::clear_Messages()
{

    for(int i=0;i<rowCount();i++)
    {
        Items.at(i)->deleteLater();
    }
    beginRemoveRows(QModelIndex(),0,Items.size()-1);
    Items.clear();
    qDebug()<<"clearing Dialogs";
    endRemoveRows();

}

QObject *MessagesModel::getFwdPost(int id)
{
    for(int i=0;i<rowCount();i++)
        if(Items.at(i)->id()==id)
            return static_cast<QObject*>(Items.at(i)->fwdPost());
    return nullptr;
}

QObject *MessagesModel::getFwdMessage(int id)
{
    for(int i=0;i<rowCount();i++)
        if(Items.at(i)->id()==id)
            return static_cast<QObject*>(Items.at(i)->fwdMessage());
    return nullptr;
}

void MessagesModel::setDataProvider(Parser *DataProvider)
{
    if (m_DataProvider == DataProvider)
        return;

    m_DataProvider = DataProvider;
    connect(m_DataProvider,&Parser::MessageGenerated,this,&MessagesModel::addMessageItem);
    connect(m_DataProvider,&Parser::LongPollMesageGenerated,this,&MessagesModel::addLongPollMessageItem);
connect(this,&MessagesModel::userIdRegister,m_DataProvider,&Parser::DialogIdDo);

    emit dataProviderChanged(DataProvider);

    emit dataProviderChanged(DataProvider);
}

void MessagesModel::setActive(bool isActive)
{
    if (m_isActive == isActive)
        return;

    m_isActive = isActive;
    emit isActiveChanged(isActive);
}

void MessagesModel::setUserId(int userId)
{
    if (m_userId == userId)
        return;

    qDebug()<<"setUserId";
    emit userIdRegister(m_userId,1);
    m_userId = userId;
    emit userIdChanged(userId);
    emit userIdRegister(m_userId,0);
}

