#ifndef DIALOGSMODEL_H
#define DIALOGSMODEL_H
#include <QObject>
#include <QAbstractListModel>
#include<QHash>

#include <QJsonValue>
#include <QJsonObject>
#include <QVariantMap>
#include <QDateTime>
#include "../parser.h"
#include "items/messageitem.h"
class DialogsModel: public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(Parser* DataProvider READ DataProvider WRITE setDataProvider NOTIFY dataProviderChanged)
    Q_PROPERTY(bool isActive READ isActive WRITE setActive NOTIFY isActiveChanged)

public:
    explicit DialogsModel(QObject *parent = 0);
    enum TypeInputs {
        ID,
        USER_ID,
        FIRST_NAME,
        LAST_NAME,
        USER_PHOTO,
        FROM_ID,
        DATE,
        READ_STATE,
        OUT,
        TITLE,
        BODY,
        //   QVariant geo;  // Массив с полями
        PHOTO_NDX,
        FWDMSGNDX,
        EMOJI,
        IMPORTANT,
        DELETED,
        CHAT_ID,
        CHAT_ACTIVE,
        PUSH_SETTINGS,
        USER_COUNT,
        ADMIN_ID,
        ACTION,
        ACTION_MID,
        ACTION_EMAIL,
        ACTION_TEXT,
        PHOTO_50,
        PHOTO_100,
        PHOTO_200
    };

    int rowCount(const QModelIndex &parent= QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    QHash<int, QByteArray> roleNames() const;

    void addDialogItem(MessageItem *value);
    Parser* DataProvider() const;

    bool isActive() const;

signals:


    void dataProviderChanged(Parser* DataProvider);

    void isActiveChanged(bool isActive);

public slots:
    void clear_Dialogs();
    void setDataProvider(Parser* DataProvider);

    void setActive(bool isActive);

private:
    QList<MessageItem*> Items;  //Записи стены пользователя
    QHash<int,QByteArray> roles; //Роли



    Parser* m_DataProvider;
    bool m_isActive;
};

#endif // DIALOGSMODEL_H
