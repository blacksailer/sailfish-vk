#ifndef MESSAGESMODEL_H
#define MESSAGESMODEL_H
#include <QObject>
#include <QAbstractListModel>
#include<QHash>

#include <QVariantMap>
#include <QDateTime>
#include "../parser.h"
#include "items/messageitem.h"

class MessagesModel: public QAbstractListModel
{
    Q_OBJECT

public:
    explicit MessagesModel(QObject *parent = 0);
    ~MessagesModel();
    Q_PROPERTY(Parser* DataProvider READ DataProvider WRITE setDataProvider NOTIFY dataProviderChanged)
    Q_PROPERTY(bool isActive READ isActive WRITE setActive NOTIFY isActiveChanged)
    Q_PROPERTY(int userId READ userId WRITE setUserId NOTIFY userIdChanged)

    enum TypeInputs {
        ID,
        USER_ID,
        FIRST_NAME,
        LAST_NAME,
        USER_PHOTO,
        FROM_ID,
        DATE,
        READ_STATE,
        OUT,
        TITLE,
        BODY,
        //   QVariant geo;  // Массив с полями
        PHOTOATTACHMENTS,
        AUDIOATTACHMENTS,
        VIDEOATTACHMENTS,
        DOCATTACHMENTS,
        GIFATTACHMENTS,
        STICKERATTACHMENTS,
        FWDMSGNDX,
        FWDPOST,
        EMOJI,
        IMPORTANT,
        DELETED,
        CHAT_ID,
        CHAT_ACTIVE,
        PUSH_SETTINGS,
        USER_COUNT,
        ADMIN_ID,
        ACTION,
        ACTION_MID,
        ACTION_EMAIL,
        ACTION_TEXT,
        PHOTO_50,
        PHOTO_100,
        PHOTO_200
    };



    int rowCount(const QModelIndex &parent= QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    QHash<int, QByteArray> roleNames() const;

    void addMessageItem( MessageItem *value);
    void addLongPollMessageItem(MessageItem* value);

    Parser* DataProvider() const;

    bool isActive() const;

    int userId() const;

signals:


    void dataProviderChanged(Parser* DataProvider);

    void isActiveChanged(bool isActive);

    void userIdChanged(int userId);
    //0 - Создан, 1 - Удален
    void userIdRegister(int userId,int type);

public slots:
    void clear_Messages();
 QObject* getFwdPost(int id);
 QObject* getFwdMessage(int id);
 void setDataProvider(Parser* DataProvider);

 void setActive(bool isActive);

 void setUserId(int userId);

private:
    QList<MessageItem*> Items;  //Записи стены пользователя
    QHash<int,QByteArray> roles; //Роли


    Parser* m_DataProvider;
    bool m_isActive;
    int m_userId=0;
};

#endif // MESSAGESMODEL_H
