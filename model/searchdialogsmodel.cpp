#include "searchdialogsmodel.h"

SearchDialogsModel::SearchDialogsModel(QObject *parent) :
    QAbstractListModel(parent)
{
    roles=roleNames();
}

SearchDialogsModel::~SearchDialogsModel()
{
    foreach (auto itm, Items) {
        delete itm;
    }
    Items.clear();
}
int SearchDialogsModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return Items.size();
}

QVariant SearchDialogsModel::data(const QModelIndex &index, int role) const
{

    if(!index.isValid())
        return QVariant();
    if(index.row()>=Items.size())
        return QVariant();
    if(role==ID)
        return Items.at(index.row())->id;
    else if(role==PHOTO_50)
        return Items.at(index.row())->photo_50;
    else if(role==PHOTO_100)
        return Items.at(index.row())->photo_100;
    else if(role==NAME && Items.at(index.row())->item_type==BasicItemInfo::ItemType::GROUP)
        return static_cast<GroupItem*>(Items.at(index.row()))->name;
    else if(role==FIRSTNAME && Items.at(index.row())->item_type==BasicItemInfo::ItemType::USER)
    {
        qDebug()<<static_cast<UserItem*>(Items.at(index.row()))->first_name;
        return  static_cast<UserItem*>(Items.at(index.row()))->first_name;
    }
    else if(role==LASTNAME && Items.at(index.row())->item_type==BasicItemInfo::ItemType::USER)
        return  static_cast<UserItem*>(Items.at(index.row()))->last_name;
    else if(role==TITLE)
    {
        if(Items.at(index.row())->item_type==BasicItemInfo::ItemType::CHAT)
            return  static_cast<ChatItem*>(Items.at(index.row()))->title;
    }
    else if(role==TYPE)
        return Items.at(index.row())->item_type;

        return QVariant();

}

QHash<int, QByteArray> SearchDialogsModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[ID] = "id";
    roles[PHOTO_50] = "photo_50";
    roles[PHOTO_100]="photo_100";
    roles[NAME] = "name";
    roles[FIRSTNAME] = "firstName";
    roles[LASTNAME]="lastName";
    roles[TITLE] = "title";
    roles[TYPE]="itemType";
    return roles;

}

void SearchDialogsModel::addItem(BasicItemInfo* itm)
{
    beginInsertRows(QModelIndex(),rowCount(),rowCount());
    Items.append(itm);
    if(    itm->item_type==BasicItemInfo::ItemType::USER)
               qDebug()<<  static_cast<UserItem*>(itm)->last_name;

    endInsertRows();
}

void SearchDialogsModel::clearAll()
{
    for (int i = 0; i < Items.size(); ++i) {
        delete Items.at(i);
    }
    //Почему -2?
    beginRemoveRows(QModelIndex(),0,rowCount()-2);
    Items.clear();
    endRemoveRows();
}
