#ifndef SEARCHDIALOGSMODEL_H
#define SEARCHDIALOGSMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include "basiciteminfo.h"
#include "items/useritem.h"
#include "items/groupitem.h"
#include "items/chatitem.h"
#include "parser.h"
class SearchDialogsModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(Parser* DataProvider READ DataProvider WRITE setDataProvider NOTIFY dataProviderChanged)

public:
    explicit SearchDialogsModel(QObject *parent = 0);
    ~SearchDialogsModel();
signals:

    void dataProviderChanged(Parser* DataProvider);

public slots:

// QAbstractItemModel interface
void setDataProvider(Parser* DataProvider)
{
    if (m_DataProvider == DataProvider)
        return;

    m_DataProvider = DataProvider;
    connect(m_DataProvider,&Parser::searchDialogsGenerated,this,&SearchDialogsModel::addItem);
    emit dataProviderChanged(DataProvider);
}

public:
    enum TypeInputs{
        ID,
        PHOTO_50,
        PHOTO_100,
        FIRSTNAME,
        LASTNAME,
        NAME,
        TITLE,
        TYPE
    };

    int rowCount(const QModelIndex &parent=QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;


    Parser* DataProvider() const
    {
        return m_DataProvider;
    }
public slots:
    void addItem(BasicItemInfo *itm);

    void clearAll();
private:
    QHash<int,QByteArray> roles; //Роли
    QList<BasicItemInfo*> Items;

    Parser* m_DataProvider;
};


#endif // SEARCHDIALOGSMODEL_H
