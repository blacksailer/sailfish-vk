#ifndef RECURSINGNEWSFEEDMODEL_H
#define RECURSINGNEWSFEEDMODEL_H


#include <QObject>
#include <QAbstractListModel>
#include<QHash>

#include <QJsonValue>
#include <QJsonObject>
#include <QVariantMap>
#include <QDateTime>
#include <QDebug>
#include "../parser.h"
#include "items/recursingwallitem.h"

class RecursingNewsFeedModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(Parser* DataProvider READ DataProvider WRITE setDataProvider NOTIFY dataProviderChanged)
    Q_PROPERTY(bool isActive READ isActive WRITE setActive NOTIFY isActiveChanged)
public:
    explicit RecursingNewsFeedModel(QObject *parent = 0);
    RecursingNewsFeedModel(RecursingWallItem* ChildItem);
    ~RecursingNewsFeedModel();
    enum TypeInputs {
        ID,
        SOURCE_ID,
        FROM_NAME,
        FROM_SURNAME,
        FROM_PHOTO,
        OWNER_ID,
        DATE,
        POST_TYPE,
        TEXT,
        ATTACHMENTS,
        AUDIOATTACHMENTS,
        VIDEOATTACHMENTS,
        DOCATTACHMENTS,
        GIFATTACHMENTS,
        USER_INFO,
        LIKES_COUNT,
        USER_LIKES,
        USER_CANLIKE,
        REPOSTS_COUNT,
        COMMENTS_COUNT,
        COMMENTS_CANPOST,
        CHILD_WALLITEM

    };

    // QAbstractItemModel interface
    //REIMPLEMENTED
    QModelIndex indexFromItem(RecursingWallItem* item) const;
    int rowCount(const QModelIndex &parent= QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    QHash<int, QByteArray> roleNames() const;



    Parser *DataProvider() const;

    bool isActive() const;

public slots:
    void clear_Newsfeed();
    QObject* getChild(int id);
    void setDataProvider(Parser *DataProvider);
    void setActive(bool isActive);
    //МОЕ,САМ РУКАМИ ДЕЛАЛ
    void addNewsItem(RecursingWallItem* value,const bool appended);
    void updateItem();

signals:
    void dataProviderChanged(Parser* DataProvider);
    void isActiveChanged(bool isActive);

protected:
    QList<RecursingWallItem*> Items;  //Записи стены пользователя
    QHash<int,QByteArray> roles; //Роли
private:
    Parser* m_DataProvider;



    bool m_isActive;
};

#endif // RECURSINGNEWSFEEDMODEL_H
