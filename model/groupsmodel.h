#ifndef GROUPSMODEL_H
#define GROUPSMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <QSortFilterProxyModel>
#include<QHash>
#include <QVariantMap>
#include <QDateTime>
#include "../parser.h"

class GroupsModel : public QAbstractListModel
{
    Q_PROPERTY(Parser* DataProvider READ DataProvider WRITE setDataProvider NOTIFY dataProviderChanged)
    Q_PROPERTY(bool isSearchMode READ isSearchMode WRITE setIsSearchMode NOTIFY isSearchModeChanged)
    Q_PROPERTY(int totalCount READ totalCount WRITE setTotalCount NOTIFY totalCountChanged)
    Q_OBJECT

public:
    explicit GroupsModel(QObject *parent = 0);
    enum TypeInputs{
        GROUP_ID,
        PHOTO_50,
        PHOTO_100,
        PHOTO_200,
        NAME,
        IS_CLOSED,
        IS_ADMIN,
        IS_MEMBER,
        TYPE,
        STATUS
    };
    int rowCount(const QModelIndex &parent= QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;

    void addGroups(QVariantList value);
    Parser* DataProvider() const
    {
        return m_DataProvider;
    }

    bool isSearchMode() const
    {
        return m_isSearchMode;
    }

    int totalCount() const
    {
        return m_totalCount;
    }

public slots:
    void clearGroups();
    void setDataProvider(Parser* DataProvider);

    void setIsSearchMode(bool isSearchMode);

    void setTotalCount(int totalCount)
    {
        if (m_totalCount == totalCount)
            return;

        m_totalCount = totalCount;
        emit totalCountChanged(totalCount);
    }

signals:
    void dataProviderChanged(Parser* DataProvider);

    void isSearchModeChanged(bool isSearchMode);

    void totalCountChanged(int totalCount);

private:
    QVariantList Items;  //Записи стены пользователя
    QVariantList SearchItems;  //Записи стены пользователя

    QHash<int,QByteArray> roles; //Роли

    Parser* m_DataProvider;
    bool m_isSearchMode;
    int m_totalCount;
};

#endif // GROUPSMODEL_H
