#ifndef GROUPITEM_H
#define GROUPITEM_H


#include "basiciteminfo.h"
class GroupItem:public BasicItemInfo
{
public:
    GroupItem();

    QString name;
    //string 	название сообщества.
    QString screen_name;
    //string 	короткий адрес, например, apiclub.
    int is_closed;
    /*integer 	является ли сообщество закрытым. Возможные значения:

        0 — открытое;
        1 — закрытое;
        2 — частное.
    */

    QString deactivated;
//    string 	возвращается в случае, если сообщество удалено или заблокировано. Возможные значения:

//        deleted — сообщество удалено;
//        banned — сообщество заблокировано;

    bool is_admin;
   /* integer, [0, 1]
    Требуется scope = groups 	информация о том, является ли текущий пользователь руководителем. Возможные значения:

        1 — является;
        0 — не является.
    */
    int admin_level;
   /* integer
    Требуется scope = groups 	уровень полномочий текущего пользователя (если is_admin = 1):

        1 — модератор;
        2 — редактор;
        3 — администратор.
    */
    bool is_member;
    /*integer, [0, 1]
    Требуется scope = groups 	информация о том, является ли текущий пользователь участником. Возможные значения:

        1 — является;
        0 — не является.
    */
    bool invited_by;
    /*integer
    Требуется scope = groups 	идентификатор пользователя, который отправил приглашение в сообщество.
    Поле возвращается только для метода groups.getInvites.
    */
    QString type;
    /*string 	тип сообщества:

        group — группа;
        page — публичная страница;
        event — мероприятие.
    */
    bool has_photo;
   /* integer, [0, 1]	информация о том, установлена ли у сообщества главная фотография. Возможные значения:

        1 — установлена;
        0 — не установлена.
    */
    GroupItem(const BasicItemInfo &itm);
};

#endif // GROUPITEM_H
