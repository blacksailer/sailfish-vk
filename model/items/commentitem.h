#ifndef COMMENTITEM_H
#define COMMENTITEM_H

#include <QObject>
#include <QDebug>
#include <QVariant>

class CommentItem : public QObject
{
    Q_OBJECT
public:
    inline bool operator==( const CommentItem& rhs){
        qDebug()<<"Comment Comparison"<<this->id()<<rhs.id();
        return this->id()==rhs.id();
    }

    explicit CommentItem(QObject* parent=0);
    Q_PROPERTY(int id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(int from_id READ from_id WRITE setFrom_id NOTIFY from_idChanged)
    Q_PROPERTY(int date READ date WRITE setDate NOTIFY dateChanged)
    Q_PROPERTY(int Sex READ Sex WRITE setSex NOTIFY sexChanged)
    Q_PROPERTY(QString text READ Text WRITE setText NOTIFY TextChanged)
    Q_PROPERTY(int reply_to_user READ reply_to_user WRITE setReply_to_user NOTIFY reply_to_userChanged)
    Q_PROPERTY(QString reply_to_userName READ reply_to_userName WRITE setReply_to_userName NOTIFY reply_to_userNameChanged)
    Q_PROPERTY(int reply_to_comment READ reply_to_comment WRITE setReply_to_comment NOTIFY reply_to_commentChanged)
    Q_PROPERTY(QString firstName READ firstName WRITE setFirstName NOTIFY firstNameChanged)
    Q_PROPERTY(QString lastName READ lastName WRITE setLastName NOTIFY lastNameChanged)
    Q_PROPERTY(QString fromPhoto READ fromPhoto WRITE setFromPhoto NOTIFY fromPhotoChanged)

    //Служебное
    Q_PROPERTY(int totalCommentsCount READ totalCommentsCount WRITE setTotalCommentsCount NOTIFY totalCommentsCountChanged)

    Q_PROPERTY(QVariantList GridAttachments READ GridAttachments WRITE setGridAttachments)
    Q_PROPERTY(QVariantList VideoAttachments READ VideoAttachments WRITE setVideoAttachments NOTIFY VideoAttachmentsChanged)
    Q_PROPERTY(QVariantList ListAttachments READ ListAttachments WRITE setListAttachments)
    Q_PROPERTY(QVariantList DocAttachments READ DocAttachments WRITE setDocAttachments NOTIFY DocAttachmentsChanged)
    Q_PROPERTY(QVariantList GifAttachments READ GifAttachments WRITE setGifAttachments NOTIFY GifAttachmentsChanged)
    Q_PROPERTY(QVariantList stickerAttachments READ stickerAttachments WRITE setStickerAttachments NOTIFY stickerAttachmentsChanged)

    int id() const
    {
        return m_id;
    }
    int date() const
    {
        return m_date;
    }

    QString Text() const
    {
        return m_text;
    }

    int reply_to_user() const
    {
        return m_reply_to_user;
    }

    int reply_to_comment() const
    {
        return m_reply_to_comment;
    }

    QVariantList GridAttachments() const
    {
        return m_GridAttachments;
    }

    QVariantList VideoAttachments() const
    {
        return m_VideoAttachments;
    }

    QVariantList ListAttachments() const
    {
        return m_ListAttachments;
    }

    QVariantList DocAttachments() const
    {
        return m_DocAttachments;
    }

    QVariantList GifAttachments() const
    {
        return m_GifAttachments;
    }

    int from_id() const
    {
        return m_from_id;
    }

    QString firstName() const
    {
        return m_firstName;
    }

    QString lastName() const
    {
        return m_lastName;
    }

    QString fromPhoto() const
    {
        return m_fromPhoto;
    }

    int totalCommentsCount() const
    {
        return m_totalCommentsCount;
    }

    QString reply_to_userName() const
    {
        return m_reply_to_userName;
    }

    int Sex() const
    {
        return m_Sex;
    }

    QVariantList stickerAttachments() const
    {
        return m_stickerAttachments;
    }

public slots:
    void setId(int id)
    {
        if (m_id == id)
            return;

        m_id = id;
        emit idChanged(id);
    }
    void setDate(int date)
    {
        if (m_date == date)
            return;

        m_date = date;
        emit dateChanged(date);
    }

    void setText(QString text)
    {
        if (m_text == text)
            return;

        m_text = text;
        emit TextChanged(text);
    }

    void setReply_to_user(int reply_to_user)
    {
        if (m_reply_to_user == reply_to_user)
            return;

        m_reply_to_user = reply_to_user;
        emit reply_to_userChanged(reply_to_user);
    }

    void setReply_to_comment(int reply_to_comment)
    {
        if (m_reply_to_comment == reply_to_comment)
            return;

        m_reply_to_comment = reply_to_comment;
        emit reply_to_commentChanged(reply_to_comment);
    }

    void setGridAttachments(QVariantList GridAttachments)
    {
        m_GridAttachments = GridAttachments;
    }

    void setVideoAttachments(QVariantList VideoAttachments)
    {
        if (m_VideoAttachments == VideoAttachments)
            return;

        m_VideoAttachments = VideoAttachments;
        emit VideoAttachmentsChanged(VideoAttachments);
    }

    void setListAttachments(QVariantList ListAttachments)
    {
        m_ListAttachments = ListAttachments;
    }

    void setDocAttachments(QVariantList DocAttachments)
    {
        if (m_DocAttachments == DocAttachments)
            return;

        m_DocAttachments = DocAttachments;
        emit DocAttachmentsChanged(DocAttachments);
    }

    void setGifAttachments(QVariantList GifAttachments)
    {
        if (m_GifAttachments == GifAttachments)
            return;

        m_GifAttachments = GifAttachments;
        emit GifAttachmentsChanged(GifAttachments);
    }

    void setFrom_id(int from_id)
    {
        if (m_from_id == from_id)
            return;

        m_from_id = from_id;
        emit from_idChanged(from_id);
    }

    void setFirstName(QString firstName)
    {
        if (m_firstName == firstName)
            return;

        m_firstName = firstName;
        emit firstNameChanged(firstName);
    }

    void setLastName(QString lastName)
    {
        if (m_lastName == lastName)
            return;

        m_lastName = lastName;
        emit lastNameChanged(lastName);
    }

    void setFromPhoto(QString fromPhoto)
    {
        if (m_fromPhoto == fromPhoto)
            return;

        m_fromPhoto = fromPhoto;
        emit fromPhotoChanged(fromPhoto);
    }

    void setTotalCommentsCount(int totalCommentsCount)
    {
        if (m_totalCommentsCount == totalCommentsCount)
            return;

        m_totalCommentsCount = totalCommentsCount;
        emit totalCommentsCountChanged(totalCommentsCount);
    }

    void setReply_to_userName(QString reply_to_userName)
    {
        if (m_reply_to_userName == reply_to_userName)
            return;

        m_reply_to_userName = reply_to_userName;
        emit reply_to_userNameChanged(reply_to_userName);
    }

    void setSex(int Sex)
    {
        if (m_Sex == Sex)
            return;

        m_Sex = Sex;
        emit sexChanged(Sex);
    }

    void setStickerAttachments(QVariantList stickerAttachments)
    {
        if (m_stickerAttachments == stickerAttachments)
            return;

        m_stickerAttachments = stickerAttachments;
        emit stickerAttachmentsChanged(stickerAttachments);
    }

signals:
    void idChanged(int id);
    void dateChanged(int date);

    void TextChanged(QString text);

    void reply_to_userChanged(int reply_to_user);

    void reply_to_commentChanged(int reply_to_comment);

    void VideoAttachmentsChanged(QVariantList VideoAttachments);

    void DocAttachmentsChanged(QVariantList DocAttachments);

    void GifAttachmentsChanged(QVariantList GifAttachments);

    void from_idChanged(int from_id);

    void firstNameChanged(QString firstName);

    void lastNameChanged(QString lastName);

    void fromPhotoChanged(QString fromPhoto);

    void totalCommentsCountChanged(int totalCommentsCount);

    void reply_to_userNameChanged(QString reply_to_userName);

    void sexChanged(int Sex);

    void stickerAttachmentsChanged(QVariantList stickerAttachments);

private:
    int m_id;
    int m_date;
    QString m_text;
    int m_reply_to_user;
    int m_reply_to_comment;
    QVariantList m_GridAttachments;
    QVariantList m_VideoAttachments;
    QVariantList m_ListAttachments;
    QVariantList m_DocAttachments;
    QVariantList m_GifAttachments;
    int m_from_id;
    QString m_firstName;
    QString m_lastName;
    QString m_fromPhoto;
    int m_totalCommentsCount;
    QString m_reply_to_userName;
    int m_Sex;
    QVariantList m_stickerAttachments;
};

#endif // COMMENTITEM_H
