#ifndef RECURSINGWALLITEM_H
#define RECURSINGWALLITEM_H

#include <QObject>
#include <QDebug>
#include <QVariant>
class RecursingWallItem :public QObject
{

    Q_OBJECT

    int m_id;

    int m_fromId;

    QString m_fromName;

    QString m_FromSurname;

    QString m_fromPhoto;

    int m_ownerId;

    int m_date;

    QString m_postType;

    QString m_text;


    int m_Likes_count;

    bool m_userLikes;

    bool m_userCanLikes;

    int m_repostCount;


    RecursingWallItem* m_repostedPost=nullptr;

 //  SubListNewsFeed* m_children=nullptr;



    QVariantList m_GridAttachments;

    QVariantList m_ListAttachments;

    QVariantList m_VideoAttachments;

    QVariantList m_DocAttachments;

    QVariantList m_GifAttachments;

    int m_commentsCount;

    bool m_canUserComment;

public:

    explicit RecursingWallItem(QObject* parent=0);
~RecursingWallItem();
    Q_PROPERTY(int id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(int fromId READ fromId WRITE setFromId NOTIFY fromIdChanged)
    Q_PROPERTY(QString fromName READ fromName WRITE setFromName NOTIFY fromNameChanged)
    Q_PROPERTY(QString FromSurname READ FromSurname WRITE setFromSurname NOTIFY FromSurnameChanged)
    Q_PROPERTY(QString fromPhoto READ fromPhoto WRITE setFromPhoto NOTIFY fromPhotoChanged)
    Q_PROPERTY(int ownerId READ ownerId WRITE setOwnerId NOTIFY ownerIdChanged)
    Q_PROPERTY(int date READ date WRITE setDate NOTIFY dateChanged)
    Q_PROPERTY(QString postType READ postType WRITE setPostType NOTIFY postTypeChanged)
    Q_PROPERTY(QString text READ text WRITE setText NOTIFY textChanged)
    Q_PROPERTY(int Likes_count READ Likes_count WRITE setLikes_count NOTIFY Likes_countChanged)
    Q_PROPERTY(bool userLikes READ userLikes WRITE setUserLikes NOTIFY userLikesChanged)
    Q_PROPERTY(bool userCanLikes READ userCanLikes WRITE setUserCanLikes NOTIFY userCanLikesChanged)
    Q_PROPERTY(int repostCount READ repostCount WRITE setRepostCount NOTIFY repostCountChanged)
    Q_PROPERTY(int commentsCount READ commentsCount WRITE setCommentsCount NOTIFY commentsCountChanged)
    Q_PROPERTY(bool canUserComment READ canUserComment WRITE setCanUserComment NOTIFY canUserCommentChanged)
    Q_PROPERTY(RecursingWallItem* repostedPost READ repostedPost WRITE setRepostedPost NOTIFY repostedPostChanged)
 //   Q_PROPERTY(SubListNewsFeed* children READ children)

    Q_PROPERTY(QVariantList GridAttachments READ GridAttachments WRITE setGridAttachments)
    Q_PROPERTY(QVariantList VideoAttachments READ VideoAttachments WRITE setVideoAttachments NOTIFY VideoAttachmentsChanged)
    Q_PROPERTY(QVariantList ListAttachments READ ListAttachments WRITE setListAttachments)
    Q_PROPERTY(QVariantList DocAttachments READ DocAttachments WRITE setDocAttachments NOTIFY DocAttachmentsChanged)
    Q_PROPERTY(QVariantList GifAttachments READ GifAttachments WRITE setGifAttachments NOTIFY GifAttachmentsChanged)
    /*
    //Repost Data
    int Repost_wall_id;
    int Repost_owner_id;
    int Repost_from_id;
    QString Repost_name;
    QString Repost_surname;
    QString Repost_photo;
    int Repost_date;
    QString Repost_post_type;
    QString Repost_text;
*/

    int id() const
    {
        return m_id;
    }

    int fromId() const
    {
        return m_fromId;
    }

    QString fromName() const
    {
        return m_fromName;
    }

    QString FromSurname() const
    {
        return m_FromSurname;
    }

    QString fromPhoto() const
    {
        return m_fromPhoto;
    }

    int ownerId() const
    {
        return m_ownerId;
    }

    int date() const
    {
        return m_date;
    }

    QString postType() const
    {
        return m_postType;
    }

    QString text() const
    {
        return m_text;
    }

    int Likes_count() const
    {
        return m_Likes_count;
    }

    bool userLikes() const
    {
        return m_userLikes;
    }

    bool userCanLikes() const
    {
        return m_userCanLikes;
    }

    int repostCount() const
    {
        return m_repostCount;
    }


    RecursingWallItem* repostedPost() const
    {
        return m_repostedPost;
    }

    QVariantList GridAttachments() const
    {
        return m_GridAttachments;
    }

    QVariantList ListAttachments() const
    {
        return m_ListAttachments;
    }

    QVariantList VideoAttachments() const
    {
        qDebug()<<"VideoAtt"<<m_VideoAttachments;
        return m_VideoAttachments;
    }

    QVariantList DocAttachments() const
    {
        return m_DocAttachments;
    }

    QVariantList GifAttachments() const
    {
        return m_GifAttachments;
    }

    int commentsCount() const
    {
        return m_commentsCount;
    }

    bool canUserComment() const
    {
        return m_canUserComment;
    }

signals:

    void idChanged(int id);

    void fromIdChanged(int fromId);

    void fromNameChanged(QString fromName);

    void FromSurnameChanged(QString FromSurname);

    void fromPhotoChanged(QString fromPhoto);

    void ownerIdChanged(int ownerId);

    void dateChanged(int date);

    void postTypeChanged(QString postType);

    void textChanged(QString text);

    void Likes_countChanged(int Likes_count);

    void userLikesChanged(bool userLikes);

    void userCanLikesChanged(bool userCanLikes);

    void repostCountChanged(int repostCount);


    void repostedPostChanged(RecursingWallItem* repostedPost);

    void VideoAttachmentsChanged(QVariantList VideoAttachments);

    void DocAttachmentsChanged(QVariantList DocAttachments);

    void GifAttachmentsChanged(QVariantList GifAttachments);

    void commentsCountChanged(int commentsCount);

    void canUserCommentChanged(bool canUserComment);

public slots:
    void setId(int id)
    {
        if (m_id == id)
            return;

        m_id = id;
        emit idChanged(id);
    }
    void setFromId(int fromId)
    {
        if (m_fromId == fromId)
            return;

        m_fromId = fromId;
        emit fromIdChanged(fromId);
    }
    void setFromName(QString fromName)
    {
        if (m_fromName == fromName)
            return;

        m_fromName = fromName;
        emit fromNameChanged(fromName);
    }
    void setFromSurname(QString FromSurname)
    {
        if (m_FromSurname == FromSurname)
            return;

        m_FromSurname = FromSurname;
        emit FromSurnameChanged(FromSurname);
    }
    void setFromPhoto(QString fromPhoto)
    {
        if (m_fromPhoto == fromPhoto)
            return;

        m_fromPhoto = fromPhoto;
        emit fromPhotoChanged(fromPhoto);
    }
    void setOwnerId(int ownerId)
    {
        if (m_ownerId == ownerId)
            return;

        m_ownerId = ownerId;
        emit ownerIdChanged(ownerId);
    }
    void setDate(int date)
    {
        if (m_date == date)
            return;

        m_date = date;
        emit dateChanged(date);
    }
    void setPostType(QString postType)
    {
        if (m_postType == postType)
            return;

        m_postType = postType;
        emit postTypeChanged(postType);
    }
    void setText(QString text)
    {
        if (m_text == text)
            return;

        m_text = text;
        emit textChanged(text);
    }

    void setLikes_count(int Likes_count)
    {
        if (m_Likes_count == Likes_count)
            return;

        m_Likes_count = Likes_count;
        emit Likes_countChanged(Likes_count);
    }
    void setUserLikes(bool userLikes)
    {
        if (m_userLikes == userLikes)
            return;

        m_userLikes = userLikes;
        emit userLikesChanged(userLikes);
    }
    void setUserCanLikes(bool userCanLikes)
    {
        if (m_userCanLikes == userCanLikes)
            return;

        m_userCanLikes = userCanLikes;
        emit userCanLikesChanged(userCanLikes);
    }
    void setRepostCount(int repostCount)
    {
        if (m_repostCount == repostCount)
            return;

        m_repostCount = repostCount;
        emit repostCountChanged(repostCount);
    }

    void setRepostedPost(RecursingWallItem* repostedPost)
    {
        if (m_repostedPost == repostedPost)
            return;


        if(repostedPost!=nullptr)
        {
            if(m_repostedPost!=nullptr)
                m_repostedPost->deleteLater();
        m_repostedPost = repostedPost;
        m_repostedPost->setParent(this);
        emit repostedPostChanged(repostedPost);
        }
    }

    void setListAttachments(QVariantList ListAttachments)
    {
        m_ListAttachments = ListAttachments;
    }
    void setGridAttachments(QVariantList GridAttachments)
    {
        m_GridAttachments = GridAttachments;
    }
    void setVideoAttachments(QVariantList VideoAttachments)
    {
        qDebug()<<"VideoResult"<<VideoAttachments;


        m_VideoAttachments = VideoAttachments;
       // emit VideoAttachmentsChanged(VideoAttachments);
    }
    void setDocAttachments(QVariantList DocAttachments)
    {
        if (m_DocAttachments == DocAttachments)
            return;

        m_DocAttachments = DocAttachments;
        emit DocAttachmentsChanged(DocAttachments);
    }
    void setGifAttachments(QVariantList GifAttachments)
    {
        if (m_GifAttachments == GifAttachments)
            return;

        m_GifAttachments = GifAttachments;
        emit GifAttachmentsChanged(GifAttachments);
    }
    void setCommentsCount(int commentsCount)
    {
        if (m_commentsCount == commentsCount)
            return;

        m_commentsCount = commentsCount;
        emit commentsCountChanged(commentsCount);
    }
    void setCanUserComment(bool canUserComment)
    {
        if (m_canUserComment == canUserComment)
            return;

        m_canUserComment = canUserComment;
        emit canUserCommentChanged(canUserComment);
    }
};

#endif // RECURSINGWALLITEM_H
