#ifndef PHOTOITEM_H
#define PHOTOITEM_H
#include <QtCore>

class PhotoItem
{
public:
    PhotoItem();
    int Id;
    int Album_id;
    int Owner_id;
    int date;
    QString Text;
    QString Photo_75;
    QString Photo_130;
    QString Photo_604;
    QString Photo_807;
    QString Photo_1280;
    int Width;
    int Height;
    QString Access_key;

};

#endif // PHOTOITEM_H
