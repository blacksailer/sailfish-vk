#include "groupitem.h"

GroupItem::GroupItem()
{
    item_type=BasicItemInfo::ItemType::GROUP;
}
GroupItem::GroupItem(const BasicItemInfo &itm)
{
    this->id=itm.id;
    this->photo_50=itm.photo_50;
    this->photo_100=itm.photo_100;
    this->photo_200=itm.photo_200;
    item_type=BasicItemInfo::ItemType::GROUP;
}
