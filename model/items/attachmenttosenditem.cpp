#include "attachmenttosenditem.h"


AttachmentToSendItem::AttachmentToSendItem(QObject *parent)
{
    setParent(parent);
}

AttachmentToSendItem::AttachmentToSendItem(QString source,int type)
{
    setSource(source);
    setType(static_cast<FileType>(type));
}

AttachmentToSendItem::~AttachmentToSendItem()
{
    qDebug()<<"Killed";
}

QString AttachmentToSendItem::Url() const
{
    return m_Url;
}

double AttachmentToSendItem::progress() const
{
    return m_progress;
}

int AttachmentToSendItem::total() const
{
    return m_total;
}

QString AttachmentToSendItem::Source() const
{
    return m_Source;
}

AttachmentToSendItem::FileType AttachmentToSendItem::Type() const
{
    return m_Type;
}

int AttachmentToSendItem::Status() const
{
    return m_Status;
}

void AttachmentToSendItem::setUrl(QString Url)
{
    if (m_Url == Url)
        return;

    m_Url = Url;
    emit UrlChanged(Url);
}

void AttachmentToSendItem::setProgress(double progress)
{
    if (m_progress == progress)
        return;

    m_progress = progress;
    emit progressChanged(progress);
}

void AttachmentToSendItem::setTotal(int total)
{
    if (m_total == total)
        return;

    m_total = total;
    emit totalChanged(total);
}

void AttachmentToSendItem::setSource(QString Source)
{
    if (m_Source == Source)
        return;

    m_Source = Source;
    emit SourceChanged(Source);
}

void AttachmentToSendItem::setType(AttachmentToSendItem::FileType Type)
{
    if (m_Type == Type)
        return;

    m_Type = Type;
    emit TypeChanged(Type);
}

void AttachmentToSendItem::setStatus(int Status)
{
    if (m_Status == Status)
        return;

    m_Status = Status;
    emit StatusChanged(Status);
}

