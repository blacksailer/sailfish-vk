#ifndef MESSAGEITEM_H
#define MESSAGEITEM_H
#include <QtCore>
#include <QObject>
#include <QVariant>
#include "wallitem.h"
#include "recursingwallitem.h"
class MessageItem :public QObject
{
    Q_OBJECT


public:
    MessageItem(QObject* parent =0);
    Q_PROPERTY(int id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(int userId READ userId WRITE setUserId NOTIFY userIdChanged)
    Q_PROPERTY(QString firstName READ firstName WRITE setFirstName NOTIFY firstNameChanged)
    Q_PROPERTY(QString lastName READ lastName WRITE setLastName NOTIFY lastNameChanged)
    Q_PROPERTY(QString userPhoto READ userPhoto WRITE setUserPhoto NOTIFY userPhotoChanged)
    Q_PROPERTY(int fromId READ fromId WRITE setFromId NOTIFY fromIdChanged)
    Q_PROPERTY(int date READ date WRITE setDate NOTIFY dateChanged)
    Q_PROPERTY(bool readState READ readState WRITE setReadState NOTIFY readStateChanged)
    Q_PROPERTY(bool out READ out WRITE setOut NOTIFY outChanged)
    Q_PROPERTY(QString title READ title WRITE setTitle NOTIFY titleChanged)
    Q_PROPERTY(QString body READ body WRITE setBody NOTIFY bodyChanged)
    Q_PROPERTY(bool emoji READ emoji WRITE setEmoji NOTIFY emojiChanged)
    Q_PROPERTY(bool important READ important WRITE setImportant NOTIFY importantChanged)
    Q_PROPERTY(bool deleted READ deleted WRITE setdeleted NOTIFY deletedChanged)

    //Прикрепления
    Q_PROPERTY(MessageItem* fwdMessage READ fwdMessage WRITE setFwdMessage NOTIFY fwdMessageChanged)
    Q_PROPERTY(RecursingWallItem* fwdPost READ fwdPost WRITE setFwdPost NOTIFY fwdPostChanged)
    Q_PROPERTY(QVariantList GridAttachments READ GridAttachments WRITE setGridAttachments NOTIFY GridAttachmentsChanged)
    Q_PROPERTY(QVariantList VideoAttachments READ VideoAttachments WRITE setVideoAttachments NOTIFY VideoAttachmentsChanged)
    Q_PROPERTY(QVariantList ListAttachments READ ListAttachments WRITE setListAttachments NOTIFY ListAttachmentsChanged)
    Q_PROPERTY(QVariantList DocAttachments READ DocAttachments WRITE setDocAttachments NOTIFY DocAttachmentsChanged)
    Q_PROPERTY(QVariantList GifAttachments READ GifAttachments WRITE setGifAttachments NOTIFY GifAttachmentsChanged)
    Q_PROPERTY(QVariantList stickerAttachments READ stickerAttachments WRITE setStickerAttachments NOTIFY stickerAttachmentsChanged)
    // Параметры Мультидиалогов
    Q_PROPERTY(int chatId READ chatId WRITE setChatId NOTIFY chatIdChanged)
    Q_PROPERTY(QStringList chatActive READ chatActive WRITE setChatActive NOTIFY chatActiveChanged)
    Q_PROPERTY(QString pushSettings READ pushSettings WRITE setPushSettings NOTIFY pushSettingsChanged)
    Q_PROPERTY(int userCount READ userCount WRITE setUserCount NOTIFY userCountChanged)
    Q_PROPERTY(int adminId READ adminId WRITE setAdminId NOTIFY adminIdChanged)
    Q_PROPERTY(QString action READ action WRITE setAction NOTIFY actionChanged)
    Q_PROPERTY(int actionMid READ actionMid WRITE setActionMid NOTIFY actionMidChanged)
    Q_PROPERTY(QString actionEmail READ actionEmail WRITE setActionEmail NOTIFY actionEmailChanged)
    Q_PROPERTY(QString actionText READ actionText WRITE setActionText NOTIFY actionTextChanged)
    Q_PROPERTY(QString photo50 READ photo50 WRITE setPhoto50 NOTIFY photo50Changed)
    Q_PROPERTY(QString photo100 READ photo100 WRITE setPhoto100 NOTIFY photo100Changed)
    Q_PROPERTY(QString photo200 READ photo200 WRITE setPhoto200 NOTIFY photo200Changed)
    int id() const
    {
        return m_id;
    }

    int userId() const
    {
        return m_userId;
    }

    QString firstName() const
    {
        return m_firstName;
    }

    QString lastName() const
    {
        return m_lastName;
    }

    QString userPhoto() const
    {
        return m_userPhoto;
    }

    int fromId() const
    {
        return m_fromId;
    }

    int date() const
    {
        return m_date;
    }

    bool readState() const
    {
        return m_readState;
    }

    bool out() const
    {
        return m_out;
    }

    QString title() const
    {
        return m_title;
    }

    QString body() const
    {
        return m_body;
    }

    bool emoji() const
    {
        return m_emoji;
    }

    bool important() const
    {
        return m_important;
    }

    bool deleted() const
    {
        return m_deleted;
    }

    MessageItem* fwdMessage() const
    {
        return m_fwdMessage;
    }

    QVariantList GridAttachments() const
    {
        return m_GridAttachments;
    }

    QVariantList ListAttachments() const
    {
        return m_ListAttachments;
    }

    int chatId() const
    {
        return m_chatId;
    }

    QStringList chatActive() const
    {
        return m_chatActive;
    }

    QString pushSettings() const
    {
        return m_pushSettings;
    }

    int userCount() const
    {
        return m_userCount;
    }

    int adminId() const
    {
        return m_adminId;
    }

    QString action() const
    {
        return m_action;
    }

    int actionMid() const
    {
        return m_actionMid;
    }

    QString actionEmail() const
    {
        return m_actionEmail;
    }

    QString actionText() const
    {
        return m_actionText;
    }

    QString photo50() const
    {
        return m_photo50;
    }

    QString photo100() const
    {
        return m_photo100;
    }

    QString photo200() const
    {
        return m_photo200;
    }

    RecursingWallItem* fwdPost() const
    {
        return m_fwdPost;
    }

    QVariantList VideoAttachments() const
    {
        return m_VideoAttachments;
    }

    QVariantList DocAttachments() const
    {
        return m_DocAttachments;
    }

    QVariantList GifAttachments() const
    {
        return m_GifAttachments;
    }

    QVariantList stickerAttachments() const
    {
        return m_stickerAttachments;
    }

public slots:
    void setId(int id)
    {
        if (m_id == id)
            return;

        m_id = id;
        emit idChanged(id);
    }

    void setUserId(int userId)
    {
        if (m_userId == userId)
            return;

        m_userId = userId;
        emit userIdChanged(userId);
    }

    void setFirstName(QString firstName)
    {
        if (m_firstName == firstName)
            return;

        m_firstName = firstName;
        emit firstNameChanged(firstName);
    }

    void setLastName(QString lastName)
    {
        if (m_lastName == lastName)
            return;

        m_lastName = lastName;
        emit lastNameChanged(lastName);
    }

    void setUserPhoto(QString userPhoto)
    {
        if (m_userPhoto == userPhoto)
            return;

        m_userPhoto = userPhoto;
        emit userPhotoChanged(userPhoto);
    }

    void setFromId(int fromId)
    {
        if (m_fromId == fromId)
            return;

        m_fromId = fromId;
        emit fromIdChanged(fromId);
    }

    void setDate(int date)
    {
        if (m_date == date)
            return;

        m_date = date;
        emit dateChanged(date);
    }

    void setReadState(bool readState)
    {
        if (m_readState == readState)
            return;

        m_readState = readState;
        emit readStateChanged(readState);
    }

    void setOut(bool out)
    {
        if (m_out == out)
            return;

        m_out = out;
        emit outChanged(out);
    }

    void setTitle(QString title)
    {
        if (m_title == title)
            return;

        m_title = title;
        emit titleChanged(title);
    }

    void setBody(QString body)
    {
        if (m_body == body)
            return;

        m_body = body;
        emit bodyChanged(body);
    }

    void setEmoji(bool emoji)
    {
        if (m_emoji == emoji)
            return;

        m_emoji = emoji;
        emit emojiChanged(emoji);
    }

    void setImportant(bool important)
    {
        if (m_important == important)
            return;

        m_important = important;
        emit importantChanged(important);
    }

    void setdeleted(bool deleted)
    {
        if (m_deleted == deleted)
            return;

        m_deleted = deleted;
        emit deletedChanged(deleted);
    }

    void setFwdMessage(MessageItem* fwdMessage)
    {
        if (m_fwdMessage == fwdMessage)
            return;

        if(fwdMessage!=nullptr)
        {
            if(m_fwdMessage!=nullptr)
                m_fwdMessage->deleteLater();
            m_fwdMessage=fwdMessage;
            m_fwdMessage->setParent(this);
            emit fwdMessageChanged(fwdMessage);
        }

    }

    void setGridAttachments(QVariantList GridAttachments)
    {
        m_GridAttachments = GridAttachments;
    }

    void setListAttachments(QVariantList ListAttachments)
    {
        m_ListAttachments = ListAttachments;
    }

    void setChatId(int chatId)
    {
        if (m_chatId == chatId)
            return;

        m_chatId = chatId;
        emit chatIdChanged(chatId);
    }

    void setChatActive(QStringList chatActive)
    {
        if (m_chatActive == chatActive)
            return;

        m_chatActive = chatActive;
        emit chatActiveChanged(chatActive);
    }

    void setPushSettings(QString pushSettings)
    {
        if (m_pushSettings == pushSettings)
            return;

        m_pushSettings = pushSettings;
        emit pushSettingsChanged(pushSettings);
    }

    void setUserCount(int userCount)
    {
        if (m_userCount == userCount)
            return;

        m_userCount = userCount;
        emit userCountChanged(userCount);
    }

    void setAdminId(int adminId)
    {
        if (m_adminId == adminId)
            return;

        m_adminId = adminId;
        emit adminIdChanged(adminId);
    }

    void setAction(QString action)
    {
        if (m_action == action)
            return;

        m_action = action;
        emit actionChanged(action);
    }

    void setActionMid(int actionMid)
    {
        if (m_actionMid == actionMid)
            return;

        m_actionMid = actionMid;
        emit actionMidChanged(actionMid);
    }

    void setActionEmail(QString actionEmail)
    {
        if (m_actionEmail == actionEmail)
            return;

        m_actionEmail = actionEmail;
        emit actionEmailChanged(actionEmail);
    }

    void setActionText(QString actionText)
    {
        if (m_actionText == actionText)
            return;

        m_actionText = actionText;
        emit actionTextChanged(actionText);
    }

    void setPhoto50(QString photo50)
    {
        if (m_photo50 == photo50)
            return;

        m_photo50 = photo50;
        emit photo50Changed(photo50);
    }

    void setPhoto100(QString photo100)
    {
        if (m_photo100 == photo100)
            return;

        m_photo100 = photo100;
        emit photo100Changed(photo100);
    }

    void setPhoto200(QString photo200)
    {
        if (m_photo200 == photo200)
            return;

        m_photo200 = photo200;
        emit photo200Changed(photo200);
    }

    void setFwdPost(RecursingWallItem* fwdPost)
    {
        if (m_fwdPost == fwdPost)
            return;

        if(fwdPost!=nullptr)
        {
            if(m_fwdPost!=nullptr)
                m_fwdPost->deleteLater();
            m_fwdPost = fwdPost;
            m_fwdPost->setParent(this);
            emit fwdPostChanged(fwdPost);
        }


    }

    void setVideoAttachments(QVariantList VideoAttachments)
    {
        if (m_VideoAttachments == VideoAttachments)
            return;

        m_VideoAttachments = VideoAttachments;
        emit VideoAttachmentsChanged(VideoAttachments);
    }

    void setDocAttachments(QVariantList DocAttachments)
    {
        if (m_DocAttachments == DocAttachments)
            return;

        m_DocAttachments = DocAttachments;
        emit DocAttachmentsChanged(DocAttachments);
    }

    void setGifAttachments(QVariantList GifAttachments)
    {
        if (m_GifAttachments == GifAttachments)
            return;

        m_GifAttachments = GifAttachments;
        emit GifAttachmentsChanged(GifAttachments);
    }

    void setStickerAttachments(QVariantList stickerAttachments)
    {
        if (m_stickerAttachments == stickerAttachments)
            return;

        m_stickerAttachments = stickerAttachments;
        emit stickerAttachmentsChanged(stickerAttachments);
    }

signals:
    void idChanged(int id);

    void userIdChanged(int userId);

    void firstNameChanged(QString firstName);

    void lastNameChanged(QString lastName);

    void userPhotoChanged(QString userPhoto);

    void fromIdChanged(int fromId);

    void dateChanged(int date);

    void readStateChanged(bool readState);

    void outChanged(bool out);

    void titleChanged(QString title);

    void bodyChanged(QString body);

    void emojiChanged(bool emoji);

    void importantChanged(bool important);

    void deletedChanged(bool deleted);

    void fwdMessageChanged(MessageItem* fwdMessage);

    void chatIdChanged(int chatId);

    void chatActiveChanged(QStringList chatActive);

    void pushSettingsChanged(QString pushSettings);

    void userCountChanged(int userCount);

    void adminIdChanged(int adminId);

    void actionChanged(QString action);

    void actionMidChanged(int actionMid);

    void actionEmailChanged(QString actionEmail);

    void actionTextChanged(QString actionText);

    void photo50Changed(QString photo50);

    void photo100Changed(QString photo100);

    void photo200Changed(QString photo200);

    void fwdPostChanged(RecursingWallItem* fwdPost);

    void VideoAttachmentsChanged(QVariantList VideoAttachments);

    void DocAttachmentsChanged(QVariantList DocAttachments);

    void GifAttachmentsChanged(QVariantList GifAttachments);

    void stickerAttachmentsChanged(QVariantList stickerAttachments);

    void GridAttachmentsChanged(QVariantList GridAttachments);

    void ListAttachmentsChanged(QVariantList ListAttachments);

private:

    /*
 //   QVariant geo;  // Массив с полями

    // Параметры Мультидиалогов
    QStringList Chat_active; // идентификаторы последних авторов
    QString Push_settings;//  sound или disabled_until
    QString Action; //служебное сообщение где action = chat_create || chat_title_update || chat_invite_user || chat_kick_user || chat_photo_update || chat_photo_remove
    int Action_mid; //служебное число, индтфкр польз-ля где action = chat_invite_user || chat_kick_user
    QString Action_email; //служебное сообщение где action = chat_invite_user || chat_kick_user && action_mid<0
    QString Action_text; //служебное сообщение где action = chat_create || chat_title_update

    */
    int m_id;
    int m_userId;
    QString m_firstName;
    QString m_lastName;
    QString m_userPhoto;
    int m_fromId;
    int m_date;
    bool m_readState;
    bool m_out;
    QString m_title;
    QString m_body;
    bool m_emoji;
    bool m_important;
    bool m_deleted;
    MessageItem* m_fwdMessage=nullptr;
    QVariantList m_GridAttachments;
    QVariantList m_ListAttachments;
    int m_chatId;
    QStringList m_chatActive;
    QString m_pushSettings;
    int m_userCount;
    int m_adminId;
    QString m_action;
    int m_actionMid;
    QString m_actionEmail;
    QString m_actionText;
    QString m_photo50;
    QString m_photo100;
    QString m_photo200;
    RecursingWallItem* m_fwdPost=nullptr;
    QVariantList m_VideoAttachments;
    QVariantList m_DocAttachments;
    QVariantList m_GifAttachments;
    QVariantList m_stickerAttachments;
};

#endif // MESSAGEITEM_H
