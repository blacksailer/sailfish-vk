#ifndef ATTACHMENTTOSENDITEM_H
#define ATTACHMENTTOSENDITEM_H

#include <QObject>
#include <QDebug>

class AttachmentToSendItem : public QObject
{
    Q_OBJECT

public:
    explicit AttachmentToSendItem(QObject* parent=0);
    AttachmentToSendItem(QString source, int type);
    ~AttachmentToSendItem();
    Q_ENUMS(FileType)
    Q_PROPERTY(QString Url READ Url WRITE setUrl NOTIFY UrlChanged)
    Q_PROPERTY(double progress READ progress WRITE setProgress NOTIFY progressChanged)
    Q_PROPERTY(int total READ total WRITE setTotal NOTIFY totalChanged)
    Q_PROPERTY(QString Source READ Source WRITE setSource NOTIFY SourceChanged)
    Q_PROPERTY(FileType Type READ Type WRITE setType NOTIFY TypeChanged)
    Q_PROPERTY(int Status READ Status WRITE setStatus NOTIFY StatusChanged)
    Q_PROPERTY(QString SendUrl READ SendUrl WRITE setSendUrl NOTIFY SendUrlChanged)
    enum FileType {
        PHOTO,
        VIDEO,
        AUDIO,
        DOC,
        WALL
    };
    QString Url() const;

    double progress() const;

    int total() const;

    QString Source() const;

    FileType Type() const;

    int Status() const;

    QString SendUrl() const
    {
        return m_SendUrl;
    }

signals:

    void UrlChanged(QString Url);

    void progressChanged(double progress);

    void totalChanged(int total);

    void SourceChanged(QString Source);

    void TypeChanged(FileType Type);

    void StatusChanged(int Status);

    void SendUrlChanged(QString SendUrl);

public slots:
    void setUrl(QString Url);
    void setProgress(double progress);

    void setTotal(int total);

    void setSource(QString Source);

    void setType(FileType Type);

    void setStatus(int Status);

    void setSendUrl(QString SendUrl)
    {
        if (m_SendUrl == SendUrl)
            return;

        m_SendUrl = SendUrl;
        emit SendUrlChanged(SendUrl);
    }

private:
    QString m_Url="";
    double m_progress=0;
    int m_total;
    QString m_Source;
    FileType m_Type;
    int m_Status=2;
    QString m_SendUrl;
};

#endif // ATTACHMENTTOSENDITEM_H
