#ifndef CHATITEM_H
#define CHATITEM_H
#include "basiciteminfo.h"

class ChatItem:public BasicItemInfo
{
public:
    ChatItem();

    int id;
    QString type;
    //string	тип диалога.
    QString title;
    //string	название беседы.
    int admin_id;
    //integer	идентификатор пользователя, который является создателем беседы.
    QList<int> users;
    //array	список идентификаторов (integer) участников беседы.
    //push_settings
//    object	настройки оповещений для диалога. Объект, содержащий поля:

//        sound (integer, [0,1]) — указывает, включен ли звук оповещений (1 — включен, 0 — отключен);
//        disabled_until (integer) — указывает, до какого времени оповещения для чата отключены. -1 — отключены навсегда (бессрочно).

    int left;
    //integer, [1]	флаг, указывающий, что пользователь покинул беседу. Всегда содержит 1.
    int kicked;
    //integer, [1]	флаг, указывающий, что пользователь был исключен из беседы. Всегда содержит 1.
    ChatItem(const BasicItemInfo &itm);
};

#endif // CHATITEM_H
