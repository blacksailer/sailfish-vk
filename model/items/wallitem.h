#ifndef WALLITEM_H
#define WALLITEM_H

#include <QtCore>
#include <QVariant>
class WallItem : QVariant
{
//    Q_OBJECT
public:

    WallItem();
  //  Q_PROPERTY(int Id READ Id WRITE setId NOTIFY IdChanged)

    int Id;
    int From_id;
    QString From_name;
    QString From_surname;
    QString From_photo;
    int Owner_id;
    int date;
    QString Post_type;
    QString Text;

    //Repost Data
    int Repost_wall_id;
    int Repost_owner_id;
    int Repost_from_id;
    QString Repost_name;
    QString Repost_surname;
    QString Repost_photo;

    int Repost_date;
    QString Repost_post_type;
    QString Repost_text;

    //Likes_Reposts
    int Likes_count;
    bool User_likes;
    bool User_canlike;
    int Reposts_count;

    enum TypeInputs {
        ID,
        FROM_ID,
        OWNER_ID,
        DATE,
        POST_TYPE,
        TEXT

    };

    void setValue(TypeInputs type,QVariant value);
};

#endif // WALLITEM_H
