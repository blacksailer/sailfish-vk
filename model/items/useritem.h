#ifndef USERITEM_H
#define USERITEM_H

#include "basiciteminfo.h"
class UserItem :public BasicItemInfo
{
public:
    UserItem();
    UserItem(const BasicItemInfo& itm);

    QString first_name;
//    string	имя.
     QString last_name;
//    string	фамилия.
     QString deactivated;
//    string	поле возвращается, если страница пользователя удалена или заблокирована, содержит значение deleted или banned. В этом случае дополнительные поля fields не возвращаются.
    int hidden;
//    integer	возвращается 1 при вызове без access_token, если пользователь установил настройку «Кому в интернете видна моя страница» — «Только пользователям ВКонтакте». В этом случае дополнительные поля fields не возвращаются.

};

#endif // USERITEM_H
