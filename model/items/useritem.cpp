#include "useritem.h"

UserItem::UserItem()
{
    item_type=BasicItemInfo::ItemType::USER;

}

UserItem::UserItem(const BasicItemInfo &itm)
{
    this->id=itm.id;
    this->photo_50=itm.photo_50;
    this->photo_100=itm.photo_100;
    this->photo_200=itm.photo_200;
    item_type=BasicItemInfo::ItemType::USER;
}
