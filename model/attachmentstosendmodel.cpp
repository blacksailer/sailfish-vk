#include "attachmentstosendmodel.h"

void AttachmentsToSendModel::doConnect()
{
    connect(mngr,&QNetworkAccessManager::finished,this,&AttachmentsToSendModel::rqstEnd);

}

void AttachmentsToSendModel::sendPhoto(AttachmentToSendItem *itm)
{
    QNetworkRequest rqst;
    rqst.setUrl(QUrl(m_UploadPhotoServer));
    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

    QFile *file = new QFile(itm->Source().right(itm->Source().length()-7));

    QHttpPart imagePart;
    imagePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/jpeg"));
    imagePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"photo\"; filename=\""+file->fileName()+"\""));

    if(file->open(QIODevice::ReadOnly)){
        imagePart.setBodyDevice(file);
        file->setParent(multiPart);
        multiPart->append(imagePart);
        QNetworkReply *reply = mngr->post(rqst,multiPart);
        connect(reply, SIGNAL(uploadProgress(qint64, qint64)),this, SLOT(progressChanged(qint64, qint64)));
        Requests[reply]=itm;
        Files[reply]=file;
        itm->setStatus(1);
        emit dataChanged(index(Items.indexOf(Requests[(QNetworkReply*)sender()])),index(Items.indexOf(Requests[(QNetworkReply*)sender()])));
    }
    else
    {
        qDebug()<<"File not open"<<file->fileName()<<file->errorString();
        file->deleteLater();
        emit errorOccured("не могу открыть файл");
    }

}

AttachmentsToSendModel::AttachmentsToSendModel(QObject *parent)
{
    Q_UNUSED(parent)
    mngr = new QNetworkAccessManager(this);
    roles=roleNames();
    doConnect();
}

AttachmentsToSendModel::~AttachmentsToSendModel()
{
    qDebug()<<"Killed";
}

void AttachmentsToSendModel::addAttachment(QString source, int Type, QString value)
{
    qDebug()<<source;
    AttachmentToSendItem::FileType Tp = static_cast<AttachmentToSendItem::FileType>(Type);
    beginInsertRows(QModelIndex(),rowCount(),rowCount());
    AttachmentToSendItem* itm = new AttachmentToSendItem(this);
    QQmlEngine::setObjectOwnership(itm, QQmlEngine::CppOwnership);

    itm->setType(Tp);

    //Если прикрепление из ВК
    if(value.length()>0)
    {
        qDebug()<<value;
        switch (Tp) {
        case AttachmentToSendItem::PHOTO:
            itm->setSource(value);
            itm->setSendUrl(source);
            itm->setStatus(0);
            break;
        default:
            itm->setSendUrl(source);
            itm->setStatus(0);
            break;
        }

        Items.append(itm);
        endInsertRows();
    }
    else
    {
        itm->setSource(source);
        Items.append(itm);
        endInsertRows();
        switch (Tp) {
        case AttachmentToSendItem::PHOTO:
            qDebug()<<"Photo";
            sendPhoto(itm);
            break;
        case AttachmentToSendItem::WALL:
            itm->setSendUrl(source);
            itm->setStatus(0);
            qDebug()<<"Wall";
            break;
        default:
            break;
        }
    }
}
void AttachmentsToSendModel::progressChanged(qint64 a, qint64 b) {
    if (b > 0) {
        qDebug()<< "Uploading " << a  << "/" << b << "%" << (double)a/(double)b*100.0;
        Requests[(QNetworkReply*)sender()]->setProgress((double)a/(double)b);
        Requests[(QNetworkReply*)sender()]->setTotal(b);
        emit dataChanged(index(Items.indexOf(Requests[(QNetworkReply*)sender()])),index(Items.indexOf(Requests[(QNetworkReply*)sender()])));
    }
}
void AttachmentsToSendModel::removeAttachment(int index)
{
    beginRemoveRows(QModelIndex(),index,index);
    Items.at(index)->deleteLater();
    Items.removeAt(index);
    qDebug()<<"deleting Attach";
    endRemoveRows();
}

void AttachmentsToSendModel::clear()
{
    while(Items.count()>0)
        removeAttachment(0);
}

void AttachmentsToSendModel::setPhotoAttachmentId(QString id, int indexIns)
{
    Items[indexIns]->setSendUrl(id);
    emit dataChanged(index(indexIns),index(indexIns));

}

QString AttachmentsToSendModel::getAllSendUrls()
{
    QStringList result;
    foreach (auto itm, Items) {
        result.append(itm->SendUrl());
    }
    return result.join(',');
}

void AttachmentsToSendModel::rqstEnd(QNetworkReply *reply)
{

    QString json=reply->readAll();
    qDebug()<<Requests[reply]<<json;
    Requests[reply]->setStatus(0);
    emit dataChanged(index(Items.indexOf(Requests[(QNetworkReply*)sender()])),index(Items.indexOf(Requests[(QNetworkReply*)sender()])));
    Files[reply]->close();
    Files[reply]->deleteLater();
    emit photosavemessagesphoto(json,Items.indexOf( Requests[reply]));
    Requests.remove(reply);
    Files.remove(reply);
    reply->deleteLater();
}

void AttachmentsToSendModel::qstEnd()
{
    //    qDebug()<<reply->readAll();
}




int AttachmentsToSendModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return Items.count();
}

QVariant AttachmentsToSendModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();
    if(index.row()>=Items.size())
        return QVariant();
    if(role==TYPE)
        return Items.at(index.row())->Type();
    if(role==SOURCE)
        return Items.at(index.row())->Source();
    if(role==URL)
        return Items.at(index.row())->Url();
    if(role==STATUS)
        return Items.at(index.row())->Status();
    if(role==PROGRESS)
        return Items.at(index.row())->progress();
    if(role==TOTAL)
        return Items.at(index.row())->total();
    if(role==SENDURL)
        return Items.at(index.row())->SendUrl();

    return QVariant();

}

//bool AttachmentsToSendModel::insertRows(int row, int count, const QModelIndex &parent)
//{
//}

//bool AttachmentsToSendModel::removeRows(int row, int count, const QModelIndex &parent)
//{
//    beginRemoveRows(QModelIndex(),row,count);
//    for(int i=0;i<count;i++)
//    {
//        Items.at(row+i)->deleteLater();
//        Items.removeAt(row+i);
//    }
//    qDebug()<<"deleting Attach";
//    endRemoveRows();

//}

QHash<int, QByteArray> AttachmentsToSendModel::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[TYPE] = "type";
    roles[SOURCE] = "c_source";
    roles[STATUS]="c_status";
    roles[URL] = "c_url";
    roles[PROGRESS] = "c_progress";
    roles[TOTAL] = "c_total";
    roles[SENDURL] = "c_sendurl";
    return roles;


}

//NETWORK

//void AttachmentsToSendModel::photo_saveMessagesPhoto(QString server,QString photo,QString hash)
//{

//    QNetworkRequest req;
//    QUrlQuery req_url;
//    req_url.setQuery("https://api.vk.com/method/photo.saveMessagesPhoto?");
//    req_url.addQueryItem("server",server);
//    req_url.addQueryItem("photo",photo);
//    req_url.addQueryItem("hash",hash);
//    req_url.addQueryItem("v",api_version);
//    req_url.addQueryItem("lang",lang);
//    req_url.addQueryItem("access_token",token_id);

//    req.setUrl(QUrl(req_url.query()));


//    QNetworkReply* Answer = manager->get(req);
//    Replies[Answer]=Method_Id::PHOTO_SAVEMESSAGESPHOTO;


//}

//void AttachmentsToSendModel::onPhoto_saveMessagesPhoto(QNetworkReply *reply)
//{
//    qDebug()<<reply->readAll();
//    QJsonDocument response = QJsonDocument::fromJson(reply->readAll());
//    emit photo_savemessagesphoto_end(response);
//    reply->deleteLater();

//}

