#include "proxydialogsmodel.h"


ProxyDialogsModel::ProxyDialogsModel()
{
    FModel = new DialogsModel();
    this->setSourceModel(FModel);
    this->setFilterRole(3);
}

int ProxyDialogsModel::modelCount()
{
    return sourceModel()->rowCount();
}

void ProxyDialogsModel::clear_Dialogs()
{
    FModel->clear_Dialogs();
}
