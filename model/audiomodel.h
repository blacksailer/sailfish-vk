#ifndef AUDIOMODEL_H
#define AUDIOMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include<QHash>

#include <QJsonValue>
#include <QJsonObject>
#include <QVariantMap>
#include <QDebug>
class AudioModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit AudioModel(QObject *parent = 0);
    enum TypeInputs {
        ID,
        OWNER_ID,
        URL,
        GENRE_ID,
        DURATION,
        TITLE,
        LYRICS_ID,
        ARTIST
    };

    int rowCount(const QModelIndex &parent= QModelIndex()) const;
     QVariant data(const QModelIndex &index, int role) const;
     Qt::ItemFlags flags(const QModelIndex &index) const;
         QHash<int, QByteArray> roleNames() const;

         void addNewsItem(const QVariantMap &value);
signals:


public slots:
         void clear_Audios();

private:
   QList<QVariantMap> *Items;  //Записи стены пользователя
   QHash<int,QByteArray> roles; //Роли

};

#endif // AUDIOMODEL_H
