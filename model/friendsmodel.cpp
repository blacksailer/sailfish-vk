#include "friendsmodel.h"

FriendsModel::FriendsModel(QObject *parent) :
    QAbstractListModel(parent)
{
    roles = roleNames();
}

int FriendsModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    if(isSearchMode())
        return SearchItems.size();
    else
        return Items.size();
}

QVariant FriendsModel::data(const QModelIndex &index, int role) const
{

    if(!index.isValid())
        return QVariant();
    if(isSearchMode())
    {
        if(index.row()>=SearchItems.size())
            return QVariant();
        if(role==USER_ID)
            return SearchItems.at(index.row()).toMap()["id"];
        else if(role==PHOTO_50)
            return SearchItems.at(index.row()).toMap()["photo_50"];
        else if(role==PHOTO_100)
            return SearchItems.at(index.row()).toMap()["photo_100"];
        else if(role==NAME)
            return SearchItems.at(index.row()).toMap()["first_name"];
        else if(role==SURNAME)
            return SearchItems.at(index.row()).toMap()["last_name"];
        else if(role==ONLINE)
            return SearchItems.at(index.row()).toMap()["online"];
        else
            return QVariant();
    }
    else
    {
        if(index.row()>=Items.size())
            return QVariant();
        if(role==USER_ID)
            return Items.at(index.row()).toMap()["id"];
        else if(role==PHOTO_50)
            return Items.at(index.row()).toMap()["photo_50"];
        else if(role==PHOTO_100)
            return Items.at(index.row()).toMap()["photo_100"];
        else if(role==NAME)
            return Items.at(index.row()).toMap()["first_name"];
        else if(role==SURNAME)
            return Items.at(index.row()).toMap()["last_name"];
        else if(role==ONLINE)
            return Items.at(index.row()).toMap()["online"];
        else
            return QVariant();

    }
}



QHash<int, QByteArray> FriendsModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[USER_ID] = "user_id";
    roles[PHOTO_50] = "photo_50";
    roles[PHOTO_100]="photo_100";
    roles[NAME] = "first_name";
    roles[SURNAME]="surname";
    roles[ONLINE] = "online";
    return roles;
}

void FriendsModel::addFriends(QVariantList value)
{
    foreach ( auto item, value) {
        setTotalCount(item.toMap()["totalCount"].toInt());
        beginInsertRows(QModelIndex(),rowCount(),rowCount());
        if(isSearchMode())
            SearchItems.append(item);
        else
            Items.append(item);
        endInsertRows();
    }
}

Parser *FriendsModel::DataProvider() const
{
    return m_DataProvider;
}

void FriendsModel::clearFriends()
{
    beginRemoveRows(QModelIndex(),0,rowCount());
    if(isSearchMode())
        SearchItems.clear();
    else
        Items.clear();
    endRemoveRows();
}

void FriendsModel::setDataProvider(Parser *DataProvider)
{
    if (m_DataProvider == DataProvider)
        return;

    m_DataProvider = DataProvider;
    connect(m_DataProvider,&Parser::friendsGenerated,this,&FriendsModel::addFriends);

    emit dataProviderChanged(DataProvider);
}

void FriendsModel::setIsSearchMode(bool isSearchMode)
{
    if (m_isSearchMode == isSearchMode)
        return;
    if(rowCount()>0)
    {
    beginRemoveRows(QModelIndex(),0,rowCount());
    endRemoveRows();
    }

    m_isSearchMode = isSearchMode;
    emit isSearchModeChanged(isSearchMode);
    if(rowCount()>0)
    {
        beginInsertRows(QModelIndex(),0,rowCount()-1);
        endInsertRows();
    }

}

