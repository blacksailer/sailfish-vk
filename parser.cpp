
#include "parser.h"

Parser::Parser(QObject *parent) :
    QObject(parent)
{
}

Parser::Parser(AudioModel *audiomodel, GlobalSettings *GlSett)
{
    RecWallposts = new QList<RecursingWallItem*>;
    //db = new DatabaseSqlite();
    Audio_model=audiomodel;
    Ids="";
    GlobalSett=GlSett;
    Notifications = new NotificationsHelper(this,GlSett);
    connect(this,&Parser::userInfoGenerated,this,&Parser::sendPostponedNotification);
}

Parser::~Parser()
{
    RecWallposts->clear();
    delete RecWallposts;
}



RecursingWallItem *Parser::getPostItem(QJsonArray copy_History_array, QJsonArray Profiles, QJsonArray Groups)
{
    RecursingWallItem* result = new RecursingWallItem();

    QList<RecursingWallItem*> history;
    for(int i=0;i<copy_History_array.size();i++)
    {
        QJsonValue RepostItem = copy_History_array.at(i);

        qDebug()<<"Enter PostItem";
        QJsonObject RepostItemObj = RepostItem.toObject();
        history.append(getPostItem(RepostItemObj,Profiles,Groups));
    }

    for(int i=1;i<copy_History_array.size();i++)
        history.at(i-1)->setRepostedPost(history.at(i));

    result = history.at(0);

    return result;
}

RecursingWallItem *Parser::getPostItem(QJsonObject Wall_object, QJsonArray Profiles, QJsonArray Groups)
{
    RecursingWallItem* result = new RecursingWallItem();

    QJsonObject RepostItemObj = Wall_object;

    result->setId(RepostItemObj["id"].toInt());
    if(RepostItemObj.contains("to_id"))
        result->setOwnerId(RepostItemObj["to_id"].toInt());
    else
        result->setOwnerId(RepostItemObj["owner_id"].toInt());
    result->setFromId(RepostItemObj["from_id"].toInt());
    result->setDate(RepostItemObj["date"].toInt());
    result->setPostType(RepostItemObj["post_type"].toString());
    result->setText(RepostItemObj["text"].toString());

    QJsonArray attachments = RepostItemObj["attachments"].toArray();

    auto ParsedAttachments = getAttachments(attachments);

    result->setGridAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::PHOTO]);
    result->setListAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::AUDIO]);
    result->setVideoAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::VIDEO]);
    result->setDocAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::DOC]);
    result->setGifAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::GIF]);

    if(!RepostItemObj["copy_history"].isNull())
    {
        QJsonArray Repost = RepostItemObj["copy_history"].toArray();
        auto Child_Element = getPostItem(Repost,Profiles,Groups);
        result->setRepostedPost(Child_Element);
    }

    foreach (const QJsonValue & value, Profiles)
    {
        QJsonObject response1=value.toObject();
        QVariantMap resultProfile;
        resultProfile["id"] = response1["id"].toInt();
        resultProfile["first_name"] = response1["first_name"].toString();
        resultProfile["last_name"] = response1["last_name"].toString();
        resultProfile["photo_50"] = response1["photo_50"].toString();

        if(result->fromId()==resultProfile["id"].toInt())
        {
            result->setFromName(resultProfile["first_name"].toString());
            result->setFromSurname(resultProfile["last_name"].toString());
            result->setFromPhoto(resultProfile["photo_50"].toString());

        }


    }

    foreach (const QJsonValue & value, Groups)
    {
        QJsonObject response1=value.toObject();
        QVariantMap resultGroup;
        resultGroup["id"] = response1["id"].toInt();
        resultGroup["name"] = response1["name"].toString();
        resultGroup["photo"] = response1["photo_50"].toString();
        if(result->fromId()<0)
        {
            if(abs(result->fromId())==resultGroup["id"].toInt())
            {
                result->setFromName(resultGroup["name"].toString());
                result->setFromPhoto(resultGroup["photo"].toString());
            }

        }


    }
    return result;
}

MessageItem *Parser::getFwdMsgItem(QJsonArray FwdPosts,QJsonArray Users, QJsonArray Groups)
{
    if(FwdPosts.isEmpty())
        return nullptr;
    else{
        MessageItem* result = new MessageItem();

        QList<MessageItem*> history;
        for(int i=0;i<FwdPosts.size();i++)
        {
            QJsonValue FwdMsgItem = FwdPosts.at(i);

            qDebug()<<"Enter PostItem";
            QJsonObject FwdMsgItemObj = FwdMsgItem.toObject();
            history.append(getFwdMsgItem(FwdMsgItemObj,Users,Groups));
        }
        for(int i=1;i<FwdPosts.size();i++)
            history.at(i-1)->setFwdMessage(history.at(i));

        result = history.at(0);

        return result;
    }
}

MessageItem *Parser::getFwdMsgItem(QJsonObject MessageObj,QJsonArray Users, QJsonArray Groups)
{
    MessageItem* MsgItem = new MessageItem();

    RecursingWallItem* Child_Element = nullptr;
    if(MessageObj.keys().contains("fwd_messages"))
    {
        MsgItem->setFwdMessage(getFwdMsgItem(MessageObj["fwd_messages"].toArray(),Users,Groups));
    }
    else
        MsgItem->setFwdMessage(nullptr);
    if(MessageObj.keys().contains("attachments"))
    {
        QJsonArray attachments = MessageObj["attachments"].toArray();

        foreach (const QJsonValue & value, attachments) {
            QJsonObject WallObj = value.toObject();
            QString NewItem="xxx";
            if(WallObj["type"].toString()=="wall") {
                QJsonObject obj = WallObj["wall"].toObject();
                Child_Element=getPostItem(obj,Users,Groups);
            }
        }

        auto ParsedAttachments = getAttachments(attachments);

        MsgItem->setGridAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::PHOTO]);
        MsgItem->setListAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::AUDIO]);
        MsgItem->setVideoAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::VIDEO]);
        MsgItem->setDocAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::DOC]);
        MsgItem->setGifAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::GIF]);
        MsgItem->setStickerAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::STICKER]);

    }

    MsgItem->setFwdPost(Child_Element);

    MsgItem->setId(MessageObj["id"].toInt());
    MsgItem->setUserId(MessageObj["user_id"].toInt());
    MsgItem->setFromId(MessageObj["from_id"].toInt());
    MsgItem->setDate((MessageObj["date"].toInt()));
    if(MessageObj["out"].toInt()>0)
        MsgItem->setOut(true);
    else
        MsgItem->setOut(false);
    if(MessageObj["read_state"].toInt()>0)
        MsgItem->setReadState(true);
    else
        MsgItem->setReadState(false);
    qDebug()<<MessageObj["out"].toInt();

    MsgItem->setBody(MessageObj["body"].toString());
    MsgItem->setEmoji(MessageObj["emoji"].toBool());
    MsgItem->setImportant(MessageObj["important"].toBool());
    MsgItem->setdeleted(MessageObj["deleted"].toBool());
    foreach (const QJsonValue user, Users) {
        QJsonObject UserObj = user.toObject();
        if(!MsgItem->out())
        {
            if(UserObj["id"].toInt()==MsgItem->userId())
            {
                MsgItem->setFirstName(UserObj["first_name"].toString());
                MsgItem->setLastName(UserObj["last_name"].toString());
                MsgItem->setPhoto50(UserObj["photo_50"].toString());
            }
        }
        else
            if(UserObj["id"].toInt()==GlobalSett->User_Id())
            {
                MsgItem->setFirstName(UserObj["first_name"].toString());
                MsgItem->setLastName(UserObj["last_name"].toString());
                MsgItem->setPhoto50(UserObj["photo_50"].toString());
            }
    }
    MsgItem->setAction(MessageObj["action"].toString());
    MsgItem->setActionMid(MessageObj["action_mid"].toInt());
    MsgItem->setActionEmail(MessageObj["action_email"].toString());
    MsgItem->setActionText(MessageObj["action_text"].toString());


    return MsgItem;
}

CommentItem *Parser::getCommentItem(QJsonObject itm, QJsonArray Users, QJsonArray Groups,QJsonArray DatUsers = QJsonArray())
{
    CommentItem* Comment = new CommentItem();
    Comment->setId(itm["id"].toInt());
    Comment->setFrom_id(itm["from_id"].toInt());
    Comment->setDate(itm["date"].toInt());
    Comment->setReply_to_comment(itm["reply_to_comment"].toInt());
    Comment->setReply_to_user(itm["reply_to_user"].toInt());
    Comment->setText(itm["text"].toString());

    QJsonArray attachments = itm["attachments"].toArray();

    auto ParsedAttachments = getAttachments(attachments);

    Comment->setGridAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::PHOTO]);
    Comment->setListAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::AUDIO]);
    Comment->setVideoAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::VIDEO]);
    Comment->setDocAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::DOC]);
    Comment->setGifAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::GIF]);
    Comment->setStickerAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::STICKER]);

    foreach (const QJsonValue & value, Users)
    {
        QJsonObject response1=value.toObject();

        if(Comment->from_id()==response1["id"].toInt())
        {
            Comment->setSex(response1["sex"].toInt());
            Comment->setFirstName(response1["first_name"].toString());
            Comment->setLastName(response1["last_name"].toString());
            Comment->setFromPhoto(response1["photo_50"].toString());
        }
    }
    foreach (const QJsonValue & value, DatUsers)
    {
        QJsonObject response1=value.toObject();
        if(Comment->reply_to_user()==response1["id"].toInt())
            Comment->setReply_to_userName(response1["first_name"].toString());

    }

    foreach (const QJsonValue & value, Groups)
    {
        QJsonObject response1=value.toObject();
        QVariantMap result;
        result["id"] = response1["id"].toInt();
        result["name"] = response1["name"].toString();
        result["photo"] = response1["photo_50"].toString();

        if(Comment->from_id()<0)
        {
            if(abs(Comment->from_id())==result["id"].toInt())
            {
                Comment->setFirstName(result["name"].toString());
                Comment->setFromPhoto(result["photo"].toString());
            }
        }

    }

    return Comment;
}

QMap<Parser::ATTACHMENTS_OBJECT, QVariantList> Parser::getAttachments(QJsonArray attachments)
{
    QMap<Parser::ATTACHMENTS_OBJECT, QVariantList> Result;
    QVariantList PhotoResult;
    QVariantList AudioResult;
    QVariantList VideoResult;
    QVariantList StickerResult;
    QVariantList DocResult;
    QVariantList GifResult;
    foreach(const QJsonValue &AttValue,attachments)
    {
        QJsonObject AttObj = AttValue.toObject();
        if(AttObj["type"].toString()=="photo")
        {
            QVariantMap  PhotoAttachments;
            QJsonObject PhotoObj = AttObj["photo"].toObject();
            PhotoAttachments["photo_130"]="";
            PhotoAttachments["photo_604"]="";
            PhotoAttachments["photo_1280"]="";

            PhotoAttachments["id"] = PhotoObj["id"].toInt();
            PhotoAttachments["owner_id"] = PhotoObj["owner_id"].toInt();
            PhotoAttachments["user_id"] = PhotoObj["user_id"].toInt();
            PhotoAttachments["album_id"] =PhotoObj["album_id"].toInt();
            PhotoAttachments["date"]=QDateTime::fromMSecsSinceEpoch(PhotoObj["date"].toInt()).toString();
            PhotoAttachments["text"]=PhotoObj["text"].toString();
            PhotoAttachments["photo_75"]=PhotoObj["photo_75"].toString();
            if(!PhotoObj["photo_130"].isNull())
                PhotoAttachments["photo_130"]=PhotoObj["photo_130"].toString();
            if(!PhotoObj["photo_604"].isNull())
                PhotoAttachments["photo_604"]=PhotoObj["photo_604"].toString();
            if(!PhotoObj["photo_1280"].isNull())
                PhotoAttachments["photo_1280"]=PhotoObj["photo_1280"].toString();
            PhotoAttachments["width"]=PhotoObj["width"].toInt();
            PhotoAttachments["height"]=PhotoObj["height"].toInt();
            PhotoAttachments["access_key"]=PhotoObj["access_key"].toString();
            PhotoResult.append(PhotoAttachments);
        }
        if(AttObj["type"].toString()=="audio")
        {
            QJsonObject AudioObj = AttObj["audio"].toObject();
            QVariantMap  AudioAttachment;
            AudioAttachment["type"]="audio";

            AudioAttachment["id"] = AudioObj["id"].toInt();
            AudioAttachment["title"] =AudioObj["title"].toString();
            AudioAttachment["duration"]=AudioObj["duration"].toInt();
            AudioAttachment["url"]=AudioObj["url"].toString();
            AudioAttachment["lyrics_id"]=AudioObj["lyrics_id"].toInt();
            AudioAttachment["genre_id"]=AudioObj["genre_id"].toInt();
            AudioAttachment["owner_id"]=AudioObj["owner_id"].toInt();
            AudioAttachment["artist"]=AudioObj["artist"].toString();
            AudioResult.append(AudioAttachment);
        }

        if(AttObj["type"].toString()=="video")
        {
            QJsonObject VideoObj = AttObj["video"].toObject();
            QVariantMap  VideoAttachment;
            foreach (QString key, VideoObj.keys()) {
                if(VideoObj[key].isDouble())
                    VideoAttachment[key]=VideoObj[key].toDouble();
                else
                    VideoAttachment[key]=VideoObj[key].toString();
            }
            VideoResult.append(VideoAttachment);
        }
        if(AttObj["type"].toString()=="doc")
        {
            QVariantMap  DocAttachment;
            QJsonObject DocObj = AttObj["doc"].toObject();
            DocAttachment=DocObj.toVariantMap();
            if(DocAttachment["type"]!=3)
                DocResult.append(DocAttachment);
            else
                GifResult.append(DocAttachment);
        }
        if(AttObj["type"].toString()=="sticker")
        {
            QVariantMap  StickerAttachment;
            QJsonObject StickerObj = AttObj["sticker"].toObject();
            StickerAttachment=StickerObj.toVariantMap();
            StickerResult.append(StickerAttachment);

        }
    }
    Result[ATTACHMENTS_OBJECT::PHOTO]=PhotoResult;
    Result[ATTACHMENTS_OBJECT::VIDEO]=VideoResult;
    Result[ATTACHMENTS_OBJECT::AUDIO]=AudioResult;
    Result[ATTACHMENTS_OBJECT::STICKER]=StickerResult;
    Result[ATTACHMENTS_OBJECT::DOC]=DocResult;
    Result[ATTACHMENTS_OBJECT::GIF]=GifResult;

    return Result;
}
bool Parser::checkError(QJsonObject doc)
{

    if(doc.contains("error"))
    {

        // Ошибка в выполнении. Сообщить пользователя
        QJsonObject ErrorBody=doc["error"].toObject();
        qDebug()<<ErrorBody.keys()<<ErrorBody["error_code"].toString()<<ErrorBody["error_msg"].toString();
        emit errorOccurred(ErrorBody["error_code"].toInt());
        return false;
    }
    else
        return true;

}

void Parser::get_Wall_Answer(QJsonDocument doc,bool appended)
{
    QJsonObject response2=doc.object();
    if(checkError(response2))
    {
        QJsonObject response1=response2["response"].toObject();
        qDebug()<<response1.keys()<<doc.toJson();
        //int count = response1["count"].toInt();

        QJsonArray Items = response1["items"].toArray();
        QJsonArray Profiles = response1["profiles"].toArray();
        QJsonArray Groups = response1["groups"].toArray();

        foreach (const QJsonValue & value, Items) {
            RecursingWallItem* NewItem = new RecursingWallItem();

            QJsonObject obj = value.toObject();
            NewItem->setId(obj["id"].toInt());

            qDebug()<<"Adding to parser";

            NewItem->setFromId(obj["from_id"].toInt());
            NewItem->setOwnerId(obj["owner_id"].toInt());

            //NewItem.date =QDateTime::fromTime_t(obj["date"].toInt()).toString("dd/MM hh:mm");
            NewItem->setDate(obj["date"].toInt());

            NewItem->setPostType(obj["post_type"].toString());
            NewItem->setText(obj["text"].toString());

            QJsonValue at=obj["likes"];

            QJsonObject temp = at.toObject();
            NewItem->setLikes_count(temp["count"].toInt());
            NewItem->setUserLikes(temp["user_likes"].toInt());
            NewItem->setUserCanLikes(temp["can_like"].toInt());
            auto CommentsObject = obj["comments"].toObject();
            NewItem->setCommentsCount(CommentsObject["count"].toInt());
            NewItem->setCanUserComment(CommentsObject["can_post"].toInt());
            at = obj["reposts"];
            temp=at.toObject();
            NewItem->setRepostCount(temp["count"].toInt());

            RecursingWallItem* Child_Element = nullptr;

            if(!obj["copy_history"].isNull())
            {
                QJsonArray Repost = obj["copy_history"].toArray();
                Child_Element = getPostItem(Repost,Profiles,Groups);
                NewItem->setRepostedPost(Child_Element);
                RecWallposts->append(NewItem);
                qDebug()<<"Add item to Model"<<NewItem->repostedPost()<<Child_Element;

            }
            else
            {
                QJsonArray attachments = obj["attachments"].toArray();

                auto ParsedAttachments = getAttachments(attachments);

                NewItem->setGridAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::PHOTO]);
                NewItem->setListAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::AUDIO]);
                NewItem->setVideoAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::VIDEO]);
                NewItem->setDocAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::DOC]);
                NewItem->setGifAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::GIF]);



                RecWallposts->append(NewItem);
            }

        }
        foreach (const QJsonValue & value, Profiles)
        {
            QJsonObject response1=value.toObject();
            QVariantMap result;
            result["id"] = response1["id"].toInt();
            result["first_name"] = response1["first_name"].toString();
            result["last_name"] = response1["last_name"].toString();
            result["photo_50"] = response1["photo_50"].toString();

            for(int i=0;i<RecWallposts->count();i++)
            {
                RecursingWallItem* a = RecWallposts->at(i);
                if(a->fromId()==result["id"].toInt())
                {
                    a->setFromName(result["first_name"].toString());
                    a->setFromSurname(result["last_name"].toString());
                    a->setFromPhoto(result["photo_50"].toString());
                }
            }
        }

        foreach (const QJsonValue & value, Groups)
        {
            QJsonObject response1=value.toObject();
            QVariantMap result;
            result["id"] = response1["id"].toInt();
            result["name"] = response1["name"].toString();
            result["photo"] = response1["photo_50"].toString();
            for(int i=0;i<RecWallposts->count();i++)
            {
                RecursingWallItem* a = RecWallposts->at(i);
                if(a->fromId()<0)
                {
                    if(abs(a->fromId())==result["id"].toInt())
                    {
                        a->setFromName(result["name"].toString());
                        a->setFromPhoto(result["photo"].toString());
                    }
                }
            }
        }

        for(int i=0;i<RecWallposts->count();i++)
        {

            RecursingWallItem* a = RecWallposts->at(i);

            emit wallGenerated(a,appended);

        }
        RecWallposts->clear();
    }
}

void Parser::get_Wall_Comments(QJsonDocument doc)
{
    QJsonObject response2=doc.object();
    if(checkError(response2))
    {
        QList<CommentItem*> listItems;

        auto response1 = response2["response"].toObject();
        int count = response1["count"].toInt();

        QJsonArray Items = response1["items"].toArray();
        QJsonArray Profiles = response1["profiles"].toArray();
        QJsonArray DatProfiles = response1["users"].toArray();
        QJsonArray Groups = response1["groups"].toArray();

        foreach (const QJsonValue & value, Items) {

            CommentItem* Comment = getCommentItem(value.toObject(),Profiles,Groups,DatProfiles);
            Comment->setTotalCommentsCount(count);
            listItems.append(Comment);
        }

        foreach (CommentItem* itm, listItems) {
            emit commentGenerated(itm,true);
        }
    }
}

void Parser::getWallCreateCommentAnswer(QJsonDocument doc)
{
    QJsonObject response=doc.object();
    auto comment = response["response"].toObject();
    emit newCommentIdGenerated(comment["comment_id"].toInt());
}

void Parser::user_get_answered(QJsonDocument ans)
{

    QVariantMap result;

    QJsonObject response2=ans.object();
    if(checkError(response2))
    {
        QJsonArray arr = response2["response"].toArray();
        foreach(const QJsonValue &AttValue,arr)
        {
            QJsonObject response1=AttValue.toObject();
            result= response1.toVariantMap();
        }
        emit userInfoGenerated(result);
    }
}

void Parser::group_get_answered(QJsonDocument ans)
{
    QJsonObject response=ans.object();
    if(checkError(response))
    {
        QJsonObject ResponseItem = response["response"].toObject();
        QJsonArray arr=ResponseItem["items"].toArray();
        QVariantList result;
        foreach(const QJsonValue &AttValue,arr)
        {
            QVariantMap GroupItem;
            QJsonObject itm = AttValue.toObject();
            GroupItem["id"]=itm["id"].toInt();
            GroupItem["name"]=itm["name"].toString();
            GroupItem["is_closed"]=itm["is_closed"].toInt();
            GroupItem["type"]=itm["type"].toString();
            GroupItem["is_admin"]=itm["is_admin"].toInt();
            GroupItem["is_member"]=itm["is_member"].toInt();
            GroupItem["photo_50"]=itm["photo_50"].toString();
            GroupItem["photo_100"]=itm["photo_100"].toString();
            GroupItem["photo_200"]=itm["photo_200"].toString();
            GroupItem["totalCount"]=ResponseItem["count"].toInt();
            GroupItem["status"]=ResponseItem["status"].toString();

            result.append(GroupItem);
        }
        emit groupsGenerated(result);
    }
}

void Parser::group_getbyid_answered(QJsonDocument ans)
{
    QJsonObject response=ans.object();
    if(checkError(response))
    {
        QJsonArray arr = response["response"].toArray();
        foreach(const QJsonValue &AttValue,arr)
        {
            QJsonObject response1=AttValue.toObject();
            emit groupInfoGenerated(response1.toVariantMap());
            break;
        }
    }
}

void Parser::friends_get_asnwer(QJsonDocument doc)
{
    qDebug()<<"Parser friends";
    QJsonObject response=doc.object();
    if(checkError(response))
    {
        QJsonObject response1=response["response"].toObject();
        QJsonArray Items = response1["items"].toArray();
        auto FriendsList=Items.toVariantList();
        QVariantList result;
        for(int i=0;i<FriendsList.size();i++)
        {   auto FriendItem = FriendsList.at(i).toMap();
            FriendItem["totalCount"]=response1["count"].toInt();
            result.append(FriendItem);
        }
        qDebug()<<response;
        emit friendsGenerated(result);
    }
}

void Parser::get_RecNewsfeed_answer(QJsonDocument doc, bool appended)
{

    QJsonObject response2=doc.object();
    if(checkError(response2))
    {
        QJsonObject response1=response2["response"].toObject();

        //для загрзки продолжения ленты
        emit setStart(response1["next_from"].toString());

        QJsonArray Items = response1["items"].toArray();
        QJsonArray Profiles = response1["profiles"].toArray();
        QJsonArray Groups = response1["groups"].toArray();

        foreach (const QJsonValue & value, Items) {
            RecursingWallItem* NewItem = new RecursingWallItem;

            QJsonObject obj = value.toObject();
            NewItem->setId(obj["post_id"].toInt());  //POST_ID а не ID в WallModel!!!!!

            qDebug()<<"Adding to parser";

            NewItem->setFromId(obj["source_id"].toInt()); //source_id а не FROM_ID в WallModel!!!!!
            NewItem->setOwnerId(obj["owner_id"].toInt());

            //     NewItem->date =QDateTime::fromTime_t(obj["date"].toInt()).toString("dd/MM hh:mm");
            NewItem->setDate(obj["date"].toInt());
            NewItem->setPostType(obj["post_type"].toString());
            NewItem->setText(obj["text"].toString());

            QJsonValue at=obj["likes"];

            QJsonObject temp = at.toObject();
            NewItem->setLikes_count(temp["count"].toInt());
            NewItem->setUserLikes(temp["user_likes"].toInt());
            NewItem->setUserCanLikes(temp["can_like"].toInt());
            at = obj["reposts"];
            temp=at.toObject();
            NewItem->setRepostCount(temp["count"].toInt());

            auto CommentsObject = obj["comments"].toObject();
            NewItem->setCommentsCount(CommentsObject["count"].toInt());
            NewItem->setCanUserComment(CommentsObject["can_post"].toInt());

            RecursingWallItem* Child_Element = nullptr;
            if(!obj["copy_history"].isNull())
            {
                QJsonArray Repost = obj["copy_history"].toArray();
                Child_Element = getPostItem(Repost,Profiles,Groups);
                NewItem->setRepostedPost(Child_Element);
                RecWallposts->append(NewItem);
                qDebug()<<"Add item to Model"<<NewItem->repostedPost()<<Child_Element;
            }
            else
            {
                QJsonArray attachments = obj["attachments"].toArray();

                auto ParsedAttachments = getAttachments(attachments);

                NewItem->setGridAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::PHOTO]);
                NewItem->setListAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::AUDIO]);
                NewItem->setVideoAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::VIDEO]);
                NewItem->setDocAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::DOC]);
                NewItem->setGifAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::GIF]);

                RecWallposts->append(NewItem);
            }
        }

        foreach (const QJsonValue & value, Profiles)
        {
            QJsonObject response1=value.toObject();
            QVariantMap result;
            result["id"] = response1["id"].toInt();
            result["first_name"] = response1["first_name"].toString();
            result["last_name"] = response1["last_name"].toString();
            result["photo_50"] = response1["photo_50"].toString();

            for(int i=0;i<RecWallposts->count();i++)
            {
                RecursingWallItem* a = RecWallposts->at(i);
                if(a->fromId()==result["id"].toInt())
                {
                    a->setFromName(result["first_name"].toString());
                    a->setFromSurname(result["last_name"].toString());
                    a->setFromPhoto(result["photo_50"].toString());
                }
            }
        }

        foreach (const QJsonValue & value, Groups)
        {
            QJsonObject response1=value.toObject();
            QVariantMap result;
            result["id"] = response1["id"].toInt();
            result["name"] = response1["name"].toString();
            result["photo"] = response1["photo_50"].toString();
            for(int i=0;i<RecWallposts->count();i++)
            {
                RecursingWallItem* a = RecWallposts->at(i);
                if(a->fromId()<0)
                {
                    if(abs(a->fromId())==result["id"].toInt())
                    {
                        a->setFromName(result["name"].toString());
                        a->setFromPhoto(result["photo"].toString());
                    }
                }
            }
        }

        if(!appended)
        {
            for(int i=RecWallposts->count()-1;i>=0;i--)
            {

                RecursingWallItem* a = RecWallposts->at(i);

                emit newsfeedGenerated(a,appended);

            }
        }
        else
        {
            for(int i=0;i<RecWallposts->count();i++)
            {

                RecursingWallItem* a = RecWallposts->at(i);
                emit newsfeedGenerated(a,appended);
            }

        }

        RecWallposts->clear();
    }
}

void Parser::get_Audio_answer(QJsonDocument doc)
{

    QJsonObject response = doc.object();
    if(checkError(response))
    {
        QJsonObject response1=response["response"].toObject();
        int count = response1["count"].toInt();
        qDebug()<<"Audio get"<<response1.keys() <<count;

        QJsonArray Items = response1["items"].toArray();

        foreach(const QJsonValue &value,Items)
        {
            qDebug()<<"Enter foreach";
            QJsonObject AudioObj = value.toObject();
            QVariantMap  AudioAttachment;

            AudioAttachment["id"] = AudioObj["id"].toInt();
            AudioAttachment["title"] =AudioObj["title"].toString();
            AudioAttachment["duration"]=AudioObj["duration"].toInt();
            AudioAttachment["url"]=AudioObj["url"].toString();
            AudioAttachment["lyrics_id"]=AudioObj["lyrics_id"].toInt();
            AudioAttachment["genre_id"]=AudioObj["genre_id"].toInt();
            AudioAttachment["owner_id"]=AudioObj["owner_id"].toInt();
            AudioAttachment["artist"]=AudioObj["artist"].toString();
            Audio_model->addNewsItem(AudioAttachment);
        }
    }
}

void Parser::get_Video_answer(QJsonDocument doc)
{
    QJsonObject response = doc.object();
    if(checkError(response))
    {
        QJsonObject response1=response["response"].toObject();
        QJsonObject Videos=response1["videos"].toObject();
        QJsonArray VideoItems = Videos["items"].toArray();
        QJsonArray Users = response1["users"].toArray();
        QJsonArray Groups = response1["groups"].toArray();

        QVariantMap VideoMap;
        foreach(const QJsonValue &value,VideoItems)
        {
            QJsonObject VideoObj = value.toObject();
            foreach (QString key, VideoObj.keys()) {
                if(VideoObj[key].isDouble())
                    VideoMap[key]=VideoObj[key].toDouble();
                else
                    VideoMap[key]=VideoObj[key].toString();
            }

            QJsonObject Files = VideoObj["files"].toObject();
            QStringList FileList;
            qDebug()<<Files;
            foreach (QString key, Files.keys()) {
                FileList.append(Files[key].toString());
            }
            qDebug()<<FileList;
            VideoMap["files_array"]=FileList;
            if(VideoMap["owner_id"].toInt()>0){
                foreach (auto itm, Users) {
                    auto itmObj = itm.toObject();
                    if(itmObj["id"].toInt()==VideoMap["owner_id"].toInt())
                    {
                        VideoMap["owner_name"]=itmObj["first_name"].toString()+" "+itmObj["last_name"].toString();
                        VideoMap["photo_50"]=itmObj["photo_50"].toString();
                    }
                }
            }
            else
            {
                foreach (auto itm, Groups) {
                    auto itmObj = itm.toObject();
                    if(itmObj["id"].toInt()==VideoMap["owner_id"].toInt()*-1)
                    {       VideoMap["owner_name"]=itmObj["name"].toString();
                        VideoMap["photo_50"]=itmObj["photo_50"].toString();
                    }


                }
            }

        }

        emit videoGenerated(VideoMap);

    }
}

void Parser::getComments_Video_answer(QJsonDocument doc)
{
    QJsonObject response2=doc.object();
    if(checkError(response2))
    {
        QList<CommentItem*> listItems;

        auto response1 = response2["response"].toObject();
        int count = response1["count"].toInt();

        QJsonArray Items = response1["items"].toArray();
        QJsonArray Profiles = response1["profiles"].toArray();
        QJsonArray DatProfiles = response1["users"].toArray();
        QJsonArray Groups = response1["groups"].toArray();

        foreach (const QJsonValue & value, Items) {

            CommentItem* Comment = getCommentItem(value.toObject(),Profiles,Groups,DatProfiles);
            Comment->setTotalCommentsCount(count);
            listItems.append(Comment);
        }

        foreach (CommentItem* itm, listItems) {
            emit commentGenerated(itm,true);
        }
    }
}
void Parser::getDialogs_Messages_answer(QJsonDocument doc)
{

    QJsonObject response = doc.object();
    if(checkError(response))
    {
        QJsonObject response1=response["response"].toObject();
        QJsonObject dialogs = response1["dialogs"].toObject();
        QJsonArray Users = response1["users"].toArray();
        QJsonArray Groups = response1["groups"].toArray();
        //int count = dialogs["count"].toInt();

        QJsonArray Items = dialogs["items"].toArray();

        foreach(const QJsonValue &value,Items)
        {
            QJsonObject MessageObj = value.toObject();
            QJsonObject DialogObj = MessageObj["message"].toObject();

            QVariantList ValuesToDb;
            for(QJsonObject::iterator it =DialogObj.begin();it!=DialogObj.end();++it)
            {
                QJsonValue temp = it.value();
                ValuesToDb<<temp.toVariant();
            }

            emit addMessagetoDb(DialogObj.keys(),ValuesToDb);

            MessageItem* DialogHeader = new MessageItem();
            DialogHeader->setId(DialogObj["id"].toInt());
            int uid = DialogObj["user_id"].toInt();
            DialogHeader->setUserId(uid);



            DialogHeader->setFromId(DialogObj["from_id"].toInt());
            DialogHeader->setDate((DialogObj["date"].toInt()));
            if(DialogObj["read_state"].toInt()>0)
                DialogHeader->setReadState(true);
            else
                DialogHeader->setReadState(false);

            DialogHeader->setOut(DialogObj["out"].toBool());
            DialogHeader->setTitle(" ... ");
            DialogHeader->setBody(DialogObj["body"].toString());
            if(DialogObj["chat_id"].isNull())
                DialogHeader->setChatId(0);
            else
            {
                DialogHeader->setChatId(DialogObj["chat_id"].toInt());
                DialogHeader->setPhoto50(DialogObj["photo_50"].toString());
                DialogHeader->setPhoto100(DialogObj["photo_100"].toString());
                DialogHeader->setPhoto200(DialogObj["photo_200"].toString());
                DialogHeader->setTitle(DialogObj["title"].toString());

            }
            foreach(const QJsonValue &valueUser,Users)
            {
                QJsonObject UserObj = valueUser.toObject();
                if(UserObj["id"].toInt()==DialogHeader->userId())
                {
                    DialogHeader->setFirstName(UserObj["first_name"].toString());
                    DialogHeader->setLastName(UserObj["last_name"].toString());
                    DialogHeader->setUserPhoto(UserObj["photo_50"].toString());
                    DialogHeader->setPhoto50(UserObj["photo_50"].toString());
                    break;
                }
            }
            foreach(const QJsonValue &valueGroup,Groups)
            {
                QJsonObject GroupObj = valueGroup.toObject();
                if(GroupObj["id"].toInt()*-1==DialogHeader->userId())
                {
                    qDebug()<<GroupObj.keys();
                    DialogHeader->setFirstName(GroupObj["name"].toString());
                    DialogHeader->setLastName("");
                    DialogHeader->setUserPhoto(GroupObj["photo_50"].toString());
                    DialogHeader->setPhoto50(GroupObj["photo_50"].toString());
                    DialogHeader->setPhoto100(GroupObj["photo_100"].toString());
                    DialogHeader->setPhoto200(GroupObj["photo_200"].toString());

                    break;
                }
            }



            DialogHeader->setEmoji(DialogObj["emoji"].toBool());
            DialogHeader->setImportant(DialogObj["important"].toBool());
            DialogHeader->setdeleted(DialogObj["deleted"].toBool());


            emit dialogGenerated(DialogHeader);
        }
    }
}

void Parser::searchDialogs_Messages_answer(QJsonDocument doc)
{
    QJsonObject json = doc.object();
    if(checkError(json))
    {
        QJsonArray response=json["response"].toArray();
        foreach(const QJsonValue &value,response)
        {
            QJsonObject Item = value.toObject();

            if(Item["type"].toString()== "profile"){
                UserItem* UserResult = new UserItem;
                UserResult->item_type=BasicItemInfo::ItemType::USER;
                UserResult->id=Item["id"].toInt();
                UserResult->first_name=Item["first_name"].toString();
                UserResult->last_name=Item["last_name"].toString();
                UserResult->photo_50=Item["photo_50"].toString();
                UserResult->photo_100=Item["photo_100"].toString();
                UserResult->photo_200=Item["photo_200"].toString();
                qDebug()<<UserResult->first_name<<UserResult->last_name;
                emit searchDialogsGenerated(UserResult);
            }
            else if(Item["type"].toString()== "group"){
                GroupItem *GroupResult = new GroupItem;
                GroupResult->item_type=BasicItemInfo::ItemType::GROUP;

                GroupResult->id=Item["id"].toInt();
                GroupResult->photo_50=Item["photo_50"].toString();
                GroupResult->photo_100=Item["photo_100"].toString();
                GroupResult->photo_200=Item["photo_200"].toString();
                GroupResult->name=Item["name"].toString();
                emit searchDialogsGenerated(GroupResult);
            }
            else if(Item["type"].toString()== "chat"){
                ChatItem *ChatResult = new ChatItem;
                ChatResult->item_type=BasicItemInfo::ItemType::CHAT;

                ChatResult->id=Item["id"].toInt();
                ChatResult->photo_50=Item["photo_50"].toString();
                ChatResult->photo_100=Item["photo_100"].toString();
                ChatResult->photo_200=Item["photo_200"].toString();
                ChatResult->title=Item["title"].toString();
                emit searchDialogsGenerated(ChatResult);
            }
            else{
                BasicItemInfo* BasicResult= new BasicItemInfo;
                BasicResult->id=Item["id"].toInt();
                BasicResult->photo_50=Item["photo_50"].toString();
                BasicResult->photo_100=Item["photo_100"].toString();
                BasicResult->photo_200=Item["photo_200"].toString();
                emit searchDialogsGenerated(BasicResult);
            }
        }
    }
}

void Parser::getHistory_Messages_answer(QJsonDocument doc)
{

    QJsonObject response = doc.object();
    if(checkError(response))
    {
        QJsonObject response1=response["response"].toObject();
        QJsonObject Messages=response1["messages"].toObject();
        QJsonArray Users = response1["users"].toArray();
        QJsonArray Groups = response1["groups"].toArray();

        //int count = Messages["count"].toInt();

        QJsonArray Items = Messages["items"].toArray();
        foreach(const QJsonValue &value,Items)
        {
            MessageItem* MessageHistory= new MessageItem();
            QJsonObject MessageObj = value.toObject();
            RecursingWallItem* Child_Element = nullptr;
            if(MessageObj.keys().contains("fwd_messages"))
            {
                MessageHistory->setFwdMessage(getFwdMsgItem(MessageObj["fwd_messages"].toArray(),Users,Groups));
            }
            else
                MessageHistory->setFwdMessage(nullptr);
            if(MessageObj.keys().contains("attachments"))
            {
                QJsonArray attachments = MessageObj["attachments"].toArray();
                foreach (const QJsonValue & value, attachments) {
                    QJsonObject WallObj = value.toObject();
                    if(WallObj["type"].toString()=="wall") {
                        QJsonObject obj = WallObj["wall"].toObject();
                        Child_Element=getPostItem(obj,Users,Groups);
                    }
                }

                auto ParsedAttachments = getAttachments(attachments);

                MessageHistory->setGridAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::PHOTO]);
                MessageHistory->setListAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::AUDIO]);
                MessageHistory->setVideoAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::VIDEO]);
                MessageHistory->setDocAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::DOC]);
                MessageHistory->setGifAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::GIF]);
                MessageHistory->setStickerAttachments(ParsedAttachments[ATTACHMENTS_OBJECT::STICKER]);

            }

            MessageHistory->setFwdPost(Child_Element);

            MessageHistory->setId(MessageObj["id"].toInt());
            MessageHistory->setUserId(MessageObj["user_id"].toInt());
            MessageHistory->setFromId(MessageObj["from_id"].toInt());
            MessageHistory->setDate((MessageObj["date"].toInt()));
            if(MessageObj["out"].toInt()>0)
                MessageHistory->setOut(true);
            else
                MessageHistory->setOut(false);
            if(MessageObj["read_state"].toInt()>0)
                MessageHistory->setReadState(true);
            else
                MessageHistory->setReadState(false);

            MessageHistory->setBody(MessageObj["body"].toString());
            MessageHistory->setEmoji(MessageObj["emoji"].toBool());
            MessageHistory->setImportant(MessageObj["important"].toBool());
            MessageHistory->setdeleted(MessageObj["deleted"].toBool());
            foreach (const QJsonValue user, Users) {
                QJsonObject UserObj = user.toObject();
                if(UserObj["id"].toInt()==MessageHistory->fromId())
                {
                    MessageHistory->setFirstName(UserObj["first_name"].toString());
                    MessageHistory->setLastName(UserObj["last_name"].toString());
                    MessageHistory->setPhoto50(UserObj["photo_50"].toString());
                }
            }
            MessageHistory->setAction(MessageObj["action"].toString());
            MessageHistory->setActionMid(MessageObj["action_mid"].toInt());
            MessageHistory->setActionEmail(MessageObj["action_email"].toString());
            MessageHistory->setActionText(MessageObj["action_text"].toString());

            emit MessageGenerated(MessageHistory);
        }


    }
}

void Parser::getMessage_ById_answer(QJsonDocument doc)
{
    QJsonObject response = doc.object();
    if(checkError(response))
    {
        QJsonObject response1=response["response"].toObject();
        qDebug()<<response.keys();
        QJsonObject Messages=response1["messages"].toObject();
        QJsonArray Users = response1["users"].toArray();
        //int count = Messages["count"].toInt();

        QJsonArray Items = Messages["items"].toArray();
        foreach(const QJsonValue &value,Items)
        {
            MessageItem* MessageHistory= new MessageItem();
            QJsonObject MessageObj = value.toObject();

            if(ActiveDialogsIds.contains(MessageObj["user_id"].toInt()))
            {
                MessageHistory= getFwdMsgItem(MessageObj,Users,QJsonArray());

                emit LongPollMesageGenerated(MessageHistory);
            }
        }
    }

}

void Parser::getLikes_add_answer(QJsonDocument doc)
{
    Q_UNUSED(doc)
}

void Parser::getLikes_delete_answer(QJsonDocument doc)
{
    Q_UNUSED(doc)

}

void Parser::getLikes_isLiked_answer(QJsonDocument doc)
{
    Q_UNUSED(doc)

}

void Parser::getLongPollServer_answer(QJsonDocument doc)
{

    QJsonObject response = doc.object();
    if(checkError(response))
    {
        QJsonObject response1=response["response"].toObject();
        emit longPollData(response1["key"].toString(),response1["server"].toString(),response1["ts"].toInt(),response1["pts"].toInt());
        //    db->createLongPollTable();
        //
    }
}

void Parser::getInitiateLongPoll_answer(QJsonDocument doc)
{
    QJsonObject response = doc.object();
    if(checkError(response))
    {
        if(response.contains("failed"))
        {
            qDebug()<<"Ошибка-"<<response["failed"].toInt();
            switch(response["failed"].toInt())
            {
            case 1:
                emit tsLongPoll(response["ts"].toInt());
                break;
            case 2:
                qDebug()<<"2 Error on LongPOll Server is 2";
                emit reconnectToLongPoll();
                break;
            case 3:
                qDebug()<<"3 Error on LongPOll Server is 3";
                emit reconnectToLongPoll();
                break;
            }
        }
        else
        {
            emit tsLongPoll(response["ts"].toInt());
            QJsonArray Updates = response["updates"].toArray();
            if(!Updates.empty())
            {
                QStringList message_ids;
                QString user_ids="";
                for(auto it=Updates.begin();it!=Updates.end();++it) {
                    QJsonValueRef temp=*it;
                    QJsonArray UpdateItem=temp.toArray();

                    if(UpdateItem.first().toInt()== 4){
                        //Новое сообщение
                        //2000000202 -- chatid messages.getChat?chat_ids
                        qDebug()<<"Сообщение от-"<<UpdateItem.at(3).toInt();
                        qDebug()<<"Текст-"<<UpdateItem.at(6).toString();
                        int flags = UpdateItem.at(2).toInt();
                        PostponedNotification notify;
                        if(!(flags & LONGPOLL_FLAGS::OUTBOX) && !ActiveDialogsIds.contains(UpdateItem.at(3).toInt()))
                        {
                            if(UpdateItem.at(6).toString()!="")
                                notify.text=UpdateItem.at(6).toString();
                            else
                            {
                                QVariantMap Attachments= UpdateItem.at(7).toObject().toVariantMap();
                                if(Attachments["attach1_type"].toString()=="photo")
                                    notify.text="Фотография";
                                else if(Attachments["attach1_type"].toString()=="video")
                                    notify.text="Видео";
                                else if(Attachments["attach1_type"].toString()=="audio")
                                    notify.text="Аудио";
                                else if(Attachments["attach1_type"].toString()=="wall")
                                    notify.text="Запись со стены";
                                else if(Attachments["attach1_type"].toString()=="sticker")
                                    notify.text="Стикер";
                                else if(Attachments["attach1_type"].toString()=="link")
                                    notify.text="Ссылка";
                                else
                                    notify.text="Вложение";
                            }
                            notify.sender_id=UpdateItem.at(3).toInt();
                            if(UpdateItem.at(3).toInt()>2000000000)
                            {
                                notify.name = UpdateItem.at(5).toString();
                                Notifications->notifyMessage(notify.sender_id,notify.name,notify.text);

                            }
                            else
                            {
                                PostponedNotificationList.append(notify);
                                if(user_ids.size()>0)
                                    user_ids.append(","+QString::number(notify.sender_id));
                                else
                                    user_ids.append(QString::number(notify.sender_id));
                            }

                            qDebug()<<"Получить по ИД сообщения - "<<QString::number(UpdateItem.at(3).toInt());


                        }
                        message_ids.append(QString::number(UpdateItem.at(1).toInt()));


                    }
                    else if(UpdateItem.first().toInt()== 80){
                        emit setUnreadCount(UpdateItem.at(1).toInt());
                    }else {
                        qDebug()<<"Фича от LongPoll(код)-"<<UpdateItem.first().toInt();
                    }

                }
                //Обеспечивает переписку в "прямом эфире"
                if(message_ids.size()>0)
                    emit getMessageById(message_ids);
                if(user_ids.size()>0)
                    emit need_user_data(user_ids,"","Nom");
            }


        }

        //emit ptsLongPoll(response1["pts"].toInt());
        //    db->createLongPollTable();
        //    db->insertLongPoll(response1["key"].toString(),response1["server"].toString(),response1["ts"].toInt(),response1["pts"].toInt());
        qDebug()<<doc.toVariant();
    }
    else
        qDebug()<<doc.toVariant();
}

void Parser::getNotifications(QJsonDocument doc)
{
    Q_UNUSED(doc)

}

void Parser::DialogIdDo(int userId, int type)
{
    if(type==1)
        for(int i=0;i<ActiveDialogsIds.size();i++)
        {   if(userId==ActiveDialogsIds.at(i))
            {
                ActiveDialogsIds.removeAt(i);
                break;
            }
        }
    else if(type ==0)
        ActiveDialogsIds.append(userId);

    qDebug()<<ActiveDialogsIds;
}

void Parser::sendPostponedNotification(QVariantMap user)
{
    QList<int> IdsToRemove;
    if(PostponedNotificationList.size()>0)
    {
        for (int i = 0; i < PostponedNotificationList.size(); ++i) {
            if(PostponedNotificationList.at(i).sender_id==user["id"].toInt()){
                auto itm = PostponedNotificationList.at(i);
                itm.name=user["first_name"].toString()+" "+user["last_name"].toString();
                PostponedNotificationList.replace(i,itm);
                Notifications->notifyMessage(PostponedNotificationList.at(i).sender_id,PostponedNotificationList.at(i).name,PostponedNotificationList.at(i).text);
                IdsToRemove.append(i);
            }
        }
        for (int i = 0; i < IdsToRemove.size(); ++i) {
            PostponedNotificationList.removeAt(IdsToRemove[i]-i);
        }
    }
}
