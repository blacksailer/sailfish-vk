﻿#include "databasesqlite.h"

DatabaseSqlite* DatabaseSqlite::p_instance=nullptr;
DatabaseSqlite::DatabaseSqlite()
{
    dbase = QSqlDatabase::addDatabase("QSQLITE");
    dbase.setDatabaseName(QStandardPaths::writableLocation(QStandardPaths::DataLocation)+"/my_db2.sqlite");


    createMessagesTable(); //Создаем таблицу сообщений
    createLongPollTable();
}



void DatabaseSqlite::createTokensTable(QString token, int expire, int u_id)
{
    if (!dbase.open()) {
        qDebug() << "I can't open a DB!";
        return;
    }

    QSqlQuery a_query;
    // DDL query
    QString str = "CREATE TABLE IF NOT EXISTS tokens ("
                  "id integer PRIMARY KEY NOT NULL, "
                  "token_id VARCHAR(255), "
                  "expires_in integer, "
                  "user_id integer "
                  ");";
    bool b = a_query.exec(str);
    if (!b) {
        qDebug() << "I can't create a table!";
    }
    QString str_insert = "INSERT INTO tokens(token_id, expires_in, user_id) "
                         "VALUES ('%1', %2, %3);";
    str = str_insert.arg(token)
            .arg(expire)
            .arg(u_id);
    b = a_query.exec(str);
    if (!b) {
        qDebug() << "I can't insert a token!";
    }
    //.....
    if (!a_query.exec("SELECT * FROM tokens WHERE rowid=(SELECT MAX(rowid) FROM tokens)")) {
        qDebug() << "Даже селект не получается, я пас.";
        return;
    }
    QSqlRecord rec = a_query.record();

    int number = 0,
            age = 0;
    QString address = "";

    while (a_query.next()) {
        number = a_query.value(rec.indexOf("id")).toInt();
        age = a_query.value(rec.indexOf("expires_in")).toInt();
        address = a_query.value(rec.indexOf("token_id")).toString();

        qDebug() << "number is " << number
                 << ". age is " << age
                 << ". address" << address;
    }
    dbase.close();
}

int DatabaseSqlite::selectToken()
{
    int result=0;
    if (!dbase.open()) {
        qDebug() << "I can't open a DB!";
        return -1;
    }

    QSqlQuery a_query;
    // DDL query
    if (!a_query.exec("SELECT expires_in FROM tokens WHERE rowid=(SELECT max(rowid) FROM tokens)")) {
        qDebug() << "Даже селект не получается, я пас."<<dbase.lastError();
        dbase.close();
        return -1;
    }
    QSqlRecord rec = a_query.record();

    while (a_query.next()) {
        result = a_query.value(rec.indexOf("expires_in")).toInt();
        qDebug() << "number is " << result;
    }
    dbase.close();
    return result;

}

QVariantList DatabaseSqlite::getToken()
{
    QVariantList result;
    if (!dbase.open()) {
        qDebug() << "I can't create a table!";
        result.append("open Error");
        qDebug()<<dbase.lastError();

        return result;
    }

    QSqlQuery a_query;
    // DDL query
    if (!a_query.exec("SELECT * FROM tokens WHERE id=(SELECT max(id) FROM tokens)")) {
        qDebug() << "Даже селект не получается, я пас.";
        result.append("Select Error");
        return result;
    }
    QSqlRecord rec = a_query.record();

    while (a_query.next()) {
        result.append(a_query.value(rec.indexOf("token_id")).toString());
        result.append(a_query.value(rec.indexOf("expires_in")).toInt());
        result.append(a_query.value(rec.indexOf("user_id")).toInt());

    }
    dbase.close();


    return result;
}



void DatabaseSqlite::createMessagesTable()
{
    if (!dbase.open()) {
        qDebug() << "Что-то пошло не так!";
        qDebug()<<dbase.lastError();

        return;
    }
    QString str = "CREATE TABLE IF NOT EXISTS messages"
                  "("
                  "       [id]  INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                  "          [message_id]  INTEGER NULL, "
                  "          [user_id] INTEGER NULL, "
                  "          [from_id]  INTEGER NULL, "
                  "          [date]  INTEGER NULL, "
                  "      [read_state]  BOOLEAN NOT NULL DEFAULT 0, "
                  "          out  BOOLEAN NOT NULL DEFAULT 0, "
                  "          title  TEXT NULL, "
                  "          body  TEXT NULL, "
                  "          fwd_messages  BOOLEAN NULL, "
                  "          emoji  BOOLEAN NOT NULL DEFAULT 0, "
                  "          important  BOOLEAN NOT NULL DEFAULT 0, "
                  "          deleted  BOOLEAN NOT NULL DEFAULT 0, "
                  "          chat_id  INTEGER NULL, "
                  "          chat_active  TEXT NULL, "
                  "          push_settings  TEXT NULL, "
                  "          users_count  INTEGER NULL, "
                  "          admin_id  INTEGER NULL, "
                  "          action  TEXT NULL, "
                  "          action_mid  INTEGER NULL, "
                  "          action_email  TEXT NULL, "
                  "          action_text  TEXT NULL, "
                  "          photo_50  TEXT NULL, "
                  "          photo_100  TEXT NULL, "
                  "          photo_200  TEXT NULL"
                  "         "
                  ");";

    QSqlQuery a_query;
    // DDL query
    bool b = a_query.exec(str);
    if (!b) {
        qDebug() << "Messages";
    }
    str=              "CREATE TABLE IF NOT EXISTS geo ("
                      "id INTEGER PRIMARY KEY AUTOINCREMENT,"
                      "          message_id  INTEGER NULL,"
                      "type TEXT NULL,"
                      "coordinates TEXT NULL,"
                      "place BOOLEAN NULL,"
                      "place_id BOOLEAN NULL,"
                      "place_title TEXT NULL,"
                      "place_latitude REAL NULL,"
                      "place_longtitude REAL NULL,"
                      "place_created INTEGER NULL,"
                      "place_icon TEXT NULL,"
                      "place_country TEXT NULL,"
                      "place_city TEXT NULL"
                      ");";


    // DDL query
    b = a_query.exec(str);
    if (!b) {
        qDebug() << "I can't create a table!";
    }
    str=               "CREATE TABLE IF NOT EXISTS  attachments ("
                       "id INTEGER PRIMARY KEY AUTOINCREMENT,"
                       " message_id INTEGER NULL,  "
                       "photo BOOLEAN false,"
                       "video BOOLEAN false,"
                       "audio BOOLEAN false,"
                       "doc BOOLEAN false,"
                       "wall BOOLEAN false,"
                       "wall_reply BOOLEAN false,"
                       "sticker BOOLEAN false"
                       ");";

    b = a_query.exec(str);
    if (!b) {
        qDebug() << "I can't create a table!";
    }
    str=             "CREATE TABLE IF NOT EXISTS  photo ("
                     "id INTEGER,"
                     "album_id INTEGER,"
                     "owner_id INTEGER,"
                     "user_id  INTEGER DEFAULT 100,"
                     "photo_text TEXT,"
                     "photo_75 TEXT NULL,"
                     "photo_130 TEXT NULL,"
                     "photo_640 TEXT NULL,"
                     "photo_807 TEXT NULL,"
                     "photo_1280 TEXT NULL,"
                     "photo_2560 TEXT NULL,"
                     "date INTEGER,"
                     " PRIMARY KEY (id,owner_id)"
                     ");";

    // DDL query
    b = a_query.exec(str);
    if (!b) {
        qDebug() << "I can't create a table!";
    }
    str=           "CREATE TABLE IF NOT EXISTS video ("
                   "id INTEGER  ,"
                   "owner_id INTEGER NULL,"
                   "title TEXT NULL,"
                   "description TEXT NULL,"
                   "duration INTEGER NULL,"
                   "photo_130 TEXT NULL,"
                   "photo_320 TEXT NULL,"
                   "photo_640 TEXT NULL,"
                   "date INTEGER,"
                   "adding_date INTEGER,"
                   "views INTEGER DEFAULT 0,"
                   "comments INTEGER DEFAULT 0,"
                   "player TEXT NULL,"
                   "access_key TEXT NULL,"
                   "processing INTEGER NULL,"
                   "live INTEGER NULL,"
                   " PRIMARY KEY (id,owner_id)"
                   ");";


    // DDL query
    b = a_query.exec(str);
    if (!b) {
        qDebug() << "I can't create a table!";
    }
    str=                "CREATE TABLE IF NOT EXISTS audio ("
                        "id INTEGER ,"
                        "owner_id INTEGER,"
                        "artist TEXT NULL,"
                        "title TEXT NULL,"
                        "duration INTEGER,"
                        "url TEXT,"
                        "lyrics_id INTEGER NULL,"
                        "album_id INTEGER NULL,"
                        "genre_id INTEGER NULL,"
                        "date INTEGER,"
                        " PRIMARY KEY (id,owner_id)"
                        ");";


    // DDL query
    b = a_query.exec(str);
    if (!b) {
        qDebug() << "I can't create a table!";
    }
    str=             "CREATE TABLE IF NOT EXISTS doc("
                     "id INTEGER,"
                     "owner_id INTEGER,"
                     "title TEXT NULL,"
                     "size INTEGER,"
                     "ext TEXT,"
                     "url TEXT,"
                     "photo_100 TEXT NULL,"
                     "photo_130 TEXT NULL,"
                     " PRIMARY KEY (id,owner_id)"
                     ");";


    // DDL query
    b = a_query.exec(str);
    if (!b) {
        qDebug() << "I can't create a table!";
    }
    str=                "CREATE TABLE IF NOT EXISTS sticker ("
                        "id INTEGER,"
                        "product_id INTEGER,"
                        "photo_64 TEXT NULL,"
                        "photo_128 TEXT NULL,"
                        "photo_256 TEXT NULL,"
                        "photo_352 TEXT NULL,"
                        "PRIMARY KEY(id,product_id)"
                        ");";

    // DDL query
    b = a_query.exec(str);
    if (!b) {
        qDebug() << "I can't create a table!";
    }
    str=           "CREATE TABLE IF NOT EXISTS wall ("
                   "id INTEGER,"
                   "to_id INTEGER,"
                   "from_id INTEGER,"
                   "date INTEGER,"
                   "text TEXT,"
                   "comments_count INTEGER,"
                   "likes_count INTEGER DEFAULT 0,"
                   "likes_user_likes BOOLEAN,"
                   "likes_can_like BOOLEAN,"
                   "likes_can_publish BOOLEAN,"
                   "reposts_count INTEGER,"
                   "reposts_user_reposted BOOLEAN DEFAULT 1,"
                   "geo INTEGER NULL,"
                   "post_source TEXT NULL,"
                   "signer_id INTEGER NULL,"
                   "copy_owner_id INTEGER NULL,"
                   "copy_post_id INTEGER NULL,"
                   "copy_text TEXT NULL,"
                   "PRIMARY KEY(id,from_id)"
                   ");";

    // DDL query
    b = a_query.exec(str);
    if (!b) {
        qDebug() << "I can't create a table!";
    }
    str=                "CREATE TABLE IF NOT EXISTS wall_reply ("
                        "id INTEGER,"
                        "to_id INTEGER,"
                        "from_id INTEGER,"
                        "date INTEGER,"
                        "text TEXT,"
                        "likes_count INTEGER DEFAULT 0,"
                        "likes_user_likes BOOLEAN,"
                        "likes_can_like BOOLEAN,"
                        "reply_to_uid INTEGER,"
                        "reply_to_cid INTEGER,"
                        "PRIMARY KEY(id,from_id)"
                        ");";

    b = a_query.exec(str);
    if (!b) {
        qDebug() << "I can't create a table!";
    }
    str=           "CREATE TABLE IF NOT EXISTS clojure_message("
                   "ancestor INTEGER NOT NULL,"
                   "descendant INTEGER NOT NULL,"
                   "depth INTEGER NOT NULL DEFAULT 0,"
                   "PRIMARY KEY(ancestor,descendant),"
                   "FOREIGN KEY(ancestor) REFERENCES messages(id),"
                   "FOREIGN KEY(descendant) REFERENCES messages(id)"
                   ");";

    b = a_query.exec(str);
    if (!b) {
        qDebug() << "I can't create a table!";
    }
    dbase.close();

}

void DatabaseSqlite::createLongPollTable()
{
    QString str =  "CREATE TABLE IF NOT EXISTS  longpoll("
                   "[key] TEXT NULL,"
                   "[server] TEXT NULL,"
                   "[ts] INTEGER NULL,"
                   "[pts] INTEGER NULL);" ;
    if (!dbase.open()) {
        qDebug() << "Что-то пошло не так!";
        qDebug()<<dbase.lastError();

        return;
    }

    QSqlQuery a_query;
    // DDL query
    bool b = a_query.exec(str);
    if (!b) {
        qDebug() << "I can't create a table!";
    }
    dbase.close();

}

bool DatabaseSqlite::insertLongPoll(QString key, QString server, int ts, int pts)
{
    if (!dbase.open()) {
        qDebug() << "Что-то пошло не так! longpoll";
        return false;
    }
    QSqlQuery a_query(dbase);

    qDebug() << " longpoll insert";

    QString str;
    QString str_insert = "INSERT INTO longpoll(key,server, ts, pts) "
                         "VALUES ('%1', '%2', %3, %4);";
    str = str_insert.arg(key)
            .arg(server)
            .arg(QString::number(ts))
            .arg(QString::number(pts));

    bool b = a_query.exec(str);
    if (!b) {
        qDebug()<<str << "Кажется данные не вставляются, проверьте дверь, может она закрыта?"<<a_query.lastError().text();
    }
    qDebug()<<b<<str <<a_query.lastError().text();

    dbase.close();

    return true;
}

void DatabaseSqlite::insertMessage(QStringList Columns, QVariantList Values)
{
    if (!dbase.open()) {
        qDebug() << "I can't open a DB!";
        qWarning()<<"БД не открыто";
        return;
    }


    QSqlQuery a_query;
    // SELECT id of inserted message
    QString str_select = "SELECT id FROM messages WHERE message_id=? AND user_id=?";

    a_query.prepare(str_select);

    for(int j=0;j<Columns.count();j++)
        if(Columns.at(j)=="id" || Columns.at(j)=="user_id")
            a_query.addBindValue(Values.at(j),QSql::Out);



    bool isSuccess_query = a_query.exec();
    if (!isSuccess_query) {
        qDebug() << a_query.lastError().text();
        qDebug()<<a_query.lastQuery();
        return;
    }

    QSqlRecord rec = a_query.record();

    int rows_count=0;
    while (a_query.next()) {
        qDebug() << "message_id is " << a_query.value(rec.indexOf("id")).toInt();
        rows_count++;
    }


    if(rows_count>0)
        return;


    //insert new message
    QString str_insert = "INSERT INTO messages(";
    QVariantMap attachments;

    for(int j=0;j<Columns.count();j++)
    {
        if(Columns.at(j)=="id")
            str_insert.append("message_id,");
        else if(Columns.at(j)=="attachments")
            attachments["attachments"]=Values.at(j);
        else //if(Values.at(j)==)
            str_insert.append(Columns.at(j)+",");
    }//Удаляем запятую
    str_insert.remove(str_insert.length()-1,1);

    //" messages_id,read_state,out,title,body,emoji,important,deleted)"
    str_insert.append(") VALUES (");
    for(int it=0;it<Columns.count();it++)
    {
        if(Columns.at(it)!="attachments")
            str_insert.append("?,");
    }
    str_insert.replace(str_insert.length()-1,1,")");
    a_query.prepare(str_insert);
    for(int j=0;j<Values.count();j++)
    {
        qDebug()<<Values.at(j).type();
        switch(Values.at(j).type())
        {case QVariant::String:
            a_query.addBindValue(Values.at(j).toString());
            break;
        case QVariant::Bool:
            a_query.addBindValue(Values.at(j).toBool());
            break;
        case QVariant::List:
            if(Values.at(j).toList().count()>0)
            {
            qDebug()<<Values.at(j).toList().first().toMap().contains("type");
            if(!Values.at(j).toList().first().toMap().contains("type"))
                a_query.addBindValue("-5");
            }
            else
                a_query.addBindValue("-5");

            break;
        case QVariant::Double:
            a_query.addBindValue(Values.at(j).toInt());

            break;
        case QVariant::Int:
            a_query.addBindValue(Values.at(j).toInt());

            break;
        default:
            a_query.addBindValue("-5");
            break;
        }
    }
    bool b = a_query.exec();
    if (!b) {
        qDebug() << a_query.lastError().text();
        qDebug() << a_query.lastQuery();
        qDebug()<<Columns<<Values<<"Values number ="<<Values.count();
        return;
    }

    // SELECT id of inserted message
    str_select = "SELECT id FROM messages WHERE message_id=? AND user_id=?";

    a_query.prepare(str_select);

    for(int j=0;j<Columns.count();j++)
        if(Columns.at(j)=="id" || Columns.at(j)=="user_id")
            a_query.addBindValue(Values.at(j).toInt(),QSql::Out);


    isSuccess_query = a_query.exec();
    if (!isSuccess_query) {
        qDebug() << a_query.lastError().text();
        qDebug()<<a_query.lastQuery();
        return;
    }

    rec = a_query.record();

    int message_id = 0;
    while (a_query.next()) {
        message_id = a_query.value(rec.indexOf("id")).toInt();
        qDebug() << "message_id is " << message_id;
    }

    // insert into closure table id of inserted message
    str_insert = "INSERT INTO clojure_message VALUES (?,?,?)";
    a_query.prepare(str_insert);
    a_query.addBindValue(message_id);
    a_query.addBindValue(message_id);
    a_query.addBindValue(1);

    isSuccess_query = a_query.exec();
    if (!isSuccess_query) {
        qDebug() << a_query.lastError().text();
        qDebug()<<a_query.lastQuery();
        return;
    }

    insertAttachments(attachments,message_id);

    dbase.close();

}

void DatabaseSqlite::insertAttachments(QVariantMap attach, int message_id)
{
    qDebug()<<attach<<message_id;

    //insert to 'attachments'

    //insert to destination
}

QStringList DatabaseSqlite::LongPollPrevious()
{
    qDebug()<<"LongPollPrevious";
    QStringList result;
    if (!dbase.open()) {
        qDebug() << "Что-то пошло не так!";
        return result;
    }

    QSqlQuery a_query(dbase);
    // DDL query
    if (!a_query.exec("SELECT ts,pts,COUNT(rowid) AS cnt FROM longpoll WHERE rowid=(SELECT max(rowid) FROM longpoll);")) {
        qDebug() << "Даже селект не получается, я пас.";
        return result;
    }
    QSqlRecord rec;
while (a_query.next()) {
     rec = a_query.record();

    result.append(rec.value(rec.indexOf("ts")).toString());
    result.append(rec.value(rec.indexOf("pts")).toString());
    result.append(rec.value(rec.indexOf("cnt")).toString());
}
    dbase.close();
    qDebug()<<"LongPollPrevious"<<rec.count()<<result<<dbase.isOpen();

    return result;
}




void DatabaseSqlite::updateLongPoll(DatabaseSqlite::LongPollColumns key, QString value)
{
    if (!dbase.open()) {
        qDebug() << "Что-то пошло не так! longpoll update"<<dbase.lastError();
        return;
    }
    QSqlQuery a_query(dbase);

    qDebug() << " longpoll update";

    QString str_insert;
    switch (key) {
    case KEY:
        str_insert = "UPDATE longpoll SET key = '"+value + "' WHERE rowid=(SELECT max(rowid) FROM longpoll);";
        break;
    case SERVER:
        str_insert = "UPDATE longpoll SET server = '"+value + "' WHERE rowid=(SELECT max(rowid) FROM longpoll);";
        break;
    case TS:
        str_insert = "UPDATE longpoll SET ts = "+value + " WHERE rowid=(SELECT max(rowid) FROM longpoll);";
        break;
    case PTS:
        str_insert = "UPDATE longpoll SET pts = "+value + " WHERE rowid=(SELECT max(rowid) FROM longpoll);";
        break;
    }

    bool b = a_query.exec(str_insert);
    if (!b) {
        qDebug() << "Кажется данные не вставляются, проверьте дверь, может она закрыта? longpoll update" ;
    }
    dbase.close();

}
