#include "dbusadaptor.h"

DBusAdaptor::DBusAdaptor(QGuiApplication *application)
    : app(application)
{

    setParent(app);
    connect(application, SIGNAL(aboutToQuit()), SIGNAL(aboutToQuit()));
    view=nullptr;
    PageStarter = new NotificationPageStarter(this);
    Settings =new GlobalSettings(app);
    JsonParser=new Parser (&MusicModel,Settings);
    a=new vkapi(JsonParser,Settings);

    new SkAdaptor(this);

    QDBusConnection dbus = QDBusConnection::sessionBus();

    qDebug() << "Register object" << dbus.registerObject("/SK", this); //object path
    bool ready = dbus.registerService("org.blacksailer.workshop.sk");
           qDebug() << "Register service" << ready; //object path

       if (!ready) {
           qWarning() << "Service already registered, exiting...";
           app->quit();

       }
}

DBusAdaptor::~DBusAdaptor()
{
    QDBusConnection dbus = QDBusConnection::sessionBus();

    dbus.unregisterObject("/SK"); //object path
    bool ready = dbus.unregisterService("org.blacksailer.workshop.sk");
           qDebug() << "Unregister service" << ready; //object path


}


void DBusAdaptor::ShowApp(QStringList item)
{
    qDebug() <<"Parameters"<< item;
    if (!view) {
        qDebug() << "Construct view";
        view = SailfishApp::createView();

        QObject::connect(view,&QQuickView::destroyed, this, &DBusAdaptor::onViewDestroyed);
        QObject::connect(view, SIGNAL(closing(QQuickCloseEvent*)), this, SLOT(onViewClosing(QQuickCloseEvent*)));

        qmlRegisterType<BanInfo>("VKPoco",1,0,"BanInfo");
        qmlRegisterType<Career>("VKPoco",1,0,"Career");
        qmlRegisterType<City>("VKPoco",1,0,"City");
        qmlRegisterType<Contacts>("VKPoco",1,0,"Contacts");
        qmlRegisterType<Counters>("VKPoco",1,0,"Counters");
        qmlRegisterType<Country>("VKPoco",1,0,"Country");
        qmlRegisterType<Cover>("VKPoco",1,0,"Cover");
        qmlRegisterType<Education>("VKPoco",1,0,"Education");
        qmlRegisterType<LastSeen>("VKPoco",1,0,"LastSeen");
        qmlRegisterType<Market>("VKPoco",1,0,"Market");
        qmlRegisterType<Currency>("VKPoco",1,0,"Currency");
        qmlRegisterType<Military>("VKPoco",1,0,"Military");
        qmlRegisterType<Occupation>("VKPoco",1,0,"Occupation");
        qmlRegisterType<Personal>("VKPoco",1,0,"Personal");
        qmlRegisterType<Place>("VKPoco",1,0,"Place");
        qmlRegisterType<Relative>("VKPoco",1,0,"Relative");
    //    qmlRegisterType<School>("VKPoco",1,0,"School");
        qmlRegisterType<University>("VKPoco",1,0,"University");
        qmlRegisterType<UserContacts>("VKPoco",1,0,"UserContacts");



        qmlRegisterInterface<vkapi>("vkapi");
        qmlRegisterType<RecursingWallItem>("VKItems",1,0,"RecursingWallItem");
        qmlRegisterType<MessageItem>("VKItems",1,0,"Message");

        qmlRegisterType<UserPage>("VKModels",1,0,"UsersInfo");
        qmlRegisterType<GroupPage>("VKModels",1,0,"GroupInfoC");

        qmlRegisterType<RecursingNewsFeedModel>("VKModels",1,0,"NewsfeedModel");
        qmlRegisterType<ProxyDialogsModel>("VKModels",1,0,"DialogsModel");
        qmlRegisterType<SearchDialogsModel>("VKModels",1,0,"SearchDialogsModel");

        qmlRegisterType<WallModel>("VKModels",1,0,"WallModel");
        qmlRegisterType<MessagesModel>("VKModels",1,0,"MessageModel");
        qmlRegisterType<FriendsModel>("VKModels",1,0,"FriendsModel");
        qmlRegisterType<GroupsModel>("VKModels",1,0,"GroupsModel");

        qmlRegisterType<AttachmentsToSendModel>("VKModels",1,0,"AttachmentsToSendModel");
        qmlRegisterType<CommentsModel>("VKModels",1,0,"CommentsModel");

        qmlRegisterType<Parser>("VKModels",1,0,"Parser");

        view->rootContext()->setContextProperty("vkapi",a);
        view->rootContext()->setContextProperty("c_dataProvider",JsonParser);
        view->rootContext()->setContextProperty("globalSettings",Settings);
        view->rootContext()->setContextProperty("musicModel",&MusicModel);
        view->rootContext()->setContextProperty("c_PageStarter",PageStarter);


        view->setTitle("SK");
        view->setSource(QUrl("qrc:///qml/harbour-SK.qml"));
        view->showFullScreen();
    }
    else if (view->windowState() == Qt::WindowNoState) {
        qDebug() << "Create view";
        view->create();
        view->showFullScreen();
    }
    else {
        qDebug() << "Show view";
        view->raise();
        view->requestActivate();
    }
}

void DBusAdaptor::openConversation(int id)
{
    qDebug() <<"Parameters"<< id;
    PageStarter->addPage(NotificationPageStarter::DIALOG,id);
    ShowApp(QStringList());

}

void DBusAdaptor::onViewDestroyed()
{
    qDebug() << "Window destroyed";
    view = nullptr;
}

void DBusAdaptor::onViewClosing(QQuickCloseEvent *v)
{
    Q_UNUSED(v)
    qDebug() << "Window closed";
    view->destroy();
    view->deleteLater();
}


