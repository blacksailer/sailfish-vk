#ifndef BASICITEMINFO_H
#define BASICITEMINFO_H

#include <QtCore>

class BasicItemInfo
{
public:
    BasicItemInfo();
    int id;
//    integer	идентификатор пользователя.

     QString photo_50;
 //    string 	URL главной фотографии с размером 50x50px.
     QString photo_100;
 //    string 	URL главной фотографии с размером 100х100px.
     QString photo_200;
 //    string 	URL главной фотографии в максимальном размере.
 //    QString title = " ... ";

     enum  ItemType{
         USER,
         GROUP,
         CHAT
     };

     ItemType item_type;
};

#endif // BASICITEMINFO_H
