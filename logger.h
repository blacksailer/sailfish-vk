#ifndef LOGGER_H
#define LOGGER_H

#include <QObject>
#include <QFile>
#include <QDateTime>
#include <QStandardPaths>
#include <QDebug>
class Logger : public QObject
{
    Q_OBJECT
private:
    QFile Network_Log;
    QString Network_Log_Name="vkapi.log";


public:
    explicit Logger(QObject *parent = 0);

signals:

public slots:
    void writeToNetwork(const QString data);

};

#endif // LOGGER_H
