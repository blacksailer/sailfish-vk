#include "sknetworkcookiejar.h"

SKNetworkCookieJar::SKNetworkCookieJar()
{
    QString cookiespath="/home/nemo/.local/share/harbour-SK/harbour-SK/.QtWebKit/cookies.db";
    QFile cookieFile(cookiespath);

    // Для тестов с куками
    // cookieFile.remove();

    if(cookieFile.exists())
    {
        setCooks(cookiespath);
    }
    else
        qWarning()<<"CookieFIle doesnt exist!!!";
}

void SKNetworkCookieJar::setCooks(QString cookiespath)
{
    QSqlDatabase cookiedb = QSqlDatabase::addDatabase("QSQLITE");
    cookiedb.setDatabaseName(cookiespath);

    if(cookiedb.open())
    {
        QSqlQuery query("SELECT * FROM cookies");
        while(query.next()) {
            QByteArray cookieId=query.value(0).toByteArray();
            QByteArray cookie = query.value(1).toByteArray();
            //            qDebug()<<cookieId<<cookie;
            QNetworkCookie cook;
            QList<QNetworkCookie> cooks = QNetworkCookie::parseCookies(cookie);
            //    cook.parseCookies(cookie);
            //    jar->insertCookie(cook);
            qDebug()<<cooks.count();
            for(int i=0;i<cooks.count();i++)
            {
                this->insertCookie(cooks.at(i));
                qDebug()<<cooks.at(i).domain()<<cooks.at(i).expirationDate()<<cooks.at(i).name()<<cooks.at(i).path()<<cooks.at(i).value();
            }
        }
    }
    else
        qDebug()<<"Cannot connect to db";
    qDebug()<<this->allCookies().count();
}


