#ifndef SKNETWORKCOOKIEJAR_H
#define SKNETWORKCOOKIEJAR_H
#include <QQmlNetworkAccessManagerFactory>
#include <QNetworkProxy>
#include <QNetworkDiskCache>
#include <QNetworkCookie>
#include <QNetworkCookieJar>
#include <QNetworkAccessManager>
#include <QtSql>

class SKNetworkCookieJar : public QNetworkCookieJar
{
public:
    SKNetworkCookieJar();

signals:

public slots:

    // QNetworkCookieJar interface
public:
    void setCooks(QString cookiespath);
};

#endif // SKNETWORKCOOKIEJAR_H
