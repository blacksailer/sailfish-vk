#include "guinetworkaccessmanagerfactory.h"

GUINetworkAccessManagerFactory::GUINetworkAccessManagerFactory(QString offlinePath)
{
offPath=offlinePath;
}



QNetworkAccessManager *GUINetworkAccessManagerFactory::create(QObject *parent)
{
    QNetworkAccessManager *nam = new QNetworkAccessManager(parent);
    QNetworkDiskCache *cache = new QNetworkDiskCache(parent);
    SKNetworkCookieJar *jar = new SKNetworkCookieJar();
            cache->setCacheDirectory(offPath);
    cache->setMaximumCacheSize(1024 * 1024 * 100);



    nam->setCache(cache);
    nam->setCookieJar(jar);

    qDebug() << "--NetworkCache directory " << cache->cacheDirectory();
    qDebug() << "--NetworkCache size " << cache->cacheSize();
    return nam;
}
