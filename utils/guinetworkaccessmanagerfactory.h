#ifndef GUINETWORKACCESSMANAGERFACTORY_H
#define GUINETWORKACCESSMANAGERFACTORY_H
#include <QQmlNetworkAccessManagerFactory>
#include <QNetworkProxy>
#include <QNetworkDiskCache>
#include <QNetworkCookie>
#include <QNetworkCookieJar>
#include <QNetworkAccessManager>
#include <QtSql>
#include "sknetworkcookiejar.h"
class GUINetworkAccessManagerFactory: public QQmlNetworkAccessManagerFactory
{
public:
    GUINetworkAccessManagerFactory(QString offlinePath);

signals:

public slots:

    // QQmlNetworkAccessManagerFactory interface
public:
    QNetworkAccessManager *create(QObject *parent) override;

private:
    QString offPath;
};

#endif // GUINETWORKACCESSMANAGERFACTORY_H
