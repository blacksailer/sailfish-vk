#include "globalsettings.h"

GlobalSettings::GlobalSettings(QObject *parent) :
    QObject(parent)
{
    qDebug()<<"Constructor";
    db = DatabaseSqlite::getInstance();
   QStringList temp = db->LongPollPrevious();
   if(temp.length()>0)
   {
   qDebug()<<"Previous LongPoll"<<temp.at(0)<<temp.at(1);
   setTs_LongPoll(temp.at(0).toUInt());
   setPts_LongPoll(temp.at(1).toInt());
   }
}

int GlobalSettings::LongPollTimeout() const
{
    return m_LongPollTimeout;
}

bool GlobalSettings::UseSSL() const
{
    return m_UseSSL;
}

QString GlobalSettings::Key_LongPoll() const
{
    return m_Key_LongPoll;
}

QString GlobalSettings::Server_LongPoll() const
{
    return m_Server_LongPoll;
}

unsigned int GlobalSettings::Ts_LongPoll() const
{
    return m_Ts_LongPoll;
}

int GlobalSettings::Pts_LongPoll() const
{
    return m_Pts_LongPoll;
}

int GlobalSettings::unreadCount() const
{
    return m_unreadCount;
}

int GlobalSettings::User_Id() const
{
    return m_User_Id;
}

QString GlobalSettings::start_FROM() const
{
    return m_start_FROM;
}

int GlobalSettings::companionId() const
{
    return m_companionId;
}

void GlobalSettings::setLongPollData(QString key, QString server, unsigned int ts, int pts)
{
    db->insertLongPoll(key,server,ts,pts);
    setKey_LongPoll(key);
    setServer_LongPoll(server);
    setTs_LongPoll(ts);
    setPts_LongPoll(pts);
}

void GlobalSettings::setLongPollTimeout(int arg)
{
    if (m_LongPollTimeout != arg) {
        m_LongPollTimeout = arg;
        emit LongPollTimeoutChanged(arg);
    }
}

void GlobalSettings::setUseSSL(bool arg)
{
    if (m_UseSSL != arg) {
        m_UseSSL = arg;
        emit UseSSLChanged(arg);
    }
}

void GlobalSettings::setKey_LongPoll(QString arg)
{
    if (m_Key_LongPoll != arg) {
        m_Key_LongPoll = arg;
        emit Key_LongPollChanged(arg);
    }
}

void GlobalSettings::setServer_LongPoll(QString arg)
{
    if (m_Server_LongPoll != arg) {
        m_Server_LongPoll = arg;
        emit Server_LongPollChanged(arg);
    }
}

void GlobalSettings::setTs_LongPoll(unsigned int arg)
{
    if (m_Ts_LongPoll != arg) {
        m_Ts_LongPoll = arg;
    }
    qDebug()<<arg;
    db->updateLongPoll(DatabaseSqlite::LongPollColumns::TS ,QString::number(arg));
    //Костыль? При приходе пустого массива от сервера лонгполл, параметр не меняется.
    //Если не испускать сигнал, не будет продолжен ответ с сервером    
    emit Ts_LongPollChanged(arg);
}

void GlobalSettings::setPts_LongPoll(int arg)
{
    if (m_Pts_LongPoll != arg) {
        m_Pts_LongPoll = arg;
        db->updateLongPoll(DatabaseSqlite::LongPollColumns::PTS ,QString::number(arg));
        emit Pts_LongPollChanged(arg);
    }
}

void GlobalSettings::setUnreadCount(int arg)
{
    if (m_unreadCount != arg) {
        m_unreadCount = arg;
        emit unreadCountChanged(arg);
    }
}

void GlobalSettings::setUser_Id(int arg)
{
    if (m_User_Id != arg) {
        m_User_Id = arg;
        emit User_IdChanged(arg);
    }
}

void GlobalSettings::setStart_FROM(QString arg)
{
    if (m_start_FROM != arg) {
        m_start_FROM = arg;
        emit start_FROMChanged(arg);
    }
}

void GlobalSettings::setCompanionId(int companionId)
{
    if (m_companionId == companionId)
        return;

    m_companionId = companionId;
    emit companionIdChanged(companionId);
}
